/**
 * Swiper 6.3.5
 * Most modern mobile touch slider and framework with hardware accelerated transitions
 * https://swiperjs.com
 *
 * Copyright 2014-2020 Vladimir Kharlampidi
 *
 * Released under the MIT License
 *
 * Released on: October 30, 2020
 */

(function (global, factory) {
	typeof exports === "object" && typeof module !== "undefined" ? (module.exports = factory()) : typeof define === "function" && define.amd ? define(factory) : ((global = global || self), (global.Swiper = factory()));
})(this, function () {
	"use strict";

	function _defineProperties(target, props) {
		for (var i = 0; i < props.length; i++) {
			var descriptor = props[i];
			descriptor.enumerable = descriptor.enumerable || false;
			descriptor.configurable = true;
			if ("value" in descriptor) descriptor.writable = true;
			Object.defineProperty(target, descriptor.key, descriptor);
		}
	}

	function _createClass(Constructor, protoProps, staticProps) {
		if (protoProps) _defineProperties(Constructor.prototype, protoProps);
		if (staticProps) _defineProperties(Constructor, staticProps);
		return Constructor;
	}

	function _extends() {
		_extends =
			Object.assign ||
			function (target) {
				for (var i = 1; i < arguments.length; i++) {
					var source = arguments[i];

					for (var key in source) {
						if (Object.prototype.hasOwnProperty.call(source, key)) {
							target[key] = source[key];
						}
					}
				}

				return target;
			};

		return _extends.apply(this, arguments);
	}

	/**
	 * SSR Window 3.0.0-alpha.4
	 * Better handling for window object in SSR environment
	 * https://github.com/nolimits4web/ssr-window
	 *
	 * Copyright 2020, Vladimir Kharlampidi
	 *
	 * Licensed under MIT
	 *
	 * Released on: May 20, 2020
	 */

	/* eslint-disable no-param-reassign */
	function isObject(obj) {
		return obj !== null && typeof obj === "object" && "constructor" in obj && obj.constructor === Object;
	}

	function extend(target, src) {
		if (target === void 0) {
			target = {};
		}

		if (src === void 0) {
			src = {};
		}

		Object.keys(src).forEach(function (key) {
			if (typeof target[key] === "undefined") target[key] = src[key];
			else if (isObject(src[key]) && isObject(target[key]) && Object.keys(src[key]).length > 0) {
				extend(target[key], src[key]);
			}
		});
	}

	var ssrDocument = {
		body: {},
		addEventListener: function addEventListener() {},
		removeEventListener: function removeEventListener() {},
		activeElement: {
			blur: function blur() {},
			nodeName: "",
		},
		querySelector: function querySelector() {
			return null;
		},
		querySelectorAll: function querySelectorAll() {
			return [];
		},
		getElementById: function getElementById() {
			return null;
		},
		createEvent: function createEvent() {
			return {
				initEvent: function initEvent() {},
			};
		},
		createElement: function createElement() {
			return {
				children: [],
				childNodes: [],
				style: {},
				setAttribute: function setAttribute() {},
				getElementsByTagName: function getElementsByTagName() {
					return [];
				},
			};
		},
		createElementNS: function createElementNS() {
			return {};
		},
		importNode: function importNode() {
			return null;
		},
		location: {
			hash: "",
			host: "",
			hostname: "",
			href: "",
			origin: "",
			pathname: "",
			protocol: "",
			search: "",
		},
	};

	function getDocument() {
		var doc = typeof document !== "undefined" ? document : {};
		extend(doc, ssrDocument);
		return doc;
	}

	var ssrWindow = {
		document: ssrDocument,
		navigator: {
			userAgent: "",
		},
		location: {
			hash: "",
			host: "",
			hostname: "",
			href: "",
			origin: "",
			pathname: "",
			protocol: "",
			search: "",
		},
		history: {
			replaceState: function replaceState() {},
			pushState: function pushState() {},
			go: function go() {},
			back: function back() {},
		},
		CustomEvent: function CustomEvent() {
			return this;
		},
		addEventListener: function addEventListener() {},
		removeEventListener: function removeEventListener() {},
		getComputedStyle: function getComputedStyle() {
			return {
				getPropertyValue: function getPropertyValue() {
					return "";
				},
			};
		},
		Image: function Image() {},
		Date: function Date() {},
		screen: {},
		setTimeout: function setTimeout() {},
		clearTimeout: function clearTimeout() {},
		matchMedia: function matchMedia() {
			return {};
		},
		requestAnimationFrame: function requestAnimationFrame(callback) {
			if (typeof setTimeout === "undefined") {
				callback();
				return null;
			}

			return setTimeout(callback, 0);
		},
		cancelAnimationFrame: function cancelAnimationFrame(id) {
			if (typeof setTimeout === "undefined") {
				return;
			}

			clearTimeout(id);
		},
	};

	function getWindow() {
		var win = typeof window !== "undefined" ? window : {};
		extend(win, ssrWindow);
		return win;
	}

	/**
	 * Dom7 3.0.0-alpha.7
	 * Minimalistic JavaScript library for DOM manipulation, with a jQuery-compatible API
	 * https://framework7.io/docs/dom7.html
	 *
	 * Copyright 2020, Vladimir Kharlampidi
	 *
	 * Licensed under MIT
	 *
	 * Released on: July 14, 2020
	 */

	function _inheritsLoose(subClass, superClass) {
		subClass.prototype = Object.create(superClass.prototype);
		subClass.prototype.constructor = subClass;
		subClass.__proto__ = superClass;
	}

	function _getPrototypeOf(o) {
		_getPrototypeOf = Object.setPrototypeOf
			? Object.getPrototypeOf
			: function _getPrototypeOf(o) {
					return o.__proto__ || Object.getPrototypeOf(o);
			  };
		return _getPrototypeOf(o);
	}

	function _setPrototypeOf(o, p) {
		_setPrototypeOf =
			Object.setPrototypeOf ||
			function _setPrototypeOf(o, p) {
				o.__proto__ = p;
				return o;
			};

		return _setPrototypeOf(o, p);
	}

	function _isNativeReflectConstruct() {
		if (typeof Reflect === "undefined" || !Reflect.construct) return false;
		if (Reflect.construct.sham) return false;
		if (typeof Proxy === "function") return true;

		try {
			Date.prototype.toString.call(Reflect.construct(Date, [], function () {}));
			return true;
		} catch (e) {
			return false;
		}
	}

	function _construct(Parent, args, Class) {
		if (_isNativeReflectConstruct()) {
			_construct = Reflect.construct;
		} else {
			_construct = function _construct(Parent, args, Class) {
				var a = [null];
				a.push.apply(a, args);
				var Constructor = Function.bind.apply(Parent, a);
				var instance = new Constructor();
				if (Class) _setPrototypeOf(instance, Class.prototype);
				return instance;
			};
		}

		return _construct.apply(null, arguments);
	}

	function _isNativeFunction(fn) {
		return Function.toString.call(fn).indexOf("[native code]") !== -1;
	}

	function _wrapNativeSuper(Class) {
		var _cache = typeof Map === "function" ? new Map() : undefined;

		_wrapNativeSuper = function _wrapNativeSuper(Class) {
			if (Class === null || !_isNativeFunction(Class)) return Class;

			if (typeof Class !== "function") {
				throw new TypeError("Super expression must either be null or a function");
			}

			if (typeof _cache !== "undefined") {
				if (_cache.has(Class)) return _cache.get(Class);

				_cache.set(Class, Wrapper);
			}

			function Wrapper() {
				return _construct(Class, arguments, _getPrototypeOf(this).constructor);
			}

			Wrapper.prototype = Object.create(Class.prototype, {
				constructor: {
					value: Wrapper,
					enumerable: false,
					writable: true,
					configurable: true,
				},
			});
			return _setPrototypeOf(Wrapper, Class);
		};

		return _wrapNativeSuper(Class);
	}

	function _assertThisInitialized(self) {
		if (self === void 0) {
			throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
		}

		return self;
	}
	/* eslint-disable no-proto */

	function makeReactive(obj) {
		var proto = obj.__proto__;
		Object.defineProperty(obj, "__proto__", {
			get: function get() {
				return proto;
			},
			set: function set(value) {
				proto.__proto__ = value;
			},
		});
	}

	var Dom7 = /*#__PURE__*/ (function (_Array) {
		_inheritsLoose(Dom7, _Array);

		function Dom7(items) {
			var _this;

			_this = _Array.call.apply(_Array, [this].concat(items)) || this;
			makeReactive(_assertThisInitialized(_this));
			return _this;
		}

		return Dom7;
	})(/*#__PURE__*/ _wrapNativeSuper(Array));

	function arrayFlat(arr) {
		if (arr === void 0) {
			arr = [];
		}

		var res = [];
		arr.forEach(function (el) {
			if (Array.isArray(el)) {
				res.push.apply(res, arrayFlat(el));
			} else {
				res.push(el);
			}
		});
		return res;
	}

	function arrayFilter(arr, callback) {
		return Array.prototype.filter.call(arr, callback);
	}

	function arrayUnique(arr) {
		var uniqueArray = [];

		for (var i = 0; i < arr.length; i += 1) {
			if (uniqueArray.indexOf(arr[i]) === -1) uniqueArray.push(arr[i]);
		}

		return uniqueArray;
	}

	function qsa(selector, context) {
		if (typeof selector !== "string") {
			return [selector];
		}

		var a = [];
		var res = context.querySelectorAll(selector);

		for (var i = 0; i < res.length; i += 1) {
			a.push(res[i]);
		}

		return a;
	}

	function $(selector, context) {
		var window = getWindow();
		var document = getDocument();
		var arr = [];

		if (!context && selector instanceof Dom7) {
			return selector;
		}

		if (!selector) {
			return new Dom7(arr);
		}

		if (typeof selector === "string") {
			var html = selector.trim();

			if (html.indexOf("<") >= 0 && html.indexOf(">") >= 0) {
				var toCreate = "div";
				if (html.indexOf("<li") === 0) toCreate = "ul";
				if (html.indexOf("<tr") === 0) toCreate = "tbody";
				if (html.indexOf("<td") === 0 || html.indexOf("<th") === 0) toCreate = "tr";
				if (html.indexOf("<tbody") === 0) toCreate = "table";
				if (html.indexOf("<option") === 0) toCreate = "select";
				var tempParent = document.createElement(toCreate);
				tempParent.innerHTML = html;

				for (var i = 0; i < tempParent.childNodes.length; i += 1) {
					arr.push(tempParent.childNodes[i]);
				}
			} else {
				arr = qsa(selector.trim(), context || document);
			} // arr = qsa(selector, document);
		} else if (selector.nodeType || selector === window || selector === document) {
			arr.push(selector);
		} else if (Array.isArray(selector)) {
			if (selector instanceof Dom7) return selector;
			arr = selector;
		}

		return new Dom7(arrayUnique(arr));
	}

	$.fn = Dom7.prototype;

	function addClass() {
		for (var _len = arguments.length, classes = new Array(_len), _key = 0; _key < _len; _key++) {
			classes[_key] = arguments[_key];
		}

		var classNames = arrayFlat(
			classes.map(function (c) {
				return c.split(" ");
			})
		);
		this.forEach(function (el) {
			var _el$classList;

			(_el$classList = el.classList).add.apply(_el$classList, classNames);
		});
		return this;
	}

	function removeClass() {
		for (var _len2 = arguments.length, classes = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
			classes[_key2] = arguments[_key2];
		}

		var classNames = arrayFlat(
			classes.map(function (c) {
				return c.split(" ");
			})
		);
		this.forEach(function (el) {
			var _el$classList2;

			(_el$classList2 = el.classList).remove.apply(_el$classList2, classNames);
		});
		return this;
	}

	function toggleClass() {
		for (var _len3 = arguments.length, classes = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
			classes[_key3] = arguments[_key3];
		}

		var classNames = arrayFlat(
			classes.map(function (c) {
				return c.split(" ");
			})
		);
		this.forEach(function (el) {
			classNames.forEach(function (className) {
				el.classList.toggle(className);
			});
		});
	}

	function hasClass() {
		for (var _len4 = arguments.length, classes = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
			classes[_key4] = arguments[_key4];
		}

		var classNames = arrayFlat(
			classes.map(function (c) {
				return c.split(" ");
			})
		);
		return (
			arrayFilter(this, function (el) {
				return (
					classNames.filter(function (className) {
						return el.classList.contains(className);
					}).length > 0
				);
			}).length > 0
		);
	}

	function attr(attrs, value) {
		if (arguments.length === 1 && typeof attrs === "string") {
			// Get attr
			if (this[0]) return this[0].getAttribute(attrs);
			return undefined;
		} // Set attrs

		for (var i = 0; i < this.length; i += 1) {
			if (arguments.length === 2) {
				// String
				this[i].setAttribute(attrs, value);
			} else {
				// Object
				for (var attrName in attrs) {
					this[i][attrName] = attrs[attrName];
					this[i].setAttribute(attrName, attrs[attrName]);
				}
			}
		}

		return this;
	}

	function removeAttr(attr) {
		for (var i = 0; i < this.length; i += 1) {
			this[i].removeAttribute(attr);
		}

		return this;
	}

	function transform(transform) {
		for (var i = 0; i < this.length; i += 1) {
			this[i].style.transform = transform;
		}

		return this;
	}

	function transition(duration) {
		for (var i = 0; i < this.length; i += 1) {
			this[i].style.transition = typeof duration !== "string" ? duration + "ms" : duration;
		}

		return this;
	}

	function on() {
		for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
			args[_key5] = arguments[_key5];
		}

		var eventType = args[0],
			targetSelector = args[1],
			listener = args[2],
			capture = args[3];

		if (typeof args[1] === "function") {
			eventType = args[0];
			listener = args[1];
			capture = args[2];
			targetSelector = undefined;
		}

		if (!capture) capture = false;

		function handleLiveEvent(e) {
			var target = e.target;
			if (!target) return;
			var eventData = e.target.dom7EventData || [];

			if (eventData.indexOf(e) < 0) {
				eventData.unshift(e);
			}

			if ($(target).is(targetSelector)) listener.apply(target, eventData);
			else {
				var _parents = $(target).parents(); // eslint-disable-line

				for (var k = 0; k < _parents.length; k += 1) {
					if ($(_parents[k]).is(targetSelector)) listener.apply(_parents[k], eventData);
				}
			}
		}

		function handleEvent(e) {
			var eventData = e && e.target ? e.target.dom7EventData || [] : [];

			if (eventData.indexOf(e) < 0) {
				eventData.unshift(e);
			}

			listener.apply(this, eventData);
		}

		var events = eventType.split(" ");
		var j;

		for (var i = 0; i < this.length; i += 1) {
			var el = this[i];

			if (!targetSelector) {
				for (j = 0; j < events.length; j += 1) {
					var event = events[j];
					if (!el.dom7Listeners) el.dom7Listeners = {};
					if (!el.dom7Listeners[event]) el.dom7Listeners[event] = [];
					el.dom7Listeners[event].push({
						listener: listener,
						proxyListener: handleEvent,
					});
					el.addEventListener(event, handleEvent, capture);
				}
			} else {
				// Live events
				for (j = 0; j < events.length; j += 1) {
					var _event = events[j];
					if (!el.dom7LiveListeners) el.dom7LiveListeners = {};
					if (!el.dom7LiveListeners[_event]) el.dom7LiveListeners[_event] = [];

					el.dom7LiveListeners[_event].push({
						listener: listener,
						proxyListener: handleLiveEvent,
					});

					el.addEventListener(_event, handleLiveEvent, capture);
				}
			}
		}

		return this;
	}

	function off() {
		for (var _len6 = arguments.length, args = new Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {
			args[_key6] = arguments[_key6];
		}

		var eventType = args[0],
			targetSelector = args[1],
			listener = args[2],
			capture = args[3];

		if (typeof args[1] === "function") {
			eventType = args[0];
			listener = args[1];
			capture = args[2];
			targetSelector = undefined;
		}

		if (!capture) capture = false;
		var events = eventType.split(" ");

		for (var i = 0; i < events.length; i += 1) {
			var event = events[i];

			for (var j = 0; j < this.length; j += 1) {
				var el = this[j];
				var handlers = void 0;

				if (!targetSelector && el.dom7Listeners) {
					handlers = el.dom7Listeners[event];
				} else if (targetSelector && el.dom7LiveListeners) {
					handlers = el.dom7LiveListeners[event];
				}

				if (handlers && handlers.length) {
					for (var k = handlers.length - 1; k >= 0; k -= 1) {
						var handler = handlers[k];

						if (listener && handler.listener === listener) {
							el.removeEventListener(event, handler.proxyListener, capture);
							handlers.splice(k, 1);
						} else if (listener && handler.listener && handler.listener.dom7proxy && handler.listener.dom7proxy === listener) {
							el.removeEventListener(event, handler.proxyListener, capture);
							handlers.splice(k, 1);
						} else if (!listener) {
							el.removeEventListener(event, handler.proxyListener, capture);
							handlers.splice(k, 1);
						}
					}
				}
			}
		}

		return this;
	}

	function trigger() {
		var window = getWindow();

		for (var _len9 = arguments.length, args = new Array(_len9), _key9 = 0; _key9 < _len9; _key9++) {
			args[_key9] = arguments[_key9];
		}

		var events = args[0].split(" ");
		var eventData = args[1];

		for (var i = 0; i < events.length; i += 1) {
			var event = events[i];

			for (var j = 0; j < this.length; j += 1) {
				var el = this[j];

				if (window.CustomEvent) {
					var evt = new window.CustomEvent(event, {
						detail: eventData,
						bubbles: true,
						cancelable: true,
					});
					el.dom7EventData = args.filter(function (data, dataIndex) {
						return dataIndex > 0;
					});
					el.dispatchEvent(evt);
					el.dom7EventData = [];
					delete el.dom7EventData;
				}
			}
		}

		return this;
	}

	function transitionEnd(callback) {
		var dom = this;

		function fireCallBack(e) {
			if (e.target !== this) return;
			callback.call(this, e);
			dom.off("transitionend", fireCallBack);
		}

		if (callback) {
			dom.on("transitionend", fireCallBack);
		}

		return this;
	}

	function outerWidth(includeMargins) {
		if (this.length > 0) {
			if (includeMargins) {
				var _styles = this.styles();

				return this[0].offsetWidth + parseFloat(_styles.getPropertyValue("margin-right")) + parseFloat(_styles.getPropertyValue("margin-left"));
			}

			return this[0].offsetWidth;
		}

		return null;
	}

	function outerHeight(includeMargins) {
		if (this.length > 0) {
			if (includeMargins) {
				var _styles2 = this.styles();

				return this[0].offsetHeight + parseFloat(_styles2.getPropertyValue("margin-top")) + parseFloat(_styles2.getPropertyValue("margin-bottom"));
			}

			return this[0].offsetHeight;
		}

		return null;
	}

	function offset() {
		if (this.length > 0) {
			var window = getWindow();
			var document = getDocument();
			var el = this[0];
			var box = el.getBoundingClientRect();
			var body = document.body;
			var clientTop = el.clientTop || body.clientTop || 0;
			var clientLeft = el.clientLeft || body.clientLeft || 0;
			var scrollTop = el === window ? window.scrollY : el.scrollTop;
			var scrollLeft = el === window ? window.scrollX : el.scrollLeft;
			return {
				top: box.top + scrollTop - clientTop,
				left: box.left + scrollLeft - clientLeft,
			};
		}

		return null;
	}

	function styles() {
		var window = getWindow();
		if (this[0]) return window.getComputedStyle(this[0], null);
		return {};
	}

	function css(props, value) {
		var window = getWindow();
		var i;

		if (arguments.length === 1) {
			if (typeof props === "string") {
				// .css('width')
				if (this[0]) return window.getComputedStyle(this[0], null).getPropertyValue(props);
			} else {
				// .css({ width: '100px' })
				for (i = 0; i < this.length; i += 1) {
					for (var _prop in props) {
						this[i].style[_prop] = props[_prop];
					}
				}

				return this;
			}
		}

		if (arguments.length === 2 && typeof props === "string") {
			// .css('width', '100px')
			for (i = 0; i < this.length; i += 1) {
				this[i].style[props] = value;
			}

			return this;
		}

		return this;
	}

	function each(callback) {
		if (!callback) return this;
		this.forEach(function (el, index) {
			callback.apply(el, [el, index]);
		});
		return this;
	}

	function filter(callback) {
		var result = arrayFilter(this, callback);
		return $(result);
	}

	function html(html) {
		if (typeof html === "undefined") {
			return this[0] ? this[0].innerHTML : null;
		}

		for (var i = 0; i < this.length; i += 1) {
			this[i].innerHTML = html;
		}

		return this;
	}

	function text(text) {
		if (typeof text === "undefined") {
			return this[0] ? this[0].textContent.trim() : null;
		}

		for (var i = 0; i < this.length; i += 1) {
			this[i].textContent = text;
		}

		return this;
	}

	function is(selector) {
		var window = getWindow();
		var document = getDocument();
		var el = this[0];
		var compareWith;
		var i;
		if (!el || typeof selector === "undefined") return false;

		if (typeof selector === "string") {
			if (el.matches) return el.matches(selector);
			if (el.webkitMatchesSelector) return el.webkitMatchesSelector(selector);
			if (el.msMatchesSelector) return el.msMatchesSelector(selector);
			compareWith = $(selector);

			for (i = 0; i < compareWith.length; i += 1) {
				if (compareWith[i] === el) return true;
			}

			return false;
		}

		if (selector === document) {
			return el === document;
		}

		if (selector === window) {
			return el === window;
		}

		if (selector.nodeType || selector instanceof Dom7) {
			compareWith = selector.nodeType ? [selector] : selector;

			for (i = 0; i < compareWith.length; i += 1) {
				if (compareWith[i] === el) return true;
			}

			return false;
		}

		return false;
	}

	function index() {
		var child = this[0];
		var i;

		if (child) {
			i = 0; // eslint-disable-next-line

			while ((child = child.previousSibling) !== null) {
				if (child.nodeType === 1) i += 1;
			}

			return i;
		}

		return undefined;
	}

	function eq(index) {
		if (typeof index === "undefined") return this;
		var length = this.length;

		if (index > length - 1) {
			return $([]);
		}

		if (index < 0) {
			var returnIndex = length + index;
			if (returnIndex < 0) return $([]);
			return $([this[returnIndex]]);
		}

		return $([this[index]]);
	}

	function append() {
		var newChild;
		var document = getDocument();

		for (var k = 0; k < arguments.length; k += 1) {
			newChild = k < 0 || arguments.length <= k ? undefined : arguments[k];

			for (var i = 0; i < this.length; i += 1) {
				if (typeof newChild === "string") {
					var tempDiv = document.createElement("div");
					tempDiv.innerHTML = newChild;

					while (tempDiv.firstChild) {
						this[i].appendChild(tempDiv.firstChild);
					}
				} else if (newChild instanceof Dom7) {
					for (var j = 0; j < newChild.length; j += 1) {
						this[i].appendChild(newChild[j]);
					}
				} else {
					this[i].appendChild(newChild);
				}
			}
		}

		return this;
	}

	function prepend(newChild) {
		var document = getDocument();
		var i;
		var j;

		for (i = 0; i < this.length; i += 1) {
			if (typeof newChild === "string") {
				var tempDiv = document.createElement("div");
				tempDiv.innerHTML = newChild;

				for (j = tempDiv.childNodes.length - 1; j >= 0; j -= 1) {
					this[i].insertBefore(tempDiv.childNodes[j], this[i].childNodes[0]);
				}
			} else if (newChild instanceof Dom7) {
				for (j = 0; j < newChild.length; j += 1) {
					this[i].insertBefore(newChild[j], this[i].childNodes[0]);
				}
			} else {
				this[i].insertBefore(newChild, this[i].childNodes[0]);
			}
		}

		return this;
	}

	function next(selector) {
		if (this.length > 0) {
			if (selector) {
				if (this[0].nextElementSibling && $(this[0].nextElementSibling).is(selector)) {
					return $([this[0].nextElementSibling]);
				}

				return $([]);
			}

			if (this[0].nextElementSibling) return $([this[0].nextElementSibling]);
			return $([]);
		}

		return $([]);
	}

	function nextAll(selector) {
		var nextEls = [];
		var el = this[0];
		if (!el) return $([]);

		while (el.nextElementSibling) {
			var _next = el.nextElementSibling; // eslint-disable-line

			if (selector) {
				if ($(_next).is(selector)) nextEls.push(_next);
			} else nextEls.push(_next);

			el = _next;
		}

		return $(nextEls);
	}

	function prev(selector) {
		if (this.length > 0) {
			var el = this[0];

			if (selector) {
				if (el.previousElementSibling && $(el.previousElementSibling).is(selector)) {
					return $([el.previousElementSibling]);
				}

				return $([]);
			}

			if (el.previousElementSibling) return $([el.previousElementSibling]);
			return $([]);
		}

		return $([]);
	}

	function prevAll(selector) {
		var prevEls = [];
		var el = this[0];
		if (!el) return $([]);

		while (el.previousElementSibling) {
			var _prev = el.previousElementSibling; // eslint-disable-line

			if (selector) {
				if ($(_prev).is(selector)) prevEls.push(_prev);
			} else prevEls.push(_prev);

			el = _prev;
		}

		return $(prevEls);
	}

	function parent(selector) {
		var parents = []; // eslint-disable-line

		for (var i = 0; i < this.length; i += 1) {
			if (this[i].parentNode !== null) {
				if (selector) {
					if ($(this[i].parentNode).is(selector)) parents.push(this[i].parentNode);
				} else {
					parents.push(this[i].parentNode);
				}
			}
		}

		return $(parents);
	}

	function parents(selector) {
		var parents = []; // eslint-disable-line

		for (var i = 0; i < this.length; i += 1) {
			var _parent = this[i].parentNode; // eslint-disable-line

			while (_parent) {
				if (selector) {
					if ($(_parent).is(selector)) parents.push(_parent);
				} else {
					parents.push(_parent);
				}

				_parent = _parent.parentNode;
			}
		}

		return $(parents);
	}

	function closest(selector) {
		var closest = this; // eslint-disable-line

		if (typeof selector === "undefined") {
			return $([]);
		}

		if (!closest.is(selector)) {
			closest = closest.parents(selector).eq(0);
		}

		return closest;
	}

	function find(selector) {
		var foundElements = [];

		for (var i = 0; i < this.length; i += 1) {
			var found = this[i].querySelectorAll(selector);

			for (var j = 0; j < found.length; j += 1) {
				foundElements.push(found[j]);
			}
		}

		return $(foundElements);
	}

	function children(selector) {
		var children = []; // eslint-disable-line

		for (var i = 0; i < this.length; i += 1) {
			var childNodes = this[i].children;

			for (var j = 0; j < childNodes.length; j += 1) {
				if (!selector || $(childNodes[j]).is(selector)) {
					children.push(childNodes[j]);
				}
			}
		}

		return $(children);
	}

	function remove() {
		for (var i = 0; i < this.length; i += 1) {
			if (this[i].parentNode) this[i].parentNode.removeChild(this[i]);
		}

		return this;
	}

	var Methods = {
		addClass: addClass,
		removeClass: removeClass,
		hasClass: hasClass,
		toggleClass: toggleClass,
		attr: attr,
		removeAttr: removeAttr,
		transform: transform,
		transition: transition,
		on: on,
		off: off,
		trigger: trigger,
		transitionEnd: transitionEnd,
		outerWidth: outerWidth,
		outerHeight: outerHeight,
		styles: styles,
		offset: offset,
		css: css,
		each: each,
		html: html,
		text: text,
		is: is,
		index: index,
		eq: eq,
		append: append,
		prepend: prepend,
		next: next,
		nextAll: nextAll,
		prev: prev,
		prevAll: prevAll,
		parent: parent,
		parents: parents,
		closest: closest,
		find: find,
		children: children,
		filter: filter,
		remove: remove,
	};
	Object.keys(Methods).forEach(function (methodName) {
		$.fn[methodName] = Methods[methodName];
	});

	function deleteProps(obj) {
		var object = obj;
		Object.keys(object).forEach(function (key) {
			try {
				object[key] = null;
			} catch (e) {
				// no getter for object
			}

			try {
				delete object[key];
			} catch (e) {
				// something got wrong
			}
		});
	}

	function nextTick(callback, delay) {
		if (delay === void 0) {
			delay = 0;
		}

		return setTimeout(callback, delay);
	}

	function now() {
		return Date.now();
	}

	function getTranslate(el, axis) {
		if (axis === void 0) {
			axis = "x";
		}

		var window = getWindow();
		var matrix;
		var curTransform;
		var transformMatrix;
		var curStyle = window.getComputedStyle(el, null);

		if (window.WebKitCSSMatrix) {
			curTransform = curStyle.transform || curStyle.webkitTransform;

			if (curTransform.split(",").length > 6) {
				curTransform = curTransform
					.split(", ")
					.map(function (a) {
						return a.replace(",", ".");
					})
					.join(", ");
			} // Some old versions of Webkit choke when 'none' is passed; pass
			// empty string instead in this case

			transformMatrix = new window.WebKitCSSMatrix(curTransform === "none" ? "" : curTransform);
		} else {
			transformMatrix = curStyle.MozTransform || curStyle.OTransform || curStyle.MsTransform || curStyle.msTransform || curStyle.transform || curStyle.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,");
			matrix = transformMatrix.toString().split(",");
		}

		if (axis === "x") {
			// Latest Chrome and webkits Fix
			if (window.WebKitCSSMatrix) curTransform = transformMatrix.m41;
			// Crazy IE10 Matrix
			else if (matrix.length === 16) curTransform = parseFloat(matrix[12]);
			// Normal Browsers
			else curTransform = parseFloat(matrix[4]);
		}

		if (axis === "y") {
			// Latest Chrome and webkits Fix
			if (window.WebKitCSSMatrix) curTransform = transformMatrix.m42;
			// Crazy IE10 Matrix
			else if (matrix.length === 16) curTransform = parseFloat(matrix[13]);
			// Normal Browsers
			else curTransform = parseFloat(matrix[5]);
		}

		return curTransform || 0;
	}

	function isObject$1(o) {
		return typeof o === "object" && o !== null && o.constructor && o.constructor === Object;
	}

	function extend$1() {
		var to = Object(arguments.length <= 0 ? undefined : arguments[0]);

		for (var i = 1; i < arguments.length; i += 1) {
			var nextSource = i < 0 || arguments.length <= i ? undefined : arguments[i];

			if (nextSource !== undefined && nextSource !== null) {
				var keysArray = Object.keys(Object(nextSource));

				for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex += 1) {
					var nextKey = keysArray[nextIndex];
					var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);

					if (desc !== undefined && desc.enumerable) {
						if (isObject$1(to[nextKey]) && isObject$1(nextSource[nextKey])) {
							extend$1(to[nextKey], nextSource[nextKey]);
						} else if (!isObject$1(to[nextKey]) && isObject$1(nextSource[nextKey])) {
							to[nextKey] = {};
							extend$1(to[nextKey], nextSource[nextKey]);
						} else {
							to[nextKey] = nextSource[nextKey];
						}
					}
				}
			}
		}

		return to;
	}

	function bindModuleMethods(instance, obj) {
		Object.keys(obj).forEach(function (key) {
			if (isObject$1(obj[key])) {
				Object.keys(obj[key]).forEach(function (subKey) {
					if (typeof obj[key][subKey] === "function") {
						obj[key][subKey] = obj[key][subKey].bind(instance);
					}
				});
			}

			instance[key] = obj[key];
		});
	}

	var support;

	function calcSupport() {
		var window = getWindow();
		var document = getDocument();
		return {
			touch: !!("ontouchstart" in window || (window.DocumentTouch && document instanceof window.DocumentTouch)),
			pointerEvents: !!window.PointerEvent && "maxTouchPoints" in window.navigator && window.navigator.maxTouchPoints >= 0,
			observer: (function checkObserver() {
				return "MutationObserver" in window || "WebkitMutationObserver" in window;
			})(),
			passiveListener: (function checkPassiveListener() {
				var supportsPassive = false;

				try {
					var opts = Object.defineProperty({}, "passive", {
						// eslint-disable-next-line
						get: function get() {
							supportsPassive = true;
						},
					});
					window.addEventListener("testPassiveListener", null, opts);
				} catch (e) {
					// No support
				}

				return supportsPassive;
			})(),
			gestures: (function checkGestures() {
				return "ongesturestart" in window;
			})(),
		};
	}

	function getSupport() {
		if (!support) {
			support = calcSupport();
		}

		return support;
	}

	var device;

	function calcDevice(_temp) {
		var _ref = _temp === void 0 ? {} : _temp,
			userAgent = _ref.userAgent;

		var support = getSupport();
		var window = getWindow();
		var platform = window.navigator.platform;
		var ua = userAgent || window.navigator.userAgent;
		var device = {
			ios: false,
			android: false,
		};
		var screenWidth = window.screen.width;
		var screenHeight = window.screen.height;
		var android = ua.match(/(Android);?[\s\/]+([\d.]+)?/); // eslint-disable-line

		var ipad = ua.match(/(iPad).*OS\s([\d_]+)/);
		var ipod = ua.match(/(iPod)(.*OS\s([\d_]+))?/);
		var iphone = !ipad && ua.match(/(iPhone\sOS|iOS)\s([\d_]+)/);
		var windows = platform === "Win32";
		var macos = platform === "MacIntel"; // iPadOs 13 fix

		var iPadScreens = ["1024x1366", "1366x1024", "834x1194", "1194x834", "834x1112", "1112x834", "768x1024", "1024x768", "820x1180", "1180x820", "810x1080", "1080x810"];

		if (!ipad && macos && support.touch && iPadScreens.indexOf(screenWidth + "x" + screenHeight) >= 0) {
			ipad = ua.match(/(Version)\/([\d.]+)/);
			if (!ipad) ipad = [0, 1, "13_0_0"];
			macos = false;
		} // Android

		if (android && !windows) {
			device.os = "android";
			device.android = true;
		}

		if (ipad || iphone || ipod) {
			device.os = "ios";
			device.ios = true;
		} // Export object

		return device;
	}

	function getDevice(overrides) {
		if (overrides === void 0) {
			overrides = {};
		}

		if (!device) {
			device = calcDevice(overrides);
		}

		return device;
	}

	var browser;

	function calcBrowser() {
		var window = getWindow();

		function isSafari() {
			var ua = window.navigator.userAgent.toLowerCase();
			return ua.indexOf("safari") >= 0 && ua.indexOf("chrome") < 0 && ua.indexOf("android") < 0;
		}

		return {
			isEdge: !!window.navigator.userAgent.match(/Edge/g),
			isSafari: isSafari(),
			isWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(window.navigator.userAgent),
		};
	}

	function getBrowser() {
		if (!browser) {
			browser = calcBrowser();
		}

		return browser;
	}

	var Resize = {
		name: "resize",
		create: function create() {
			var swiper = this;
			extend$1(swiper, {
				resize: {
					resizeHandler: function resizeHandler() {
						if (!swiper || swiper.destroyed || !swiper.initialized) return;
						swiper.emit("beforeResize");
						swiper.emit("resize");
					},
					orientationChangeHandler: function orientationChangeHandler() {
						if (!swiper || swiper.destroyed || !swiper.initialized) return;
						swiper.emit("orientationchange");
					},
				},
			});
		},
		on: {
			init: function init(swiper) {
				var window = getWindow(); // Emit resize

				window.addEventListener("resize", swiper.resize.resizeHandler); // Emit orientationchange

				window.addEventListener("orientationchange", swiper.resize.orientationChangeHandler);
			},
			destroy: function destroy(swiper) {
				var window = getWindow();
				window.removeEventListener("resize", swiper.resize.resizeHandler);
				window.removeEventListener("orientationchange", swiper.resize.orientationChangeHandler);
			},
		},
	};

	var Observer = {
		attach: function attach(target, options) {
			if (options === void 0) {
				options = {};
			}

			var window = getWindow();
			var swiper = this;
			var ObserverFunc = window.MutationObserver || window.WebkitMutationObserver;
			var observer = new ObserverFunc(function (mutations) {
				// The observerUpdate event should only be triggered
				// once despite the number of mutations.  Additional
				// triggers are redundant and are very costly
				if (mutations.length === 1) {
					swiper.emit("observerUpdate", mutations[0]);
					return;
				}

				var observerUpdate = function observerUpdate() {
					swiper.emit("observerUpdate", mutations[0]);
				};

				if (window.requestAnimationFrame) {
					window.requestAnimationFrame(observerUpdate);
				} else {
					window.setTimeout(observerUpdate, 0);
				}
			});
			observer.observe(target, {
				attributes: typeof options.attributes === "undefined" ? true : options.attributes,
				childList: typeof options.childList === "undefined" ? true : options.childList,
				characterData: typeof options.characterData === "undefined" ? true : options.characterData,
			});
			swiper.observer.observers.push(observer);
		},
		init: function init() {
			var swiper = this;
			if (!swiper.support.observer || !swiper.params.observer) return;

			if (swiper.params.observeParents) {
				var containerParents = swiper.$el.parents();

				for (var i = 0; i < containerParents.length; i += 1) {
					swiper.observer.attach(containerParents[i]);
				}
			} // Observe container

			swiper.observer.attach(swiper.$el[0], {
				childList: swiper.params.observeSlideChildren,
			}); // Observe wrapper

			swiper.observer.attach(swiper.$wrapperEl[0], {
				attributes: false,
			});
		},
		destroy: function destroy() {
			var swiper = this;
			swiper.observer.observers.forEach(function (observer) {
				observer.disconnect();
			});
			swiper.observer.observers = [];
		},
	};
	var Observer$1 = {
		name: "observer",
		params: {
			observer: false,
			observeParents: false,
			observeSlideChildren: false,
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				observer: _extends(
					_extends({}, Observer),
					{},
					{
						observers: [],
					}
				),
			});
		},
		on: {
			init: function init(swiper) {
				swiper.observer.init();
			},
			destroy: function destroy(swiper) {
				swiper.observer.destroy();
			},
		},
	};

	var modular = {
		useParams: function useParams(instanceParams) {
			var instance = this;
			if (!instance.modules) return;
			Object.keys(instance.modules).forEach(function (moduleName) {
				var module = instance.modules[moduleName]; // Extend params

				if (module.params) {
					extend$1(instanceParams, module.params);
				}
			});
		},
		useModules: function useModules(modulesParams) {
			if (modulesParams === void 0) {
				modulesParams = {};
			}

			var instance = this;
			if (!instance.modules) return;
			Object.keys(instance.modules).forEach(function (moduleName) {
				var module = instance.modules[moduleName];
				var moduleParams = modulesParams[moduleName] || {}; // Add event listeners

				if (module.on && instance.on) {
					Object.keys(module.on).forEach(function (moduleEventName) {
						instance.on(moduleEventName, module.on[moduleEventName]);
					});
				} // Module create callback

				if (module.create) {
					module.create.bind(instance)(moduleParams);
				}
			});
		},
	};

	/* eslint-disable no-underscore-dangle */
	var eventsEmitter = {
		on: function on(events, handler, priority) {
			var self = this;
			if (typeof handler !== "function") return self;
			var method = priority ? "unshift" : "push";
			events.split(" ").forEach(function (event) {
				if (!self.eventsListeners[event]) self.eventsListeners[event] = [];
				self.eventsListeners[event][method](handler);
			});
			return self;
		},
		once: function once(events, handler, priority) {
			var self = this;
			if (typeof handler !== "function") return self;

			function onceHandler() {
				self.off(events, onceHandler);

				if (onceHandler.__emitterProxy) {
					delete onceHandler.__emitterProxy;
				}

				for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
					args[_key] = arguments[_key];
				}

				handler.apply(self, args);
			}

			onceHandler.__emitterProxy = handler;
			return self.on(events, onceHandler, priority);
		},
		onAny: function onAny(handler, priority) {
			var self = this;
			if (typeof handler !== "function") return self;
			var method = priority ? "unshift" : "push";

			if (self.eventsAnyListeners.indexOf(handler) < 0) {
				self.eventsAnyListeners[method](handler);
			}

			return self;
		},
		offAny: function offAny(handler) {
			var self = this;
			if (!self.eventsAnyListeners) return self;
			var index = self.eventsAnyListeners.indexOf(handler);

			if (index >= 0) {
				self.eventsAnyListeners.splice(index, 1);
			}

			return self;
		},
		off: function off(events, handler) {
			var self = this;
			if (!self.eventsListeners) return self;
			events.split(" ").forEach(function (event) {
				if (typeof handler === "undefined") {
					self.eventsListeners[event] = [];
				} else if (self.eventsListeners[event]) {
					self.eventsListeners[event].forEach(function (eventHandler, index) {
						if (eventHandler === handler || (eventHandler.__emitterProxy && eventHandler.__emitterProxy === handler)) {
							self.eventsListeners[event].splice(index, 1);
						}
					});
				}
			});
			return self;
		},
		emit: function emit() {
			var self = this;
			if (!self.eventsListeners) return self;
			var events;
			var data;
			var context;

			for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
				args[_key2] = arguments[_key2];
			}

			if (typeof args[0] === "string" || Array.isArray(args[0])) {
				events = args[0];
				data = args.slice(1, args.length);
				context = self;
			} else {
				events = args[0].events;
				data = args[0].data;
				context = args[0].context || self;
			}

			data.unshift(context);
			var eventsArray = Array.isArray(events) ? events : events.split(" ");
			eventsArray.forEach(function (event) {
				if (self.eventsAnyListeners && self.eventsAnyListeners.length) {
					self.eventsAnyListeners.forEach(function (eventHandler) {
						eventHandler.apply(context, [event].concat(data));
					});
				}

				if (self.eventsListeners && self.eventsListeners[event]) {
					var handlers = [];
					self.eventsListeners[event].forEach(function (eventHandler) {
						handlers.push(eventHandler);
					});
					handlers.forEach(function (eventHandler) {
						eventHandler.apply(context, data);
					});
				}
			});
			return self;
		},
	};

	function updateSize() {
		var swiper = this;
		var width;
		var height;
		var $el = swiper.$el;

		if (typeof swiper.params.width !== "undefined" && swiper.params.width !== null) {
			width = swiper.params.width;
		} else {
			width = $el[0].clientWidth;
		}

		if (typeof swiper.params.height !== "undefined" && swiper.params.width !== null) {
			height = swiper.params.height;
		} else {
			height = $el[0].clientHeight;
		}

		if ((width === 0 && swiper.isHorizontal()) || (height === 0 && swiper.isVertical())) {
			return;
		} // Subtract paddings

		width = width - parseInt($el.css("padding-left") || 0, 10) - parseInt($el.css("padding-right") || 0, 10);
		height = height - parseInt($el.css("padding-top") || 0, 10) - parseInt($el.css("padding-bottom") || 0, 10);
		if (Number.isNaN(width)) width = 0;
		if (Number.isNaN(height)) height = 0;
		extend$1(swiper, {
			width: width,
			height: height,
			size: swiper.isHorizontal() ? width : height,
		});
	}

	function updateSlides() {
		var swiper = this;
		var window = getWindow();
		var params = swiper.params;
		var $wrapperEl = swiper.$wrapperEl,
			swiperSize = swiper.size,
			rtl = swiper.rtlTranslate,
			wrongRTL = swiper.wrongRTL;
		var isVirtual = swiper.virtual && params.virtual.enabled;
		var previousSlidesLength = isVirtual ? swiper.virtual.slides.length : swiper.slides.length;
		var slides = $wrapperEl.children("." + swiper.params.slideClass);
		var slidesLength = isVirtual ? swiper.virtual.slides.length : slides.length;
		var snapGrid = [];
		var slidesGrid = [];
		var slidesSizesGrid = [];

		function slidesForMargin(slideEl, slideIndex) {
			if (!params.cssMode) return true;

			if (slideIndex === slides.length - 1) {
				return false;
			}

			return true;
		}

		var offsetBefore = params.slidesOffsetBefore;

		if (typeof offsetBefore === "function") {
			offsetBefore = params.slidesOffsetBefore.call(swiper);
		}

		var offsetAfter = params.slidesOffsetAfter;

		if (typeof offsetAfter === "function") {
			offsetAfter = params.slidesOffsetAfter.call(swiper);
		}

		var previousSnapGridLength = swiper.snapGrid.length;
		var previousSlidesGridLength = swiper.snapGrid.length;
		var spaceBetween = params.spaceBetween;
		var slidePosition = -offsetBefore;
		var prevSlideSize = 0;
		var index = 0;

		if (typeof swiperSize === "undefined") {
			return;
		}

		if (typeof spaceBetween === "string" && spaceBetween.indexOf("%") >= 0) {
			spaceBetween = (parseFloat(spaceBetween.replace("%", "")) / 100) * swiperSize;
		}

		swiper.virtualSize = -spaceBetween; // reset margins

		if (rtl)
			slides.css({
				marginLeft: "",
				marginTop: "",
			});
		else
			slides.css({
				marginRight: "",
				marginBottom: "",
			});
		var slidesNumberEvenToRows;

		if (params.slidesPerColumn > 1) {
			if (Math.floor(slidesLength / params.slidesPerColumn) === slidesLength / swiper.params.slidesPerColumn) {
				slidesNumberEvenToRows = slidesLength;
			} else {
				slidesNumberEvenToRows = Math.ceil(slidesLength / params.slidesPerColumn) * params.slidesPerColumn;
			}

			if (params.slidesPerView !== "auto" && params.slidesPerColumnFill === "row") {
				slidesNumberEvenToRows = Math.max(slidesNumberEvenToRows, params.slidesPerView * params.slidesPerColumn);
			}
		} // Calc slides

		var slideSize;
		var slidesPerColumn = params.slidesPerColumn;
		var slidesPerRow = slidesNumberEvenToRows / slidesPerColumn;
		var numFullColumns = Math.floor(slidesLength / params.slidesPerColumn);

		for (var i = 0; i < slidesLength; i += 1) {
			slideSize = 0;
			var slide = slides.eq(i);

			if (params.slidesPerColumn > 1) {
				// Set slides order
				var newSlideOrderIndex = void 0;
				var column = void 0;
				var row = void 0;

				if (params.slidesPerColumnFill === "row" && params.slidesPerGroup > 1) {
					var groupIndex = Math.floor(i / (params.slidesPerGroup * params.slidesPerColumn));
					var slideIndexInGroup = i - params.slidesPerColumn * params.slidesPerGroup * groupIndex;
					var columnsInGroup = groupIndex === 0 ? params.slidesPerGroup : Math.min(Math.ceil((slidesLength - groupIndex * slidesPerColumn * params.slidesPerGroup) / slidesPerColumn), params.slidesPerGroup);
					row = Math.floor(slideIndexInGroup / columnsInGroup);
					column = slideIndexInGroup - row * columnsInGroup + groupIndex * params.slidesPerGroup;
					newSlideOrderIndex = column + (row * slidesNumberEvenToRows) / slidesPerColumn;
					slide.css({
						"-webkit-box-ordinal-group": newSlideOrderIndex,
						"-moz-box-ordinal-group": newSlideOrderIndex,
						"-ms-flex-order": newSlideOrderIndex,
						"-webkit-order": newSlideOrderIndex,
						order: newSlideOrderIndex,
					});
				} else if (params.slidesPerColumnFill === "column") {
					column = Math.floor(i / slidesPerColumn);
					row = i - column * slidesPerColumn;

					if (column > numFullColumns || (column === numFullColumns && row === slidesPerColumn - 1)) {
						row += 1;

						if (row >= slidesPerColumn) {
							row = 0;
							column += 1;
						}
					}
				} else {
					row = Math.floor(i / slidesPerRow);
					column = i - row * slidesPerRow;
				}

				slide.css("margin-" + (swiper.isHorizontal() ? "top" : "left"), row !== 0 && params.spaceBetween && params.spaceBetween + "px");
			}

			if (slide.css("display") === "none") continue; // eslint-disable-line

			if (params.slidesPerView === "auto") {
				var slideStyles = window.getComputedStyle(slide[0], null);
				var currentTransform = slide[0].style.transform;
				var currentWebKitTransform = slide[0].style.webkitTransform;

				if (currentTransform) {
					slide[0].style.transform = "none";
				}

				if (currentWebKitTransform) {
					slide[0].style.webkitTransform = "none";
				}

				if (params.roundLengths) {
					slideSize = swiper.isHorizontal() ? slide.outerWidth(true) : slide.outerHeight(true);
				} else {
					// eslint-disable-next-line
					if (swiper.isHorizontal()) {
						var width = parseFloat(slideStyles.getPropertyValue("width") || 0);
						var paddingLeft = parseFloat(slideStyles.getPropertyValue("padding-left") || 0);
						var paddingRight = parseFloat(slideStyles.getPropertyValue("padding-right") || 0);
						var marginLeft = parseFloat(slideStyles.getPropertyValue("margin-left") || 0);
						var marginRight = parseFloat(slideStyles.getPropertyValue("margin-right") || 0);
						var boxSizing = slideStyles.getPropertyValue("box-sizing");

						if (boxSizing && boxSizing === "border-box") {
							slideSize = width + marginLeft + marginRight;
						} else {
							var _slide$ = slide[0],
								clientWidth = _slide$.clientWidth,
								offsetWidth = _slide$.offsetWidth;
							slideSize = width + paddingLeft + paddingRight + marginLeft + marginRight + (offsetWidth - clientWidth);
						}
					} else {
						var height = parseFloat(slideStyles.getPropertyValue("height") || 0);
						var paddingTop = parseFloat(slideStyles.getPropertyValue("padding-top") || 0);
						var paddingBottom = parseFloat(slideStyles.getPropertyValue("padding-bottom") || 0);
						var marginTop = parseFloat(slideStyles.getPropertyValue("margin-top") || 0);
						var marginBottom = parseFloat(slideStyles.getPropertyValue("margin-bottom") || 0);

						var _boxSizing = slideStyles.getPropertyValue("box-sizing");

						if (_boxSizing && _boxSizing === "border-box") {
							slideSize = height + marginTop + marginBottom;
						} else {
							var _slide$2 = slide[0],
								clientHeight = _slide$2.clientHeight,
								offsetHeight = _slide$2.offsetHeight;
							slideSize = height + paddingTop + paddingBottom + marginTop + marginBottom + (offsetHeight - clientHeight);
						}
					}
				}

				if (currentTransform) {
					slide[0].style.transform = currentTransform;
				}

				if (currentWebKitTransform) {
					slide[0].style.webkitTransform = currentWebKitTransform;
				}

				if (params.roundLengths) slideSize = Math.floor(slideSize);
			} else {
				slideSize = (swiperSize - (params.slidesPerView - 1) * spaceBetween) / params.slidesPerView;
				if (params.roundLengths) slideSize = Math.floor(slideSize);

				if (slides[i]) {
					if (swiper.isHorizontal()) {
						slides[i].style.width = slideSize + "px";
					} else {
						slides[i].style.height = slideSize + "px";
					}
				}
			}

			if (slides[i]) {
				slides[i].swiperSlideSize = slideSize;
			}

			slidesSizesGrid.push(slideSize);

			if (params.centeredSlides) {
				slidePosition = slidePosition + slideSize / 2 + prevSlideSize / 2 + spaceBetween;
				if (prevSlideSize === 0 && i !== 0) slidePosition = slidePosition - swiperSize / 2 - spaceBetween;
				if (i === 0) slidePosition = slidePosition - swiperSize / 2 - spaceBetween;
				if (Math.abs(slidePosition) < 1 / 1000) slidePosition = 0;
				if (params.roundLengths) slidePosition = Math.floor(slidePosition);
				if (index % params.slidesPerGroup === 0) snapGrid.push(slidePosition);
				slidesGrid.push(slidePosition);
			} else {
				if (params.roundLengths) slidePosition = Math.floor(slidePosition);
				if ((index - Math.min(swiper.params.slidesPerGroupSkip, index)) % swiper.params.slidesPerGroup === 0) snapGrid.push(slidePosition);
				slidesGrid.push(slidePosition);
				slidePosition = slidePosition + slideSize + spaceBetween;
			}

			swiper.virtualSize += slideSize + spaceBetween;
			prevSlideSize = slideSize;
			index += 1;
		}

		swiper.virtualSize = Math.max(swiper.virtualSize, swiperSize) + offsetAfter;
		var newSlidesGrid;

		if (rtl && wrongRTL && (params.effect === "slide" || params.effect === "coverflow")) {
			$wrapperEl.css({
				width: swiper.virtualSize + params.spaceBetween + "px",
			});
		}

		if (params.setWrapperSize) {
			if (swiper.isHorizontal())
				$wrapperEl.css({
					width: swiper.virtualSize + params.spaceBetween + "px",
				});
			else
				$wrapperEl.css({
					height: swiper.virtualSize + params.spaceBetween + "px",
				});
		}

		if (params.slidesPerColumn > 1) {
			swiper.virtualSize = (slideSize + params.spaceBetween) * slidesNumberEvenToRows;
			swiper.virtualSize = Math.ceil(swiper.virtualSize / params.slidesPerColumn) - params.spaceBetween;
			if (swiper.isHorizontal())
				$wrapperEl.css({
					width: swiper.virtualSize + params.spaceBetween + "px",
				});
			else
				$wrapperEl.css({
					height: swiper.virtualSize + params.spaceBetween + "px",
				});

			if (params.centeredSlides) {
				newSlidesGrid = [];

				for (var _i = 0; _i < snapGrid.length; _i += 1) {
					var slidesGridItem = snapGrid[_i];
					if (params.roundLengths) slidesGridItem = Math.floor(slidesGridItem);
					if (snapGrid[_i] < swiper.virtualSize + snapGrid[0]) newSlidesGrid.push(slidesGridItem);
				}

				snapGrid = newSlidesGrid;
			}
		} // Remove last grid elements depending on width

		if (!params.centeredSlides) {
			newSlidesGrid = [];

			for (var _i2 = 0; _i2 < snapGrid.length; _i2 += 1) {
				var _slidesGridItem = snapGrid[_i2];
				if (params.roundLengths) _slidesGridItem = Math.floor(_slidesGridItem);

				if (snapGrid[_i2] <= swiper.virtualSize - swiperSize) {
					newSlidesGrid.push(_slidesGridItem);
				}
			}

			snapGrid = newSlidesGrid;

			if (Math.floor(swiper.virtualSize - swiperSize) - Math.floor(snapGrid[snapGrid.length - 1]) > 1) {
				snapGrid.push(swiper.virtualSize - swiperSize);
			}
		}

		if (snapGrid.length === 0) snapGrid = [0];

		if (params.spaceBetween !== 0) {
			if (swiper.isHorizontal()) {
				if (rtl)
					slides.filter(slidesForMargin).css({
						marginLeft: spaceBetween + "px",
					});
				else
					slides.filter(slidesForMargin).css({
						marginRight: spaceBetween + "px",
					});
			} else
				slides.filter(slidesForMargin).css({
					marginBottom: spaceBetween + "px",
				});
		}

		if (params.centeredSlides && params.centeredSlidesBounds) {
			var allSlidesSize = 0;
			slidesSizesGrid.forEach(function (slideSizeValue) {
				allSlidesSize += slideSizeValue + (params.spaceBetween ? params.spaceBetween : 0);
			});
			allSlidesSize -= params.spaceBetween;
			var maxSnap = allSlidesSize - swiperSize;
			snapGrid = snapGrid.map(function (snap) {
				if (snap < 0) return -offsetBefore;
				if (snap > maxSnap) return maxSnap + offsetAfter;
				return snap;
			});
		}

		if (params.centerInsufficientSlides) {
			var _allSlidesSize = 0;
			slidesSizesGrid.forEach(function (slideSizeValue) {
				_allSlidesSize += slideSizeValue + (params.spaceBetween ? params.spaceBetween : 0);
			});
			_allSlidesSize -= params.spaceBetween;

			if (_allSlidesSize < swiperSize) {
				var allSlidesOffset = (swiperSize - _allSlidesSize) / 2;
				snapGrid.forEach(function (snap, snapIndex) {
					snapGrid[snapIndex] = snap - allSlidesOffset;
				});
				slidesGrid.forEach(function (snap, snapIndex) {
					slidesGrid[snapIndex] = snap + allSlidesOffset;
				});
			}
		}

		extend$1(swiper, {
			slides: slides,
			snapGrid: snapGrid,
			slidesGrid: slidesGrid,
			slidesSizesGrid: slidesSizesGrid,
		});

		if (slidesLength !== previousSlidesLength) {
			swiper.emit("slidesLengthChange");
		}

		if (snapGrid.length !== previousSnapGridLength) {
			if (swiper.params.watchOverflow) swiper.checkOverflow();
			swiper.emit("snapGridLengthChange");
		}

		if (slidesGrid.length !== previousSlidesGridLength) {
			swiper.emit("slidesGridLengthChange");
		}

		if (params.watchSlidesProgress || params.watchSlidesVisibility) {
			swiper.updateSlidesOffset();
		}
	}

	function updateAutoHeight(speed) {
		var swiper = this;
		var activeSlides = [];
		var newHeight = 0;
		var i;

		if (typeof speed === "number") {
			swiper.setTransition(speed);
		} else if (speed === true) {
			swiper.setTransition(swiper.params.speed);
		} // Find slides currently in view

		if (swiper.params.slidesPerView !== "auto" && swiper.params.slidesPerView > 1) {
			if (swiper.params.centeredSlides) {
				swiper.visibleSlides.each(function (slide) {
					activeSlides.push(slide);
				});
			} else {
				for (i = 0; i < Math.ceil(swiper.params.slidesPerView); i += 1) {
					var index = swiper.activeIndex + i;
					if (index > swiper.slides.length) break;
					activeSlides.push(swiper.slides.eq(index)[0]);
				}
			}
		} else {
			activeSlides.push(swiper.slides.eq(swiper.activeIndex)[0]);
		} // Find new height from highest slide in view

		for (i = 0; i < activeSlides.length; i += 1) {
			if (typeof activeSlides[i] !== "undefined") {
				var height = activeSlides[i].offsetHeight;
				newHeight = height > newHeight ? height : newHeight;
			}
		} // Update Height

		if (newHeight) swiper.$wrapperEl.css("height", newHeight + "px");
	}

	function updateSlidesOffset() {
		var swiper = this;
		var slides = swiper.slides;

		for (var i = 0; i < slides.length; i += 1) {
			slides[i].swiperSlideOffset = swiper.isHorizontal() ? slides[i].offsetLeft : slides[i].offsetTop;
		}
	}

	function updateSlidesProgress(translate) {
		if (translate === void 0) {
			translate = (this && this.translate) || 0;
		}

		var swiper = this;
		var params = swiper.params;
		var slides = swiper.slides,
			rtl = swiper.rtlTranslate;
		if (slides.length === 0) return;
		if (typeof slides[0].swiperSlideOffset === "undefined") swiper.updateSlidesOffset();
		var offsetCenter = -translate;
		if (rtl) offsetCenter = translate; // Visible Slides

		slides.removeClass(params.slideVisibleClass);
		swiper.visibleSlidesIndexes = [];
		swiper.visibleSlides = [];

		for (var i = 0; i < slides.length; i += 1) {
			var slide = slides[i];
			var slideProgress = (offsetCenter + (params.centeredSlides ? swiper.minTranslate() : 0) - slide.swiperSlideOffset) / (slide.swiperSlideSize + params.spaceBetween);

			if (params.watchSlidesVisibility || (params.centeredSlides && params.autoHeight)) {
				var slideBefore = -(offsetCenter - slide.swiperSlideOffset);
				var slideAfter = slideBefore + swiper.slidesSizesGrid[i];
				var isVisible = (slideBefore >= 0 && slideBefore < swiper.size - 1) || (slideAfter > 1 && slideAfter <= swiper.size) || (slideBefore <= 0 && slideAfter >= swiper.size);

				if (isVisible) {
					swiper.visibleSlides.push(slide);
					swiper.visibleSlidesIndexes.push(i);
					slides.eq(i).addClass(params.slideVisibleClass);
				}
			}

			slide.progress = rtl ? -slideProgress : slideProgress;
		}

		swiper.visibleSlides = $(swiper.visibleSlides);
	}

	function updateProgress(translate) {
		var swiper = this;

		if (typeof translate === "undefined") {
			var multiplier = swiper.rtlTranslate ? -1 : 1; // eslint-disable-next-line

			translate = (swiper && swiper.translate && swiper.translate * multiplier) || 0;
		}

		var params = swiper.params;
		var translatesDiff = swiper.maxTranslate() - swiper.minTranslate();
		var progress = swiper.progress,
			isBeginning = swiper.isBeginning,
			isEnd = swiper.isEnd;
		var wasBeginning = isBeginning;
		var wasEnd = isEnd;

		if (translatesDiff === 0) {
			progress = 0;
			isBeginning = true;
			isEnd = true;
		} else {
			progress = (translate - swiper.minTranslate()) / translatesDiff;
			isBeginning = progress <= 0;
			isEnd = progress >= 1;
		}

		extend$1(swiper, {
			progress: progress,
			isBeginning: isBeginning,
			isEnd: isEnd,
		});
		if (params.watchSlidesProgress || params.watchSlidesVisibility || (params.centeredSlides && params.autoHeight)) swiper.updateSlidesProgress(translate);

		if (isBeginning && !wasBeginning) {
			swiper.emit("reachBeginning toEdge");
		}

		if (isEnd && !wasEnd) {
			swiper.emit("reachEnd toEdge");
		}

		if ((wasBeginning && !isBeginning) || (wasEnd && !isEnd)) {
			swiper.emit("fromEdge");
		}

		swiper.emit("progress", progress);
	}

	function updateSlidesClasses() {
		var swiper = this;
		var slides = swiper.slides,
			params = swiper.params,
			$wrapperEl = swiper.$wrapperEl,
			activeIndex = swiper.activeIndex,
			realIndex = swiper.realIndex;
		var isVirtual = swiper.virtual && params.virtual.enabled;
		slides.removeClass(params.slideActiveClass + " " + params.slideNextClass + " " + params.slidePrevClass + " " + params.slideDuplicateActiveClass + " " + params.slideDuplicateNextClass + " " + params.slideDuplicatePrevClass);
		var activeSlide;

		if (isVirtual) {
			activeSlide = swiper.$wrapperEl.find("." + params.slideClass + '[data-swiper-slide-index="' + activeIndex + '"]');
		} else {
			activeSlide = slides.eq(activeIndex);
		} // Active classes

		activeSlide.addClass(params.slideActiveClass);

		if (params.loop) {
			// Duplicate to all looped slides
			if (activeSlide.hasClass(params.slideDuplicateClass)) {
				$wrapperEl.children("." + params.slideClass + ":not(." + params.slideDuplicateClass + ')[data-swiper-slide-index="' + realIndex + '"]').addClass(params.slideDuplicateActiveClass);
			} else {
				$wrapperEl.children("." + params.slideClass + "." + params.slideDuplicateClass + '[data-swiper-slide-index="' + realIndex + '"]').addClass(params.slideDuplicateActiveClass);
			}
		} // Next Slide

		var nextSlide = activeSlide
			.nextAll("." + params.slideClass)
			.eq(0)
			.addClass(params.slideNextClass);

		if (params.loop && nextSlide.length === 0) {
			nextSlide = slides.eq(0);
			nextSlide.addClass(params.slideNextClass);
		} // Prev Slide

		var prevSlide = activeSlide
			.prevAll("." + params.slideClass)
			.eq(0)
			.addClass(params.slidePrevClass);

		if (params.loop && prevSlide.length === 0) {
			prevSlide = slides.eq(-1);
			prevSlide.addClass(params.slidePrevClass);
		}

		if (params.loop) {
			// Duplicate to all looped slides
			if (nextSlide.hasClass(params.slideDuplicateClass)) {
				$wrapperEl.children("." + params.slideClass + ":not(." + params.slideDuplicateClass + ')[data-swiper-slide-index="' + nextSlide.attr("data-swiper-slide-index") + '"]').addClass(params.slideDuplicateNextClass);
			} else {
				$wrapperEl.children("." + params.slideClass + "." + params.slideDuplicateClass + '[data-swiper-slide-index="' + nextSlide.attr("data-swiper-slide-index") + '"]').addClass(params.slideDuplicateNextClass);
			}

			if (prevSlide.hasClass(params.slideDuplicateClass)) {
				$wrapperEl.children("." + params.slideClass + ":not(." + params.slideDuplicateClass + ')[data-swiper-slide-index="' + prevSlide.attr("data-swiper-slide-index") + '"]').addClass(params.slideDuplicatePrevClass);
			} else {
				$wrapperEl.children("." + params.slideClass + "." + params.slideDuplicateClass + '[data-swiper-slide-index="' + prevSlide.attr("data-swiper-slide-index") + '"]').addClass(params.slideDuplicatePrevClass);
			}
		}

		swiper.emitSlidesClasses();
	}

	function updateActiveIndex(newActiveIndex) {
		var swiper = this;
		var translate = swiper.rtlTranslate ? swiper.translate : -swiper.translate;
		var slidesGrid = swiper.slidesGrid,
			snapGrid = swiper.snapGrid,
			params = swiper.params,
			previousIndex = swiper.activeIndex,
			previousRealIndex = swiper.realIndex,
			previousSnapIndex = swiper.snapIndex;
		var activeIndex = newActiveIndex;
		var snapIndex;

		if (typeof activeIndex === "undefined") {
			for (var i = 0; i < slidesGrid.length; i += 1) {
				if (typeof slidesGrid[i + 1] !== "undefined") {
					if (translate >= slidesGrid[i] && translate < slidesGrid[i + 1] - (slidesGrid[i + 1] - slidesGrid[i]) / 2) {
						activeIndex = i;
					} else if (translate >= slidesGrid[i] && translate < slidesGrid[i + 1]) {
						activeIndex = i + 1;
					}
				} else if (translate >= slidesGrid[i]) {
					activeIndex = i;
				}
			} // Normalize slideIndex

			if (params.normalizeSlideIndex) {
				if (activeIndex < 0 || typeof activeIndex === "undefined") activeIndex = 0;
			}
		}

		if (snapGrid.indexOf(translate) >= 0) {
			snapIndex = snapGrid.indexOf(translate);
		} else {
			var skip = Math.min(params.slidesPerGroupSkip, activeIndex);
			snapIndex = skip + Math.floor((activeIndex - skip) / params.slidesPerGroup);
		}

		if (snapIndex >= snapGrid.length) snapIndex = snapGrid.length - 1;

		if (activeIndex === previousIndex) {
			if (snapIndex !== previousSnapIndex) {
				swiper.snapIndex = snapIndex;
				swiper.emit("snapIndexChange");
			}

			return;
		} // Get real index

		var realIndex = parseInt(swiper.slides.eq(activeIndex).attr("data-swiper-slide-index") || activeIndex, 10);
		extend$1(swiper, {
			snapIndex: snapIndex,
			realIndex: realIndex,
			previousIndex: previousIndex,
			activeIndex: activeIndex,
		});
		swiper.emit("activeIndexChange");
		swiper.emit("snapIndexChange");

		if (previousRealIndex !== realIndex) {
			swiper.emit("realIndexChange");
		}

		if (swiper.initialized || swiper.params.runCallbacksOnInit) {
			swiper.emit("slideChange");
		}
	}

	function updateClickedSlide(e) {
		var swiper = this;
		var params = swiper.params;
		var slide = $(e.target).closest("." + params.slideClass)[0];
		var slideFound = false;

		if (slide) {
			for (var i = 0; i < swiper.slides.length; i += 1) {
				if (swiper.slides[i] === slide) slideFound = true;
			}
		}

		if (slide && slideFound) {
			swiper.clickedSlide = slide;

			if (swiper.virtual && swiper.params.virtual.enabled) {
				swiper.clickedIndex = parseInt($(slide).attr("data-swiper-slide-index"), 10);
			} else {
				swiper.clickedIndex = $(slide).index();
			}
		} else {
			swiper.clickedSlide = undefined;
			swiper.clickedIndex = undefined;
			return;
		}

		if (params.slideToClickedSlide && swiper.clickedIndex !== undefined && swiper.clickedIndex !== swiper.activeIndex) {
			swiper.slideToClickedSlide();
		}
	}

	var update = {
		updateSize: updateSize,
		updateSlides: updateSlides,
		updateAutoHeight: updateAutoHeight,
		updateSlidesOffset: updateSlidesOffset,
		updateSlidesProgress: updateSlidesProgress,
		updateProgress: updateProgress,
		updateSlidesClasses: updateSlidesClasses,
		updateActiveIndex: updateActiveIndex,
		updateClickedSlide: updateClickedSlide,
	};

	function getSwiperTranslate(axis) {
		if (axis === void 0) {
			axis = this.isHorizontal() ? "x" : "y";
		}

		var swiper = this;
		var params = swiper.params,
			rtl = swiper.rtlTranslate,
			translate = swiper.translate,
			$wrapperEl = swiper.$wrapperEl;

		if (params.virtualTranslate) {
			return rtl ? -translate : translate;
		}

		if (params.cssMode) {
			return translate;
		}

		var currentTranslate = getTranslate($wrapperEl[0], axis);
		if (rtl) currentTranslate = -currentTranslate;
		return currentTranslate || 0;
	}

	function setTranslate(translate, byController) {
		var swiper = this;
		var rtl = swiper.rtlTranslate,
			params = swiper.params,
			$wrapperEl = swiper.$wrapperEl,
			wrapperEl = swiper.wrapperEl,
			progress = swiper.progress;
		var x = 0;
		var y = 0;
		var z = 0;

		if (swiper.isHorizontal()) {
			x = rtl ? -translate : translate;
		} else {
			y = translate;
		}

		if (params.roundLengths) {
			x = Math.floor(x);
			y = Math.floor(y);
		}

		if (params.cssMode) {
			wrapperEl[swiper.isHorizontal() ? "scrollLeft" : "scrollTop"] = swiper.isHorizontal() ? -x : -y;
		} else if (!params.virtualTranslate) {
			$wrapperEl.transform("translate3d(" + x + "px, " + y + "px, " + z + "px)");
		}

		swiper.previousTranslate = swiper.translate;
		swiper.translate = swiper.isHorizontal() ? x : y; // Check if we need to update progress

		var newProgress;
		var translatesDiff = swiper.maxTranslate() - swiper.minTranslate();

		if (translatesDiff === 0) {
			newProgress = 0;
		} else {
			newProgress = (translate - swiper.minTranslate()) / translatesDiff;
		}

		if (newProgress !== progress) {
			swiper.updateProgress(translate);
		}

		swiper.emit("setTranslate", swiper.translate, byController);
	}

	function minTranslate() {
		return -this.snapGrid[0];
	}

	function maxTranslate() {
		return -this.snapGrid[this.snapGrid.length - 1];
	}

	function translateTo(translate, speed, runCallbacks, translateBounds, internal) {
		if (translate === void 0) {
			translate = 0;
		}

		if (speed === void 0) {
			speed = this.params.speed;
		}

		if (runCallbacks === void 0) {
			runCallbacks = true;
		}

		if (translateBounds === void 0) {
			translateBounds = true;
		}

		var swiper = this;
		var params = swiper.params,
			wrapperEl = swiper.wrapperEl;

		if (swiper.animating && params.preventInteractionOnTransition) {
			return false;
		}

		var minTranslate = swiper.minTranslate();
		var maxTranslate = swiper.maxTranslate();
		var newTranslate;
		if (translateBounds && translate > minTranslate) newTranslate = minTranslate;
		else if (translateBounds && translate < maxTranslate) newTranslate = maxTranslate;
		else newTranslate = translate; // Update progress

		swiper.updateProgress(newTranslate);

		if (params.cssMode) {
			var isH = swiper.isHorizontal();

			if (speed === 0) {
				wrapperEl[isH ? "scrollLeft" : "scrollTop"] = -newTranslate;
			} else {
				// eslint-disable-next-line
				if (wrapperEl.scrollTo) {
					var _wrapperEl$scrollTo;

					wrapperEl.scrollTo(((_wrapperEl$scrollTo = {}), (_wrapperEl$scrollTo[isH ? "left" : "top"] = -newTranslate), (_wrapperEl$scrollTo.behavior = "smooth"), _wrapperEl$scrollTo));
				} else {
					wrapperEl[isH ? "scrollLeft" : "scrollTop"] = -newTranslate;
				}
			}

			return true;
		}

		if (speed === 0) {
			swiper.setTransition(0);
			swiper.setTranslate(newTranslate);

			if (runCallbacks) {
				swiper.emit("beforeTransitionStart", speed, internal);
				swiper.emit("transitionEnd");
			}
		} else {
			swiper.setTransition(speed);
			swiper.setTranslate(newTranslate);

			if (runCallbacks) {
				swiper.emit("beforeTransitionStart", speed, internal);
				swiper.emit("transitionStart");
			}

			if (!swiper.animating) {
				swiper.animating = true;

				if (!swiper.onTranslateToWrapperTransitionEnd) {
					swiper.onTranslateToWrapperTransitionEnd = function transitionEnd(e) {
						if (!swiper || swiper.destroyed) return;
						if (e.target !== this) return;
						swiper.$wrapperEl[0].removeEventListener("transitionend", swiper.onTranslateToWrapperTransitionEnd);
						swiper.$wrapperEl[0].removeEventListener("webkitTransitionEnd", swiper.onTranslateToWrapperTransitionEnd);
						swiper.onTranslateToWrapperTransitionEnd = null;
						delete swiper.onTranslateToWrapperTransitionEnd;

						if (runCallbacks) {
							swiper.emit("transitionEnd");
						}
					};
				}

				swiper.$wrapperEl[0].addEventListener("transitionend", swiper.onTranslateToWrapperTransitionEnd);
				swiper.$wrapperEl[0].addEventListener("webkitTransitionEnd", swiper.onTranslateToWrapperTransitionEnd);
			}
		}

		return true;
	}

	var translate = {
		getTranslate: getSwiperTranslate,
		setTranslate: setTranslate,
		minTranslate: minTranslate,
		maxTranslate: maxTranslate,
		translateTo: translateTo,
	};

	function setTransition(duration, byController) {
		var swiper = this;

		if (!swiper.params.cssMode) {
			swiper.$wrapperEl.transition(duration);
		}

		swiper.emit("setTransition", duration, byController);
	}

	function transitionStart(runCallbacks, direction) {
		if (runCallbacks === void 0) {
			runCallbacks = true;
		}

		var swiper = this;
		var activeIndex = swiper.activeIndex,
			params = swiper.params,
			previousIndex = swiper.previousIndex;
		if (params.cssMode) return;

		if (params.autoHeight) {
			swiper.updateAutoHeight();
		}

		var dir = direction;

		if (!dir) {
			if (activeIndex > previousIndex) dir = "next";
			else if (activeIndex < previousIndex) dir = "prev";
			else dir = "reset";
		}

		swiper.emit("transitionStart");

		if (runCallbacks && activeIndex !== previousIndex) {
			if (dir === "reset") {
				swiper.emit("slideResetTransitionStart");
				return;
			}

			swiper.emit("slideChangeTransitionStart");

			if (dir === "next") {
				swiper.emit("slideNextTransitionStart");
			} else {
				swiper.emit("slidePrevTransitionStart");
			}
		}
	}

	function transitionEnd$1(runCallbacks, direction) {
		if (runCallbacks === void 0) {
			runCallbacks = true;
		}

		var swiper = this;
		var activeIndex = swiper.activeIndex,
			previousIndex = swiper.previousIndex,
			params = swiper.params;
		swiper.animating = false;
		if (params.cssMode) return;
		swiper.setTransition(0);
		var dir = direction;

		if (!dir) {
			if (activeIndex > previousIndex) dir = "next";
			else if (activeIndex < previousIndex) dir = "prev";
			else dir = "reset";
		}

		swiper.emit("transitionEnd");

		if (runCallbacks && activeIndex !== previousIndex) {
			if (dir === "reset") {
				swiper.emit("slideResetTransitionEnd");
				return;
			}

			swiper.emit("slideChangeTransitionEnd");

			if (dir === "next") {
				swiper.emit("slideNextTransitionEnd");
			} else {
				swiper.emit("slidePrevTransitionEnd");
			}
		}
	}

	var transition$1 = {
		setTransition: setTransition,
		transitionStart: transitionStart,
		transitionEnd: transitionEnd$1,
	};

	function slideTo(index, speed, runCallbacks, internal) {
		if (index === void 0) {
			index = 0;
		}

		if (speed === void 0) {
			speed = this.params.speed;
		}

		if (runCallbacks === void 0) {
			runCallbacks = true;
		}

		if (typeof index !== "number" && typeof index !== "string") {
			throw new Error("The 'index' argument cannot have type other than 'number' or 'string'. [" + typeof index + "] given.");
		}

		if (typeof index === "string") {
			/**
			 * The `index` argument converted from `string` to `number`.
			 * @type {number}
			 */
			var indexAsNumber = parseInt(index, 10);
			/**
			 * Determines whether the `index` argument is a valid `number`
			 * after being converted from the `string` type.
			 * @type {boolean}
			 */

			var isValidNumber = isFinite(indexAsNumber);

			if (!isValidNumber) {
				throw new Error("The passed-in 'index' (string) couldn't be converted to 'number'. [" + index + "] given.");
			} // Knowing that the converted `index` is a valid number,
			// we can update the original argument's value.

			index = indexAsNumber;
		}

		var swiper = this;
		var slideIndex = index;
		if (slideIndex < 0) slideIndex = 0;
		var params = swiper.params,
			snapGrid = swiper.snapGrid,
			slidesGrid = swiper.slidesGrid,
			previousIndex = swiper.previousIndex,
			activeIndex = swiper.activeIndex,
			rtl = swiper.rtlTranslate,
			wrapperEl = swiper.wrapperEl;

		if (swiper.animating && params.preventInteractionOnTransition) {
			return false;
		}

		var skip = Math.min(swiper.params.slidesPerGroupSkip, slideIndex);
		var snapIndex = skip + Math.floor((slideIndex - skip) / swiper.params.slidesPerGroup);
		if (snapIndex >= snapGrid.length) snapIndex = snapGrid.length - 1;

		if ((activeIndex || params.initialSlide || 0) === (previousIndex || 0) && runCallbacks) {
			swiper.emit("beforeSlideChangeStart");
		}

		var translate = -snapGrid[snapIndex]; // Update progress

		swiper.updateProgress(translate); // Normalize slideIndex

		if (params.normalizeSlideIndex) {
			for (var i = 0; i < slidesGrid.length; i += 1) {
				if (-Math.floor(translate * 100) >= Math.floor(slidesGrid[i] * 100)) {
					slideIndex = i;
				}
			}
		} // Directions locks

		if (swiper.initialized && slideIndex !== activeIndex) {
			if (!swiper.allowSlideNext && translate < swiper.translate && translate < swiper.minTranslate()) {
				return false;
			}

			if (!swiper.allowSlidePrev && translate > swiper.translate && translate > swiper.maxTranslate()) {
				if ((activeIndex || 0) !== slideIndex) return false;
			}
		}

		var direction;
		if (slideIndex > activeIndex) direction = "next";
		else if (slideIndex < activeIndex) direction = "prev";
		else direction = "reset"; // Update Index

		if ((rtl && -translate === swiper.translate) || (!rtl && translate === swiper.translate)) {
			swiper.updateActiveIndex(slideIndex); // Update Height

			if (params.autoHeight) {
				swiper.updateAutoHeight();
			}

			swiper.updateSlidesClasses();

			if (params.effect !== "slide") {
				swiper.setTranslate(translate);
			}

			if (direction !== "reset") {
				swiper.transitionStart(runCallbacks, direction);
				swiper.transitionEnd(runCallbacks, direction);
			}

			return false;
		}

		if (params.cssMode) {
			var isH = swiper.isHorizontal();
			var t = -translate;

			if (rtl) {
				t = wrapperEl.scrollWidth - wrapperEl.offsetWidth - t;
			}

			if (speed === 0) {
				wrapperEl[isH ? "scrollLeft" : "scrollTop"] = t;
			} else {
				// eslint-disable-next-line
				if (wrapperEl.scrollTo) {
					var _wrapperEl$scrollTo;

					wrapperEl.scrollTo(((_wrapperEl$scrollTo = {}), (_wrapperEl$scrollTo[isH ? "left" : "top"] = t), (_wrapperEl$scrollTo.behavior = "smooth"), _wrapperEl$scrollTo));
				} else {
					wrapperEl[isH ? "scrollLeft" : "scrollTop"] = t;
				}
			}

			return true;
		}

		if (speed === 0) {
			swiper.setTransition(0);
			swiper.setTranslate(translate);
			swiper.updateActiveIndex(slideIndex);
			swiper.updateSlidesClasses();
			swiper.emit("beforeTransitionStart", speed, internal);
			swiper.transitionStart(runCallbacks, direction);
			swiper.transitionEnd(runCallbacks, direction);
		} else {
			swiper.setTransition(speed);
			swiper.setTranslate(translate);
			swiper.updateActiveIndex(slideIndex);
			swiper.updateSlidesClasses();
			swiper.emit("beforeTransitionStart", speed, internal);
			swiper.transitionStart(runCallbacks, direction);

			if (!swiper.animating) {
				swiper.animating = true;

				if (!swiper.onSlideToWrapperTransitionEnd) {
					swiper.onSlideToWrapperTransitionEnd = function transitionEnd(e) {
						if (!swiper || swiper.destroyed) return;
						if (e.target !== this) return;
						swiper.$wrapperEl[0].removeEventListener("transitionend", swiper.onSlideToWrapperTransitionEnd);
						swiper.$wrapperEl[0].removeEventListener("webkitTransitionEnd", swiper.onSlideToWrapperTransitionEnd);
						swiper.onSlideToWrapperTransitionEnd = null;
						delete swiper.onSlideToWrapperTransitionEnd;
						swiper.transitionEnd(runCallbacks, direction);
					};
				}

				swiper.$wrapperEl[0].addEventListener("transitionend", swiper.onSlideToWrapperTransitionEnd);
				swiper.$wrapperEl[0].addEventListener("webkitTransitionEnd", swiper.onSlideToWrapperTransitionEnd);
			}
		}

		return true;
	}

	function slideToLoop(index, speed, runCallbacks, internal) {
		if (index === void 0) {
			index = 0;
		}

		if (speed === void 0) {
			speed = this.params.speed;
		}

		if (runCallbacks === void 0) {
			runCallbacks = true;
		}

		var swiper = this;
		var newIndex = index;

		if (swiper.params.loop) {
			newIndex += swiper.loopedSlides;
		}

		return swiper.slideTo(newIndex, speed, runCallbacks, internal);
	}

	/* eslint no-unused-vars: "off" */
	function slideNext(speed, runCallbacks, internal) {
		if (speed === void 0) {
			speed = this.params.speed;
		}

		if (runCallbacks === void 0) {
			runCallbacks = true;
		}

		var swiper = this;
		var params = swiper.params,
			animating = swiper.animating;
		var increment = swiper.activeIndex < params.slidesPerGroupSkip ? 1 : params.slidesPerGroup;

		if (params.loop) {
			if (animating && params.loopPreventsSlide) return false;
			swiper.loopFix(); // eslint-disable-next-line

			swiper._clientLeft = swiper.$wrapperEl[0].clientLeft;
		}

		return swiper.slideTo(swiper.activeIndex + increment, speed, runCallbacks, internal);
	}

	/* eslint no-unused-vars: "off" */
	function slidePrev(speed, runCallbacks, internal) {
		if (speed === void 0) {
			speed = this.params.speed;
		}

		if (runCallbacks === void 0) {
			runCallbacks = true;
		}

		var swiper = this;
		var params = swiper.params,
			animating = swiper.animating,
			snapGrid = swiper.snapGrid,
			slidesGrid = swiper.slidesGrid,
			rtlTranslate = swiper.rtlTranslate;

		if (params.loop) {
			if (animating && params.loopPreventsSlide) return false;
			swiper.loopFix(); // eslint-disable-next-line

			swiper._clientLeft = swiper.$wrapperEl[0].clientLeft;
		}

		var translate = rtlTranslate ? swiper.translate : -swiper.translate;

		function normalize(val) {
			if (val < 0) return -Math.floor(Math.abs(val));
			return Math.floor(val);
		}

		var normalizedTranslate = normalize(translate);
		var normalizedSnapGrid = snapGrid.map(function (val) {
			return normalize(val);
		});
		var currentSnap = snapGrid[normalizedSnapGrid.indexOf(normalizedTranslate)];
		var prevSnap = snapGrid[normalizedSnapGrid.indexOf(normalizedTranslate) - 1];

		if (typeof prevSnap === "undefined" && params.cssMode) {
			snapGrid.forEach(function (snap) {
				if (!prevSnap && normalizedTranslate >= snap) prevSnap = snap;
			});
		}

		var prevIndex;

		if (typeof prevSnap !== "undefined") {
			prevIndex = slidesGrid.indexOf(prevSnap);
			if (prevIndex < 0) prevIndex = swiper.activeIndex - 1;
		}

		return swiper.slideTo(prevIndex, speed, runCallbacks, internal);
	}

	/* eslint no-unused-vars: "off" */
	function slideReset(speed, runCallbacks, internal) {
		if (speed === void 0) {
			speed = this.params.speed;
		}

		if (runCallbacks === void 0) {
			runCallbacks = true;
		}

		var swiper = this;
		return swiper.slideTo(swiper.activeIndex, speed, runCallbacks, internal);
	}

	/* eslint no-unused-vars: "off" */
	function slideToClosest(speed, runCallbacks, internal, threshold) {
		if (speed === void 0) {
			speed = this.params.speed;
		}

		if (runCallbacks === void 0) {
			runCallbacks = true;
		}

		if (threshold === void 0) {
			threshold = 0.5;
		}

		var swiper = this;
		var index = swiper.activeIndex;
		var skip = Math.min(swiper.params.slidesPerGroupSkip, index);
		var snapIndex = skip + Math.floor((index - skip) / swiper.params.slidesPerGroup);
		var translate = swiper.rtlTranslate ? swiper.translate : -swiper.translate;

		if (translate >= swiper.snapGrid[snapIndex]) {
			// The current translate is on or after the current snap index, so the choice
			// is between the current index and the one after it.
			var currentSnap = swiper.snapGrid[snapIndex];
			var nextSnap = swiper.snapGrid[snapIndex + 1];

			if (translate - currentSnap > (nextSnap - currentSnap) * threshold) {
				index += swiper.params.slidesPerGroup;
			}
		} else {
			// The current translate is before the current snap index, so the choice
			// is between the current index and the one before it.
			var prevSnap = swiper.snapGrid[snapIndex - 1];
			var _currentSnap = swiper.snapGrid[snapIndex];

			if (translate - prevSnap <= (_currentSnap - prevSnap) * threshold) {
				index -= swiper.params.slidesPerGroup;
			}
		}

		index = Math.max(index, 0);
		index = Math.min(index, swiper.slidesGrid.length - 1);
		return swiper.slideTo(index, speed, runCallbacks, internal);
	}

	function slideToClickedSlide() {
		var swiper = this;
		var params = swiper.params,
			$wrapperEl = swiper.$wrapperEl;
		var slidesPerView = params.slidesPerView === "auto" ? swiper.slidesPerViewDynamic() : params.slidesPerView;
		var slideToIndex = swiper.clickedIndex;
		var realIndex;

		if (params.loop) {
			if (swiper.animating) return;
			realIndex = parseInt($(swiper.clickedSlide).attr("data-swiper-slide-index"), 10);

			if (params.centeredSlides) {
				if (slideToIndex < swiper.loopedSlides - slidesPerView / 2 || slideToIndex > swiper.slides.length - swiper.loopedSlides + slidesPerView / 2) {
					swiper.loopFix();
					slideToIndex = $wrapperEl
						.children("." + params.slideClass + '[data-swiper-slide-index="' + realIndex + '"]:not(.' + params.slideDuplicateClass + ")")
						.eq(0)
						.index();
					nextTick(function () {
						swiper.slideTo(slideToIndex);
					});
				} else {
					swiper.slideTo(slideToIndex);
				}
			} else if (slideToIndex > swiper.slides.length - slidesPerView) {
				swiper.loopFix();
				slideToIndex = $wrapperEl
					.children("." + params.slideClass + '[data-swiper-slide-index="' + realIndex + '"]:not(.' + params.slideDuplicateClass + ")")
					.eq(0)
					.index();
				nextTick(function () {
					swiper.slideTo(slideToIndex);
				});
			} else {
				swiper.slideTo(slideToIndex);
			}
		} else {
			swiper.slideTo(slideToIndex);
		}
	}

	var slide = {
		slideTo: slideTo,
		slideToLoop: slideToLoop,
		slideNext: slideNext,
		slidePrev: slidePrev,
		slideReset: slideReset,
		slideToClosest: slideToClosest,
		slideToClickedSlide: slideToClickedSlide,
	};

	function loopCreate() {
		var swiper = this;
		var document = getDocument();
		var params = swiper.params,
			$wrapperEl = swiper.$wrapperEl; // Remove duplicated slides

		$wrapperEl.children("." + params.slideClass + "." + params.slideDuplicateClass).remove();
		var slides = $wrapperEl.children("." + params.slideClass);

		if (params.loopFillGroupWithBlank) {
			var blankSlidesNum = params.slidesPerGroup - (slides.length % params.slidesPerGroup);

			if (blankSlidesNum !== params.slidesPerGroup) {
				for (var i = 0; i < blankSlidesNum; i += 1) {
					var blankNode = $(document.createElement("div")).addClass(params.slideClass + " " + params.slideBlankClass);
					$wrapperEl.append(blankNode);
				}

				slides = $wrapperEl.children("." + params.slideClass);
			}
		}

		if (params.slidesPerView === "auto" && !params.loopedSlides) params.loopedSlides = slides.length;
		swiper.loopedSlides = Math.ceil(parseFloat(params.loopedSlides || params.slidesPerView, 10));
		swiper.loopedSlides += params.loopAdditionalSlides;

		if (swiper.loopedSlides > slides.length) {
			swiper.loopedSlides = slides.length;
		}

		var prependSlides = [];
		var appendSlides = [];
		slides.each(function (el, index) {
			var slide = $(el);

			if (index < swiper.loopedSlides) {
				appendSlides.push(el);
			}

			if (index < slides.length && index >= slides.length - swiper.loopedSlides) {
				prependSlides.push(el);
			}

			slide.attr("data-swiper-slide-index", index);
		});

		for (var _i = 0; _i < appendSlides.length; _i += 1) {
			$wrapperEl.append($(appendSlides[_i].cloneNode(true)).addClass(params.slideDuplicateClass));
		}

		for (var _i2 = prependSlides.length - 1; _i2 >= 0; _i2 -= 1) {
			$wrapperEl.prepend($(prependSlides[_i2].cloneNode(true)).addClass(params.slideDuplicateClass));
		}
	}

	function loopFix() {
		var swiper = this;
		swiper.emit("beforeLoopFix");
		var activeIndex = swiper.activeIndex,
			slides = swiper.slides,
			loopedSlides = swiper.loopedSlides,
			allowSlidePrev = swiper.allowSlidePrev,
			allowSlideNext = swiper.allowSlideNext,
			snapGrid = swiper.snapGrid,
			rtl = swiper.rtlTranslate;
		var newIndex;
		swiper.allowSlidePrev = true;
		swiper.allowSlideNext = true;
		var snapTranslate = -snapGrid[activeIndex];
		var diff = snapTranslate - swiper.getTranslate(); // Fix For Negative Oversliding

		if (activeIndex < loopedSlides) {
			newIndex = slides.length - loopedSlides * 3 + activeIndex;
			newIndex += loopedSlides;
			var slideChanged = swiper.slideTo(newIndex, 0, false, true);

			if (slideChanged && diff !== 0) {
				swiper.setTranslate((rtl ? -swiper.translate : swiper.translate) - diff);
			}
		} else if (activeIndex >= slides.length - loopedSlides) {
			// Fix For Positive Oversliding
			newIndex = -slides.length + activeIndex + loopedSlides;
			newIndex += loopedSlides;

			var _slideChanged = swiper.slideTo(newIndex, 0, false, true);

			if (_slideChanged && diff !== 0) {
				swiper.setTranslate((rtl ? -swiper.translate : swiper.translate) - diff);
			}
		}

		swiper.allowSlidePrev = allowSlidePrev;
		swiper.allowSlideNext = allowSlideNext;
		swiper.emit("loopFix");
	}

	function loopDestroy() {
		var swiper = this;
		var $wrapperEl = swiper.$wrapperEl,
			params = swiper.params,
			slides = swiper.slides;
		$wrapperEl.children("." + params.slideClass + "." + params.slideDuplicateClass + ",." + params.slideClass + "." + params.slideBlankClass).remove();
		slides.removeAttr("data-swiper-slide-index");
	}

	var loop = {
		loopCreate: loopCreate,
		loopFix: loopFix,
		loopDestroy: loopDestroy,
	};

	function setGrabCursor(moving) {
		var swiper = this;
		if (swiper.support.touch || !swiper.params.simulateTouch || (swiper.params.watchOverflow && swiper.isLocked) || swiper.params.cssMode) return;
		var el = swiper.el;
		el.style.cursor = "move";
		el.style.cursor = moving ? "-webkit-grabbing" : "-webkit-grab";
		el.style.cursor = moving ? "-moz-grabbin" : "-moz-grab";
		el.style.cursor = moving ? "grabbing" : "grab";
	}

	function unsetGrabCursor() {
		var swiper = this;

		if (swiper.support.touch || (swiper.params.watchOverflow && swiper.isLocked) || swiper.params.cssMode) {
			return;
		}

		swiper.el.style.cursor = "";
	}

	var grabCursor = {
		setGrabCursor: setGrabCursor,
		unsetGrabCursor: unsetGrabCursor,
	};

	function appendSlide(slides) {
		var swiper = this;
		var $wrapperEl = swiper.$wrapperEl,
			params = swiper.params;

		if (params.loop) {
			swiper.loopDestroy();
		}

		if (typeof slides === "object" && "length" in slides) {
			for (var i = 0; i < slides.length; i += 1) {
				if (slides[i]) $wrapperEl.append(slides[i]);
			}
		} else {
			$wrapperEl.append(slides);
		}

		if (params.loop) {
			swiper.loopCreate();
		}

		if (!(params.observer && swiper.support.observer)) {
			swiper.update();
		}
	}

	function prependSlide(slides) {
		var swiper = this;
		var params = swiper.params,
			$wrapperEl = swiper.$wrapperEl,
			activeIndex = swiper.activeIndex;

		if (params.loop) {
			swiper.loopDestroy();
		}

		var newActiveIndex = activeIndex + 1;

		if (typeof slides === "object" && "length" in slides) {
			for (var i = 0; i < slides.length; i += 1) {
				if (slides[i]) $wrapperEl.prepend(slides[i]);
			}

			newActiveIndex = activeIndex + slides.length;
		} else {
			$wrapperEl.prepend(slides);
		}

		if (params.loop) {
			swiper.loopCreate();
		}

		if (!(params.observer && swiper.support.observer)) {
			swiper.update();
		}

		swiper.slideTo(newActiveIndex, 0, false);
	}

	function addSlide(index, slides) {
		var swiper = this;
		var $wrapperEl = swiper.$wrapperEl,
			params = swiper.params,
			activeIndex = swiper.activeIndex;
		var activeIndexBuffer = activeIndex;

		if (params.loop) {
			activeIndexBuffer -= swiper.loopedSlides;
			swiper.loopDestroy();
			swiper.slides = $wrapperEl.children("." + params.slideClass);
		}

		var baseLength = swiper.slides.length;

		if (index <= 0) {
			swiper.prependSlide(slides);
			return;
		}

		if (index >= baseLength) {
			swiper.appendSlide(slides);
			return;
		}

		var newActiveIndex = activeIndexBuffer > index ? activeIndexBuffer + 1 : activeIndexBuffer;
		var slidesBuffer = [];

		for (var i = baseLength - 1; i >= index; i -= 1) {
			var currentSlide = swiper.slides.eq(i);
			currentSlide.remove();
			slidesBuffer.unshift(currentSlide);
		}

		if (typeof slides === "object" && "length" in slides) {
			for (var _i = 0; _i < slides.length; _i += 1) {
				if (slides[_i]) $wrapperEl.append(slides[_i]);
			}

			newActiveIndex = activeIndexBuffer > index ? activeIndexBuffer + slides.length : activeIndexBuffer;
		} else {
			$wrapperEl.append(slides);
		}

		for (var _i2 = 0; _i2 < slidesBuffer.length; _i2 += 1) {
			$wrapperEl.append(slidesBuffer[_i2]);
		}

		if (params.loop) {
			swiper.loopCreate();
		}

		if (!(params.observer && swiper.support.observer)) {
			swiper.update();
		}

		if (params.loop) {
			swiper.slideTo(newActiveIndex + swiper.loopedSlides, 0, false);
		} else {
			swiper.slideTo(newActiveIndex, 0, false);
		}
	}

	function removeSlide(slidesIndexes) {
		var swiper = this;
		var params = swiper.params,
			$wrapperEl = swiper.$wrapperEl,
			activeIndex = swiper.activeIndex;
		var activeIndexBuffer = activeIndex;

		if (params.loop) {
			activeIndexBuffer -= swiper.loopedSlides;
			swiper.loopDestroy();
			swiper.slides = $wrapperEl.children("." + params.slideClass);
		}

		var newActiveIndex = activeIndexBuffer;
		var indexToRemove;

		if (typeof slidesIndexes === "object" && "length" in slidesIndexes) {
			for (var i = 0; i < slidesIndexes.length; i += 1) {
				indexToRemove = slidesIndexes[i];
				if (swiper.slides[indexToRemove]) swiper.slides.eq(indexToRemove).remove();
				if (indexToRemove < newActiveIndex) newActiveIndex -= 1;
			}

			newActiveIndex = Math.max(newActiveIndex, 0);
		} else {
			indexToRemove = slidesIndexes;
			if (swiper.slides[indexToRemove]) swiper.slides.eq(indexToRemove).remove();
			if (indexToRemove < newActiveIndex) newActiveIndex -= 1;
			newActiveIndex = Math.max(newActiveIndex, 0);
		}

		if (params.loop) {
			swiper.loopCreate();
		}

		if (!(params.observer && swiper.support.observer)) {
			swiper.update();
		}

		if (params.loop) {
			swiper.slideTo(newActiveIndex + swiper.loopedSlides, 0, false);
		} else {
			swiper.slideTo(newActiveIndex, 0, false);
		}
	}

	function removeAllSlides() {
		var swiper = this;
		var slidesIndexes = [];

		for (var i = 0; i < swiper.slides.length; i += 1) {
			slidesIndexes.push(i);
		}

		swiper.removeSlide(slidesIndexes);
	}

	var manipulation = {
		appendSlide: appendSlide,
		prependSlide: prependSlide,
		addSlide: addSlide,
		removeSlide: removeSlide,
		removeAllSlides: removeAllSlides,
	};

	function onTouchStart(event) {
		var swiper = this;
		var document = getDocument();
		var window = getWindow();
		var data = swiper.touchEventsData;
		var params = swiper.params,
			touches = swiper.touches;

		if (swiper.animating && params.preventInteractionOnTransition) {
			return;
		}

		var e = event;
		if (e.originalEvent) e = e.originalEvent;
		var $targetEl = $(e.target);

		if (params.touchEventsTarget === "wrapper") {
			if (!$targetEl.closest(swiper.wrapperEl).length) return;
		}

		data.isTouchEvent = e.type === "touchstart";
		if (!data.isTouchEvent && "which" in e && e.which === 3) return;
		if (!data.isTouchEvent && "button" in e && e.button > 0) return;
		if (data.isTouched && data.isMoved) return; // change target el for shadow root componenet

		var swipingClassHasValue = !!params.noSwipingClass && params.noSwipingClass !== "";

		if (swipingClassHasValue && e.target && e.target.shadowRoot && event.path && event.path[0]) {
			$targetEl = $(event.path[0]);
		}

		if (params.noSwiping && $targetEl.closest(params.noSwipingSelector ? params.noSwipingSelector : "." + params.noSwipingClass)[0]) {
			swiper.allowClick = true;
			return;
		}

		if (params.swipeHandler) {
			if (!$targetEl.closest(params.swipeHandler)[0]) return;
		}

		touches.currentX = e.type === "touchstart" ? e.targetTouches[0].pageX : e.pageX;
		touches.currentY = e.type === "touchstart" ? e.targetTouches[0].pageY : e.pageY;
		var startX = touches.currentX;
		var startY = touches.currentY; // Do NOT start if iOS edge swipe is detected. Otherwise iOS app cannot swipe-to-go-back anymore

		var edgeSwipeDetection = params.edgeSwipeDetection || params.iOSEdgeSwipeDetection;
		var edgeSwipeThreshold = params.edgeSwipeThreshold || params.iOSEdgeSwipeThreshold;

		if (edgeSwipeDetection && (startX <= edgeSwipeThreshold || startX >= window.screen.width - edgeSwipeThreshold)) {
			return;
		}

		extend$1(data, {
			isTouched: true,
			isMoved: false,
			allowTouchCallbacks: true,
			isScrolling: undefined,
			startMoving: undefined,
		});
		touches.startX = startX;
		touches.startY = startY;
		data.touchStartTime = now();
		swiper.allowClick = true;
		swiper.updateSize();
		swiper.swipeDirection = undefined;
		if (params.threshold > 0) data.allowThresholdMove = false;

		if (e.type !== "touchstart") {
			var preventDefault = true;
			if ($targetEl.is(data.formElements)) preventDefault = false;

			if (document.activeElement && $(document.activeElement).is(data.formElements) && document.activeElement !== $targetEl[0]) {
				document.activeElement.blur();
			}

			var shouldPreventDefault = preventDefault && swiper.allowTouchMove && params.touchStartPreventDefault;

			if (params.touchStartForcePreventDefault || shouldPreventDefault) {
				e.preventDefault();
			}
		}

		swiper.emit("touchStart", e);
	}

	function onTouchMove(event) {
		var document = getDocument();
		var swiper = this;
		var data = swiper.touchEventsData;
		var params = swiper.params,
			touches = swiper.touches,
			rtl = swiper.rtlTranslate;
		var e = event;
		if (e.originalEvent) e = e.originalEvent;

		if (!data.isTouched) {
			if (data.startMoving && data.isScrolling) {
				swiper.emit("touchMoveOpposite", e);
			}

			return;
		}

		if (data.isTouchEvent && e.type !== "touchmove") return;
		var targetTouch = e.type === "touchmove" && e.targetTouches && (e.targetTouches[0] || e.changedTouches[0]);
		var pageX = e.type === "touchmove" ? targetTouch.pageX : e.pageX;
		var pageY = e.type === "touchmove" ? targetTouch.pageY : e.pageY;

		if (e.preventedByNestedSwiper) {
			touches.startX = pageX;
			touches.startY = pageY;
			return;
		}

		if (!swiper.allowTouchMove) {
			// isMoved = true;
			swiper.allowClick = false;

			if (data.isTouched) {
				extend$1(touches, {
					startX: pageX,
					startY: pageY,
					currentX: pageX,
					currentY: pageY,
				});
				data.touchStartTime = now();
			}

			return;
		}

		if (data.isTouchEvent && params.touchReleaseOnEdges && !params.loop) {
			if (swiper.isVertical()) {
				// Vertical
				if ((pageY < touches.startY && swiper.translate <= swiper.maxTranslate()) || (pageY > touches.startY && swiper.translate >= swiper.minTranslate())) {
					data.isTouched = false;
					data.isMoved = false;
					return;
				}
			} else if ((pageX < touches.startX && swiper.translate <= swiper.maxTranslate()) || (pageX > touches.startX && swiper.translate >= swiper.minTranslate())) {
				return;
			}
		}

		if (data.isTouchEvent && document.activeElement) {
			if (e.target === document.activeElement && $(e.target).is(data.formElements)) {
				data.isMoved = true;
				swiper.allowClick = false;
				return;
			}
		}

		if (data.allowTouchCallbacks) {
			swiper.emit("touchMove", e);
		}

		if (e.targetTouches && e.targetTouches.length > 1) return;
		touches.currentX = pageX;
		touches.currentY = pageY;
		var diffX = touches.currentX - touches.startX;
		var diffY = touches.currentY - touches.startY;
		if (swiper.params.threshold && Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2)) < swiper.params.threshold) return;

		if (typeof data.isScrolling === "undefined") {
			var touchAngle;

			if ((swiper.isHorizontal() && touches.currentY === touches.startY) || (swiper.isVertical() && touches.currentX === touches.startX)) {
				data.isScrolling = false;
			} else {
				// eslint-disable-next-line
				if (diffX * diffX + diffY * diffY >= 25) {
					touchAngle = (Math.atan2(Math.abs(diffY), Math.abs(diffX)) * 180) / Math.PI;
					data.isScrolling = swiper.isHorizontal() ? touchAngle > params.touchAngle : 90 - touchAngle > params.touchAngle;
				}
			}
		}

		if (data.isScrolling) {
			swiper.emit("touchMoveOpposite", e);
		}

		if (typeof data.startMoving === "undefined") {
			if (touches.currentX !== touches.startX || touches.currentY !== touches.startY) {
				data.startMoving = true;
			}
		}

		if (data.isScrolling) {
			data.isTouched = false;
			return;
		}

		if (!data.startMoving) {
			return;
		}

		swiper.allowClick = false;

		if (!params.cssMode && e.cancelable) {
			e.preventDefault();
		}

		if (params.touchMoveStopPropagation && !params.nested) {
			e.stopPropagation();
		}

		if (!data.isMoved) {
			if (params.loop) {
				swiper.loopFix();
			}

			data.startTranslate = swiper.getTranslate();
			swiper.setTransition(0);

			if (swiper.animating) {
				swiper.$wrapperEl.trigger("webkitTransitionEnd transitionend");
			}

			data.allowMomentumBounce = false; // Grab Cursor

			if (params.grabCursor && (swiper.allowSlideNext === true || swiper.allowSlidePrev === true)) {
				swiper.setGrabCursor(true);
			}

			swiper.emit("sliderFirstMove", e);
		}

		swiper.emit("sliderMove", e);
		data.isMoved = true;
		var diff = swiper.isHorizontal() ? diffX : diffY;
		touches.diff = diff;
		diff *= params.touchRatio;
		if (rtl) diff = -diff;
		swiper.swipeDirection = diff > 0 ? "prev" : "next";
		data.currentTranslate = diff + data.startTranslate;
		var disableParentSwiper = true;
		var resistanceRatio = params.resistanceRatio;

		if (params.touchReleaseOnEdges) {
			resistanceRatio = 0;
		}

		if (diff > 0 && data.currentTranslate > swiper.minTranslate()) {
			disableParentSwiper = false;
			if (params.resistance) data.currentTranslate = swiper.minTranslate() - 1 + Math.pow(-swiper.minTranslate() + data.startTranslate + diff, resistanceRatio);
		} else if (diff < 0 && data.currentTranslate < swiper.maxTranslate()) {
			disableParentSwiper = false;
			if (params.resistance) data.currentTranslate = swiper.maxTranslate() + 1 - Math.pow(swiper.maxTranslate() - data.startTranslate - diff, resistanceRatio);
		}

		if (disableParentSwiper) {
			e.preventedByNestedSwiper = true;
		} // Directions locks

		if (!swiper.allowSlideNext && swiper.swipeDirection === "next" && data.currentTranslate < data.startTranslate) {
			data.currentTranslate = data.startTranslate;
		}

		if (!swiper.allowSlidePrev && swiper.swipeDirection === "prev" && data.currentTranslate > data.startTranslate) {
			data.currentTranslate = data.startTranslate;
		} // Threshold

		if (params.threshold > 0) {
			if (Math.abs(diff) > params.threshold || data.allowThresholdMove) {
				if (!data.allowThresholdMove) {
					data.allowThresholdMove = true;
					touches.startX = touches.currentX;
					touches.startY = touches.currentY;
					data.currentTranslate = data.startTranslate;
					touches.diff = swiper.isHorizontal() ? touches.currentX - touches.startX : touches.currentY - touches.startY;
					return;
				}
			} else {
				data.currentTranslate = data.startTranslate;
				return;
			}
		}

		if (!params.followFinger || params.cssMode) return; // Update active index in free mode

		if (params.freeMode || params.watchSlidesProgress || params.watchSlidesVisibility) {
			swiper.updateActiveIndex();
			swiper.updateSlidesClasses();
		}

		if (params.freeMode) {
			// Velocity
			if (data.velocities.length === 0) {
				data.velocities.push({
					position: touches[swiper.isHorizontal() ? "startX" : "startY"],
					time: data.touchStartTime,
				});
			}

			data.velocities.push({
				position: touches[swiper.isHorizontal() ? "currentX" : "currentY"],
				time: now(),
			});
		} // Update progress

		swiper.updateProgress(data.currentTranslate); // Update translate

		swiper.setTranslate(data.currentTranslate);
	}

	function onTouchEnd(event) {
		var swiper = this;
		var data = swiper.touchEventsData;
		var params = swiper.params,
			touches = swiper.touches,
			rtl = swiper.rtlTranslate,
			$wrapperEl = swiper.$wrapperEl,
			slidesGrid = swiper.slidesGrid,
			snapGrid = swiper.snapGrid;
		var e = event;
		if (e.originalEvent) e = e.originalEvent;

		if (data.allowTouchCallbacks) {
			swiper.emit("touchEnd", e);
		}

		data.allowTouchCallbacks = false;

		if (!data.isTouched) {
			if (data.isMoved && params.grabCursor) {
				swiper.setGrabCursor(false);
			}

			data.isMoved = false;
			data.startMoving = false;
			return;
		} // Return Grab Cursor

		if (params.grabCursor && data.isMoved && data.isTouched && (swiper.allowSlideNext === true || swiper.allowSlidePrev === true)) {
			swiper.setGrabCursor(false);
		} // Time diff

		var touchEndTime = now();
		var timeDiff = touchEndTime - data.touchStartTime; // Tap, doubleTap, Click

		if (swiper.allowClick) {
			swiper.updateClickedSlide(e);
			swiper.emit("tap click", e);

			if (timeDiff < 300 && touchEndTime - data.lastClickTime < 300) {
				swiper.emit("doubleTap doubleClick", e);
			}
		}

		data.lastClickTime = now();
		nextTick(function () {
			if (!swiper.destroyed) swiper.allowClick = true;
		});

		if (!data.isTouched || !data.isMoved || !swiper.swipeDirection || touches.diff === 0 || data.currentTranslate === data.startTranslate) {
			data.isTouched = false;
			data.isMoved = false;
			data.startMoving = false;
			return;
		}

		data.isTouched = false;
		data.isMoved = false;
		data.startMoving = false;
		var currentPos;

		if (params.followFinger) {
			currentPos = rtl ? swiper.translate : -swiper.translate;
		} else {
			currentPos = -data.currentTranslate;
		}

		if (params.cssMode) {
			return;
		}

		if (params.freeMode) {
			if (currentPos < -swiper.minTranslate()) {
				swiper.slideTo(swiper.activeIndex);
				return;
			}

			if (currentPos > -swiper.maxTranslate()) {
				if (swiper.slides.length < snapGrid.length) {
					swiper.slideTo(snapGrid.length - 1);
				} else {
					swiper.slideTo(swiper.slides.length - 1);
				}

				return;
			}

			if (params.freeModeMomentum) {
				if (data.velocities.length > 1) {
					var lastMoveEvent = data.velocities.pop();
					var velocityEvent = data.velocities.pop();
					var distance = lastMoveEvent.position - velocityEvent.position;
					var time = lastMoveEvent.time - velocityEvent.time;
					swiper.velocity = distance / time;
					swiper.velocity /= 2;

					if (Math.abs(swiper.velocity) < params.freeModeMinimumVelocity) {
						swiper.velocity = 0;
					} // this implies that the user stopped moving a finger then released.
					// There would be no events with distance zero, so the last event is stale.

					if (time > 150 || now() - lastMoveEvent.time > 300) {
						swiper.velocity = 0;
					}
				} else {
					swiper.velocity = 0;
				}

				swiper.velocity *= params.freeModeMomentumVelocityRatio;
				data.velocities.length = 0;
				var momentumDuration = 1000 * params.freeModeMomentumRatio;
				var momentumDistance = swiper.velocity * momentumDuration;
				var newPosition = swiper.translate + momentumDistance;
				if (rtl) newPosition = -newPosition;
				var doBounce = false;
				var afterBouncePosition;
				var bounceAmount = Math.abs(swiper.velocity) * 20 * params.freeModeMomentumBounceRatio;
				var needsLoopFix;

				if (newPosition < swiper.maxTranslate()) {
					if (params.freeModeMomentumBounce) {
						if (newPosition + swiper.maxTranslate() < -bounceAmount) {
							newPosition = swiper.maxTranslate() - bounceAmount;
						}

						afterBouncePosition = swiper.maxTranslate();
						doBounce = true;
						data.allowMomentumBounce = true;
					} else {
						newPosition = swiper.maxTranslate();
					}

					if (params.loop && params.centeredSlides) needsLoopFix = true;
				} else if (newPosition > swiper.minTranslate()) {
					if (params.freeModeMomentumBounce) {
						if (newPosition - swiper.minTranslate() > bounceAmount) {
							newPosition = swiper.minTranslate() + bounceAmount;
						}

						afterBouncePosition = swiper.minTranslate();
						doBounce = true;
						data.allowMomentumBounce = true;
					} else {
						newPosition = swiper.minTranslate();
					}

					if (params.loop && params.centeredSlides) needsLoopFix = true;
				} else if (params.freeModeSticky) {
					var nextSlide;

					for (var j = 0; j < snapGrid.length; j += 1) {
						if (snapGrid[j] > -newPosition) {
							nextSlide = j;
							break;
						}
					}

					if (Math.abs(snapGrid[nextSlide] - newPosition) < Math.abs(snapGrid[nextSlide - 1] - newPosition) || swiper.swipeDirection === "next") {
						newPosition = snapGrid[nextSlide];
					} else {
						newPosition = snapGrid[nextSlide - 1];
					}

					newPosition = -newPosition;
				}

				if (needsLoopFix) {
					swiper.once("transitionEnd", function () {
						swiper.loopFix();
					});
				} // Fix duration

				if (swiper.velocity !== 0) {
					if (rtl) {
						momentumDuration = Math.abs((-newPosition - swiper.translate) / swiper.velocity);
					} else {
						momentumDuration = Math.abs((newPosition - swiper.translate) / swiper.velocity);
					}

					if (params.freeModeSticky) {
						// If freeModeSticky is active and the user ends a swipe with a slow-velocity
						// event, then durations can be 20+ seconds to slide one (or zero!) slides.
						// It's easy to see this when simulating touch with mouse events. To fix this,
						// limit single-slide swipes to the default slide duration. This also has the
						// nice side effect of matching slide speed if the user stopped moving before
						// lifting finger or mouse vs. moving slowly before lifting the finger/mouse.
						// For faster swipes, also apply limits (albeit higher ones).
						var moveDistance = Math.abs((rtl ? -newPosition : newPosition) - swiper.translate);
						var currentSlideSize = swiper.slidesSizesGrid[swiper.activeIndex];

						if (moveDistance < currentSlideSize) {
							momentumDuration = params.speed;
						} else if (moveDistance < 2 * currentSlideSize) {
							momentumDuration = params.speed * 1.5;
						} else {
							momentumDuration = params.speed * 2.5;
						}
					}
				} else if (params.freeModeSticky) {
					swiper.slideToClosest();
					return;
				}

				if (params.freeModeMomentumBounce && doBounce) {
					swiper.updateProgress(afterBouncePosition);
					swiper.setTransition(momentumDuration);
					swiper.setTranslate(newPosition);
					swiper.transitionStart(true, swiper.swipeDirection);
					swiper.animating = true;
					$wrapperEl.transitionEnd(function () {
						if (!swiper || swiper.destroyed || !data.allowMomentumBounce) return;
						swiper.emit("momentumBounce");
						swiper.setTransition(params.speed);
						setTimeout(function () {
							swiper.setTranslate(afterBouncePosition);
							$wrapperEl.transitionEnd(function () {
								if (!swiper || swiper.destroyed) return;
								swiper.transitionEnd();
							});
						}, 0);
					});
				} else if (swiper.velocity) {
					swiper.updateProgress(newPosition);
					swiper.setTransition(momentumDuration);
					swiper.setTranslate(newPosition);
					swiper.transitionStart(true, swiper.swipeDirection);

					if (!swiper.animating) {
						swiper.animating = true;
						$wrapperEl.transitionEnd(function () {
							if (!swiper || swiper.destroyed) return;
							swiper.transitionEnd();
						});
					}
				} else {
					swiper.updateProgress(newPosition);
				}

				swiper.updateActiveIndex();
				swiper.updateSlidesClasses();
			} else if (params.freeModeSticky) {
				swiper.slideToClosest();
				return;
			}

			if (!params.freeModeMomentum || timeDiff >= params.longSwipesMs) {
				swiper.updateProgress();
				swiper.updateActiveIndex();
				swiper.updateSlidesClasses();
			}

			return;
		} // Find current slide

		var stopIndex = 0;
		var groupSize = swiper.slidesSizesGrid[0];

		for (var i = 0; i < slidesGrid.length; i += i < params.slidesPerGroupSkip ? 1 : params.slidesPerGroup) {
			var _increment = i < params.slidesPerGroupSkip - 1 ? 1 : params.slidesPerGroup;

			if (typeof slidesGrid[i + _increment] !== "undefined") {
				if (currentPos >= slidesGrid[i] && currentPos < slidesGrid[i + _increment]) {
					stopIndex = i;
					groupSize = slidesGrid[i + _increment] - slidesGrid[i];
				}
			} else if (currentPos >= slidesGrid[i]) {
				stopIndex = i;
				groupSize = slidesGrid[slidesGrid.length - 1] - slidesGrid[slidesGrid.length - 2];
			}
		} // Find current slide size

		var ratio = (currentPos - slidesGrid[stopIndex]) / groupSize;
		var increment = stopIndex < params.slidesPerGroupSkip - 1 ? 1 : params.slidesPerGroup;

		if (timeDiff > params.longSwipesMs) {
			// Long touches
			if (!params.longSwipes) {
				swiper.slideTo(swiper.activeIndex);
				return;
			}

			if (swiper.swipeDirection === "next") {
				if (ratio >= params.longSwipesRatio) swiper.slideTo(stopIndex + increment);
				else swiper.slideTo(stopIndex);
			}

			if (swiper.swipeDirection === "prev") {
				if (ratio > 1 - params.longSwipesRatio) swiper.slideTo(stopIndex + increment);
				else swiper.slideTo(stopIndex);
			}
		} else {
			// Short swipes
			if (!params.shortSwipes) {
				swiper.slideTo(swiper.activeIndex);
				return;
			}

			var isNavButtonTarget = swiper.navigation && (e.target === swiper.navigation.nextEl || e.target === swiper.navigation.prevEl);

			if (!isNavButtonTarget) {
				if (swiper.swipeDirection === "next") {
					swiper.slideTo(stopIndex + increment);
				}

				if (swiper.swipeDirection === "prev") {
					swiper.slideTo(stopIndex);
				}
			} else if (e.target === swiper.navigation.nextEl) {
				swiper.slideTo(stopIndex + increment);
			} else {
				swiper.slideTo(stopIndex);
			}
		}
	}

	function onResize() {
		var swiper = this;
		var params = swiper.params,
			el = swiper.el;
		if (el && el.offsetWidth === 0) return; // Breakpoints

		if (params.breakpoints) {
			swiper.setBreakpoint();
		} // Save locks

		var allowSlideNext = swiper.allowSlideNext,
			allowSlidePrev = swiper.allowSlidePrev,
			snapGrid = swiper.snapGrid; // Disable locks on resize

		swiper.allowSlideNext = true;
		swiper.allowSlidePrev = true;
		swiper.updateSize();
		swiper.updateSlides();
		swiper.updateSlidesClasses();

		if ((params.slidesPerView === "auto" || params.slidesPerView > 1) && swiper.isEnd && !swiper.isBeginning && !swiper.params.centeredSlides) {
			swiper.slideTo(swiper.slides.length - 1, 0, false, true);
		} else {
			swiper.slideTo(swiper.activeIndex, 0, false, true);
		}

		if (swiper.autoplay && swiper.autoplay.running && swiper.autoplay.paused) {
			swiper.autoplay.run();
		} // Return locks after resize

		swiper.allowSlidePrev = allowSlidePrev;
		swiper.allowSlideNext = allowSlideNext;

		if (swiper.params.watchOverflow && snapGrid !== swiper.snapGrid) {
			swiper.checkOverflow();
		}
	}

	function onClick(e) {
		var swiper = this;

		if (!swiper.allowClick) {
			if (swiper.params.preventClicks) e.preventDefault();

			if (swiper.params.preventClicksPropagation && swiper.animating) {
				e.stopPropagation();
				e.stopImmediatePropagation();
			}
		}
	}

	function onScroll() {
		var swiper = this;
		var wrapperEl = swiper.wrapperEl,
			rtlTranslate = swiper.rtlTranslate;
		swiper.previousTranslate = swiper.translate;

		if (swiper.isHorizontal()) {
			if (rtlTranslate) {
				swiper.translate = wrapperEl.scrollWidth - wrapperEl.offsetWidth - wrapperEl.scrollLeft;
			} else {
				swiper.translate = -wrapperEl.scrollLeft;
			}
		} else {
			swiper.translate = -wrapperEl.scrollTop;
		} // eslint-disable-next-line

		if (swiper.translate === -0) swiper.translate = 0;
		swiper.updateActiveIndex();
		swiper.updateSlidesClasses();
		var newProgress;
		var translatesDiff = swiper.maxTranslate() - swiper.minTranslate();

		if (translatesDiff === 0) {
			newProgress = 0;
		} else {
			newProgress = (swiper.translate - swiper.minTranslate()) / translatesDiff;
		}

		if (newProgress !== swiper.progress) {
			swiper.updateProgress(rtlTranslate ? -swiper.translate : swiper.translate);
		}

		swiper.emit("setTranslate", swiper.translate, false);
	}

	var dummyEventAttached = false;

	function dummyEventListener() {}

	function attachEvents() {
		var swiper = this;
		var document = getDocument();
		var params = swiper.params,
			touchEvents = swiper.touchEvents,
			el = swiper.el,
			wrapperEl = swiper.wrapperEl,
			device = swiper.device,
			support = swiper.support;
		swiper.onTouchStart = onTouchStart.bind(swiper);
		swiper.onTouchMove = onTouchMove.bind(swiper);
		swiper.onTouchEnd = onTouchEnd.bind(swiper);

		if (params.cssMode) {
			swiper.onScroll = onScroll.bind(swiper);
		}

		swiper.onClick = onClick.bind(swiper);
		var capture = !!params.nested; // Touch Events

		if (!support.touch && support.pointerEvents) {
			el.addEventListener(touchEvents.start, swiper.onTouchStart, false);
			document.addEventListener(touchEvents.move, swiper.onTouchMove, capture);
			document.addEventListener(touchEvents.end, swiper.onTouchEnd, false);
		} else {
			if (support.touch) {
				var passiveListener =
					touchEvents.start === "touchstart" && support.passiveListener && params.passiveListeners
						? {
								passive: true,
								capture: false,
						  }
						: false;
				el.addEventListener(touchEvents.start, swiper.onTouchStart, passiveListener);
				el.addEventListener(
					touchEvents.move,
					swiper.onTouchMove,
					support.passiveListener
						? {
								passive: false,
								capture: capture,
						  }
						: capture
				);
				el.addEventListener(touchEvents.end, swiper.onTouchEnd, passiveListener);

				if (touchEvents.cancel) {
					el.addEventListener(touchEvents.cancel, swiper.onTouchEnd, passiveListener);
				}

				if (!dummyEventAttached) {
					document.addEventListener("touchstart", dummyEventListener);
					dummyEventAttached = true;
				}
			}

			if ((params.simulateTouch && !device.ios && !device.android) || (params.simulateTouch && !support.touch && device.ios)) {
				el.addEventListener("mousedown", swiper.onTouchStart, false);
				document.addEventListener("mousemove", swiper.onTouchMove, capture);
				document.addEventListener("mouseup", swiper.onTouchEnd, false);
			}
		} // Prevent Links Clicks

		if (params.preventClicks || params.preventClicksPropagation) {
			el.addEventListener("click", swiper.onClick, true);
		}

		if (params.cssMode) {
			wrapperEl.addEventListener("scroll", swiper.onScroll);
		} // Resize handler

		if (params.updateOnWindowResize) {
			swiper.on(device.ios || device.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", onResize, true);
		} else {
			swiper.on("observerUpdate", onResize, true);
		}
	}

	function detachEvents() {
		var swiper = this;
		var document = getDocument();
		var params = swiper.params,
			touchEvents = swiper.touchEvents,
			el = swiper.el,
			wrapperEl = swiper.wrapperEl,
			device = swiper.device,
			support = swiper.support;
		var capture = !!params.nested; // Touch Events

		if (!support.touch && support.pointerEvents) {
			el.removeEventListener(touchEvents.start, swiper.onTouchStart, false);
			document.removeEventListener(touchEvents.move, swiper.onTouchMove, capture);
			document.removeEventListener(touchEvents.end, swiper.onTouchEnd, false);
		} else {
			if (support.touch) {
				var passiveListener =
					touchEvents.start === "onTouchStart" && support.passiveListener && params.passiveListeners
						? {
								passive: true,
								capture: false,
						  }
						: false;
				el.removeEventListener(touchEvents.start, swiper.onTouchStart, passiveListener);
				el.removeEventListener(touchEvents.move, swiper.onTouchMove, capture);
				el.removeEventListener(touchEvents.end, swiper.onTouchEnd, passiveListener);

				if (touchEvents.cancel) {
					el.removeEventListener(touchEvents.cancel, swiper.onTouchEnd, passiveListener);
				}
			}

			if ((params.simulateTouch && !device.ios && !device.android) || (params.simulateTouch && !support.touch && device.ios)) {
				el.removeEventListener("mousedown", swiper.onTouchStart, false);
				document.removeEventListener("mousemove", swiper.onTouchMove, capture);
				document.removeEventListener("mouseup", swiper.onTouchEnd, false);
			}
		} // Prevent Links Clicks

		if (params.preventClicks || params.preventClicksPropagation) {
			el.removeEventListener("click", swiper.onClick, true);
		}

		if (params.cssMode) {
			wrapperEl.removeEventListener("scroll", swiper.onScroll);
		} // Resize handler

		swiper.off(device.ios || device.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", onResize);
	}

	var events = {
		attachEvents: attachEvents,
		detachEvents: detachEvents,
	};

	function setBreakpoint() {
		var swiper = this;
		var activeIndex = swiper.activeIndex,
			initialized = swiper.initialized,
			_swiper$loopedSlides = swiper.loopedSlides,
			loopedSlides = _swiper$loopedSlides === void 0 ? 0 : _swiper$loopedSlides,
			params = swiper.params,
			$el = swiper.$el;
		var breakpoints = params.breakpoints;
		if (!breakpoints || (breakpoints && Object.keys(breakpoints).length === 0)) return; // Get breakpoint for window width and update parameters

		var breakpoint = swiper.getBreakpoint(breakpoints);

		if (breakpoint && swiper.currentBreakpoint !== breakpoint) {
			var breakpointOnlyParams = breakpoint in breakpoints ? breakpoints[breakpoint] : undefined;

			if (breakpointOnlyParams) {
				["slidesPerView", "spaceBetween", "slidesPerGroup", "slidesPerGroupSkip", "slidesPerColumn"].forEach(function (param) {
					var paramValue = breakpointOnlyParams[param];
					if (typeof paramValue === "undefined") return;

					if (param === "slidesPerView" && (paramValue === "AUTO" || paramValue === "auto")) {
						breakpointOnlyParams[param] = "auto";
					} else if (param === "slidesPerView") {
						breakpointOnlyParams[param] = parseFloat(paramValue);
					} else {
						breakpointOnlyParams[param] = parseInt(paramValue, 10);
					}
				});
			}

			var breakpointParams = breakpointOnlyParams || swiper.originalParams;
			var wasMultiRow = params.slidesPerColumn > 1;
			var isMultiRow = breakpointParams.slidesPerColumn > 1;

			if (wasMultiRow && !isMultiRow) {
				$el.removeClass(params.containerModifierClass + "multirow " + params.containerModifierClass + "multirow-column");
				swiper.emitContainerClasses();
			} else if (!wasMultiRow && isMultiRow) {
				$el.addClass(params.containerModifierClass + "multirow");

				if (breakpointParams.slidesPerColumnFill === "column") {
					$el.addClass(params.containerModifierClass + "multirow-column");
				}

				swiper.emitContainerClasses();
			}

			var directionChanged = breakpointParams.direction && breakpointParams.direction !== params.direction;
			var needsReLoop = params.loop && (breakpointParams.slidesPerView !== params.slidesPerView || directionChanged);

			if (directionChanged && initialized) {
				swiper.changeDirection();
			}

			extend$1(swiper.params, breakpointParams);
			extend$1(swiper, {
				allowTouchMove: swiper.params.allowTouchMove,
				allowSlideNext: swiper.params.allowSlideNext,
				allowSlidePrev: swiper.params.allowSlidePrev,
			});
			swiper.currentBreakpoint = breakpoint;
			swiper.emit("_beforeBreakpoint", breakpointParams);

			if (needsReLoop && initialized) {
				swiper.loopDestroy();
				swiper.loopCreate();
				swiper.updateSlides();
				swiper.slideTo(activeIndex - loopedSlides + swiper.loopedSlides, 0, false);
			}

			swiper.emit("breakpoint", breakpointParams);
		}
	}

	function getBreakpoints(breakpoints) {
		var window = getWindow(); // Get breakpoint for window width

		if (!breakpoints) return undefined;
		var breakpoint = false;
		var points = Object.keys(breakpoints).map(function (point) {
			if (typeof point === "string" && point.indexOf("@") === 0) {
				var minRatio = parseFloat(point.substr(1));
				var value = window.innerHeight * minRatio;
				return {
					value: value,
					point: point,
				};
			}

			return {
				value: point,
				point: point,
			};
		});
		points.sort(function (a, b) {
			return parseInt(a.value, 10) - parseInt(b.value, 10);
		});

		for (var i = 0; i < points.length; i += 1) {
			var _points$i = points[i],
				point = _points$i.point,
				value = _points$i.value;

			if (value <= window.innerWidth) {
				breakpoint = point;
			}
		}

		return breakpoint || "max";
	}

	var breakpoints = {
		setBreakpoint: setBreakpoint,
		getBreakpoint: getBreakpoints,
	};

	function addClasses() {
		var swiper = this;
		var classNames = swiper.classNames,
			params = swiper.params,
			rtl = swiper.rtl,
			$el = swiper.$el,
			device = swiper.device;
		var suffixes = [];
		suffixes.push("initialized");
		suffixes.push(params.direction);

		if (params.freeMode) {
			suffixes.push("free-mode");
		}

		if (params.autoHeight) {
			suffixes.push("autoheight");
		}

		if (rtl) {
			suffixes.push("rtl");
		}

		if (params.slidesPerColumn > 1) {
			suffixes.push("multirow");

			if (params.slidesPerColumnFill === "column") {
				suffixes.push("multirow-column");
			}
		}

		if (device.android) {
			suffixes.push("android");
		}

		if (device.ios) {
			suffixes.push("ios");
		}

		if (params.cssMode) {
			suffixes.push("css-mode");
		}

		suffixes.forEach(function (suffix) {
			classNames.push(params.containerModifierClass + suffix);
		});
		$el.addClass(classNames.join(" "));
		swiper.emitContainerClasses();
	}

	function removeClasses() {
		var swiper = this;
		var $el = swiper.$el,
			classNames = swiper.classNames;
		$el.removeClass(classNames.join(" "));
		swiper.emitContainerClasses();
	}

	var classes = {
		addClasses: addClasses,
		removeClasses: removeClasses,
	};

	function loadImage(imageEl, src, srcset, sizes, checkForComplete, callback) {
		var window = getWindow();
		var image;

		function onReady() {
			if (callback) callback();
		}

		var isPicture = $(imageEl).parent("picture")[0];

		if (!isPicture && (!imageEl.complete || !checkForComplete)) {
			if (src) {
				image = new window.Image();
				image.onload = onReady;
				image.onerror = onReady;

				if (sizes) {
					image.sizes = sizes;
				}

				if (srcset) {
					image.srcset = srcset;
				}

				if (src) {
					image.src = src;
				}
			} else {
				onReady();
			}
		} else {
			// image already loaded...
			onReady();
		}
	}

	function preloadImages() {
		var swiper = this;
		swiper.imagesToLoad = swiper.$el.find("img");

		function onReady() {
			if (typeof swiper === "undefined" || swiper === null || !swiper || swiper.destroyed) return;
			if (swiper.imagesLoaded !== undefined) swiper.imagesLoaded += 1;

			if (swiper.imagesLoaded === swiper.imagesToLoad.length) {
				if (swiper.params.updateOnImagesReady) swiper.update();
				swiper.emit("imagesReady");
			}
		}

		for (var i = 0; i < swiper.imagesToLoad.length; i += 1) {
			var imageEl = swiper.imagesToLoad[i];
			swiper.loadImage(imageEl, imageEl.currentSrc || imageEl.getAttribute("src"), imageEl.srcset || imageEl.getAttribute("srcset"), imageEl.sizes || imageEl.getAttribute("sizes"), true, onReady);
		}
	}

	var images = {
		loadImage: loadImage,
		preloadImages: preloadImages,
	};

	function checkOverflow() {
		var swiper = this;
		var params = swiper.params;
		var wasLocked = swiper.isLocked;
		var lastSlidePosition = swiper.slides.length > 0 && params.slidesOffsetBefore + params.spaceBetween * (swiper.slides.length - 1) + swiper.slides[0].offsetWidth * swiper.slides.length;

		if (params.slidesOffsetBefore && params.slidesOffsetAfter && lastSlidePosition) {
			swiper.isLocked = lastSlidePosition <= swiper.size;
		} else {
			swiper.isLocked = swiper.snapGrid.length === 1;
		}

		swiper.allowSlideNext = !swiper.isLocked;
		swiper.allowSlidePrev = !swiper.isLocked; // events

		if (wasLocked !== swiper.isLocked) swiper.emit(swiper.isLocked ? "lock" : "unlock");

		if (wasLocked && wasLocked !== swiper.isLocked) {
			swiper.isEnd = false;
			if (swiper.navigation) swiper.navigation.update();
		}
	}

	var checkOverflow$1 = {
		checkOverflow: checkOverflow,
	};

	var defaults = {
		init: true,
		direction: "horizontal",
		touchEventsTarget: "container",
		initialSlide: 0,
		speed: 300,
		cssMode: false,
		updateOnWindowResize: true,
		nested: false,
		// Overrides
		width: null,
		height: null,
		//
		preventInteractionOnTransition: false,
		// ssr
		userAgent: null,
		url: null,
		// To support iOS's swipe-to-go-back gesture (when being used in-app).
		edgeSwipeDetection: false,
		edgeSwipeThreshold: 20,
		// Free mode
		freeMode: false,
		freeModeMomentum: true,
		freeModeMomentumRatio: 1,
		freeModeMomentumBounce: true,
		freeModeMomentumBounceRatio: 1,
		freeModeMomentumVelocityRatio: 1,
		freeModeSticky: false,
		freeModeMinimumVelocity: 0.02,
		// Autoheight
		autoHeight: false,
		// Set wrapper width
		setWrapperSize: false,
		// Virtual Translate
		virtualTranslate: false,
		// Effects
		effect: "slide",
		// 'slide' or 'fade' or 'cube' or 'coverflow' or 'flip'
		// Breakpoints
		breakpoints: undefined,
		// Slides grid
		spaceBetween: 0,
		slidesPerView: 1,
		slidesPerColumn: 1,
		slidesPerColumnFill: "column",
		slidesPerGroup: 1,
		slidesPerGroupSkip: 0,
		centeredSlides: false,
		centeredSlidesBounds: false,
		slidesOffsetBefore: 0,
		// in px
		slidesOffsetAfter: 0,
		// in px
		normalizeSlideIndex: true,
		centerInsufficientSlides: false,
		// Disable swiper and hide navigation when container not overflow
		watchOverflow: false,
		// Round length
		roundLengths: false,
		// Touches
		touchRatio: 1,
		touchAngle: 45,
		simulateTouch: true,
		shortSwipes: true,
		longSwipes: true,
		longSwipesRatio: 0.5,
		longSwipesMs: 300,
		followFinger: true,
		allowTouchMove: true,
		threshold: 0,
		touchMoveStopPropagation: false,
		touchStartPreventDefault: true,
		touchStartForcePreventDefault: false,
		touchReleaseOnEdges: false,
		// Unique Navigation Elements
		uniqueNavElements: true,
		// Resistance
		resistance: true,
		resistanceRatio: 0.85,
		// Progress
		watchSlidesProgress: false,
		watchSlidesVisibility: false,
		// Cursor
		grabCursor: false,
		// Clicks
		preventClicks: true,
		preventClicksPropagation: true,
		slideToClickedSlide: false,
		// Images
		preloadImages: true,
		updateOnImagesReady: true,
		// loop
		loop: false,
		loopAdditionalSlides: 0,
		loopedSlides: null,
		loopFillGroupWithBlank: false,
		loopPreventsSlide: true,
		// Swiping/no swiping
		allowSlidePrev: true,
		allowSlideNext: true,
		swipeHandler: null,
		// '.swipe-handler',
		noSwiping: true,
		noSwipingClass: "swiper-no-swiping",
		noSwipingSelector: null,
		// Passive Listeners
		passiveListeners: true,
		// NS
		containerModifierClass: "swiper-container-",
		// NEW
		slideClass: "swiper-slide",
		slideBlankClass: "swiper-slide-invisible-blank",
		slideActiveClass: "swiper-slide-active",
		slideDuplicateActiveClass: "swiper-slide-duplicate-active",
		slideVisibleClass: "swiper-slide-visible",
		slideDuplicateClass: "swiper-slide-duplicate",
		slideNextClass: "swiper-slide-next",
		slideDuplicateNextClass: "swiper-slide-duplicate-next",
		slidePrevClass: "swiper-slide-prev",
		slideDuplicatePrevClass: "swiper-slide-duplicate-prev",
		wrapperClass: "swiper-wrapper",
		// Callbacks
		runCallbacksOnInit: true,
		// Internals
		_emitClasses: false,
	};

	var prototypes = {
		modular: modular,
		eventsEmitter: eventsEmitter,
		update: update,
		translate: translate,
		transition: transition$1,
		slide: slide,
		loop: loop,
		grabCursor: grabCursor,
		manipulation: manipulation,
		events: events,
		breakpoints: breakpoints,
		checkOverflow: checkOverflow$1,
		classes: classes,
		images: images,
	};
	var extendedDefaults = {};

	var Swiper = /*#__PURE__*/ (function () {
		function Swiper() {
			var el;
			var params;

			for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
				args[_key] = arguments[_key];
			}

			if (args.length === 1 && args[0].constructor && args[0].constructor === Object) {
				params = args[0];
			} else {
				el = args[0];
				params = args[1];
			}

			if (!params) params = {};
			params = extend$1({}, params);
			if (el && !params.el) params.el = el; // Swiper Instance

			var swiper = this;
			swiper.support = getSupport();
			swiper.device = getDevice({
				userAgent: params.userAgent,
			});
			swiper.browser = getBrowser();
			swiper.eventsListeners = {};
			swiper.eventsAnyListeners = [];

			if (typeof swiper.modules === "undefined") {
				swiper.modules = {};
			}

			Object.keys(swiper.modules).forEach(function (moduleName) {
				var module = swiper.modules[moduleName];

				if (module.params) {
					var moduleParamName = Object.keys(module.params)[0];
					var moduleParams = module.params[moduleParamName];
					if (typeof moduleParams !== "object" || moduleParams === null) return;
					if (!(moduleParamName in params && "enabled" in moduleParams)) return;

					if (params[moduleParamName] === true) {
						params[moduleParamName] = {
							enabled: true,
						};
					}

					if (typeof params[moduleParamName] === "object" && !("enabled" in params[moduleParamName])) {
						params[moduleParamName].enabled = true;
					}

					if (!params[moduleParamName])
						params[moduleParamName] = {
							enabled: false,
						};
				}
			}); // Extend defaults with modules params

			var swiperParams = extend$1({}, defaults);
			swiper.useParams(swiperParams); // Extend defaults with passed params

			swiper.params = extend$1({}, swiperParams, extendedDefaults, params);
			swiper.originalParams = extend$1({}, swiper.params);
			swiper.passedParams = extend$1({}, params); // add event listeners

			if (swiper.params && swiper.params.on) {
				Object.keys(swiper.params.on).forEach(function (eventName) {
					swiper.on(eventName, swiper.params.on[eventName]);
				});
			}

			if (swiper.params && swiper.params.onAny) {
				swiper.onAny(swiper.params.onAny);
			} // Save Dom lib

			swiper.$ = $; // Find el

			var $el = $(swiper.params.el);
			el = $el[0];

			if (!el) {
				return undefined;
			}

			if ($el.length > 1) {
				var swipers = [];
				$el.each(function (containerEl) {
					var newParams = extend$1({}, params, {
						el: containerEl,
					});
					swipers.push(new Swiper(newParams));
				});
				return swipers;
			}

			el.swiper = swiper; // Find Wrapper

			var $wrapperEl;

			if (el && el.shadowRoot && el.shadowRoot.querySelector) {
				$wrapperEl = $(el.shadowRoot.querySelector("." + swiper.params.wrapperClass)); // Children needs to return slot items

				$wrapperEl.children = function (options) {
					return $el.children(options);
				};
			} else {
				$wrapperEl = $el.children("." + swiper.params.wrapperClass);
			} // Extend Swiper

			extend$1(swiper, {
				$el: $el,
				el: el,
				$wrapperEl: $wrapperEl,
				wrapperEl: $wrapperEl[0],
				// Classes
				classNames: [],
				// Slides
				slides: $(),
				slidesGrid: [],
				snapGrid: [],
				slidesSizesGrid: [],
				// isDirection
				isHorizontal: function isHorizontal() {
					return swiper.params.direction === "horizontal";
				},
				isVertical: function isVertical() {
					return swiper.params.direction === "vertical";
				},
				// RTL
				rtl: el.dir.toLowerCase() === "rtl" || $el.css("direction") === "rtl",
				rtlTranslate: swiper.params.direction === "horizontal" && (el.dir.toLowerCase() === "rtl" || $el.css("direction") === "rtl"),
				wrongRTL: $wrapperEl.css("display") === "-webkit-box",
				// Indexes
				activeIndex: 0,
				realIndex: 0,
				//
				isBeginning: true,
				isEnd: false,
				// Props
				translate: 0,
				previousTranslate: 0,
				progress: 0,
				velocity: 0,
				animating: false,
				// Locks
				allowSlideNext: swiper.params.allowSlideNext,
				allowSlidePrev: swiper.params.allowSlidePrev,
				// Touch Events
				touchEvents: (function touchEvents() {
					var touch = ["touchstart", "touchmove", "touchend", "touchcancel"];
					var desktop = ["mousedown", "mousemove", "mouseup"];

					if (swiper.support.pointerEvents) {
						desktop = ["pointerdown", "pointermove", "pointerup"];
					}

					swiper.touchEventsTouch = {
						start: touch[0],
						move: touch[1],
						end: touch[2],
						cancel: touch[3],
					};
					swiper.touchEventsDesktop = {
						start: desktop[0],
						move: desktop[1],
						end: desktop[2],
					};
					return swiper.support.touch || !swiper.params.simulateTouch ? swiper.touchEventsTouch : swiper.touchEventsDesktop;
				})(),
				touchEventsData: {
					isTouched: undefined,
					isMoved: undefined,
					allowTouchCallbacks: undefined,
					touchStartTime: undefined,
					isScrolling: undefined,
					currentTranslate: undefined,
					startTranslate: undefined,
					allowThresholdMove: undefined,
					// Form elements to match
					formElements: "input, select, option, textarea, button, video, label",
					// Last click time
					lastClickTime: now(),
					clickTimeout: undefined,
					// Velocities
					velocities: [],
					allowMomentumBounce: undefined,
					isTouchEvent: undefined,
					startMoving: undefined,
				},
				// Clicks
				allowClick: true,
				// Touches
				allowTouchMove: swiper.params.allowTouchMove,
				touches: {
					startX: 0,
					startY: 0,
					currentX: 0,
					currentY: 0,
					diff: 0,
				},
				// Images
				imagesToLoad: [],
				imagesLoaded: 0,
			}); // Install Modules

			swiper.useModules();
			swiper.emit("_swiper"); // Init

			if (swiper.params.init) {
				swiper.init();
			} // Return app instance

			return swiper;
		}

		var _proto = Swiper.prototype;

		_proto.emitContainerClasses = function emitContainerClasses() {
			var swiper = this;
			if (!swiper.params._emitClasses || !swiper.el) return;
			var classes = swiper.el.className.split(" ").filter(function (className) {
				return className.indexOf("swiper-container") === 0 || className.indexOf(swiper.params.containerModifierClass) === 0;
			});
			swiper.emit("_containerClasses", classes.join(" "));
		};

		_proto.getSlideClasses = function getSlideClasses(slideEl) {
			var swiper = this;
			return slideEl.className
				.split(" ")
				.filter(function (className) {
					return className.indexOf("swiper-slide") === 0 || className.indexOf(swiper.params.slideClass) === 0;
				})
				.join(" ");
		};

		_proto.emitSlidesClasses = function emitSlidesClasses() {
			var swiper = this;
			if (!swiper.params._emitClasses || !swiper.el) return;
			swiper.slides.each(function (slideEl) {
				var classNames = swiper.getSlideClasses(slideEl);
				swiper.emit("_slideClass", slideEl, classNames);
			});
		};

		_proto.slidesPerViewDynamic = function slidesPerViewDynamic() {
			var swiper = this;
			var params = swiper.params,
				slides = swiper.slides,
				slidesGrid = swiper.slidesGrid,
				swiperSize = swiper.size,
				activeIndex = swiper.activeIndex;
			var spv = 1;

			if (params.centeredSlides) {
				var slideSize = slides[activeIndex].swiperSlideSize;
				var breakLoop;

				for (var i = activeIndex + 1; i < slides.length; i += 1) {
					if (slides[i] && !breakLoop) {
						slideSize += slides[i].swiperSlideSize;
						spv += 1;
						if (slideSize > swiperSize) breakLoop = true;
					}
				}

				for (var _i = activeIndex - 1; _i >= 0; _i -= 1) {
					if (slides[_i] && !breakLoop) {
						slideSize += slides[_i].swiperSlideSize;
						spv += 1;
						if (slideSize > swiperSize) breakLoop = true;
					}
				}
			} else {
				for (var _i2 = activeIndex + 1; _i2 < slides.length; _i2 += 1) {
					if (slidesGrid[_i2] - slidesGrid[activeIndex] < swiperSize) {
						spv += 1;
					}
				}
			}

			return spv;
		};

		_proto.update = function update() {
			var swiper = this;
			if (!swiper || swiper.destroyed) return;
			var snapGrid = swiper.snapGrid,
				params = swiper.params; // Breakpoints

			if (params.breakpoints) {
				swiper.setBreakpoint();
			}

			swiper.updateSize();
			swiper.updateSlides();
			swiper.updateProgress();
			swiper.updateSlidesClasses();

			function setTranslate() {
				var translateValue = swiper.rtlTranslate ? swiper.translate * -1 : swiper.translate;
				var newTranslate = Math.min(Math.max(translateValue, swiper.maxTranslate()), swiper.minTranslate());
				swiper.setTranslate(newTranslate);
				swiper.updateActiveIndex();
				swiper.updateSlidesClasses();
			}

			var translated;

			if (swiper.params.freeMode) {
				setTranslate();

				if (swiper.params.autoHeight) {
					swiper.updateAutoHeight();
				}
			} else {
				if ((swiper.params.slidesPerView === "auto" || swiper.params.slidesPerView > 1) && swiper.isEnd && !swiper.params.centeredSlides) {
					translated = swiper.slideTo(swiper.slides.length - 1, 0, false, true);
				} else {
					translated = swiper.slideTo(swiper.activeIndex, 0, false, true);
				}

				if (!translated) {
					setTranslate();
				}
			}

			if (params.watchOverflow && snapGrid !== swiper.snapGrid) {
				swiper.checkOverflow();
			}

			swiper.emit("update");
		};

		_proto.changeDirection = function changeDirection(newDirection, needUpdate) {
			if (needUpdate === void 0) {
				needUpdate = true;
			}

			var swiper = this;
			var currentDirection = swiper.params.direction;

			if (!newDirection) {
				// eslint-disable-next-line
				newDirection = currentDirection === "horizontal" ? "vertical" : "horizontal";
			}

			if (newDirection === currentDirection || (newDirection !== "horizontal" && newDirection !== "vertical")) {
				return swiper;
			}

			swiper.$el.removeClass("" + swiper.params.containerModifierClass + currentDirection).addClass("" + swiper.params.containerModifierClass + newDirection);
			swiper.emitContainerClasses();
			swiper.params.direction = newDirection;
			swiper.slides.each(function (slideEl) {
				if (newDirection === "vertical") {
					slideEl.style.width = "";
				} else {
					slideEl.style.height = "";
				}
			});
			swiper.emit("changeDirection");
			if (needUpdate) swiper.update();
			return swiper;
		};

		_proto.init = function init() {
			var swiper = this;
			if (swiper.initialized) return;
			swiper.emit("beforeInit"); // Set breakpoint

			if (swiper.params.breakpoints) {
				swiper.setBreakpoint();
			} // Add Classes

			swiper.addClasses(); // Create loop

			if (swiper.params.loop) {
				swiper.loopCreate();
			} // Update size

			swiper.updateSize(); // Update slides

			swiper.updateSlides();

			if (swiper.params.watchOverflow) {
				swiper.checkOverflow();
			} // Set Grab Cursor

			if (swiper.params.grabCursor) {
				swiper.setGrabCursor();
			}

			if (swiper.params.preloadImages) {
				swiper.preloadImages();
			} // Slide To Initial Slide

			if (swiper.params.loop) {
				swiper.slideTo(swiper.params.initialSlide + swiper.loopedSlides, 0, swiper.params.runCallbacksOnInit);
			} else {
				swiper.slideTo(swiper.params.initialSlide, 0, swiper.params.runCallbacksOnInit);
			} // Attach events

			swiper.attachEvents(); // Init Flag

			swiper.initialized = true; // Emit

			swiper.emit("init");
			swiper.emit("afterInit");
		};

		_proto.destroy = function destroy(deleteInstance, cleanStyles) {
			if (deleteInstance === void 0) {
				deleteInstance = true;
			}

			if (cleanStyles === void 0) {
				cleanStyles = true;
			}

			var swiper = this;
			var params = swiper.params,
				$el = swiper.$el,
				$wrapperEl = swiper.$wrapperEl,
				slides = swiper.slides;

			if (typeof swiper.params === "undefined" || swiper.destroyed) {
				return null;
			}

			swiper.emit("beforeDestroy"); // Init Flag

			swiper.initialized = false; // Detach events

			swiper.detachEvents(); // Destroy loop

			if (params.loop) {
				swiper.loopDestroy();
			} // Cleanup styles

			if (cleanStyles) {
				swiper.removeClasses();
				$el.removeAttr("style");
				$wrapperEl.removeAttr("style");

				if (slides && slides.length) {
					slides.removeClass([params.slideVisibleClass, params.slideActiveClass, params.slideNextClass, params.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index");
				}
			}

			swiper.emit("destroy"); // Detach emitter events

			Object.keys(swiper.eventsListeners).forEach(function (eventName) {
				swiper.off(eventName);
			});

			if (deleteInstance !== false) {
				swiper.$el[0].swiper = null;
				deleteProps(swiper);
			}

			swiper.destroyed = true;
			return null;
		};

		Swiper.extendDefaults = function extendDefaults(newDefaults) {
			extend$1(extendedDefaults, newDefaults);
		};

		Swiper.installModule = function installModule(module) {
			if (!Swiper.prototype.modules) Swiper.prototype.modules = {};
			var name = module.name || Object.keys(Swiper.prototype.modules).length + "_" + now();
			Swiper.prototype.modules[name] = module;
		};

		Swiper.use = function use(module) {
			if (Array.isArray(module)) {
				module.forEach(function (m) {
					return Swiper.installModule(m);
				});
				return Swiper;
			}

			Swiper.installModule(module);
			return Swiper;
		};

		_createClass(Swiper, null, [
			{
				key: "extendedDefaults",
				get: function get() {
					return extendedDefaults;
				},
			},
			{
				key: "defaults",
				get: function get() {
					return defaults;
				},
			},
		]);

		return Swiper;
	})();

	Object.keys(prototypes).forEach(function (prototypeGroup) {
		Object.keys(prototypes[prototypeGroup]).forEach(function (protoMethod) {
			Swiper.prototype[protoMethod] = prototypes[prototypeGroup][protoMethod];
		});
	});
	Swiper.use([Resize, Observer$1]);

	var Virtual = {
		update: function update(force) {
			var swiper = this;
			var _swiper$params = swiper.params,
				slidesPerView = _swiper$params.slidesPerView,
				slidesPerGroup = _swiper$params.slidesPerGroup,
				centeredSlides = _swiper$params.centeredSlides;
			var _swiper$params$virtua = swiper.params.virtual,
				addSlidesBefore = _swiper$params$virtua.addSlidesBefore,
				addSlidesAfter = _swiper$params$virtua.addSlidesAfter;
			var _swiper$virtual = swiper.virtual,
				previousFrom = _swiper$virtual.from,
				previousTo = _swiper$virtual.to,
				slides = _swiper$virtual.slides,
				previousSlidesGrid = _swiper$virtual.slidesGrid,
				renderSlide = _swiper$virtual.renderSlide,
				previousOffset = _swiper$virtual.offset;
			swiper.updateActiveIndex();
			var activeIndex = swiper.activeIndex || 0;
			var offsetProp;
			if (swiper.rtlTranslate) offsetProp = "right";
			else offsetProp = swiper.isHorizontal() ? "left" : "top";
			var slidesAfter;
			var slidesBefore;

			if (centeredSlides) {
				slidesAfter = Math.floor(slidesPerView / 2) + slidesPerGroup + addSlidesAfter;
				slidesBefore = Math.floor(slidesPerView / 2) + slidesPerGroup + addSlidesBefore;
			} else {
				slidesAfter = slidesPerView + (slidesPerGroup - 1) + addSlidesAfter;
				slidesBefore = slidesPerGroup + addSlidesBefore;
			}

			var from = Math.max((activeIndex || 0) - slidesBefore, 0);
			var to = Math.min((activeIndex || 0) + slidesAfter, slides.length - 1);
			var offset = (swiper.slidesGrid[from] || 0) - (swiper.slidesGrid[0] || 0);
			extend$1(swiper.virtual, {
				from: from,
				to: to,
				offset: offset,
				slidesGrid: swiper.slidesGrid,
			});

			function onRendered() {
				swiper.updateSlides();
				swiper.updateProgress();
				swiper.updateSlidesClasses();

				if (swiper.lazy && swiper.params.lazy.enabled) {
					swiper.lazy.load();
				}
			}

			if (previousFrom === from && previousTo === to && !force) {
				if (swiper.slidesGrid !== previousSlidesGrid && offset !== previousOffset) {
					swiper.slides.css(offsetProp, offset + "px");
				}

				swiper.updateProgress();
				return;
			}

			if (swiper.params.virtual.renderExternal) {
				swiper.params.virtual.renderExternal.call(swiper, {
					offset: offset,
					from: from,
					to: to,
					slides: (function getSlides() {
						var slidesToRender = [];

						for (var i = from; i <= to; i += 1) {
							slidesToRender.push(slides[i]);
						}

						return slidesToRender;
					})(),
				});

				if (swiper.params.virtual.renderExternalUpdate) {
					onRendered();
				}

				return;
			}

			var prependIndexes = [];
			var appendIndexes = [];

			if (force) {
				swiper.$wrapperEl.find("." + swiper.params.slideClass).remove();
			} else {
				for (var i = previousFrom; i <= previousTo; i += 1) {
					if (i < from || i > to) {
						swiper.$wrapperEl.find("." + swiper.params.slideClass + '[data-swiper-slide-index="' + i + '"]').remove();
					}
				}
			}

			for (var _i = 0; _i < slides.length; _i += 1) {
				if (_i >= from && _i <= to) {
					if (typeof previousTo === "undefined" || force) {
						appendIndexes.push(_i);
					} else {
						if (_i > previousTo) appendIndexes.push(_i);
						if (_i < previousFrom) prependIndexes.push(_i);
					}
				}
			}

			appendIndexes.forEach(function (index) {
				swiper.$wrapperEl.append(renderSlide(slides[index], index));
			});
			prependIndexes
				.sort(function (a, b) {
					return b - a;
				})
				.forEach(function (index) {
					swiper.$wrapperEl.prepend(renderSlide(slides[index], index));
				});
			swiper.$wrapperEl.children(".swiper-slide").css(offsetProp, offset + "px");
			onRendered();
		},
		renderSlide: function renderSlide(slide, index) {
			var swiper = this;
			var params = swiper.params.virtual;

			if (params.cache && swiper.virtual.cache[index]) {
				return swiper.virtual.cache[index];
			}

			var $slideEl = params.renderSlide ? $(params.renderSlide.call(swiper, slide, index)) : $('<div class="' + swiper.params.slideClass + '" data-swiper-slide-index="' + index + '">' + slide + "</div>");
			if (!$slideEl.attr("data-swiper-slide-index")) $slideEl.attr("data-swiper-slide-index", index);
			if (params.cache) swiper.virtual.cache[index] = $slideEl;
			return $slideEl;
		},
		appendSlide: function appendSlide(slides) {
			var swiper = this;

			if (typeof slides === "object" && "length" in slides) {
				for (var i = 0; i < slides.length; i += 1) {
					if (slides[i]) swiper.virtual.slides.push(slides[i]);
				}
			} else {
				swiper.virtual.slides.push(slides);
			}

			swiper.virtual.update(true);
		},
		prependSlide: function prependSlide(slides) {
			var swiper = this;
			var activeIndex = swiper.activeIndex;
			var newActiveIndex = activeIndex + 1;
			var numberOfNewSlides = 1;

			if (Array.isArray(slides)) {
				for (var i = 0; i < slides.length; i += 1) {
					if (slides[i]) swiper.virtual.slides.unshift(slides[i]);
				}

				newActiveIndex = activeIndex + slides.length;
				numberOfNewSlides = slides.length;
			} else {
				swiper.virtual.slides.unshift(slides);
			}

			if (swiper.params.virtual.cache) {
				var cache = swiper.virtual.cache;
				var newCache = {};
				Object.keys(cache).forEach(function (cachedIndex) {
					var $cachedEl = cache[cachedIndex];
					var cachedElIndex = $cachedEl.attr("data-swiper-slide-index");

					if (cachedElIndex) {
						$cachedEl.attr("data-swiper-slide-index", parseInt(cachedElIndex, 10) + 1);
					}

					newCache[parseInt(cachedIndex, 10) + numberOfNewSlides] = $cachedEl;
				});
				swiper.virtual.cache = newCache;
			}

			swiper.virtual.update(true);
			swiper.slideTo(newActiveIndex, 0);
		},
		removeSlide: function removeSlide(slidesIndexes) {
			var swiper = this;
			if (typeof slidesIndexes === "undefined" || slidesIndexes === null) return;
			var activeIndex = swiper.activeIndex;

			if (Array.isArray(slidesIndexes)) {
				for (var i = slidesIndexes.length - 1; i >= 0; i -= 1) {
					swiper.virtual.slides.splice(slidesIndexes[i], 1);

					if (swiper.params.virtual.cache) {
						delete swiper.virtual.cache[slidesIndexes[i]];
					}

					if (slidesIndexes[i] < activeIndex) activeIndex -= 1;
					activeIndex = Math.max(activeIndex, 0);
				}
			} else {
				swiper.virtual.slides.splice(slidesIndexes, 1);

				if (swiper.params.virtual.cache) {
					delete swiper.virtual.cache[slidesIndexes];
				}

				if (slidesIndexes < activeIndex) activeIndex -= 1;
				activeIndex = Math.max(activeIndex, 0);
			}

			swiper.virtual.update(true);
			swiper.slideTo(activeIndex, 0);
		},
		removeAllSlides: function removeAllSlides() {
			var swiper = this;
			swiper.virtual.slides = [];

			if (swiper.params.virtual.cache) {
				swiper.virtual.cache = {};
			}

			swiper.virtual.update(true);
			swiper.slideTo(0, 0);
		},
	};
	var Virtual$1 = {
		name: "virtual",
		params: {
			virtual: {
				enabled: false,
				slides: [],
				cache: true,
				renderSlide: null,
				renderExternal: null,
				renderExternalUpdate: true,
				addSlidesBefore: 0,
				addSlidesAfter: 0,
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				virtual: _extends(
					_extends({}, Virtual),
					{},
					{
						slides: swiper.params.virtual.slides,
						cache: {},
					}
				),
			});
		},
		on: {
			beforeInit: function beforeInit(swiper) {
				if (!swiper.params.virtual.enabled) return;
				swiper.classNames.push(swiper.params.containerModifierClass + "virtual");
				var overwriteParams = {
					watchSlidesProgress: true,
				};
				extend$1(swiper.params, overwriteParams);
				extend$1(swiper.originalParams, overwriteParams);

				if (!swiper.params.initialSlide) {
					swiper.virtual.update();
				}
			},
			setTranslate: function setTranslate(swiper) {
				if (!swiper.params.virtual.enabled) return;
				swiper.virtual.update();
			},
		},
	};

	var Keyboard = {
		handle: function handle(event) {
			var swiper = this;
			var window = getWindow();
			var document = getDocument();
			var rtl = swiper.rtlTranslate;
			var e = event;
			if (e.originalEvent) e = e.originalEvent; // jquery fix

			var kc = e.keyCode || e.charCode;
			var pageUpDown = swiper.params.keyboard.pageUpDown;
			var isPageUp = pageUpDown && kc === 33;
			var isPageDown = pageUpDown && kc === 34;
			var isArrowLeft = kc === 37;
			var isArrowRight = kc === 39;
			var isArrowUp = kc === 38;
			var isArrowDown = kc === 40; // Directions locks

			if (!swiper.allowSlideNext && ((swiper.isHorizontal() && isArrowRight) || (swiper.isVertical() && isArrowDown) || isPageDown)) {
				return false;
			}

			if (!swiper.allowSlidePrev && ((swiper.isHorizontal() && isArrowLeft) || (swiper.isVertical() && isArrowUp) || isPageUp)) {
				return false;
			}

			if (e.shiftKey || e.altKey || e.ctrlKey || e.metaKey) {
				return undefined;
			}

			if (document.activeElement && document.activeElement.nodeName && (document.activeElement.nodeName.toLowerCase() === "input" || document.activeElement.nodeName.toLowerCase() === "textarea")) {
				return undefined;
			}

			if (swiper.params.keyboard.onlyInViewport && (isPageUp || isPageDown || isArrowLeft || isArrowRight || isArrowUp || isArrowDown)) {
				var inView = false; // Check that swiper should be inside of visible area of window

				if (swiper.$el.parents("." + swiper.params.slideClass).length > 0 && swiper.$el.parents("." + swiper.params.slideActiveClass).length === 0) {
					return undefined;
				}

				var windowWidth = window.innerWidth;
				var windowHeight = window.innerHeight;
				var swiperOffset = swiper.$el.offset();
				if (rtl) swiperOffset.left -= swiper.$el[0].scrollLeft;
				var swiperCoord = [
					[swiperOffset.left, swiperOffset.top],
					[swiperOffset.left + swiper.width, swiperOffset.top],
					[swiperOffset.left, swiperOffset.top + swiper.height],
					[swiperOffset.left + swiper.width, swiperOffset.top + swiper.height],
				];

				for (var i = 0; i < swiperCoord.length; i += 1) {
					var point = swiperCoord[i];

					if (point[0] >= 0 && point[0] <= windowWidth && point[1] >= 0 && point[1] <= windowHeight) {
						inView = true;
					}
				}

				if (!inView) return undefined;
			}

			if (swiper.isHorizontal()) {
				if (isPageUp || isPageDown || isArrowLeft || isArrowRight) {
					if (e.preventDefault) e.preventDefault();
					else e.returnValue = false;
				}

				if (((isPageDown || isArrowRight) && !rtl) || ((isPageUp || isArrowLeft) && rtl)) swiper.slideNext();
				if (((isPageUp || isArrowLeft) && !rtl) || ((isPageDown || isArrowRight) && rtl)) swiper.slidePrev();
			} else {
				if (isPageUp || isPageDown || isArrowUp || isArrowDown) {
					if (e.preventDefault) e.preventDefault();
					else e.returnValue = false;
				}

				if (isPageDown || isArrowDown) swiper.slideNext();
				if (isPageUp || isArrowUp) swiper.slidePrev();
			}

			swiper.emit("keyPress", kc);
			return undefined;
		},
		enable: function enable() {
			var swiper = this;
			var document = getDocument();
			if (swiper.keyboard.enabled) return;
			$(document).on("keydown", swiper.keyboard.handle);
			swiper.keyboard.enabled = true;
		},
		disable: function disable() {
			var swiper = this;
			var document = getDocument();
			if (!swiper.keyboard.enabled) return;
			$(document).off("keydown", swiper.keyboard.handle);
			swiper.keyboard.enabled = false;
		},
	};
	var Keyboard$1 = {
		name: "keyboard",
		params: {
			keyboard: {
				enabled: false,
				onlyInViewport: true,
				pageUpDown: true,
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				keyboard: _extends(
					{
						enabled: false,
					},
					Keyboard
				),
			});
		},
		on: {
			init: function init(swiper) {
				if (swiper.params.keyboard.enabled) {
					swiper.keyboard.enable();
				}
			},
			destroy: function destroy(swiper) {
				if (swiper.keyboard.enabled) {
					swiper.keyboard.disable();
				}
			},
		},
	};

	function isEventSupported() {
		var document = getDocument();
		var eventName = "onwheel";
		var isSupported = eventName in document;

		if (!isSupported) {
			var element = document.createElement("div");
			element.setAttribute(eventName, "return;");
			isSupported = typeof element[eventName] === "function";
		}

		if (
			!isSupported &&
			document.implementation &&
			document.implementation.hasFeature && // always returns true in newer browsers as per the standard.
			// @see http://dom.spec.whatwg.org/#dom-domimplementation-hasfeature
			document.implementation.hasFeature("", "") !== true
		) {
			// This is the only way to test support for the `wheel` event in IE9+.
			isSupported = document.implementation.hasFeature("Events.wheel", "3.0");
		}

		return isSupported;
	}

	var Mousewheel = {
		lastScrollTime: now(),
		lastEventBeforeSnap: undefined,
		recentWheelEvents: [],
		event: function event() {
			var window = getWindow();
			if (window.navigator.userAgent.indexOf("firefox") > -1) return "DOMMouseScroll";
			return isEventSupported() ? "wheel" : "mousewheel";
		},
		normalize: function normalize(e) {
			// Reasonable defaults
			var PIXEL_STEP = 10;
			var LINE_HEIGHT = 40;
			var PAGE_HEIGHT = 800;
			var sX = 0;
			var sY = 0; // spinX, spinY

			var pX = 0;
			var pY = 0; // pixelX, pixelY
			// Legacy

			if ("detail" in e) {
				sY = e.detail;
			}

			if ("wheelDelta" in e) {
				sY = -e.wheelDelta / 120;
			}

			if ("wheelDeltaY" in e) {
				sY = -e.wheelDeltaY / 120;
			}

			if ("wheelDeltaX" in e) {
				sX = -e.wheelDeltaX / 120;
			} // side scrolling on FF with DOMMouseScroll

			if ("axis" in e && e.axis === e.HORIZONTAL_AXIS) {
				sX = sY;
				sY = 0;
			}

			pX = sX * PIXEL_STEP;
			pY = sY * PIXEL_STEP;

			if ("deltaY" in e) {
				pY = e.deltaY;
			}

			if ("deltaX" in e) {
				pX = e.deltaX;
			}

			if (e.shiftKey && !pX) {
				// if user scrolls with shift he wants horizontal scroll
				pX = pY;
				pY = 0;
			}

			if ((pX || pY) && e.deltaMode) {
				if (e.deltaMode === 1) {
					// delta in LINE units
					pX *= LINE_HEIGHT;
					pY *= LINE_HEIGHT;
				} else {
					// delta in PAGE units
					pX *= PAGE_HEIGHT;
					pY *= PAGE_HEIGHT;
				}
			} // Fall-back if spin cannot be determined

			if (pX && !sX) {
				sX = pX < 1 ? -1 : 1;
			}

			if (pY && !sY) {
				sY = pY < 1 ? -1 : 1;
			}

			return {
				spinX: sX,
				spinY: sY,
				pixelX: pX,
				pixelY: pY,
			};
		},
		handleMouseEnter: function handleMouseEnter() {
			var swiper = this;
			swiper.mouseEntered = true;
		},
		handleMouseLeave: function handleMouseLeave() {
			var swiper = this;
			swiper.mouseEntered = false;
		},
		handle: function handle(event) {
			var e = event;
			var swiper = this;
			var params = swiper.params.mousewheel;

			if (swiper.params.cssMode) {
				e.preventDefault();
			}

			var target = swiper.$el;

			if (swiper.params.mousewheel.eventsTarget !== "container") {
				target = $(swiper.params.mousewheel.eventsTarget);
			}

			if (!swiper.mouseEntered && !target[0].contains(e.target) && !params.releaseOnEdges) return true;
			if (e.originalEvent) e = e.originalEvent; // jquery fix

			var delta = 0;
			var rtlFactor = swiper.rtlTranslate ? -1 : 1;
			var data = Mousewheel.normalize(e);

			if (params.forceToAxis) {
				if (swiper.isHorizontal()) {
					if (Math.abs(data.pixelX) > Math.abs(data.pixelY)) delta = -data.pixelX * rtlFactor;
					else return true;
				} else if (Math.abs(data.pixelY) > Math.abs(data.pixelX)) delta = -data.pixelY;
				else return true;
			} else {
				delta = Math.abs(data.pixelX) > Math.abs(data.pixelY) ? -data.pixelX * rtlFactor : -data.pixelY;
			}

			if (delta === 0) return true;
			if (params.invert) delta = -delta;

			if (!swiper.params.freeMode) {
				// Register the new event in a variable which stores the relevant data
				var newEvent = {
					time: now(),
					delta: Math.abs(delta),
					direction: Math.sign(delta),
					raw: event,
				}; // Keep the most recent events

				var recentWheelEvents = swiper.mousewheel.recentWheelEvents;

				if (recentWheelEvents.length >= 2) {
					recentWheelEvents.shift(); // only store the last N events
				}

				var prevEvent = recentWheelEvents.length ? recentWheelEvents[recentWheelEvents.length - 1] : undefined;
				recentWheelEvents.push(newEvent); // If there is at least one previous recorded event:
				//   If direction has changed or
				//   if the scroll is quicker than the previous one:
				//     Animate the slider.
				// Else (this is the first time the wheel is moved):
				//     Animate the slider.

				if (prevEvent) {
					if (newEvent.direction !== prevEvent.direction || newEvent.delta > prevEvent.delta || newEvent.time > prevEvent.time + 150) {
						swiper.mousewheel.animateSlider(newEvent);
					}
				} else {
					swiper.mousewheel.animateSlider(newEvent);
				} // If it's time to release the scroll:
				//   Return now so you don't hit the preventDefault.

				if (swiper.mousewheel.releaseScroll(newEvent)) {
					return true;
				}
			} else {
				// Freemode or scrollContainer:
				// If we recently snapped after a momentum scroll, then ignore wheel events
				// to give time for the deceleration to finish. Stop ignoring after 500 msecs
				// or if it's a new scroll (larger delta or inverse sign as last event before
				// an end-of-momentum snap).
				var _newEvent = {
					time: now(),
					delta: Math.abs(delta),
					direction: Math.sign(delta),
				};
				var lastEventBeforeSnap = swiper.mousewheel.lastEventBeforeSnap;
				var ignoreWheelEvents = lastEventBeforeSnap && _newEvent.time < lastEventBeforeSnap.time + 500 && _newEvent.delta <= lastEventBeforeSnap.delta && _newEvent.direction === lastEventBeforeSnap.direction;

				if (!ignoreWheelEvents) {
					swiper.mousewheel.lastEventBeforeSnap = undefined;

					if (swiper.params.loop) {
						swiper.loopFix();
					}

					var position = swiper.getTranslate() + delta * params.sensitivity;
					var wasBeginning = swiper.isBeginning;
					var wasEnd = swiper.isEnd;
					if (position >= swiper.minTranslate()) position = swiper.minTranslate();
					if (position <= swiper.maxTranslate()) position = swiper.maxTranslate();
					swiper.setTransition(0);
					swiper.setTranslate(position);
					swiper.updateProgress();
					swiper.updateActiveIndex();
					swiper.updateSlidesClasses();

					if ((!wasBeginning && swiper.isBeginning) || (!wasEnd && swiper.isEnd)) {
						swiper.updateSlidesClasses();
					}

					if (swiper.params.freeModeSticky) {
						// When wheel scrolling starts with sticky (aka snap) enabled, then detect
						// the end of a momentum scroll by storing recent (N=15?) wheel events.
						// 1. do all N events have decreasing or same (absolute value) delta?
						// 2. did all N events arrive in the last M (M=500?) msecs?
						// 3. does the earliest event have an (absolute value) delta that's
						//    at least P (P=1?) larger than the most recent event's delta?
						// 4. does the latest event have a delta that's smaller than Q (Q=6?) pixels?
						// If 1-4 are "yes" then we're near the end of a momentum scroll deceleration.
						// Snap immediately and ignore remaining wheel events in this scroll.
						// See comment above for "remaining wheel events in this scroll" determination.
						// If 1-4 aren't satisfied, then wait to snap until 500ms after the last event.
						clearTimeout(swiper.mousewheel.timeout);
						swiper.mousewheel.timeout = undefined;
						var _recentWheelEvents = swiper.mousewheel.recentWheelEvents;

						if (_recentWheelEvents.length >= 15) {
							_recentWheelEvents.shift(); // only store the last N events
						}

						var _prevEvent = _recentWheelEvents.length ? _recentWheelEvents[_recentWheelEvents.length - 1] : undefined;

						var firstEvent = _recentWheelEvents[0];

						_recentWheelEvents.push(_newEvent);

						if (_prevEvent && (_newEvent.delta > _prevEvent.delta || _newEvent.direction !== _prevEvent.direction)) {
							// Increasing or reverse-sign delta means the user started scrolling again. Clear the wheel event log.
							_recentWheelEvents.splice(0);
						} else if (_recentWheelEvents.length >= 15 && _newEvent.time - firstEvent.time < 500 && firstEvent.delta - _newEvent.delta >= 1 && _newEvent.delta <= 6) {
							// We're at the end of the deceleration of a momentum scroll, so there's no need
							// to wait for more events. Snap ASAP on the next tick.
							// Also, because there's some remaining momentum we'll bias the snap in the
							// direction of the ongoing scroll because it's better UX for the scroll to snap
							// in the same direction as the scroll instead of reversing to snap.  Therefore,
							// if it's already scrolled more than 20% in the current direction, keep going.
							var snapToThreshold = delta > 0 ? 0.8 : 0.2;
							swiper.mousewheel.lastEventBeforeSnap = _newEvent;

							_recentWheelEvents.splice(0);

							swiper.mousewheel.timeout = nextTick(function () {
								swiper.slideToClosest(swiper.params.speed, true, undefined, snapToThreshold);
							}, 0); // no delay; move on next tick
						}

						if (!swiper.mousewheel.timeout) {
							// if we get here, then we haven't detected the end of a momentum scroll, so
							// we'll consider a scroll "complete" when there haven't been any wheel events
							// for 500ms.
							swiper.mousewheel.timeout = nextTick(function () {
								var snapToThreshold = 0.5;
								swiper.mousewheel.lastEventBeforeSnap = _newEvent;

								_recentWheelEvents.splice(0);

								swiper.slideToClosest(swiper.params.speed, true, undefined, snapToThreshold);
							}, 500);
						}
					} // Emit event

					if (!ignoreWheelEvents) swiper.emit("scroll", e); // Stop autoplay

					if (swiper.params.autoplay && swiper.params.autoplayDisableOnInteraction) swiper.autoplay.stop(); // Return page scroll on edge positions

					if (position === swiper.minTranslate() || position === swiper.maxTranslate()) return true;
				}
			}

			if (e.preventDefault) e.preventDefault();
			else e.returnValue = false;
			return false;
		},
		animateSlider: function animateSlider(newEvent) {
			var swiper = this;
			var window = getWindow();

			if (this.params.mousewheel.thresholdDelta && newEvent.delta < this.params.mousewheel.thresholdDelta) {
				// Prevent if delta of wheel scroll delta is below configured threshold
				return false;
			}

			if (this.params.mousewheel.thresholdTime && now() - swiper.mousewheel.lastScrollTime < this.params.mousewheel.thresholdTime) {
				// Prevent if time between scrolls is below configured threshold
				return false;
			} // If the movement is NOT big enough and
			// if the last time the user scrolled was too close to the current one (avoid continuously triggering the slider):
			//   Don't go any further (avoid insignificant scroll movement).

			if (newEvent.delta >= 6 && now() - swiper.mousewheel.lastScrollTime < 60) {
				// Return false as a default
				return true;
			} // If user is scrolling towards the end:
			//   If the slider hasn't hit the latest slide or
			//   if the slider is a loop and
			//   if the slider isn't moving right now:
			//     Go to next slide and
			//     emit a scroll event.
			// Else (the user is scrolling towards the beginning) and
			// if the slider hasn't hit the first slide or
			// if the slider is a loop and
			// if the slider isn't moving right now:
			//   Go to prev slide and
			//   emit a scroll event.

			if (newEvent.direction < 0) {
				if ((!swiper.isEnd || swiper.params.loop) && !swiper.animating) {
					swiper.slideNext();
					swiper.emit("scroll", newEvent.raw);
				}
			} else if ((!swiper.isBeginning || swiper.params.loop) && !swiper.animating) {
				swiper.slidePrev();
				swiper.emit("scroll", newEvent.raw);
			} // If you got here is because an animation has been triggered so store the current time

			swiper.mousewheel.lastScrollTime = new window.Date().getTime(); // Return false as a default

			return false;
		},
		releaseScroll: function releaseScroll(newEvent) {
			var swiper = this;
			var params = swiper.params.mousewheel;

			if (newEvent.direction < 0) {
				if (swiper.isEnd && !swiper.params.loop && params.releaseOnEdges) {
					// Return true to animate scroll on edges
					return true;
				}
			} else if (swiper.isBeginning && !swiper.params.loop && params.releaseOnEdges) {
				// Return true to animate scroll on edges
				return true;
			}

			return false;
		},
		enable: function enable() {
			var swiper = this;
			var event = Mousewheel.event();

			if (swiper.params.cssMode) {
				swiper.wrapperEl.removeEventListener(event, swiper.mousewheel.handle);
				return true;
			}

			if (!event) return false;
			if (swiper.mousewheel.enabled) return false;
			var target = swiper.$el;

			if (swiper.params.mousewheel.eventsTarget !== "container") {
				target = $(swiper.params.mousewheel.eventsTarget);
			}

			target.on("mouseenter", swiper.mousewheel.handleMouseEnter);
			target.on("mouseleave", swiper.mousewheel.handleMouseLeave);
			target.on(event, swiper.mousewheel.handle);
			swiper.mousewheel.enabled = true;
			return true;
		},
		disable: function disable() {
			var swiper = this;
			var event = Mousewheel.event();

			if (swiper.params.cssMode) {
				swiper.wrapperEl.addEventListener(event, swiper.mousewheel.handle);
				return true;
			}

			if (!event) return false;
			if (!swiper.mousewheel.enabled) return false;
			var target = swiper.$el;

			if (swiper.params.mousewheel.eventsTarget !== "container") {
				target = $(swiper.params.mousewheel.eventsTarget);
			}

			target.off(event, swiper.mousewheel.handle);
			swiper.mousewheel.enabled = false;
			return true;
		},
	};
	var Mousewheel$1 = {
		name: "mousewheel",
		params: {
			mousewheel: {
				enabled: false,
				releaseOnEdges: false,
				invert: false,
				forceToAxis: false,
				sensitivity: 1,
				eventsTarget: "container",
				thresholdDelta: null,
				thresholdTime: null,
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				mousewheel: {
					enabled: false,
					lastScrollTime: now(),
					lastEventBeforeSnap: undefined,
					recentWheelEvents: [],
					enable: Mousewheel.enable,
					disable: Mousewheel.disable,
					handle: Mousewheel.handle,
					handleMouseEnter: Mousewheel.handleMouseEnter,
					handleMouseLeave: Mousewheel.handleMouseLeave,
					animateSlider: Mousewheel.animateSlider,
					releaseScroll: Mousewheel.releaseScroll,
				},
			});
		},
		on: {
			init: function init(swiper) {
				if (!swiper.params.mousewheel.enabled && swiper.params.cssMode) {
					swiper.mousewheel.disable();
				}

				if (swiper.params.mousewheel.enabled) swiper.mousewheel.enable();
			},
			destroy: function destroy(swiper) {
				if (swiper.params.cssMode) {
					swiper.mousewheel.enable();
				}

				if (swiper.mousewheel.enabled) swiper.mousewheel.disable();
			},
		},
	};

	var Navigation = {
		update: function update() {
			// Update Navigation Buttons
			var swiper = this;
			var params = swiper.params.navigation;
			if (swiper.params.loop) return;
			var _swiper$navigation = swiper.navigation,
				$nextEl = _swiper$navigation.$nextEl,
				$prevEl = _swiper$navigation.$prevEl;

			if ($prevEl && $prevEl.length > 0) {
				if (swiper.isBeginning) {
					$prevEl.addClass(params.disabledClass);
				} else {
					$prevEl.removeClass(params.disabledClass);
				}

				$prevEl[swiper.params.watchOverflow && swiper.isLocked ? "addClass" : "removeClass"](params.lockClass);
			}

			if ($nextEl && $nextEl.length > 0) {
				if (swiper.isEnd) {
					$nextEl.addClass(params.disabledClass);
				} else {
					$nextEl.removeClass(params.disabledClass);
				}

				$nextEl[swiper.params.watchOverflow && swiper.isLocked ? "addClass" : "removeClass"](params.lockClass);
			}
		},
		onPrevClick: function onPrevClick(e) {
			var swiper = this;
			e.preventDefault();
			if (swiper.isBeginning && !swiper.params.loop) return;
			swiper.slidePrev();
		},
		onNextClick: function onNextClick(e) {
			var swiper = this;
			e.preventDefault();
			if (swiper.isEnd && !swiper.params.loop) return;
			swiper.slideNext();
		},
		init: function init() {
			var swiper = this;
			var params = swiper.params.navigation;
			if (!(params.nextEl || params.prevEl)) return;
			var $nextEl;
			var $prevEl;

			if (params.nextEl) {
				$nextEl = $(params.nextEl);

				if (swiper.params.uniqueNavElements && typeof params.nextEl === "string" && $nextEl.length > 1 && swiper.$el.find(params.nextEl).length === 1) {
					$nextEl = swiper.$el.find(params.nextEl);
				}
			}

			if (params.prevEl) {
				$prevEl = $(params.prevEl);

				if (swiper.params.uniqueNavElements && typeof params.prevEl === "string" && $prevEl.length > 1 && swiper.$el.find(params.prevEl).length === 1) {
					$prevEl = swiper.$el.find(params.prevEl);
				}
			}

			if ($nextEl && $nextEl.length > 0) {
				$nextEl.on("click", swiper.navigation.onNextClick);
			}

			if ($prevEl && $prevEl.length > 0) {
				$prevEl.on("click", swiper.navigation.onPrevClick);
			}

			extend$1(swiper.navigation, {
				$nextEl: $nextEl,
				nextEl: $nextEl && $nextEl[0],
				$prevEl: $prevEl,
				prevEl: $prevEl && $prevEl[0],
			});
		},
		destroy: function destroy() {
			var swiper = this;
			var _swiper$navigation2 = swiper.navigation,
				$nextEl = _swiper$navigation2.$nextEl,
				$prevEl = _swiper$navigation2.$prevEl;

			if ($nextEl && $nextEl.length) {
				$nextEl.off("click", swiper.navigation.onNextClick);
				$nextEl.removeClass(swiper.params.navigation.disabledClass);
			}

			if ($prevEl && $prevEl.length) {
				$prevEl.off("click", swiper.navigation.onPrevClick);
				$prevEl.removeClass(swiper.params.navigation.disabledClass);
			}
		},
	};
	var Navigation$1 = {
		name: "navigation",
		params: {
			navigation: {
				nextEl: null,
				prevEl: null,
				hideOnClick: false,
				disabledClass: "swiper-button-disabled",
				hiddenClass: "swiper-button-hidden",
				lockClass: "swiper-button-lock",
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				navigation: _extends({}, Navigation),
			});
		},
		on: {
			init: function init(swiper) {
				swiper.navigation.init();
				swiper.navigation.update();
			},
			toEdge: function toEdge(swiper) {
				swiper.navigation.update();
			},
			fromEdge: function fromEdge(swiper) {
				swiper.navigation.update();
			},
			destroy: function destroy(swiper) {
				swiper.navigation.destroy();
			},
			click: function click(swiper, e) {
				var _swiper$navigation3 = swiper.navigation,
					$nextEl = _swiper$navigation3.$nextEl,
					$prevEl = _swiper$navigation3.$prevEl;

				if (swiper.params.navigation.hideOnClick && !$(e.target).is($prevEl) && !$(e.target).is($nextEl)) {
					var isHidden;

					if ($nextEl) {
						isHidden = $nextEl.hasClass(swiper.params.navigation.hiddenClass);
					} else if ($prevEl) {
						isHidden = $prevEl.hasClass(swiper.params.navigation.hiddenClass);
					}

					if (isHidden === true) {
						swiper.emit("navigationShow");
					} else {
						swiper.emit("navigationHide");
					}

					if ($nextEl) {
						$nextEl.toggleClass(swiper.params.navigation.hiddenClass);
					}

					if ($prevEl) {
						$prevEl.toggleClass(swiper.params.navigation.hiddenClass);
					}
				}
			},
		},
	};

	var Pagination = {
		update: function update() {
			// Render || Update Pagination bullets/items
			var swiper = this;
			var rtl = swiper.rtl;
			var params = swiper.params.pagination;
			if (!params.el || !swiper.pagination.el || !swiper.pagination.$el || swiper.pagination.$el.length === 0) return;
			var slidesLength = swiper.virtual && swiper.params.virtual.enabled ? swiper.virtual.slides.length : swiper.slides.length;
			var $el = swiper.pagination.$el; // Current/Total

			var current;
			var total = swiper.params.loop ? Math.ceil((slidesLength - swiper.loopedSlides * 2) / swiper.params.slidesPerGroup) : swiper.snapGrid.length;

			if (swiper.params.loop) {
				current = Math.ceil((swiper.activeIndex - swiper.loopedSlides) / swiper.params.slidesPerGroup);

				if (current > slidesLength - 1 - swiper.loopedSlides * 2) {
					current -= slidesLength - swiper.loopedSlides * 2;
				}

				if (current > total - 1) current -= total;
				if (current < 0 && swiper.params.paginationType !== "bullets") current = total + current;
			} else if (typeof swiper.snapIndex !== "undefined") {
				current = swiper.snapIndex;
			} else {
				current = swiper.activeIndex || 0;
			} // Types

			if (params.type === "bullets" && swiper.pagination.bullets && swiper.pagination.bullets.length > 0) {
				var bullets = swiper.pagination.bullets;
				var firstIndex;
				var lastIndex;
				var midIndex;

				if (params.dynamicBullets) {
					swiper.pagination.bulletSize = bullets.eq(0)[swiper.isHorizontal() ? "outerWidth" : "outerHeight"](true);
					$el.css(swiper.isHorizontal() ? "width" : "height", swiper.pagination.bulletSize * (params.dynamicMainBullets + 4) + "px");

					if (params.dynamicMainBullets > 1 && swiper.previousIndex !== undefined) {
						swiper.pagination.dynamicBulletIndex += current - swiper.previousIndex;

						if (swiper.pagination.dynamicBulletIndex > params.dynamicMainBullets - 1) {
							swiper.pagination.dynamicBulletIndex = params.dynamicMainBullets - 1;
						} else if (swiper.pagination.dynamicBulletIndex < 0) {
							swiper.pagination.dynamicBulletIndex = 0;
						}
					}

					firstIndex = current - swiper.pagination.dynamicBulletIndex;
					lastIndex = firstIndex + (Math.min(bullets.length, params.dynamicMainBullets) - 1);
					midIndex = (lastIndex + firstIndex) / 2;
				}

				bullets.removeClass(params.bulletActiveClass + " " + params.bulletActiveClass + "-next " + params.bulletActiveClass + "-next-next " + params.bulletActiveClass + "-prev " + params.bulletActiveClass + "-prev-prev " + params.bulletActiveClass + "-main");

				if ($el.length > 1) {
					bullets.each(function (bullet) {
						var $bullet = $(bullet);
						var bulletIndex = $bullet.index();

						if (bulletIndex === current) {
							$bullet.addClass(params.bulletActiveClass);
						}

						if (params.dynamicBullets) {
							if (bulletIndex >= firstIndex && bulletIndex <= lastIndex) {
								$bullet.addClass(params.bulletActiveClass + "-main");
							}

							if (bulletIndex === firstIndex) {
								$bullet.prev()
									.addClass(params.bulletActiveClass + "-prev")
									.prev()
									.addClass(params.bulletActiveClass + "-prev-prev");
							}

							if (bulletIndex === lastIndex) {
								$bullet.next()
									.addClass(params.bulletActiveClass + "-next")
									.next()
									.addClass(params.bulletActiveClass + "-next-next");
							}
						}
					});
				} else {
					var $bullet = bullets.eq(current);
					var bulletIndex = $bullet.index();
					$bullet.addClass(params.bulletActiveClass);

					if (params.dynamicBullets) {
						var $firstDisplayedBullet = bullets.eq(firstIndex);
						var $lastDisplayedBullet = bullets.eq(lastIndex);

						for (var i = firstIndex; i <= lastIndex; i += 1) {
							bullets.eq(i).addClass(params.bulletActiveClass + "-main");
						}

						if (swiper.params.loop) {
							if (bulletIndex >= bullets.length - params.dynamicMainBullets) {
								for (var _i = params.dynamicMainBullets; _i >= 0; _i -= 1) {
									bullets.eq(bullets.length - _i).addClass(params.bulletActiveClass + "-main");
								}

								bullets.eq(bullets.length - params.dynamicMainBullets - 1).addClass(params.bulletActiveClass + "-prev");
							} else {
								$firstDisplayedBullet
									.prev()
									.addClass(params.bulletActiveClass + "-prev")
									.prev()
									.addClass(params.bulletActiveClass + "-prev-prev");
								$lastDisplayedBullet
									.next()
									.addClass(params.bulletActiveClass + "-next")
									.next()
									.addClass(params.bulletActiveClass + "-next-next");
							}
						} else {
							$firstDisplayedBullet
								.prev()
								.addClass(params.bulletActiveClass + "-prev")
								.prev()
								.addClass(params.bulletActiveClass + "-prev-prev");
							$lastDisplayedBullet
								.next()
								.addClass(params.bulletActiveClass + "-next")
								.next()
								.addClass(params.bulletActiveClass + "-next-next");
						}
					}
				}

				if (params.dynamicBullets) {
					var dynamicBulletsLength = Math.min(bullets.length, params.dynamicMainBullets + 4);
					var bulletsOffset = (swiper.pagination.bulletSize * dynamicBulletsLength - swiper.pagination.bulletSize) / 2 - midIndex * swiper.pagination.bulletSize;
					var offsetProp = rtl ? "right" : "left";
					bullets.css(swiper.isHorizontal() ? offsetProp : "top", bulletsOffset + "px");
				}
			}

			if (params.type === "fraction") {
				$el.find("." + params.currentClass).text(params.formatFractionCurrent(current + 1));
				$el.find("." + params.totalClass).text(params.formatFractionTotal(total));
			}

			if (params.type === "progressbar") {
				var progressbarDirection;

				if (params.progressbarOpposite) {
					progressbarDirection = swiper.isHorizontal() ? "vertical" : "horizontal";
				} else {
					progressbarDirection = swiper.isHorizontal() ? "horizontal" : "vertical";
				}

				var scale = (current + 1) / total;
				var scaleX = 1;
				var scaleY = 1;

				if (progressbarDirection === "horizontal") {
					scaleX = scale;
				} else {
					scaleY = scale;
				}

				$el.find("." + params.progressbarFillClass)
					.transform("translate3d(0,0,0) scaleX(" + scaleX + ") scaleY(" + scaleY + ")")
					.transition(swiper.params.speed);
			}

			if (params.type === "custom" && params.renderCustom) {
				$el.html(params.renderCustom(swiper, current + 1, total));
				swiper.emit("paginationRender", $el[0]);
			} else {
				swiper.emit("paginationUpdate", $el[0]);
			}

			$el[swiper.params.watchOverflow && swiper.isLocked ? "addClass" : "removeClass"](params.lockClass);
		},
		render: function render() {
			// Render Container
			var swiper = this;
			var params = swiper.params.pagination;
			if (!params.el || !swiper.pagination.el || !swiper.pagination.$el || swiper.pagination.$el.length === 0) return;
			var slidesLength = swiper.virtual && swiper.params.virtual.enabled ? swiper.virtual.slides.length : swiper.slides.length;
			var $el = swiper.pagination.$el;
			var paginationHTML = "";

			if (params.type === "bullets") {
				var numberOfBullets = swiper.params.loop ? Math.ceil((slidesLength - swiper.loopedSlides * 2) / swiper.params.slidesPerGroup) : swiper.snapGrid.length;

				for (var i = 0; i < numberOfBullets; i += 1) {
					if (params.renderBullet) {
						paginationHTML += params.renderBullet.call(swiper, i, params.bulletClass);
					} else {
						paginationHTML += "<" + params.bulletElement + ' class="' + params.bulletClass + '"></' + params.bulletElement + ">";
					}
				}

				$el.html(paginationHTML);
				swiper.pagination.bullets = $el.find("." + params.bulletClass);
			}

			if (params.type === "fraction") {
				if (params.renderFraction) {
					paginationHTML = params.renderFraction.call(swiper, params.currentClass, params.totalClass);
				} else {
					paginationHTML = '<span class="' + params.currentClass + '"></span>' + " / " + ('<span class="' + params.totalClass + '"></span>');
				}

				$el.html(paginationHTML);
			}

			if (params.type === "progressbar") {
				if (params.renderProgressbar) {
					paginationHTML = params.renderProgressbar.call(swiper, params.progressbarFillClass);
				} else {
					paginationHTML = '<span class="' + params.progressbarFillClass + '"></span>';
				}

				$el.html(paginationHTML);
			}

			if (params.type !== "custom") {
				swiper.emit("paginationRender", swiper.pagination.$el[0]);
			}
		},
		init: function init() {
			var swiper = this;
			var params = swiper.params.pagination;
			if (!params.el) return;
			var $el = $(params.el);
			if ($el.length === 0) return;

			if (swiper.params.uniqueNavElements && typeof params.el === "string" && $el.length > 1) {
				$el = swiper.$el.find(params.el);
			}

			if (params.type === "bullets" && params.clickable) {
				$el.addClass(params.clickableClass);
			}

			$el.addClass(params.modifierClass + params.type);

			if (params.type === "bullets" && params.dynamicBullets) {
				$el.addClass("" + params.modifierClass + params.type + "-dynamic");
				swiper.pagination.dynamicBulletIndex = 0;

				if (params.dynamicMainBullets < 1) {
					params.dynamicMainBullets = 1;
				}
			}

			if (params.type === "progressbar" && params.progressbarOpposite) {
				$el.addClass(params.progressbarOppositeClass);
			}

			if (params.clickable) {
				$el.on("click", "." + params.bulletClass, function onClick(e) {
					e.preventDefault();
					var index = $(this).index() * swiper.params.slidesPerGroup;
					if (swiper.params.loop) index += swiper.loopedSlides;
					swiper.slideTo(index);
				});
			}

			extend$1(swiper.pagination, {
				$el: $el,
				el: $el[0],
			});
		},
		destroy: function destroy() {
			var swiper = this;
			var params = swiper.params.pagination;
			if (!params.el || !swiper.pagination.el || !swiper.pagination.$el || swiper.pagination.$el.length === 0) return;
			var $el = swiper.pagination.$el;
			$el.removeClass(params.hiddenClass);
			$el.removeClass(params.modifierClass + params.type);
			if (swiper.pagination.bullets) swiper.pagination.bullets.removeClass(params.bulletActiveClass);

			if (params.clickable) {
				$el.off("click", "." + params.bulletClass);
			}
		},
	};
	var Pagination$1 = {
		name: "pagination",
		params: {
			pagination: {
				el: null,
				bulletElement: "span",
				clickable: false,
				hideOnClick: false,
				renderBullet: null,
				renderProgressbar: null,
				renderFraction: null,
				renderCustom: null,
				progressbarOpposite: false,
				type: "bullets",
				// 'bullets' or 'progressbar' or 'fraction' or 'custom'
				dynamicBullets: false,
				dynamicMainBullets: 1,
				formatFractionCurrent: function formatFractionCurrent(number) {
					return number;
				},
				formatFractionTotal: function formatFractionTotal(number) {
					return number;
				},
				bulletClass: "swiper-pagination-bullet",
				bulletActiveClass: "swiper-pagination-bullet-active",
				modifierClass: "swiper-pagination-",
				// NEW
				currentClass: "swiper-pagination-current",
				totalClass: "swiper-pagination-total",
				hiddenClass: "swiper-pagination-hidden",
				progressbarFillClass: "swiper-pagination-progressbar-fill",
				progressbarOppositeClass: "swiper-pagination-progressbar-opposite",
				clickableClass: "swiper-pagination-clickable",
				// NEW
				lockClass: "swiper-pagination-lock",
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				pagination: _extends(
					{
						dynamicBulletIndex: 0,
					},
					Pagination
				),
			});
		},
		on: {
			init: function init(swiper) {
				swiper.pagination.init();
				swiper.pagination.render();
				swiper.pagination.update();
			},
			activeIndexChange: function activeIndexChange(swiper) {
				if (swiper.params.loop) {
					swiper.pagination.update();
				} else if (typeof swiper.snapIndex === "undefined") {
					swiper.pagination.update();
				}
			},
			snapIndexChange: function snapIndexChange(swiper) {
				if (!swiper.params.loop) {
					swiper.pagination.update();
				}
			},
			slidesLengthChange: function slidesLengthChange(swiper) {
				if (swiper.params.loop) {
					swiper.pagination.render();
					swiper.pagination.update();
				}
			},
			snapGridLengthChange: function snapGridLengthChange(swiper) {
				if (!swiper.params.loop) {
					swiper.pagination.render();
					swiper.pagination.update();
				}
			},
			destroy: function destroy(swiper) {
				swiper.pagination.destroy();
			},
			click: function click(swiper, e) {
				if (swiper.params.pagination.el && swiper.params.pagination.hideOnClick && swiper.pagination.$el.length > 0 && !$(e.target).hasClass(swiper.params.pagination.bulletClass)) {
					var isHidden = swiper.pagination.$el.hasClass(swiper.params.pagination.hiddenClass);

					if (isHidden === true) {
						swiper.emit("paginationShow");
					} else {
						swiper.emit("paginationHide");
					}

					swiper.pagination.$el.toggleClass(swiper.params.pagination.hiddenClass);
				}
			},
		},
	};

	var Scrollbar = {
		setTranslate: function setTranslate() {
			var swiper = this;
			if (!swiper.params.scrollbar.el || !swiper.scrollbar.el) return;
			var scrollbar = swiper.scrollbar,
				rtl = swiper.rtlTranslate,
				progress = swiper.progress;
			var dragSize = scrollbar.dragSize,
				trackSize = scrollbar.trackSize,
				$dragEl = scrollbar.$dragEl,
				$el = scrollbar.$el;
			var params = swiper.params.scrollbar;
			var newSize = dragSize;
			var newPos = (trackSize - dragSize) * progress;

			if (rtl) {
				newPos = -newPos;

				if (newPos > 0) {
					newSize = dragSize - newPos;
					newPos = 0;
				} else if (-newPos + dragSize > trackSize) {
					newSize = trackSize + newPos;
				}
			} else if (newPos < 0) {
				newSize = dragSize + newPos;
				newPos = 0;
			} else if (newPos + dragSize > trackSize) {
				newSize = trackSize - newPos;
			}

			if (swiper.isHorizontal()) {
				$dragEl.transform("translate3d(" + newPos + "px, 0, 0)");
				$dragEl[0].style.width = newSize + "px";
			} else {
				$dragEl.transform("translate3d(0px, " + newPos + "px, 0)");
				$dragEl[0].style.height = newSize + "px";
			}

			if (params.hide) {
				clearTimeout(swiper.scrollbar.timeout);
				$el[0].style.opacity = 1;
				swiper.scrollbar.timeout = setTimeout(function () {
					$el[0].style.opacity = 0;
					$el.transition(400);
				}, 1000);
			}
		},
		setTransition: function setTransition(duration) {
			var swiper = this;
			if (!swiper.params.scrollbar.el || !swiper.scrollbar.el) return;
			swiper.scrollbar.$dragEl.transition(duration);
		},
		updateSize: function updateSize() {
			var swiper = this;
			if (!swiper.params.scrollbar.el || !swiper.scrollbar.el) return;
			var scrollbar = swiper.scrollbar;
			var $dragEl = scrollbar.$dragEl,
				$el = scrollbar.$el;
			$dragEl[0].style.width = "";
			$dragEl[0].style.height = "";
			var trackSize = swiper.isHorizontal() ? $el[0].offsetWidth : $el[0].offsetHeight;
			var divider = swiper.size / swiper.virtualSize;
			var moveDivider = divider * (trackSize / swiper.size);
			var dragSize;

			if (swiper.params.scrollbar.dragSize === "auto") {
				dragSize = trackSize * divider;
			} else {
				dragSize = parseInt(swiper.params.scrollbar.dragSize, 10);
			}

			if (swiper.isHorizontal()) {
				$dragEl[0].style.width = dragSize + "px";
			} else {
				$dragEl[0].style.height = dragSize + "px";
			}

			if (divider >= 1) {
				$el[0].style.display = "none";
			} else {
				$el[0].style.display = "";
			}

			if (swiper.params.scrollbar.hide) {
				$el[0].style.opacity = 0;
			}

			extend$1(scrollbar, {
				trackSize: trackSize,
				divider: divider,
				moveDivider: moveDivider,
				dragSize: dragSize,
			});
			scrollbar.$el[swiper.params.watchOverflow && swiper.isLocked ? "addClass" : "removeClass"](swiper.params.scrollbar.lockClass);
		},
		getPointerPosition: function getPointerPosition(e) {
			var swiper = this;

			if (swiper.isHorizontal()) {
				return e.type === "touchstart" || e.type === "touchmove" ? e.targetTouches[0].clientX : e.clientX;
			}

			return e.type === "touchstart" || e.type === "touchmove" ? e.targetTouches[0].clientY : e.clientY;
		},
		setDragPosition: function setDragPosition(e) {
			var swiper = this;
			var scrollbar = swiper.scrollbar,
				rtl = swiper.rtlTranslate;
			var $el = scrollbar.$el,
				dragSize = scrollbar.dragSize,
				trackSize = scrollbar.trackSize,
				dragStartPos = scrollbar.dragStartPos;
			var positionRatio;
			positionRatio = (scrollbar.getPointerPosition(e) - $el.offset()[swiper.isHorizontal() ? "left" : "top"] - (dragStartPos !== null ? dragStartPos : dragSize / 2)) / (trackSize - dragSize);
			positionRatio = Math.max(Math.min(positionRatio, 1), 0);

			if (rtl) {
				positionRatio = 1 - positionRatio;
			}

			var position = swiper.minTranslate() + (swiper.maxTranslate() - swiper.minTranslate()) * positionRatio;
			swiper.updateProgress(position);
			swiper.setTranslate(position);
			swiper.updateActiveIndex();
			swiper.updateSlidesClasses();
		},
		onDragStart: function onDragStart(e) {
			var swiper = this;
			var params = swiper.params.scrollbar;
			var scrollbar = swiper.scrollbar,
				$wrapperEl = swiper.$wrapperEl;
			var $el = scrollbar.$el,
				$dragEl = scrollbar.$dragEl;
			swiper.scrollbar.isTouched = true;
			swiper.scrollbar.dragStartPos = e.target === $dragEl[0] || e.target === $dragEl ? scrollbar.getPointerPosition(e) - e.target.getBoundingClientRect()[swiper.isHorizontal() ? "left" : "top"] : null;
			e.preventDefault();
			e.stopPropagation();
			$wrapperEl.transition(100);
			$dragEl.transition(100);
			scrollbar.setDragPosition(e);
			clearTimeout(swiper.scrollbar.dragTimeout);
			$el.transition(0);

			if (params.hide) {
				$el.css("opacity", 1);
			}

			if (swiper.params.cssMode) {
				swiper.$wrapperEl.css("scroll-snap-type", "none");
			}

			swiper.emit("scrollbarDragStart", e);
		},
		onDragMove: function onDragMove(e) {
			var swiper = this;
			var scrollbar = swiper.scrollbar,
				$wrapperEl = swiper.$wrapperEl;
			var $el = scrollbar.$el,
				$dragEl = scrollbar.$dragEl;
			if (!swiper.scrollbar.isTouched) return;
			if (e.preventDefault) e.preventDefault();
			else e.returnValue = false;
			scrollbar.setDragPosition(e);
			$wrapperEl.transition(0);
			$el.transition(0);
			$dragEl.transition(0);
			swiper.emit("scrollbarDragMove", e);
		},
		onDragEnd: function onDragEnd(e) {
			var swiper = this;
			var params = swiper.params.scrollbar;
			var scrollbar = swiper.scrollbar,
				$wrapperEl = swiper.$wrapperEl;
			var $el = scrollbar.$el;
			if (!swiper.scrollbar.isTouched) return;
			swiper.scrollbar.isTouched = false;

			if (swiper.params.cssMode) {
				swiper.$wrapperEl.css("scroll-snap-type", "");
				$wrapperEl.transition("");
			}

			if (params.hide) {
				clearTimeout(swiper.scrollbar.dragTimeout);
				swiper.scrollbar.dragTimeout = nextTick(function () {
					$el.css("opacity", 0);
					$el.transition(400);
				}, 1000);
			}

			swiper.emit("scrollbarDragEnd", e);

			if (params.snapOnRelease) {
				swiper.slideToClosest();
			}
		},
		enableDraggable: function enableDraggable() {
			var swiper = this;
			if (!swiper.params.scrollbar.el) return;
			var document = getDocument();
			var scrollbar = swiper.scrollbar,
				touchEventsTouch = swiper.touchEventsTouch,
				touchEventsDesktop = swiper.touchEventsDesktop,
				params = swiper.params,
				support = swiper.support;
			var $el = scrollbar.$el;
			var target = $el[0];
			var activeListener =
				support.passiveListener && params.passiveListeners
					? {
							passive: false,
							capture: false,
					  }
					: false;
			var passiveListener =
				support.passiveListener && params.passiveListeners
					? {
							passive: true,
							capture: false,
					  }
					: false;

			if (!support.touch) {
				target.addEventListener(touchEventsDesktop.start, swiper.scrollbar.onDragStart, activeListener);
				document.addEventListener(touchEventsDesktop.move, swiper.scrollbar.onDragMove, activeListener);
				document.addEventListener(touchEventsDesktop.end, swiper.scrollbar.onDragEnd, passiveListener);
			} else {
				target.addEventListener(touchEventsTouch.start, swiper.scrollbar.onDragStart, activeListener);
				target.addEventListener(touchEventsTouch.move, swiper.scrollbar.onDragMove, activeListener);
				target.addEventListener(touchEventsTouch.end, swiper.scrollbar.onDragEnd, passiveListener);
			}
		},
		disableDraggable: function disableDraggable() {
			var swiper = this;
			if (!swiper.params.scrollbar.el) return;
			var document = getDocument();
			var scrollbar = swiper.scrollbar,
				touchEventsTouch = swiper.touchEventsTouch,
				touchEventsDesktop = swiper.touchEventsDesktop,
				params = swiper.params,
				support = swiper.support;
			var $el = scrollbar.$el;
			var target = $el[0];
			var activeListener =
				support.passiveListener && params.passiveListeners
					? {
							passive: false,
							capture: false,
					  }
					: false;
			var passiveListener =
				support.passiveListener && params.passiveListeners
					? {
							passive: true,
							capture: false,
					  }
					: false;

			if (!support.touch) {
				target.removeEventListener(touchEventsDesktop.start, swiper.scrollbar.onDragStart, activeListener);
				document.removeEventListener(touchEventsDesktop.move, swiper.scrollbar.onDragMove, activeListener);
				document.removeEventListener(touchEventsDesktop.end, swiper.scrollbar.onDragEnd, passiveListener);
			} else {
				target.removeEventListener(touchEventsTouch.start, swiper.scrollbar.onDragStart, activeListener);
				target.removeEventListener(touchEventsTouch.move, swiper.scrollbar.onDragMove, activeListener);
				target.removeEventListener(touchEventsTouch.end, swiper.scrollbar.onDragEnd, passiveListener);
			}
		},
		init: function init() {
			var swiper = this;
			if (!swiper.params.scrollbar.el) return;
			var scrollbar = swiper.scrollbar,
				$swiperEl = swiper.$el;
			var params = swiper.params.scrollbar;
			var $el = $(params.el);

			if (swiper.params.uniqueNavElements && typeof params.el === "string" && $el.length > 1 && $swiperEl.find(params.el).length === 1) {
				$el = $swiperEl.find(params.el);
			}

			var $dragEl = $el.find("." + swiper.params.scrollbar.dragClass);

			if ($dragEl.length === 0) {
				$dragEl = $('<div class="' + swiper.params.scrollbar.dragClass + '"></div>');
				$el.append($dragEl);
			}

			extend$1(scrollbar, {
				$el: $el,
				el: $el[0],
				$dragEl: $dragEl,
				dragEl: $dragEl[0],
			});

			if (params.draggable) {
				scrollbar.enableDraggable();
			}
		},
		destroy: function destroy() {
			var swiper = this;
			swiper.scrollbar.disableDraggable();
		},
	};
	var Scrollbar$1 = {
		name: "scrollbar",
		params: {
			scrollbar: {
				el: null,
				dragSize: "auto",
				hide: false,
				draggable: false,
				snapOnRelease: true,
				lockClass: "swiper-scrollbar-lock",
				dragClass: "swiper-scrollbar-drag",
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				scrollbar: _extends(
					{
						isTouched: false,
						timeout: null,
						dragTimeout: null,
					},
					Scrollbar
				),
			});
		},
		on: {
			init: function init(swiper) {
				swiper.scrollbar.init();
				swiper.scrollbar.updateSize();
				swiper.scrollbar.setTranslate();
			},
			update: function update(swiper) {
				swiper.scrollbar.updateSize();
			},
			resize: function resize(swiper) {
				swiper.scrollbar.updateSize();
			},
			observerUpdate: function observerUpdate(swiper) {
				swiper.scrollbar.updateSize();
			},
			setTranslate: function setTranslate(swiper) {
				swiper.scrollbar.setTranslate();
			},
			setTransition: function setTransition(swiper, duration) {
				swiper.scrollbar.setTransition(duration);
			},
			destroy: function destroy(swiper) {
				swiper.scrollbar.destroy();
			},
		},
	};

	var Parallax = {
		setTransform: function setTransform(el, progress) {
			var swiper = this;
			var rtl = swiper.rtl;
			var $el = $(el);
			var rtlFactor = rtl ? -1 : 1;
			var p = $el.attr("data-swiper-parallax") || "0";
			var x = $el.attr("data-swiper-parallax-x");
			var y = $el.attr("data-swiper-parallax-y");
			var scale = $el.attr("data-swiper-parallax-scale");
			var opacity = $el.attr("data-swiper-parallax-opacity");

			if (x || y) {
				x = x || "0";
				y = y || "0";
			} else if (swiper.isHorizontal()) {
				x = p;
				y = "0";
			} else {
				y = p;
				x = "0";
			}

			if (x.indexOf("%") >= 0) {
				x = parseInt(x, 10) * progress * rtlFactor + "%";
			} else {
				x = x * progress * rtlFactor + "px";
			}

			if (y.indexOf("%") >= 0) {
				y = parseInt(y, 10) * progress + "%";
			} else {
				y = y * progress + "px";
			}

			if (typeof opacity !== "undefined" && opacity !== null) {
				var currentOpacity = opacity - (opacity - 1) * (1 - Math.abs(progress));
				$el[0].style.opacity = currentOpacity;
			}

			if (typeof scale === "undefined" || scale === null) {
				$el.transform("translate3d(" + x + ", " + y + ", 0px)");
			} else {
				var currentScale = scale - (scale - 1) * (1 - Math.abs(progress));
				$el.transform("translate3d(" + x + ", " + y + ", 0px) scale(" + currentScale + ")");
			}
		},
		setTranslate: function setTranslate() {
			var swiper = this;
			var $el = swiper.$el,
				slides = swiper.slides,
				progress = swiper.progress,
				snapGrid = swiper.snapGrid;
			$el.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]").each(function (el) {
				swiper.parallax.setTransform(el, progress);
			});
			slides.each(function (slideEl, slideIndex) {
				var slideProgress = slideEl.progress;

				if (swiper.params.slidesPerGroup > 1 && swiper.params.slidesPerView !== "auto") {
					slideProgress += Math.ceil(slideIndex / 2) - progress * (snapGrid.length - 1);
				}

				slideProgress = Math.min(Math.max(slideProgress, -1), 1);
				$(slideEl)
					.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]")
					.each(function (el) {
						swiper.parallax.setTransform(el, slideProgress);
					});
			});
		},
		setTransition: function setTransition(duration) {
			if (duration === void 0) {
				duration = this.params.speed;
			}

			var swiper = this;
			var $el = swiper.$el;
			$el.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]").each(function (parallaxEl) {
				var $parallaxEl = $(parallaxEl);
				var parallaxDuration = parseInt($parallaxEl.attr("data-swiper-parallax-duration"), 10) || duration;
				if (duration === 0) parallaxDuration = 0;
				$parallaxEl.transition(parallaxDuration);
			});
		},
	};
	var Parallax$1 = {
		name: "parallax",
		params: {
			parallax: {
				enabled: false,
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				parallax: _extends({}, Parallax),
			});
		},
		on: {
			beforeInit: function beforeInit(swiper) {
				if (!swiper.params.parallax.enabled) return;
				swiper.params.watchSlidesProgress = true;
				swiper.originalParams.watchSlidesProgress = true;
			},
			init: function init(swiper) {
				if (!swiper.params.parallax.enabled) return;
				swiper.parallax.setTranslate();
			},
			setTranslate: function setTranslate(swiper) {
				if (!swiper.params.parallax.enabled) return;
				swiper.parallax.setTranslate();
			},
			setTransition: function setTransition(swiper, duration) {
				if (!swiper.params.parallax.enabled) return;
				swiper.parallax.setTransition(duration);
			},
		},
	};

	var Zoom = {
		// Calc Scale From Multi-touches
		getDistanceBetweenTouches: function getDistanceBetweenTouches(e) {
			if (e.targetTouches.length < 2) return 1;
			var x1 = e.targetTouches[0].pageX;
			var y1 = e.targetTouches[0].pageY;
			var x2 = e.targetTouches[1].pageX;
			var y2 = e.targetTouches[1].pageY;
			var distance = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
			return distance;
		},
		// Events
		onGestureStart: function onGestureStart(e) {
			var swiper = this;
			var support = swiper.support;
			var params = swiper.params.zoom;
			var zoom = swiper.zoom;
			var gesture = zoom.gesture;
			zoom.fakeGestureTouched = false;
			zoom.fakeGestureMoved = false;

			if (!support.gestures) {
				if (e.type !== "touchstart" || (e.type === "touchstart" && e.targetTouches.length < 2)) {
					return;
				}

				zoom.fakeGestureTouched = true;
				gesture.scaleStart = Zoom.getDistanceBetweenTouches(e);
			}

			if (!gesture.$slideEl || !gesture.$slideEl.length) {
				gesture.$slideEl = $(e.target).closest("." + swiper.params.slideClass);
				if (gesture.$slideEl.length === 0) gesture.$slideEl = swiper.slides.eq(swiper.activeIndex);
				gesture.$imageEl = gesture.$slideEl.find("img, svg, canvas, picture, .swiper-zoom-target");
				gesture.$imageWrapEl = gesture.$imageEl.parent("." + params.containerClass);
				gesture.maxRatio = gesture.$imageWrapEl.attr("data-swiper-zoom") || params.maxRatio;

				if (gesture.$imageWrapEl.length === 0) {
					gesture.$imageEl = undefined;
					return;
				}
			}

			if (gesture.$imageEl) {
				gesture.$imageEl.transition(0);
			}

			swiper.zoom.isScaling = true;
		},
		onGestureChange: function onGestureChange(e) {
			var swiper = this;
			var support = swiper.support;
			var params = swiper.params.zoom;
			var zoom = swiper.zoom;
			var gesture = zoom.gesture;

			if (!support.gestures) {
				if (e.type !== "touchmove" || (e.type === "touchmove" && e.targetTouches.length < 2)) {
					return;
				}

				zoom.fakeGestureMoved = true;
				gesture.scaleMove = Zoom.getDistanceBetweenTouches(e);
			}

			if (!gesture.$imageEl || gesture.$imageEl.length === 0) {
				if (e.type === "gesturechange") zoom.onGestureStart(e);
				return;
			}

			if (support.gestures) {
				zoom.scale = e.scale * zoom.currentScale;
			} else {
				zoom.scale = (gesture.scaleMove / gesture.scaleStart) * zoom.currentScale;
			}

			if (zoom.scale > gesture.maxRatio) {
				zoom.scale = gesture.maxRatio - 1 + Math.pow(zoom.scale - gesture.maxRatio + 1, 0.5);
			}

			if (zoom.scale < params.minRatio) {
				zoom.scale = params.minRatio + 1 - Math.pow(params.minRatio - zoom.scale + 1, 0.5);
			}

			gesture.$imageEl.transform("translate3d(0,0,0) scale(" + zoom.scale + ")");
		},
		onGestureEnd: function onGestureEnd(e) {
			var swiper = this;
			var device = swiper.device;
			var support = swiper.support;
			var params = swiper.params.zoom;
			var zoom = swiper.zoom;
			var gesture = zoom.gesture;

			if (!support.gestures) {
				if (!zoom.fakeGestureTouched || !zoom.fakeGestureMoved) {
					return;
				}

				if (e.type !== "touchend" || (e.type === "touchend" && e.changedTouches.length < 2 && !device.android)) {
					return;
				}

				zoom.fakeGestureTouched = false;
				zoom.fakeGestureMoved = false;
			}

			if (!gesture.$imageEl || gesture.$imageEl.length === 0) return;
			zoom.scale = Math.max(Math.min(zoom.scale, gesture.maxRatio), params.minRatio);
			gesture.$imageEl.transition(swiper.params.speed).transform("translate3d(0,0,0) scale(" + zoom.scale + ")");
			zoom.currentScale = zoom.scale;
			zoom.isScaling = false;
			if (zoom.scale === 1) gesture.$slideEl = undefined;
		},
		onTouchStart: function onTouchStart(e) {
			var swiper = this;
			var device = swiper.device;
			var zoom = swiper.zoom;
			var gesture = zoom.gesture,
				image = zoom.image;
			if (!gesture.$imageEl || gesture.$imageEl.length === 0) return;
			if (image.isTouched) return;
			if (device.android && e.cancelable) e.preventDefault();
			image.isTouched = true;
			image.touchesStart.x = e.type === "touchstart" ? e.targetTouches[0].pageX : e.pageX;
			image.touchesStart.y = e.type === "touchstart" ? e.targetTouches[0].pageY : e.pageY;
		},
		onTouchMove: function onTouchMove(e) {
			var swiper = this;
			var zoom = swiper.zoom;
			var gesture = zoom.gesture,
				image = zoom.image,
				velocity = zoom.velocity;
			if (!gesture.$imageEl || gesture.$imageEl.length === 0) return;
			swiper.allowClick = false;
			if (!image.isTouched || !gesture.$slideEl) return;

			if (!image.isMoved) {
				image.width = gesture.$imageEl[0].offsetWidth;
				image.height = gesture.$imageEl[0].offsetHeight;
				image.startX = getTranslate(gesture.$imageWrapEl[0], "x") || 0;
				image.startY = getTranslate(gesture.$imageWrapEl[0], "y") || 0;
				gesture.slideWidth = gesture.$slideEl[0].offsetWidth;
				gesture.slideHeight = gesture.$slideEl[0].offsetHeight;
				gesture.$imageWrapEl.transition(0);

				if (swiper.rtl) {
					image.startX = -image.startX;
					image.startY = -image.startY;
				}
			} // Define if we need image drag

			var scaledWidth = image.width * zoom.scale;
			var scaledHeight = image.height * zoom.scale;
			if (scaledWidth < gesture.slideWidth && scaledHeight < gesture.slideHeight) return;
			image.minX = Math.min(gesture.slideWidth / 2 - scaledWidth / 2, 0);
			image.maxX = -image.minX;
			image.minY = Math.min(gesture.slideHeight / 2 - scaledHeight / 2, 0);
			image.maxY = -image.minY;
			image.touchesCurrent.x = e.type === "touchmove" ? e.targetTouches[0].pageX : e.pageX;
			image.touchesCurrent.y = e.type === "touchmove" ? e.targetTouches[0].pageY : e.pageY;

			if (!image.isMoved && !zoom.isScaling) {
				if (swiper.isHorizontal() && ((Math.floor(image.minX) === Math.floor(image.startX) && image.touchesCurrent.x < image.touchesStart.x) || (Math.floor(image.maxX) === Math.floor(image.startX) && image.touchesCurrent.x > image.touchesStart.x))) {
					image.isTouched = false;
					return;
				}

				if (!swiper.isHorizontal() && ((Math.floor(image.minY) === Math.floor(image.startY) && image.touchesCurrent.y < image.touchesStart.y) || (Math.floor(image.maxY) === Math.floor(image.startY) && image.touchesCurrent.y > image.touchesStart.y))) {
					image.isTouched = false;
					return;
				}
			}

			if (e.cancelable) {
				e.preventDefault();
			}

			e.stopPropagation();
			image.isMoved = true;
			image.currentX = image.touchesCurrent.x - image.touchesStart.x + image.startX;
			image.currentY = image.touchesCurrent.y - image.touchesStart.y + image.startY;

			if (image.currentX < image.minX) {
				image.currentX = image.minX + 1 - Math.pow(image.minX - image.currentX + 1, 0.8);
			}

			if (image.currentX > image.maxX) {
				image.currentX = image.maxX - 1 + Math.pow(image.currentX - image.maxX + 1, 0.8);
			}

			if (image.currentY < image.minY) {
				image.currentY = image.minY + 1 - Math.pow(image.minY - image.currentY + 1, 0.8);
			}

			if (image.currentY > image.maxY) {
				image.currentY = image.maxY - 1 + Math.pow(image.currentY - image.maxY + 1, 0.8);
			} // Velocity

			if (!velocity.prevPositionX) velocity.prevPositionX = image.touchesCurrent.x;
			if (!velocity.prevPositionY) velocity.prevPositionY = image.touchesCurrent.y;
			if (!velocity.prevTime) velocity.prevTime = Date.now();
			velocity.x = (image.touchesCurrent.x - velocity.prevPositionX) / (Date.now() - velocity.prevTime) / 2;
			velocity.y = (image.touchesCurrent.y - velocity.prevPositionY) / (Date.now() - velocity.prevTime) / 2;
			if (Math.abs(image.touchesCurrent.x - velocity.prevPositionX) < 2) velocity.x = 0;
			if (Math.abs(image.touchesCurrent.y - velocity.prevPositionY) < 2) velocity.y = 0;
			velocity.prevPositionX = image.touchesCurrent.x;
			velocity.prevPositionY = image.touchesCurrent.y;
			velocity.prevTime = Date.now();
			gesture.$imageWrapEl.transform("translate3d(" + image.currentX + "px, " + image.currentY + "px,0)");
		},
		onTouchEnd: function onTouchEnd() {
			var swiper = this;
			var zoom = swiper.zoom;
			var gesture = zoom.gesture,
				image = zoom.image,
				velocity = zoom.velocity;
			if (!gesture.$imageEl || gesture.$imageEl.length === 0) return;

			if (!image.isTouched || !image.isMoved) {
				image.isTouched = false;
				image.isMoved = false;
				return;
			}

			image.isTouched = false;
			image.isMoved = false;
			var momentumDurationX = 300;
			var momentumDurationY = 300;
			var momentumDistanceX = velocity.x * momentumDurationX;
			var newPositionX = image.currentX + momentumDistanceX;
			var momentumDistanceY = velocity.y * momentumDurationY;
			var newPositionY = image.currentY + momentumDistanceY; // Fix duration

			if (velocity.x !== 0) momentumDurationX = Math.abs((newPositionX - image.currentX) / velocity.x);
			if (velocity.y !== 0) momentumDurationY = Math.abs((newPositionY - image.currentY) / velocity.y);
			var momentumDuration = Math.max(momentumDurationX, momentumDurationY);
			image.currentX = newPositionX;
			image.currentY = newPositionY; // Define if we need image drag

			var scaledWidth = image.width * zoom.scale;
			var scaledHeight = image.height * zoom.scale;
			image.minX = Math.min(gesture.slideWidth / 2 - scaledWidth / 2, 0);
			image.maxX = -image.minX;
			image.minY = Math.min(gesture.slideHeight / 2 - scaledHeight / 2, 0);
			image.maxY = -image.minY;
			image.currentX = Math.max(Math.min(image.currentX, image.maxX), image.minX);
			image.currentY = Math.max(Math.min(image.currentY, image.maxY), image.minY);
			gesture.$imageWrapEl.transition(momentumDuration).transform("translate3d(" + image.currentX + "px, " + image.currentY + "px,0)");
		},
		onTransitionEnd: function onTransitionEnd() {
			var swiper = this;
			var zoom = swiper.zoom;
			var gesture = zoom.gesture;

			if (gesture.$slideEl && swiper.previousIndex !== swiper.activeIndex) {
				if (gesture.$imageEl) {
					gesture.$imageEl.transform("translate3d(0,0,0) scale(1)");
				}

				if (gesture.$imageWrapEl) {
					gesture.$imageWrapEl.transform("translate3d(0,0,0)");
				}

				zoom.scale = 1;
				zoom.currentScale = 1;
				gesture.$slideEl = undefined;
				gesture.$imageEl = undefined;
				gesture.$imageWrapEl = undefined;
			}
		},
		// Toggle Zoom
		toggle: function toggle(e) {
			var swiper = this;
			var zoom = swiper.zoom;

			if (zoom.scale && zoom.scale !== 1) {
				// Zoom Out
				zoom.out();
			} else {
				// Zoom In
				zoom.in(e);
			}
		},
		in: function _in(e) {
			var swiper = this;
			var zoom = swiper.zoom;
			var params = swiper.params.zoom;
			var gesture = zoom.gesture,
				image = zoom.image;

			if (!gesture.$slideEl) {
				if (swiper.params.virtual && swiper.params.virtual.enabled && swiper.virtual) {
					gesture.$slideEl = swiper.$wrapperEl.children("." + swiper.params.slideActiveClass);
				} else {
					gesture.$slideEl = swiper.slides.eq(swiper.activeIndex);
				}

				gesture.$imageEl = gesture.$slideEl.find("img, svg, canvas, picture, .swiper-zoom-target");
				gesture.$imageWrapEl = gesture.$imageEl.parent("." + params.containerClass);
			}

			if (!gesture.$imageEl || gesture.$imageEl.length === 0) return;
			gesture.$slideEl.addClass("" + params.zoomedSlideClass);
			var touchX;
			var touchY;
			var offsetX;
			var offsetY;
			var diffX;
			var diffY;
			var translateX;
			var translateY;
			var imageWidth;
			var imageHeight;
			var scaledWidth;
			var scaledHeight;
			var translateMinX;
			var translateMinY;
			var translateMaxX;
			var translateMaxY;
			var slideWidth;
			var slideHeight;

			if (typeof image.touchesStart.x === "undefined" && e) {
				touchX = e.type === "touchend" ? e.changedTouches[0].pageX : e.pageX;
				touchY = e.type === "touchend" ? e.changedTouches[0].pageY : e.pageY;
			} else {
				touchX = image.touchesStart.x;
				touchY = image.touchesStart.y;
			}

			zoom.scale = gesture.$imageWrapEl.attr("data-swiper-zoom") || params.maxRatio;
			zoom.currentScale = gesture.$imageWrapEl.attr("data-swiper-zoom") || params.maxRatio;

			if (e) {
				slideWidth = gesture.$slideEl[0].offsetWidth;
				slideHeight = gesture.$slideEl[0].offsetHeight;
				offsetX = gesture.$slideEl.offset().left;
				offsetY = gesture.$slideEl.offset().top;
				diffX = offsetX + slideWidth / 2 - touchX;
				diffY = offsetY + slideHeight / 2 - touchY;
				imageWidth = gesture.$imageEl[0].offsetWidth;
				imageHeight = gesture.$imageEl[0].offsetHeight;
				scaledWidth = imageWidth * zoom.scale;
				scaledHeight = imageHeight * zoom.scale;
				translateMinX = Math.min(slideWidth / 2 - scaledWidth / 2, 0);
				translateMinY = Math.min(slideHeight / 2 - scaledHeight / 2, 0);
				translateMaxX = -translateMinX;
				translateMaxY = -translateMinY;
				translateX = diffX * zoom.scale;
				translateY = diffY * zoom.scale;

				if (translateX < translateMinX) {
					translateX = translateMinX;
				}

				if (translateX > translateMaxX) {
					translateX = translateMaxX;
				}

				if (translateY < translateMinY) {
					translateY = translateMinY;
				}

				if (translateY > translateMaxY) {
					translateY = translateMaxY;
				}
			} else {
				translateX = 0;
				translateY = 0;
			}

			gesture.$imageWrapEl.transition(300).transform("translate3d(" + translateX + "px, " + translateY + "px,0)");
			gesture.$imageEl.transition(300).transform("translate3d(0,0,0) scale(" + zoom.scale + ")");
		},
		out: function out() {
			var swiper = this;
			var zoom = swiper.zoom;
			var params = swiper.params.zoom;
			var gesture = zoom.gesture;

			if (!gesture.$slideEl) {
				if (swiper.params.virtual && swiper.params.virtual.enabled && swiper.virtual) {
					gesture.$slideEl = swiper.$wrapperEl.children("." + swiper.params.slideActiveClass);
				} else {
					gesture.$slideEl = swiper.slides.eq(swiper.activeIndex);
				}

				gesture.$imageEl = gesture.$slideEl.find("img, svg, canvas, picture, .swiper-zoom-target");
				gesture.$imageWrapEl = gesture.$imageEl.parent("." + params.containerClass);
			}

			if (!gesture.$imageEl || gesture.$imageEl.length === 0) return;
			zoom.scale = 1;
			zoom.currentScale = 1;
			gesture.$imageWrapEl.transition(300).transform("translate3d(0,0,0)");
			gesture.$imageEl.transition(300).transform("translate3d(0,0,0) scale(1)");
			gesture.$slideEl.removeClass("" + params.zoomedSlideClass);
			gesture.$slideEl = undefined;
		},
		toggleGestures: function toggleGestures(method) {
			var swiper = this;
			var zoom = swiper.zoom;
			var selector = zoom.slideSelector,
				passive = zoom.passiveListener;
			swiper.$wrapperEl[method]("gesturestart", selector, zoom.onGestureStart, passive);
			swiper.$wrapperEl[method]("gesturechange", selector, zoom.onGestureChange, passive);
			swiper.$wrapperEl[method]("gestureend", selector, zoom.onGestureEnd, passive);
		},
		enableGestures: function enableGestures() {
			if (this.zoom.gesturesEnabled) return;
			this.zoom.gesturesEnabled = true;
			this.zoom.toggleGestures("on");
		},
		disableGestures: function disableGestures() {
			if (!this.zoom.gesturesEnabled) return;
			this.zoom.gesturesEnabled = false;
			this.zoom.toggleGestures("off");
		},
		// Attach/Detach Events
		enable: function enable() {
			var swiper = this;
			var support = swiper.support;
			var zoom = swiper.zoom;
			if (zoom.enabled) return;
			zoom.enabled = true;
			var passiveListener =
				swiper.touchEvents.start === "touchstart" && support.passiveListener && swiper.params.passiveListeners
					? {
							passive: true,
							capture: false,
					  }
					: false;
			var activeListenerWithCapture = support.passiveListener
				? {
						passive: false,
						capture: true,
				  }
				: true;
			var slideSelector = "." + swiper.params.slideClass;
			swiper.zoom.passiveListener = passiveListener;
			swiper.zoom.slideSelector = slideSelector; // Scale image

			if (support.gestures) {
				swiper.$wrapperEl.on(swiper.touchEvents.start, swiper.zoom.enableGestures, passiveListener);
				swiper.$wrapperEl.on(swiper.touchEvents.end, swiper.zoom.disableGestures, passiveListener);
			} else if (swiper.touchEvents.start === "touchstart") {
				swiper.$wrapperEl.on(swiper.touchEvents.start, slideSelector, zoom.onGestureStart, passiveListener);
				swiper.$wrapperEl.on(swiper.touchEvents.move, slideSelector, zoom.onGestureChange, activeListenerWithCapture);
				swiper.$wrapperEl.on(swiper.touchEvents.end, slideSelector, zoom.onGestureEnd, passiveListener);

				if (swiper.touchEvents.cancel) {
					swiper.$wrapperEl.on(swiper.touchEvents.cancel, slideSelector, zoom.onGestureEnd, passiveListener);
				}
			} // Move image

			swiper.$wrapperEl.on(swiper.touchEvents.move, "." + swiper.params.zoom.containerClass, zoom.onTouchMove, activeListenerWithCapture);
		},
		disable: function disable() {
			var swiper = this;
			var zoom = swiper.zoom;
			if (!zoom.enabled) return;
			var support = swiper.support;
			swiper.zoom.enabled = false;
			var passiveListener =
				swiper.touchEvents.start === "touchstart" && support.passiveListener && swiper.params.passiveListeners
					? {
							passive: true,
							capture: false,
					  }
					: false;
			var activeListenerWithCapture = support.passiveListener
				? {
						passive: false,
						capture: true,
				  }
				: true;
			var slideSelector = "." + swiper.params.slideClass; // Scale image

			if (support.gestures) {
				swiper.$wrapperEl.off(swiper.touchEvents.start, swiper.zoom.enableGestures, passiveListener);
				swiper.$wrapperEl.off(swiper.touchEvents.end, swiper.zoom.disableGestures, passiveListener);
			} else if (swiper.touchEvents.start === "touchstart") {
				swiper.$wrapperEl.off(swiper.touchEvents.start, slideSelector, zoom.onGestureStart, passiveListener);
				swiper.$wrapperEl.off(swiper.touchEvents.move, slideSelector, zoom.onGestureChange, activeListenerWithCapture);
				swiper.$wrapperEl.off(swiper.touchEvents.end, slideSelector, zoom.onGestureEnd, passiveListener);

				if (swiper.touchEvents.cancel) {
					swiper.$wrapperEl.off(swiper.touchEvents.cancel, slideSelector, zoom.onGestureEnd, passiveListener);
				}
			} // Move image

			swiper.$wrapperEl.off(swiper.touchEvents.move, "." + swiper.params.zoom.containerClass, zoom.onTouchMove, activeListenerWithCapture);
		},
	};
	var Zoom$1 = {
		name: "zoom",
		params: {
			zoom: {
				enabled: false,
				maxRatio: 3,
				minRatio: 1,
				toggle: true,
				containerClass: "swiper-zoom-container",
				zoomedSlideClass: "swiper-slide-zoomed",
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				zoom: _extends(
					{
						enabled: false,
						scale: 1,
						currentScale: 1,
						isScaling: false,
						gesture: {
							$slideEl: undefined,
							slideWidth: undefined,
							slideHeight: undefined,
							$imageEl: undefined,
							$imageWrapEl: undefined,
							maxRatio: 3,
						},
						image: {
							isTouched: undefined,
							isMoved: undefined,
							currentX: undefined,
							currentY: undefined,
							minX: undefined,
							minY: undefined,
							maxX: undefined,
							maxY: undefined,
							width: undefined,
							height: undefined,
							startX: undefined,
							startY: undefined,
							touchesStart: {},
							touchesCurrent: {},
						},
						velocity: {
							x: undefined,
							y: undefined,
							prevPositionX: undefined,
							prevPositionY: undefined,
							prevTime: undefined,
						},
					},
					Zoom
				),
			});
			var scale = 1;
			Object.defineProperty(swiper.zoom, "scale", {
				get: function get() {
					return scale;
				},
				set: function set(value) {
					if (scale !== value) {
						var imageEl = swiper.zoom.gesture.$imageEl ? swiper.zoom.gesture.$imageEl[0] : undefined;
						var slideEl = swiper.zoom.gesture.$slideEl ? swiper.zoom.gesture.$slideEl[0] : undefined;
						swiper.emit("zoomChange", value, imageEl, slideEl);
					}

					scale = value;
				},
			});
		},
		on: {
			init: function init(swiper) {
				if (swiper.params.zoom.enabled) {
					swiper.zoom.enable();
				}
			},
			destroy: function destroy(swiper) {
				swiper.zoom.disable();
			},
			touchStart: function touchStart(swiper, e) {
				if (!swiper.zoom.enabled) return;
				swiper.zoom.onTouchStart(e);
			},
			touchEnd: function touchEnd(swiper, e) {
				if (!swiper.zoom.enabled) return;
				swiper.zoom.onTouchEnd(e);
			},
			doubleTap: function doubleTap(swiper, e) {
				if (swiper.params.zoom.enabled && swiper.zoom.enabled && swiper.params.zoom.toggle) {
					swiper.zoom.toggle(e);
				}
			},
			transitionEnd: function transitionEnd(swiper) {
				if (swiper.zoom.enabled && swiper.params.zoom.enabled) {
					swiper.zoom.onTransitionEnd();
				}
			},
			slideChange: function slideChange(swiper) {
				if (swiper.zoom.enabled && swiper.params.zoom.enabled && swiper.params.cssMode) {
					swiper.zoom.onTransitionEnd();
				}
			},
		},
	};

	var Lazy = {
		loadInSlide: function loadInSlide(index, loadInDuplicate) {
			if (loadInDuplicate === void 0) {
				loadInDuplicate = true;
			}

			var swiper = this;
			var params = swiper.params.lazy;
			if (typeof index === "undefined") return;
			if (swiper.slides.length === 0) return;
			var isVirtual = swiper.virtual && swiper.params.virtual.enabled;
			var $slideEl = isVirtual ? swiper.$wrapperEl.children("." + swiper.params.slideClass + '[data-swiper-slide-index="' + index + '"]') : swiper.slides.eq(index);
			var $images = $slideEl.find("." + params.elementClass + ":not(." + params.loadedClass + "):not(." + params.loadingClass + ")");

			if ($slideEl.hasClass(params.elementClass) && !$slideEl.hasClass(params.loadedClass) && !$slideEl.hasClass(params.loadingClass)) {
				$images.push($slideEl[0]);
			}

			if ($images.length === 0) return;
			$images.each(function (imageEl) {
				var $imageEl = $(imageEl);
				$imageEl.addClass(params.loadingClass);
				var background = $imageEl.attr("data-background");
				var src = $imageEl.attr("data-src");
				var srcset = $imageEl.attr("data-srcset");
				var sizes = $imageEl.attr("data-sizes");
				var $pictureEl = $imageEl.parent("picture");
				swiper.loadImage($imageEl[0], src || background, srcset, sizes, false, function () {
					if (typeof swiper === "undefined" || swiper === null || !swiper || (swiper && !swiper.params) || swiper.destroyed) return;

					if (background) {
						$imageEl.css("background-image", 'url("' + background + '")');
						$imageEl.removeAttr("data-background");
					} else {
						if (srcset) {
							$imageEl.attr("srcset", srcset);
							$imageEl.removeAttr("data-srcset");
						}

						if (sizes) {
							$imageEl.attr("sizes", sizes);
							$imageEl.removeAttr("data-sizes");
						}

						if ($pictureEl.length) {
							$pictureEl.children("source").each(function (sourceEl) {
								var $source = $(sourceEl);

								if ($source.attr("data-srcset")) {
									$source.attr("srcset", $source.attr("data-srcset"));
									$source.removeAttr("data-srcset");
								}
							});
						}

						if (src) {
							$imageEl.attr("src", src);
							$imageEl.removeAttr("data-src");
						}
					}

					$imageEl.addClass(params.loadedClass).removeClass(params.loadingClass);
					$slideEl.find("." + params.preloaderClass).remove();

					if (swiper.params.loop && loadInDuplicate) {
						var slideOriginalIndex = $slideEl.attr("data-swiper-slide-index");

						if ($slideEl.hasClass(swiper.params.slideDuplicateClass)) {
							var originalSlide = swiper.$wrapperEl.children('[data-swiper-slide-index="' + slideOriginalIndex + '"]:not(.' + swiper.params.slideDuplicateClass + ")");
							swiper.lazy.loadInSlide(originalSlide.index(), false);
						} else {
							var duplicatedSlide = swiper.$wrapperEl.children("." + swiper.params.slideDuplicateClass + '[data-swiper-slide-index="' + slideOriginalIndex + '"]');
							swiper.lazy.loadInSlide(duplicatedSlide.index(), false);
						}
					}

					swiper.emit("lazyImageReady", $slideEl[0], $imageEl[0]);

					if (swiper.params.autoHeight) {
						swiper.updateAutoHeight();
					}
				});
				swiper.emit("lazyImageLoad", $slideEl[0], $imageEl[0]);
			});
		},
		load: function load() {
			var swiper = this;
			var $wrapperEl = swiper.$wrapperEl,
				swiperParams = swiper.params,
				slides = swiper.slides,
				activeIndex = swiper.activeIndex;
			var isVirtual = swiper.virtual && swiperParams.virtual.enabled;
			var params = swiperParams.lazy;
			var slidesPerView = swiperParams.slidesPerView;

			if (slidesPerView === "auto") {
				slidesPerView = 0;
			}

			function slideExist(index) {
				if (isVirtual) {
					if ($wrapperEl.children("." + swiperParams.slideClass + '[data-swiper-slide-index="' + index + '"]').length) {
						return true;
					}
				} else if (slides[index]) return true;

				return false;
			}

			function slideIndex(slideEl) {
				if (isVirtual) {
					return $(slideEl).attr("data-swiper-slide-index");
				}

				return $(slideEl).index();
			}

			if (!swiper.lazy.initialImageLoaded) swiper.lazy.initialImageLoaded = true;

			if (swiper.params.watchSlidesVisibility) {
				$wrapperEl.children("." + swiperParams.slideVisibleClass).each(function (slideEl) {
					var index = isVirtual ? $(slideEl).attr("data-swiper-slide-index") : $(slideEl).index();
					swiper.lazy.loadInSlide(index);
				});
			} else if (slidesPerView > 1) {
				for (var i = activeIndex; i < activeIndex + slidesPerView; i += 1) {
					if (slideExist(i)) swiper.lazy.loadInSlide(i);
				}
			} else {
				swiper.lazy.loadInSlide(activeIndex);
			}

			if (params.loadPrevNext) {
				if (slidesPerView > 1 || (params.loadPrevNextAmount && params.loadPrevNextAmount > 1)) {
					var amount = params.loadPrevNextAmount;
					var spv = slidesPerView;
					var maxIndex = Math.min(activeIndex + spv + Math.max(amount, spv), slides.length);
					var minIndex = Math.max(activeIndex - Math.max(spv, amount), 0); // Next Slides

					for (var _i = activeIndex + slidesPerView; _i < maxIndex; _i += 1) {
						if (slideExist(_i)) swiper.lazy.loadInSlide(_i);
					} // Prev Slides

					for (var _i2 = minIndex; _i2 < activeIndex; _i2 += 1) {
						if (slideExist(_i2)) swiper.lazy.loadInSlide(_i2);
					}
				} else {
					var nextSlide = $wrapperEl.children("." + swiperParams.slideNextClass);
					if (nextSlide.length > 0) swiper.lazy.loadInSlide(slideIndex(nextSlide));
					var prevSlide = $wrapperEl.children("." + swiperParams.slidePrevClass);
					if (prevSlide.length > 0) swiper.lazy.loadInSlide(slideIndex(prevSlide));
				}
			}
		},
	};
	var Lazy$1 = {
		name: "lazy",
		params: {
			lazy: {
				enabled: false,
				loadPrevNext: false,
				loadPrevNextAmount: 1,
				loadOnTransitionStart: false,
				elementClass: "swiper-lazy",
				loadingClass: "swiper-lazy-loading",
				loadedClass: "swiper-lazy-loaded",
				preloaderClass: "swiper-lazy-preloader",
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				lazy: _extends(
					{
						initialImageLoaded: false,
					},
					Lazy
				),
			});
		},
		on: {
			beforeInit: function beforeInit(swiper) {
				if (swiper.params.lazy.enabled && swiper.params.preloadImages) {
					swiper.params.preloadImages = false;
				}
			},
			init: function init(swiper) {
				if (swiper.params.lazy.enabled && !swiper.params.loop && swiper.params.initialSlide === 0) {
					swiper.lazy.load();
				}
			},
			scroll: function scroll(swiper) {
				if (swiper.params.freeMode && !swiper.params.freeModeSticky) {
					swiper.lazy.load();
				}
			},
			resize: function resize(swiper) {
				if (swiper.params.lazy.enabled) {
					swiper.lazy.load();
				}
			},
			scrollbarDragMove: function scrollbarDragMove(swiper) {
				if (swiper.params.lazy.enabled) {
					swiper.lazy.load();
				}
			},
			transitionStart: function transitionStart(swiper) {
				if (swiper.params.lazy.enabled) {
					if (swiper.params.lazy.loadOnTransitionStart || (!swiper.params.lazy.loadOnTransitionStart && !swiper.lazy.initialImageLoaded)) {
						swiper.lazy.load();
					}
				}
			},
			transitionEnd: function transitionEnd(swiper) {
				if (swiper.params.lazy.enabled && !swiper.params.lazy.loadOnTransitionStart) {
					swiper.lazy.load();
				}
			},
			slideChange: function slideChange(swiper) {
				if (swiper.params.lazy.enabled && swiper.params.cssMode) {
					swiper.lazy.load();
				}
			},
		},
	};

	var Controller = {
		LinearSpline: function LinearSpline(x, y) {
			var binarySearch = (function search() {
				var maxIndex;
				var minIndex;
				var guess;
				return function (array, val) {
					minIndex = -1;
					maxIndex = array.length;

					while (maxIndex - minIndex > 1) {
						guess = (maxIndex + minIndex) >> 1;

						if (array[guess] <= val) {
							minIndex = guess;
						} else {
							maxIndex = guess;
						}
					}

					return maxIndex;
				};
			})();

			this.x = x;
			this.y = y;
			this.lastIndex = x.length - 1; // Given an x value (x2), return the expected y2 value:
			// (x1,y1) is the known point before given value,
			// (x3,y3) is the known point after given value.

			var i1;
			var i3;

			this.interpolate = function interpolate(x2) {
				if (!x2) return 0; // Get the indexes of x1 and x3 (the array indexes before and after given x2):

				i3 = binarySearch(this.x, x2);
				i1 = i3 - 1; // We have our indexes i1 & i3, so we can calculate already:
				// y2 := ((x2−x1) × (y3−y1)) ÷ (x3−x1) + y1

				return ((x2 - this.x[i1]) * (this.y[i3] - this.y[i1])) / (this.x[i3] - this.x[i1]) + this.y[i1];
			};

			return this;
		},
		// xxx: for now i will just save one spline function to to
		getInterpolateFunction: function getInterpolateFunction(c) {
			var swiper = this;

			if (!swiper.controller.spline) {
				swiper.controller.spline = swiper.params.loop ? new Controller.LinearSpline(swiper.slidesGrid, c.slidesGrid) : new Controller.LinearSpline(swiper.snapGrid, c.snapGrid);
			}
		},
		setTranslate: function setTranslate(_setTranslate, byController) {
			var swiper = this;
			var controlled = swiper.controller.control;
			var multiplier;
			var controlledTranslate;
			var Swiper = swiper.constructor;

			function setControlledTranslate(c) {
				// this will create an Interpolate function based on the snapGrids
				// x is the Grid of the scrolled scroller and y will be the controlled scroller
				// it makes sense to create this only once and recall it for the interpolation
				// the function does a lot of value caching for performance
				var translate = swiper.rtlTranslate ? -swiper.translate : swiper.translate;

				if (swiper.params.controller.by === "slide") {
					swiper.controller.getInterpolateFunction(c); // i am not sure why the values have to be multiplicated this way, tried to invert the snapGrid
					// but it did not work out

					controlledTranslate = -swiper.controller.spline.interpolate(-translate);
				}

				if (!controlledTranslate || swiper.params.controller.by === "container") {
					multiplier = (c.maxTranslate() - c.minTranslate()) / (swiper.maxTranslate() - swiper.minTranslate());
					controlledTranslate = (translate - swiper.minTranslate()) * multiplier + c.minTranslate();
				}

				if (swiper.params.controller.inverse) {
					controlledTranslate = c.maxTranslate() - controlledTranslate;
				}

				c.updateProgress(controlledTranslate);
				c.setTranslate(controlledTranslate, swiper);
				c.updateActiveIndex();
				c.updateSlidesClasses();
			}

			if (Array.isArray(controlled)) {
				for (var i = 0; i < controlled.length; i += 1) {
					if (controlled[i] !== byController && controlled[i] instanceof Swiper) {
						setControlledTranslate(controlled[i]);
					}
				}
			} else if (controlled instanceof Swiper && byController !== controlled) {
				setControlledTranslate(controlled);
			}
		},
		setTransition: function setTransition(duration, byController) {
			var swiper = this;
			var Swiper = swiper.constructor;
			var controlled = swiper.controller.control;
			var i;

			function setControlledTransition(c) {
				c.setTransition(duration, swiper);

				if (duration !== 0) {
					c.transitionStart();

					if (c.params.autoHeight) {
						nextTick(function () {
							c.updateAutoHeight();
						});
					}

					c.$wrapperEl.transitionEnd(function () {
						if (!controlled) return;

						if (c.params.loop && swiper.params.controller.by === "slide") {
							c.loopFix();
						}

						c.transitionEnd();
					});
				}
			}

			if (Array.isArray(controlled)) {
				for (i = 0; i < controlled.length; i += 1) {
					if (controlled[i] !== byController && controlled[i] instanceof Swiper) {
						setControlledTransition(controlled[i]);
					}
				}
			} else if (controlled instanceof Swiper && byController !== controlled) {
				setControlledTransition(controlled);
			}
		},
	};
	var Controller$1 = {
		name: "controller",
		params: {
			controller: {
				control: undefined,
				inverse: false,
				by: "slide", // or 'container'
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				controller: _extends(
					{
						control: swiper.params.controller.control,
					},
					Controller
				),
			});
		},
		on: {
			update: function update(swiper) {
				if (!swiper.controller.control) return;

				if (swiper.controller.spline) {
					swiper.controller.spline = undefined;
					delete swiper.controller.spline;
				}
			},
			resize: function resize(swiper) {
				if (!swiper.controller.control) return;

				if (swiper.controller.spline) {
					swiper.controller.spline = undefined;
					delete swiper.controller.spline;
				}
			},
			observerUpdate: function observerUpdate(swiper) {
				if (!swiper.controller.control) return;

				if (swiper.controller.spline) {
					swiper.controller.spline = undefined;
					delete swiper.controller.spline;
				}
			},
			setTranslate: function setTranslate(swiper, translate, byController) {
				if (!swiper.controller.control) return;
				swiper.controller.setTranslate(translate, byController);
			},
			setTransition: function setTransition(swiper, duration, byController) {
				if (!swiper.controller.control) return;
				swiper.controller.setTransition(duration, byController);
			},
		},
	};

	var A11y = {
		getRandomNumber: function getRandomNumber(size) {
			if (size === void 0) {
				size = 16;
			}

			var randomChar = function randomChar() {
				return Math.round(16 * Math.random()).toString(16);
			};

			return "x".repeat(size).replace(/x/g, randomChar);
		},
		makeElFocusable: function makeElFocusable($el) {
			$el.attr("tabIndex", "0");
			return $el;
		},
		makeElNotFocusable: function makeElNotFocusable($el) {
			$el.attr("tabIndex", "-1");
			return $el;
		},
		addElRole: function addElRole($el, role) {
			$el.attr("role", role);
			return $el;
		},
		addElRoleDescription: function addElRoleDescription($el, description) {
			$el.attr("aria-role-description", description);
			return $el;
		},
		addElControls: function addElControls($el, controls) {
			$el.attr("aria-controls", controls);
			return $el;
		},
		addElLabel: function addElLabel($el, label) {
			$el.attr("aria-label", label);
			return $el;
		},
		addElId: function addElId($el, id) {
			$el.attr("id", id);
			return $el;
		},
		addElLive: function addElLive($el, live) {
			$el.attr("aria-live", live);
			return $el;
		},
		disableEl: function disableEl($el) {
			$el.attr("aria-disabled", true);
			return $el;
		},
		enableEl: function enableEl($el) {
			$el.attr("aria-disabled", false);
			return $el;
		},
		onEnterKey: function onEnterKey(e) {
			var swiper = this;
			var params = swiper.params.a11y;
			if (e.keyCode !== 13) return;
			var $targetEl = $(e.target);

			if (swiper.navigation && swiper.navigation.$nextEl && $targetEl.is(swiper.navigation.$nextEl)) {
				if (!(swiper.isEnd && !swiper.params.loop)) {
					swiper.slideNext();
				}

				if (swiper.isEnd) {
					swiper.a11y.notify(params.lastSlideMessage);
				} else {
					swiper.a11y.notify(params.nextSlideMessage);
				}
			}

			if (swiper.navigation && swiper.navigation.$prevEl && $targetEl.is(swiper.navigation.$prevEl)) {
				if (!(swiper.isBeginning && !swiper.params.loop)) {
					swiper.slidePrev();
				}

				if (swiper.isBeginning) {
					swiper.a11y.notify(params.firstSlideMessage);
				} else {
					swiper.a11y.notify(params.prevSlideMessage);
				}
			}

			if (swiper.pagination && $targetEl.is("." + swiper.params.pagination.bulletClass)) {
				$targetEl[0].click();
			}
		},
		notify: function notify(message) {
			var swiper = this;
			var notification = swiper.a11y.liveRegion;
			if (notification.length === 0) return;
			notification.html("");
			notification.html(message);
		},
		updateNavigation: function updateNavigation() {
			var swiper = this;
			if (swiper.params.loop || !swiper.navigation) return;
			var _swiper$navigation = swiper.navigation,
				$nextEl = _swiper$navigation.$nextEl,
				$prevEl = _swiper$navigation.$prevEl;

			if ($prevEl && $prevEl.length > 0) {
				if (swiper.isBeginning) {
					swiper.a11y.disableEl($prevEl);
					swiper.a11y.makeElNotFocusable($prevEl);
				} else {
					swiper.a11y.enableEl($prevEl);
					swiper.a11y.makeElFocusable($prevEl);
				}
			}

			if ($nextEl && $nextEl.length > 0) {
				if (swiper.isEnd) {
					swiper.a11y.disableEl($nextEl);
					swiper.a11y.makeElNotFocusable($nextEl);
				} else {
					swiper.a11y.enableEl($nextEl);
					swiper.a11y.makeElFocusable($nextEl);
				}
			}
		},
		updatePagination: function updatePagination() {
			var swiper = this;
			var params = swiper.params.a11y;

			if (swiper.pagination && swiper.params.pagination.clickable && swiper.pagination.bullets && swiper.pagination.bullets.length) {
				swiper.pagination.bullets.each(function (bulletEl) {
					var $bulletEl = $(bulletEl);
					swiper.a11y.makeElFocusable($bulletEl);

					if (!swiper.params.pagination.renderBullet) {
						swiper.a11y.addElRole($bulletEl, "button");
						swiper.a11y.addElLabel($bulletEl, params.paginationBulletMessage.replace(/\{\{index\}\}/, $bulletEl.index() + 1));
					}
				});
			}
		},
		init: function init() {
			var swiper = this;
			var params = swiper.params.a11y;
			swiper.$el.append(swiper.a11y.liveRegion); // Container

			var $containerEl = swiper.$el;

			if (params.containerRoleDescriptionMessage) {
				swiper.a11y.addElRoleDescription($containerEl, params.containerRoleDescriptionMessage);
			}

			if (params.containerMessage) {
				swiper.a11y.addElLabel($containerEl, params.containerMessage);
			} // Wrapper

			var $wrapperEl = swiper.$wrapperEl;
			var wrapperId = $wrapperEl.attr("id") || "swiper-wrapper-" + swiper.a11y.getRandomNumber(16);
			var live;
			swiper.a11y.addElId($wrapperEl, wrapperId);

			if (swiper.params.autoplay && swiper.params.autoplay.enabled) {
				live = "off";
			} else {
				live = "polite";
			}

			swiper.a11y.addElLive($wrapperEl, live); // Slide

			if (params.itemRoleDescriptionMessage) {
				swiper.a11y.addElRoleDescription($(swiper.slides), params.itemRoleDescriptionMessage);
			}

			swiper.a11y.addElRole($(swiper.slides), "group");
			swiper.slides.each(function (slideEl) {
				var $slideEl = $(slideEl);
				swiper.a11y.addElLabel($slideEl, $slideEl.index() + 1 + " / " + swiper.slides.length);
			}); // Navigation

			var $nextEl;
			var $prevEl;

			if (swiper.navigation && swiper.navigation.$nextEl) {
				$nextEl = swiper.navigation.$nextEl;
			}

			if (swiper.navigation && swiper.navigation.$prevEl) {
				$prevEl = swiper.navigation.$prevEl;
			}

			if ($nextEl && $nextEl.length) {
				swiper.a11y.makeElFocusable($nextEl);

				if ($nextEl[0].tagName !== "BUTTON") {
					swiper.a11y.addElRole($nextEl, "button");
					$nextEl.on("keydown", swiper.a11y.onEnterKey);
				}

				swiper.a11y.addElLabel($nextEl, params.nextSlideMessage);
				swiper.a11y.addElControls($nextEl, wrapperId);
			}

			if ($prevEl && $prevEl.length) {
				swiper.a11y.makeElFocusable($prevEl);

				if ($prevEl[0].tagName !== "BUTTON") {
					swiper.a11y.addElRole($prevEl, "button");
					$prevEl.on("keydown", swiper.a11y.onEnterKey);
				}

				swiper.a11y.addElLabel($prevEl, params.prevSlideMessage);
				swiper.a11y.addElControls($prevEl, wrapperId);
			} // Pagination

			if (swiper.pagination && swiper.params.pagination.clickable && swiper.pagination.bullets && swiper.pagination.bullets.length) {
				swiper.pagination.$el.on("keydown", "." + swiper.params.pagination.bulletClass, swiper.a11y.onEnterKey);
			}
		},
		destroy: function destroy() {
			var swiper = this;
			if (swiper.a11y.liveRegion && swiper.a11y.liveRegion.length > 0) swiper.a11y.liveRegion.remove();
			var $nextEl;
			var $prevEl;

			if (swiper.navigation && swiper.navigation.$nextEl) {
				$nextEl = swiper.navigation.$nextEl;
			}

			if (swiper.navigation && swiper.navigation.$prevEl) {
				$prevEl = swiper.navigation.$prevEl;
			}

			if ($nextEl) {
				$nextEl.off("keydown", swiper.a11y.onEnterKey);
			}

			if ($prevEl) {
				$prevEl.off("keydown", swiper.a11y.onEnterKey);
			} // Pagination

			if (swiper.pagination && swiper.params.pagination.clickable && swiper.pagination.bullets && swiper.pagination.bullets.length) {
				swiper.pagination.$el.off("keydown", "." + swiper.params.pagination.bulletClass, swiper.a11y.onEnterKey);
			}
		},
	};
	var A11y$1 = {
		name: "a11y",
		params: {
			a11y: {
				enabled: true,
				notificationClass: "swiper-notification",
				prevSlideMessage: "Previous slide",
				nextSlideMessage: "Next slide",
				firstSlideMessage: "This is the first slide",
				lastSlideMessage: "This is the last slide",
				paginationBulletMessage: "Go to slide {{index}}",
				containerMessage: null,
				containerRoleDescriptionMessage: null,
				itemRoleDescriptionMessage: null,
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				a11y: _extends(
					_extends({}, A11y),
					{},
					{
						liveRegion: $('<span class="' + swiper.params.a11y.notificationClass + '" aria-live="assertive" aria-atomic="true"></span>'),
					}
				),
			});
		},
		on: {
			afterInit: function afterInit(swiper) {
				if (!swiper.params.a11y.enabled) return;
				swiper.a11y.init();
				swiper.a11y.updateNavigation();
			},
			toEdge: function toEdge(swiper) {
				if (!swiper.params.a11y.enabled) return;
				swiper.a11y.updateNavigation();
			},
			fromEdge: function fromEdge(swiper) {
				if (!swiper.params.a11y.enabled) return;
				swiper.a11y.updateNavigation();
			},
			paginationUpdate: function paginationUpdate(swiper) {
				if (!swiper.params.a11y.enabled) return;
				swiper.a11y.updatePagination();
			},
			destroy: function destroy(swiper) {
				if (!swiper.params.a11y.enabled) return;
				swiper.a11y.destroy();
			},
		},
	};

	var History = {
		init: function init() {
			var swiper = this;
			var window = getWindow();
			if (!swiper.params.history) return;

			if (!window.history || !window.history.pushState) {
				swiper.params.history.enabled = false;
				swiper.params.hashNavigation.enabled = true;
				return;
			}

			var history = swiper.history;
			history.initialized = true;
			history.paths = History.getPathValues(swiper.params.url);
			if (!history.paths.key && !history.paths.value) return;
			history.scrollToSlide(0, history.paths.value, swiper.params.runCallbacksOnInit);

			if (!swiper.params.history.replaceState) {
				window.addEventListener("popstate", swiper.history.setHistoryPopState);
			}
		},
		destroy: function destroy() {
			var swiper = this;
			var window = getWindow();

			if (!swiper.params.history.replaceState) {
				window.removeEventListener("popstate", swiper.history.setHistoryPopState);
			}
		},
		setHistoryPopState: function setHistoryPopState() {
			var swiper = this;
			swiper.history.paths = History.getPathValues(swiper.params.url);
			swiper.history.scrollToSlide(swiper.params.speed, swiper.history.paths.value, false);
		},
		getPathValues: function getPathValues(urlOverride) {
			var window = getWindow();
			var location;

			if (urlOverride) {
				location = new URL(urlOverride);
			} else {
				location = window.location;
			}

			var pathArray = location.pathname
				.slice(1)
				.split("/")
				.filter(function (part) {
					return part !== "";
				});
			var total = pathArray.length;
			var key = pathArray[total - 2];
			var value = pathArray[total - 1];
			return {
				key: key,
				value: value,
			};
		},
		setHistory: function setHistory(key, index) {
			var swiper = this;
			var window = getWindow();
			if (!swiper.history.initialized || !swiper.params.history.enabled) return;
			var location;

			if (swiper.params.url) {
				location = new URL(swiper.params.url);
			} else {
				location = window.location;
			}

			var slide = swiper.slides.eq(index);
			var value = History.slugify(slide.attr("data-history"));

			if (!location.pathname.includes(key)) {
				value = key + "/" + value;
			}

			var currentState = window.history.state;

			if (currentState && currentState.value === value) {
				return;
			}

			if (swiper.params.history.replaceState) {
				window.history.replaceState(
					{
						value: value,
					},
					null,
					value
				);
			} else {
				window.history.pushState(
					{
						value: value,
					},
					null,
					value
				);
			}
		},
		slugify: function slugify(text) {
			return text
				.toString()
				.replace(/\s+/g, "-")
				.replace(/[^\w-]+/g, "")
				.replace(/--+/g, "-")
				.replace(/^-+/, "")
				.replace(/-+$/, "");
		},
		scrollToSlide: function scrollToSlide(speed, value, runCallbacks) {
			var swiper = this;

			if (value) {
				for (var i = 0, length = swiper.slides.length; i < length; i += 1) {
					var slide = swiper.slides.eq(i);
					var slideHistory = History.slugify(slide.attr("data-history"));

					if (slideHistory === value && !slide.hasClass(swiper.params.slideDuplicateClass)) {
						var index = slide.index();
						swiper.slideTo(index, speed, runCallbacks);
					}
				}
			} else {
				swiper.slideTo(0, speed, runCallbacks);
			}
		},
	};
	var History$1 = {
		name: "history",
		params: {
			history: {
				enabled: false,
				replaceState: false,
				key: "slides",
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				history: _extends({}, History),
			});
		},
		on: {
			init: function init(swiper) {
				if (swiper.params.history.enabled) {
					swiper.history.init();
				}
			},
			destroy: function destroy(swiper) {
				if (swiper.params.history.enabled) {
					swiper.history.destroy();
				}
			},
			transitionEnd: function transitionEnd(swiper) {
				if (swiper.history.initialized) {
					swiper.history.setHistory(swiper.params.history.key, swiper.activeIndex);
				}
			},
			slideChange: function slideChange(swiper) {
				if (swiper.history.initialized && swiper.params.cssMode) {
					swiper.history.setHistory(swiper.params.history.key, swiper.activeIndex);
				}
			},
		},
	};

	var HashNavigation = {
		onHashCange: function onHashCange() {
			var swiper = this;
			var document = getDocument();
			swiper.emit("hashChange");
			var newHash = document.location.hash.replace("#", "");
			var activeSlideHash = swiper.slides.eq(swiper.activeIndex).attr("data-hash");

			if (newHash !== activeSlideHash) {
				var newIndex = swiper.$wrapperEl.children("." + swiper.params.slideClass + '[data-hash="' + newHash + '"]').index();
				if (typeof newIndex === "undefined") return;
				swiper.slideTo(newIndex);
			}
		},
		setHash: function setHash() {
			var swiper = this;
			var window = getWindow();
			var document = getDocument();
			if (!swiper.hashNavigation.initialized || !swiper.params.hashNavigation.enabled) return;

			if (swiper.params.hashNavigation.replaceState && window.history && window.history.replaceState) {
				window.history.replaceState(null, null, "#" + swiper.slides.eq(swiper.activeIndex).attr("data-hash") || "");
				swiper.emit("hashSet");
			} else {
				var slide = swiper.slides.eq(swiper.activeIndex);
				var hash = slide.attr("data-hash") || slide.attr("data-history");
				document.location.hash = hash || "";
				swiper.emit("hashSet");
			}
		},
		init: function init() {
			var swiper = this;
			var document = getDocument();
			var window = getWindow();
			if (!swiper.params.hashNavigation.enabled || (swiper.params.history && swiper.params.history.enabled)) return;
			swiper.hashNavigation.initialized = true;
			var hash = document.location.hash.replace("#", "");

			if (hash) {
				var speed = 0;

				for (var i = 0, length = swiper.slides.length; i < length; i += 1) {
					var slide = swiper.slides.eq(i);
					var slideHash = slide.attr("data-hash") || slide.attr("data-history");

					if (slideHash === hash && !slide.hasClass(swiper.params.slideDuplicateClass)) {
						var index = slide.index();
						swiper.slideTo(index, speed, swiper.params.runCallbacksOnInit, true);
					}
				}
			}

			if (swiper.params.hashNavigation.watchState) {
				$(window).on("hashchange", swiper.hashNavigation.onHashCange);
			}
		},
		destroy: function destroy() {
			var swiper = this;
			var window = getWindow();

			if (swiper.params.hashNavigation.watchState) {
				$(window).off("hashchange", swiper.hashNavigation.onHashCange);
			}
		},
	};
	var HashNavigation$1 = {
		name: "hash-navigation",
		params: {
			hashNavigation: {
				enabled: false,
				replaceState: false,
				watchState: false,
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				hashNavigation: _extends(
					{
						initialized: false,
					},
					HashNavigation
				),
			});
		},
		on: {
			init: function init(swiper) {
				if (swiper.params.hashNavigation.enabled) {
					swiper.hashNavigation.init();
				}
			},
			destroy: function destroy(swiper) {
				if (swiper.params.hashNavigation.enabled) {
					swiper.hashNavigation.destroy();
				}
			},
			transitionEnd: function transitionEnd(swiper) {
				if (swiper.hashNavigation.initialized) {
					swiper.hashNavigation.setHash();
				}
			},
			slideChange: function slideChange(swiper) {
				if (swiper.hashNavigation.initialized && swiper.params.cssMode) {
					swiper.hashNavigation.setHash();
				}
			},
		},
	};

	var Autoplay = {
		run: function run() {
			var swiper = this;
			var $activeSlideEl = swiper.slides.eq(swiper.activeIndex);
			var delay = swiper.params.autoplay.delay;

			if ($activeSlideEl.attr("data-swiper-autoplay")) {
				delay = $activeSlideEl.attr("data-swiper-autoplay") || swiper.params.autoplay.delay;
			}

			clearTimeout(swiper.autoplay.timeout);
			swiper.autoplay.timeout = nextTick(function () {
				var autoplayResult;

				if (swiper.params.autoplay.reverseDirection) {
					if (swiper.params.loop) {
						swiper.loopFix();
						autoplayResult = swiper.slidePrev(swiper.params.speed, true, true);
						swiper.emit("autoplay");
					} else if (!swiper.isBeginning) {
						autoplayResult = swiper.slidePrev(swiper.params.speed, true, true);
						swiper.emit("autoplay");
					} else if (!swiper.params.autoplay.stopOnLastSlide) {
						autoplayResult = swiper.slideTo(swiper.slides.length - 1, swiper.params.speed, true, true);
						swiper.emit("autoplay");
					} else {
						swiper.autoplay.stop();
					}
				} else if (swiper.params.loop) {
					swiper.loopFix();
					autoplayResult = swiper.slideNext(swiper.params.speed, true, true);
					swiper.emit("autoplay");
				} else if (!swiper.isEnd) {
					autoplayResult = swiper.slideNext(swiper.params.speed, true, true);
					swiper.emit("autoplay");
				} else if (!swiper.params.autoplay.stopOnLastSlide) {
					autoplayResult = swiper.slideTo(0, swiper.params.speed, true, true);
					swiper.emit("autoplay");
				} else {
					swiper.autoplay.stop();
				}

				if (swiper.params.cssMode && swiper.autoplay.running) swiper.autoplay.run();
				else if (autoplayResult === false) {
					swiper.autoplay.run();
				}
			}, delay);
		},
		start: function start() {
			var swiper = this;
			if (typeof swiper.autoplay.timeout !== "undefined") return false;
			if (swiper.autoplay.running) return false;
			swiper.autoplay.running = true;
			swiper.emit("autoplayStart");
			swiper.autoplay.run();
			return true;
		},
		stop: function stop() {
			var swiper = this;
			if (!swiper.autoplay.running) return false;
			if (typeof swiper.autoplay.timeout === "undefined") return false;

			if (swiper.autoplay.timeout) {
				clearTimeout(swiper.autoplay.timeout);
				swiper.autoplay.timeout = undefined;
			}

			swiper.autoplay.running = false;
			swiper.emit("autoplayStop");
			return true;
		},
		pause: function pause(speed) {
			var swiper = this;
			if (!swiper.autoplay.running) return;
			if (swiper.autoplay.paused) return;
			if (swiper.autoplay.timeout) clearTimeout(swiper.autoplay.timeout);
			swiper.autoplay.paused = true;

			if (speed === 0 || !swiper.params.autoplay.waitForTransition) {
				swiper.autoplay.paused = false;
				swiper.autoplay.run();
			} else {
				swiper.$wrapperEl[0].addEventListener("transitionend", swiper.autoplay.onTransitionEnd);
				swiper.$wrapperEl[0].addEventListener("webkitTransitionEnd", swiper.autoplay.onTransitionEnd);
			}
		},
		onVisibilityChange: function onVisibilityChange() {
			var swiper = this;
			var document = getDocument();

			if (document.visibilityState === "hidden" && swiper.autoplay.running) {
				swiper.autoplay.pause();
			}

			if (document.visibilityState === "visible" && swiper.autoplay.paused) {
				swiper.autoplay.run();
				swiper.autoplay.paused = false;
			}
		},
		onTransitionEnd: function onTransitionEnd(e) {
			var swiper = this;
			if (!swiper || swiper.destroyed || !swiper.$wrapperEl) return;
			if (e.target !== swiper.$wrapperEl[0]) return;
			swiper.$wrapperEl[0].removeEventListener("transitionend", swiper.autoplay.onTransitionEnd);
			swiper.$wrapperEl[0].removeEventListener("webkitTransitionEnd", swiper.autoplay.onTransitionEnd);
			swiper.autoplay.paused = false;

			if (!swiper.autoplay.running) {
				swiper.autoplay.stop();
			} else {
				swiper.autoplay.run();
			}
		},
	};
	var Autoplay$1 = {
		name: "autoplay",
		params: {
			autoplay: {
				enabled: false,
				delay: 3000,
				waitForTransition: true,
				disableOnInteraction: true,
				stopOnLastSlide: false,
				reverseDirection: false,
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				autoplay: _extends(
					_extends({}, Autoplay),
					{},
					{
						running: false,
						paused: false,
					}
				),
			});
		},
		on: {
			init: function init(swiper) {
				if (swiper.params.autoplay.enabled) {
					swiper.autoplay.start();
					var document = getDocument();
					document.addEventListener("visibilitychange", swiper.autoplay.onVisibilityChange);
				}
			},
			beforeTransitionStart: function beforeTransitionStart(swiper, speed, internal) {
				if (swiper.autoplay.running) {
					if (internal || !swiper.params.autoplay.disableOnInteraction) {
						swiper.autoplay.pause(speed);
					} else {
						swiper.autoplay.stop();
					}
				}
			},
			sliderFirstMove: function sliderFirstMove(swiper) {
				if (swiper.autoplay.running) {
					if (swiper.params.autoplay.disableOnInteraction) {
						swiper.autoplay.stop();
					} else {
						swiper.autoplay.pause();
					}
				}
			},
			touchEnd: function touchEnd(swiper) {
				if (swiper.params.cssMode && swiper.autoplay.paused && !swiper.params.autoplay.disableOnInteraction) {
					swiper.autoplay.run();
				}
			},
			destroy: function destroy(swiper) {
				if (swiper.autoplay.running) {
					swiper.autoplay.stop();
				}

				var document = getDocument();
				document.removeEventListener("visibilitychange", swiper.autoplay.onVisibilityChange);
			},
		},
	};

	var Fade = {
		setTranslate: function setTranslate() {
			var swiper = this;
			var slides = swiper.slides;

			for (var i = 0; i < slides.length; i += 1) {
				var $slideEl = swiper.slides.eq(i);
				var offset = $slideEl[0].swiperSlideOffset;
				var tx = -offset;
				if (!swiper.params.virtualTranslate) tx -= swiper.translate;
				var ty = 0;

				if (!swiper.isHorizontal()) {
					ty = tx;
					tx = 0;
				}

				var slideOpacity = swiper.params.fadeEffect.crossFade ? Math.max(1 - Math.abs($slideEl[0].progress), 0) : 1 + Math.min(Math.max($slideEl[0].progress, -1), 0);
				$slideEl
					.css({
						opacity: slideOpacity,
					})
					.transform("translate3d(" + tx + "px, " + ty + "px, 0px)");
			}
		},
		setTransition: function setTransition(duration) {
			var swiper = this;
			var slides = swiper.slides,
				$wrapperEl = swiper.$wrapperEl;
			slides.transition(duration);

			if (swiper.params.virtualTranslate && duration !== 0) {
				var eventTriggered = false;
				slides.transitionEnd(function () {
					if (eventTriggered) return;
					if (!swiper || swiper.destroyed) return;
					eventTriggered = true;
					swiper.animating = false;
					var triggerEvents = ["webkitTransitionEnd", "transitionend"];

					for (var i = 0; i < triggerEvents.length; i += 1) {
						$wrapperEl.trigger(triggerEvents[i]);
					}
				});
			}
		},
	};
	var EffectFade = {
		name: "effect-fade",
		params: {
			fadeEffect: {
				crossFade: false,
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				fadeEffect: _extends({}, Fade),
			});
		},
		on: {
			beforeInit: function beforeInit(swiper) {
				if (swiper.params.effect !== "fade") return;
				swiper.classNames.push(swiper.params.containerModifierClass + "fade");
				var overwriteParams = {
					slidesPerView: 1,
					slidesPerColumn: 1,
					slidesPerGroup: 1,
					watchSlidesProgress: true,
					spaceBetween: 0,
					virtualTranslate: true,
				};
				extend$1(swiper.params, overwriteParams);
				extend$1(swiper.originalParams, overwriteParams);
			},
			setTranslate: function setTranslate(swiper) {
				if (swiper.params.effect !== "fade") return;
				swiper.fadeEffect.setTranslate();
			},
			setTransition: function setTransition(swiper, duration) {
				if (swiper.params.effect !== "fade") return;
				swiper.fadeEffect.setTransition(duration);
			},
		},
	};

	var Cube = {
		setTranslate: function setTranslate() {
			var swiper = this;
			var $el = swiper.$el,
				$wrapperEl = swiper.$wrapperEl,
				slides = swiper.slides,
				swiperWidth = swiper.width,
				swiperHeight = swiper.height,
				rtl = swiper.rtlTranslate,
				swiperSize = swiper.size,
				browser = swiper.browser;
			var params = swiper.params.cubeEffect;
			var isHorizontal = swiper.isHorizontal();
			var isVirtual = swiper.virtual && swiper.params.virtual.enabled;
			var wrapperRotate = 0;
			var $cubeShadowEl;

			if (params.shadow) {
				if (isHorizontal) {
					$cubeShadowEl = $wrapperEl.find(".swiper-cube-shadow");

					if ($cubeShadowEl.length === 0) {
						$cubeShadowEl = $('<div class="swiper-cube-shadow"></div>');
						$wrapperEl.append($cubeShadowEl);
					}

					$cubeShadowEl.css({
						height: swiperWidth + "px",
					});
				} else {
					$cubeShadowEl = $el.find(".swiper-cube-shadow");

					if ($cubeShadowEl.length === 0) {
						$cubeShadowEl = $('<div class="swiper-cube-shadow"></div>');
						$el.append($cubeShadowEl);
					}
				}
			}

			for (var i = 0; i < slides.length; i += 1) {
				var $slideEl = slides.eq(i);
				var slideIndex = i;

				if (isVirtual) {
					slideIndex = parseInt($slideEl.attr("data-swiper-slide-index"), 10);
				}

				var slideAngle = slideIndex * 90;
				var round = Math.floor(slideAngle / 360);

				if (rtl) {
					slideAngle = -slideAngle;
					round = Math.floor(-slideAngle / 360);
				}

				var progress = Math.max(Math.min($slideEl[0].progress, 1), -1);
				var tx = 0;
				var ty = 0;
				var tz = 0;

				if (slideIndex % 4 === 0) {
					tx = -round * 4 * swiperSize;
					tz = 0;
				} else if ((slideIndex - 1) % 4 === 0) {
					tx = 0;
					tz = -round * 4 * swiperSize;
				} else if ((slideIndex - 2) % 4 === 0) {
					tx = swiperSize + round * 4 * swiperSize;
					tz = swiperSize;
				} else if ((slideIndex - 3) % 4 === 0) {
					tx = -swiperSize;
					tz = 3 * swiperSize + swiperSize * 4 * round;
				}

				if (rtl) {
					tx = -tx;
				}

				if (!isHorizontal) {
					ty = tx;
					tx = 0;
				}

				var transform = "rotateX(" + (isHorizontal ? 0 : -slideAngle) + "deg) rotateY(" + (isHorizontal ? slideAngle : 0) + "deg) translate3d(" + tx + "px, " + ty + "px, " + tz + "px)";

				if (progress <= 1 && progress > -1) {
					wrapperRotate = slideIndex * 90 + progress * 90;
					if (rtl) wrapperRotate = -slideIndex * 90 - progress * 90;
				}

				$slideEl.transform(transform);

				if (params.slideShadows) {
					// Set shadows
					var shadowBefore = isHorizontal ? $slideEl.find(".swiper-slide-shadow-left") : $slideEl.find(".swiper-slide-shadow-top");
					var shadowAfter = isHorizontal ? $slideEl.find(".swiper-slide-shadow-right") : $slideEl.find(".swiper-slide-shadow-bottom");

					if (shadowBefore.length === 0) {
						shadowBefore = $('<div class="swiper-slide-shadow-' + (isHorizontal ? "left" : "top") + '"></div>');
						$slideEl.append(shadowBefore);
					}

					if (shadowAfter.length === 0) {
						shadowAfter = $('<div class="swiper-slide-shadow-' + (isHorizontal ? "right" : "bottom") + '"></div>');
						$slideEl.append(shadowAfter);
					}

					if (shadowBefore.length) shadowBefore[0].style.opacity = Math.max(-progress, 0);
					if (shadowAfter.length) shadowAfter[0].style.opacity = Math.max(progress, 0);
				}
			}

			$wrapperEl.css({
				"-webkit-transform-origin": "50% 50% -" + swiperSize / 2 + "px",
				"-moz-transform-origin": "50% 50% -" + swiperSize / 2 + "px",
				"-ms-transform-origin": "50% 50% -" + swiperSize / 2 + "px",
				"transform-origin": "50% 50% -" + swiperSize / 2 + "px",
			});

			if (params.shadow) {
				if (isHorizontal) {
					$cubeShadowEl.transform("translate3d(0px, " + (swiperWidth / 2 + params.shadowOffset) + "px, " + -swiperWidth / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + params.shadowScale + ")");
				} else {
					var shadowAngle = Math.abs(wrapperRotate) - Math.floor(Math.abs(wrapperRotate) / 90) * 90;
					var multiplier = 1.5 - (Math.sin((shadowAngle * 2 * Math.PI) / 360) / 2 + Math.cos((shadowAngle * 2 * Math.PI) / 360) / 2);
					var scale1 = params.shadowScale;
					var scale2 = params.shadowScale / multiplier;
					var offset = params.shadowOffset;
					$cubeShadowEl.transform("scale3d(" + scale1 + ", 1, " + scale2 + ") translate3d(0px, " + (swiperHeight / 2 + offset) + "px, " + -swiperHeight / 2 / scale2 + "px) rotateX(-90deg)");
				}
			}

			var zFactor = browser.isSafari || browser.isWebView ? -swiperSize / 2 : 0;
			$wrapperEl.transform("translate3d(0px,0," + zFactor + "px) rotateX(" + (swiper.isHorizontal() ? 0 : wrapperRotate) + "deg) rotateY(" + (swiper.isHorizontal() ? -wrapperRotate : 0) + "deg)");
		},
		setTransition: function setTransition(duration) {
			var swiper = this;
			var $el = swiper.$el,
				slides = swiper.slides;
			slides.transition(duration).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(duration);

			if (swiper.params.cubeEffect.shadow && !swiper.isHorizontal()) {
				$el.find(".swiper-cube-shadow").transition(duration);
			}
		},
	};
	var EffectCube = {
		name: "effect-cube",
		params: {
			cubeEffect: {
				slideShadows: true,
				shadow: true,
				shadowOffset: 20,
				shadowScale: 0.94,
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				cubeEffect: _extends({}, Cube),
			});
		},
		on: {
			beforeInit: function beforeInit(swiper) {
				if (swiper.params.effect !== "cube") return;
				swiper.classNames.push(swiper.params.containerModifierClass + "cube");
				swiper.classNames.push(swiper.params.containerModifierClass + "3d");
				var overwriteParams = {
					slidesPerView: 1,
					slidesPerColumn: 1,
					slidesPerGroup: 1,
					watchSlidesProgress: true,
					resistanceRatio: 0,
					spaceBetween: 0,
					centeredSlides: false,
					virtualTranslate: true,
				};
				extend$1(swiper.params, overwriteParams);
				extend$1(swiper.originalParams, overwriteParams);
			},
			setTranslate: function setTranslate(swiper) {
				if (swiper.params.effect !== "cube") return;
				swiper.cubeEffect.setTranslate();
			},
			setTransition: function setTransition(swiper, duration) {
				if (swiper.params.effect !== "cube") return;
				swiper.cubeEffect.setTransition(duration);
			},
		},
	};

	var Flip = {
		setTranslate: function setTranslate() {
			var swiper = this;
			var slides = swiper.slides,
				rtl = swiper.rtlTranslate;

			for (var i = 0; i < slides.length; i += 1) {
				var $slideEl = slides.eq(i);
				var progress = $slideEl[0].progress;

				if (swiper.params.flipEffect.limitRotation) {
					progress = Math.max(Math.min($slideEl[0].progress, 1), -1);
				}

				var offset = $slideEl[0].swiperSlideOffset;
				var rotate = -180 * progress;
				var rotateY = rotate;
				var rotateX = 0;
				var tx = -offset;
				var ty = 0;

				if (!swiper.isHorizontal()) {
					ty = tx;
					tx = 0;
					rotateX = -rotateY;
					rotateY = 0;
				} else if (rtl) {
					rotateY = -rotateY;
				}

				$slideEl[0].style.zIndex = -Math.abs(Math.round(progress)) + slides.length;

				if (swiper.params.flipEffect.slideShadows) {
					// Set shadows
					var shadowBefore = swiper.isHorizontal() ? $slideEl.find(".swiper-slide-shadow-left") : $slideEl.find(".swiper-slide-shadow-top");
					var shadowAfter = swiper.isHorizontal() ? $slideEl.find(".swiper-slide-shadow-right") : $slideEl.find(".swiper-slide-shadow-bottom");

					if (shadowBefore.length === 0) {
						shadowBefore = $('<div class="swiper-slide-shadow-' + (swiper.isHorizontal() ? "left" : "top") + '"></div>');
						$slideEl.append(shadowBefore);
					}

					if (shadowAfter.length === 0) {
						shadowAfter = $('<div class="swiper-slide-shadow-' + (swiper.isHorizontal() ? "right" : "bottom") + '"></div>');
						$slideEl.append(shadowAfter);
					}

					if (shadowBefore.length) shadowBefore[0].style.opacity = Math.max(-progress, 0);
					if (shadowAfter.length) shadowAfter[0].style.opacity = Math.max(progress, 0);
				}

				$slideEl.transform("translate3d(" + tx + "px, " + ty + "px, 0px) rotateX(" + rotateX + "deg) rotateY(" + rotateY + "deg)");
			}
		},
		setTransition: function setTransition(duration) {
			var swiper = this;
			var slides = swiper.slides,
				activeIndex = swiper.activeIndex,
				$wrapperEl = swiper.$wrapperEl;
			slides.transition(duration).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(duration);

			if (swiper.params.virtualTranslate && duration !== 0) {
				var eventTriggered = false; // eslint-disable-next-line

				slides.eq(activeIndex).transitionEnd(function onTransitionEnd() {
					if (eventTriggered) return;
					if (!swiper || swiper.destroyed) return; // if (!$(this).hasClass(swiper.params.slideActiveClass)) return;

					eventTriggered = true;
					swiper.animating = false;
					var triggerEvents = ["webkitTransitionEnd", "transitionend"];

					for (var i = 0; i < triggerEvents.length; i += 1) {
						$wrapperEl.trigger(triggerEvents[i]);
					}
				});
			}
		},
	};
	var EffectFlip = {
		name: "effect-flip",
		params: {
			flipEffect: {
				slideShadows: true,
				limitRotation: true,
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				flipEffect: _extends({}, Flip),
			});
		},
		on: {
			beforeInit: function beforeInit(swiper) {
				if (swiper.params.effect !== "flip") return;
				swiper.classNames.push(swiper.params.containerModifierClass + "flip");
				swiper.classNames.push(swiper.params.containerModifierClass + "3d");
				var overwriteParams = {
					slidesPerView: 1,
					slidesPerColumn: 1,
					slidesPerGroup: 1,
					watchSlidesProgress: true,
					spaceBetween: 0,
					virtualTranslate: true,
				};
				extend$1(swiper.params, overwriteParams);
				extend$1(swiper.originalParams, overwriteParams);
			},
			setTranslate: function setTranslate(swiper) {
				if (swiper.params.effect !== "flip") return;
				swiper.flipEffect.setTranslate();
			},
			setTransition: function setTransition(swiper, duration) {
				if (swiper.params.effect !== "flip") return;
				swiper.flipEffect.setTransition(duration);
			},
		},
	};

	var Coverflow = {
		setTranslate: function setTranslate() {
			var swiper = this;
			var swiperWidth = swiper.width,
				swiperHeight = swiper.height,
				slides = swiper.slides,
				slidesSizesGrid = swiper.slidesSizesGrid;
			var params = swiper.params.coverflowEffect;
			var isHorizontal = swiper.isHorizontal();
			var transform = swiper.translate;
			var center = isHorizontal ? -transform + swiperWidth / 2 : -transform + swiperHeight / 2;
			var rotate = isHorizontal ? params.rotate : -params.rotate;
			var translate = params.depth; // Each slide offset from center

			for (var i = 0, length = slides.length; i < length; i += 1) {
				var $slideEl = slides.eq(i);
				var slideSize = slidesSizesGrid[i];
				var slideOffset = $slideEl[0].swiperSlideOffset;
				var offsetMultiplier = ((center - slideOffset - slideSize / 2) / slideSize) * params.modifier;
				var rotateY = isHorizontal ? rotate * offsetMultiplier : 0;
				var rotateX = isHorizontal ? 0 : rotate * offsetMultiplier; // var rotateZ = 0

				var translateZ = -translate * Math.abs(offsetMultiplier);
				var stretch = params.stretch; // Allow percentage to make a relative stretch for responsive sliders

				if (typeof stretch === "string" && stretch.indexOf("%") !== -1) {
					stretch = (parseFloat(params.stretch) / 100) * slideSize;
				}

				var translateY = isHorizontal ? 0 : stretch * offsetMultiplier;
				var translateX = isHorizontal ? stretch * offsetMultiplier : 0;
				var scale = 1 - (1 - params.scale) * Math.abs(offsetMultiplier); // Fix for ultra small values

				if (Math.abs(translateX) < 0.001) translateX = 0;
				if (Math.abs(translateY) < 0.001) translateY = 0;
				if (Math.abs(translateZ) < 0.001) translateZ = 0;
				if (Math.abs(rotateY) < 0.001) rotateY = 0;
				if (Math.abs(rotateX) < 0.001) rotateX = 0;
				if (Math.abs(scale) < 0.001) scale = 0;
				var slideTransform = "translate3d(" + translateX + "px," + translateY + "px," + translateZ + "px)  rotateX(" + rotateX + "deg) rotateY(" + rotateY + "deg) scale(" + scale + ")";
				$slideEl.transform(slideTransform);
				$slideEl[0].style.zIndex = -Math.abs(Math.round(offsetMultiplier)) + 1;

				if (params.slideShadows) {
					// Set shadows
					var $shadowBeforeEl = isHorizontal ? $slideEl.find(".swiper-slide-shadow-left") : $slideEl.find(".swiper-slide-shadow-top");
					var $shadowAfterEl = isHorizontal ? $slideEl.find(".swiper-slide-shadow-right") : $slideEl.find(".swiper-slide-shadow-bottom");

					if ($shadowBeforeEl.length === 0) {
						$shadowBeforeEl = $('<div class="swiper-slide-shadow-' + (isHorizontal ? "left" : "top") + '"></div>');
						$slideEl.append($shadowBeforeEl);
					}

					if ($shadowAfterEl.length === 0) {
						$shadowAfterEl = $('<div class="swiper-slide-shadow-' + (isHorizontal ? "right" : "bottom") + '"></div>');
						$slideEl.append($shadowAfterEl);
					}

					if ($shadowBeforeEl.length) $shadowBeforeEl[0].style.opacity = offsetMultiplier > 0 ? offsetMultiplier : 0;
					if ($shadowAfterEl.length) $shadowAfterEl[0].style.opacity = -offsetMultiplier > 0 ? -offsetMultiplier : 0;
				}
			}
		},
		setTransition: function setTransition(duration) {
			var swiper = this;
			swiper.slides.transition(duration).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(duration);
		},
	};
	var EffectCoverflow = {
		name: "effect-coverflow",
		params: {
			coverflowEffect: {
				rotate: 50,
				stretch: 0,
				depth: 100,
				scale: 1,
				modifier: 1,
				slideShadows: true,
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				coverflowEffect: _extends({}, Coverflow),
			});
		},
		on: {
			beforeInit: function beforeInit(swiper) {
				if (swiper.params.effect !== "coverflow") return;
				swiper.classNames.push(swiper.params.containerModifierClass + "coverflow");
				swiper.classNames.push(swiper.params.containerModifierClass + "3d");
				swiper.params.watchSlidesProgress = true;
				swiper.originalParams.watchSlidesProgress = true;
			},
			setTranslate: function setTranslate(swiper) {
				if (swiper.params.effect !== "coverflow") return;
				swiper.coverflowEffect.setTranslate();
			},
			setTransition: function setTransition(swiper, duration) {
				if (swiper.params.effect !== "coverflow") return;
				swiper.coverflowEffect.setTransition(duration);
			},
		},
	};

	var Thumbs = {
		init: function init() {
			var swiper = this;
			var thumbsParams = swiper.params.thumbs;
			if (swiper.thumbs.initialized) return false;
			swiper.thumbs.initialized = true;
			var SwiperClass = swiper.constructor;

			if (thumbsParams.swiper instanceof SwiperClass) {
				swiper.thumbs.swiper = thumbsParams.swiper;
				extend$1(swiper.thumbs.swiper.originalParams, {
					watchSlidesProgress: true,
					slideToClickedSlide: false,
				});
				extend$1(swiper.thumbs.swiper.params, {
					watchSlidesProgress: true,
					slideToClickedSlide: false,
				});
			} else if (isObject$1(thumbsParams.swiper)) {
				swiper.thumbs.swiper = new SwiperClass(
					extend$1({}, thumbsParams.swiper, {
						watchSlidesVisibility: true,
						watchSlidesProgress: true,
						slideToClickedSlide: false,
					})
				);
				swiper.thumbs.swiperCreated = true;
			}

			swiper.thumbs.swiper.$el.addClass(swiper.params.thumbs.thumbsContainerClass);
			swiper.thumbs.swiper.on("tap", swiper.thumbs.onThumbClick);
			return true;
		},
		onThumbClick: function onThumbClick() {
			var swiper = this;
			var thumbsSwiper = swiper.thumbs.swiper;
			if (!thumbsSwiper) return;
			var clickedIndex = thumbsSwiper.clickedIndex;
			var clickedSlide = thumbsSwiper.clickedSlide;
			if (clickedSlide && $(clickedSlide).hasClass(swiper.params.thumbs.slideThumbActiveClass)) return;
			if (typeof clickedIndex === "undefined" || clickedIndex === null) return;
			var slideToIndex;

			if (thumbsSwiper.params.loop) {
				slideToIndex = parseInt($(thumbsSwiper.clickedSlide).attr("data-swiper-slide-index"), 10);
			} else {
				slideToIndex = clickedIndex;
			}

			if (swiper.params.loop) {
				var currentIndex = swiper.activeIndex;

				if (swiper.slides.eq(currentIndex).hasClass(swiper.params.slideDuplicateClass)) {
					swiper.loopFix(); // eslint-disable-next-line

					swiper._clientLeft = swiper.$wrapperEl[0].clientLeft;
					currentIndex = swiper.activeIndex;
				}

				var prevIndex = swiper.slides
					.eq(currentIndex)
					.prevAll('[data-swiper-slide-index="' + slideToIndex + '"]')
					.eq(0)
					.index();
				var nextIndex = swiper.slides
					.eq(currentIndex)
					.nextAll('[data-swiper-slide-index="' + slideToIndex + '"]')
					.eq(0)
					.index();
				if (typeof prevIndex === "undefined") slideToIndex = nextIndex;
				else if (typeof nextIndex === "undefined") slideToIndex = prevIndex;
				else if (nextIndex - currentIndex < currentIndex - prevIndex) slideToIndex = nextIndex;
				else slideToIndex = prevIndex;
			}

			swiper.slideTo(slideToIndex);
		},
		update: function update(initial) {
			var swiper = this;
			var thumbsSwiper = swiper.thumbs.swiper;
			if (!thumbsSwiper) return;
			var slidesPerView = thumbsSwiper.params.slidesPerView === "auto" ? thumbsSwiper.slidesPerViewDynamic() : thumbsSwiper.params.slidesPerView;
			var autoScrollOffset = swiper.params.thumbs.autoScrollOffset;
			var useOffset = autoScrollOffset && !thumbsSwiper.params.loop;

			if (swiper.realIndex !== thumbsSwiper.realIndex || useOffset) {
				var currentThumbsIndex = thumbsSwiper.activeIndex;
				var newThumbsIndex;
				var direction;

				if (thumbsSwiper.params.loop) {
					if (thumbsSwiper.slides.eq(currentThumbsIndex).hasClass(thumbsSwiper.params.slideDuplicateClass)) {
						thumbsSwiper.loopFix(); // eslint-disable-next-line

						thumbsSwiper._clientLeft = thumbsSwiper.$wrapperEl[0].clientLeft;
						currentThumbsIndex = thumbsSwiper.activeIndex;
					} // Find actual thumbs index to slide to

					var prevThumbsIndex = thumbsSwiper.slides
						.eq(currentThumbsIndex)
						.prevAll('[data-swiper-slide-index="' + swiper.realIndex + '"]')
						.eq(0)
						.index();
					var nextThumbsIndex = thumbsSwiper.slides
						.eq(currentThumbsIndex)
						.nextAll('[data-swiper-slide-index="' + swiper.realIndex + '"]')
						.eq(0)
						.index();
					if (typeof prevThumbsIndex === "undefined") newThumbsIndex = nextThumbsIndex;
					else if (typeof nextThumbsIndex === "undefined") newThumbsIndex = prevThumbsIndex;
					else if (nextThumbsIndex - currentThumbsIndex === currentThumbsIndex - prevThumbsIndex) newThumbsIndex = currentThumbsIndex;
					else if (nextThumbsIndex - currentThumbsIndex < currentThumbsIndex - prevThumbsIndex) newThumbsIndex = nextThumbsIndex;
					else newThumbsIndex = prevThumbsIndex;
					direction = swiper.activeIndex > swiper.previousIndex ? "next" : "prev";
				} else {
					newThumbsIndex = swiper.realIndex;
					direction = newThumbsIndex > swiper.previousIndex ? "next" : "prev";
				}

				if (useOffset) {
					newThumbsIndex += direction === "next" ? autoScrollOffset : -1 * autoScrollOffset;
				}

				if (thumbsSwiper.visibleSlidesIndexes && thumbsSwiper.visibleSlidesIndexes.indexOf(newThumbsIndex) < 0) {
					if (thumbsSwiper.params.centeredSlides) {
						if (newThumbsIndex > currentThumbsIndex) {
							newThumbsIndex = newThumbsIndex - Math.floor(slidesPerView / 2) + 1;
						} else {
							newThumbsIndex = newThumbsIndex + Math.floor(slidesPerView / 2) - 1;
						}
					} else if (newThumbsIndex > currentThumbsIndex) {
						newThumbsIndex = newThumbsIndex - slidesPerView + 1;
					}

					thumbsSwiper.slideTo(newThumbsIndex, initial ? 0 : undefined);
				}
			} // Activate thumbs

			var thumbsToActivate = 1;
			var thumbActiveClass = swiper.params.thumbs.slideThumbActiveClass;

			if (swiper.params.slidesPerView > 1 && !swiper.params.centeredSlides) {
				thumbsToActivate = swiper.params.slidesPerView;
			}

			if (!swiper.params.thumbs.multipleActiveThumbs) {
				thumbsToActivate = 1;
			}

			thumbsToActivate = Math.floor(thumbsToActivate);
			thumbsSwiper.slides.removeClass(thumbActiveClass);

			if (thumbsSwiper.params.loop || (thumbsSwiper.params.virtual && thumbsSwiper.params.virtual.enabled)) {
				for (var i = 0; i < thumbsToActivate; i += 1) {
					thumbsSwiper.$wrapperEl.children('[data-swiper-slide-index="' + (swiper.realIndex + i) + '"]').addClass(thumbActiveClass);
				}
			} else {
				for (var _i = 0; _i < thumbsToActivate; _i += 1) {
					thumbsSwiper.slides.eq(swiper.realIndex + _i).addClass(thumbActiveClass);
				}
			}
		},
	};
	var Thumbs$1 = {
		name: "thumbs",
		params: {
			thumbs: {
				swiper: null,
				multipleActiveThumbs: true,
				autoScrollOffset: 0,
				slideThumbActiveClass: "swiper-slide-thumb-active",
				thumbsContainerClass: "swiper-container-thumbs",
			},
		},
		create: function create() {
			var swiper = this;
			bindModuleMethods(swiper, {
				thumbs: _extends(
					{
						swiper: null,
						initialized: false,
					},
					Thumbs
				),
			});
		},
		on: {
			beforeInit: function beforeInit(swiper) {
				var thumbs = swiper.params.thumbs;
				if (!thumbs || !thumbs.swiper) return;
				swiper.thumbs.init();
				swiper.thumbs.update(true);
			},
			slideChange: function slideChange(swiper) {
				if (!swiper.thumbs.swiper) return;
				swiper.thumbs.update();
			},
			update: function update(swiper) {
				if (!swiper.thumbs.swiper) return;
				swiper.thumbs.update();
			},
			resize: function resize(swiper) {
				if (!swiper.thumbs.swiper) return;
				swiper.thumbs.update();
			},
			observerUpdate: function observerUpdate(swiper) {
				if (!swiper.thumbs.swiper) return;
				swiper.thumbs.update();
			},
			setTransition: function setTransition(swiper, duration) {
				var thumbsSwiper = swiper.thumbs.swiper;
				if (!thumbsSwiper) return;
				thumbsSwiper.setTransition(duration);
			},
			beforeDestroy: function beforeDestroy(swiper) {
				var thumbsSwiper = swiper.thumbs.swiper;
				if (!thumbsSwiper) return;

				if (swiper.thumbs.swiperCreated && thumbsSwiper) {
					thumbsSwiper.destroy();
				}
			},
		},
	};

	// Swiper Class
	var components = [Virtual$1, Keyboard$1, Mousewheel$1, Navigation$1, Pagination$1, Scrollbar$1, Parallax$1, Zoom$1, Lazy$1, Controller$1, A11y$1, History$1, HashNavigation$1, Autoplay$1, EffectFade, EffectCube, EffectFlip, EffectCoverflow, Thumbs$1];
	Swiper.use(components);

	return Swiper;
});

/*!
 * jQuery JavaScript Library v3.2.1
 * https://jquery.com/
 *
 * Includes Sizzle.js
 * https://sizzlejs.com/
 *
 * Copyright JS Foundation and other contributors
 * Released under the MIT license
 * https://jquery.org/license
 *
 * Date: 2017-03-20T18:59Z
 */
!(function (a, b) {
	"use strict";
	"object" == typeof module && "object" == typeof module.exports
		? (module.exports = a.document
				? b(a, !0)
				: function (a) {
						if (!a.document) throw new Error("jQuery requires a window with a document");
						return b(a);
				  })
		: b(a);
})("undefined" != typeof window ? window : this, function (a, b) {
	"use strict";
	function c(a, b) {
		b = b || ca;
		var c = b.createElement("script");
		(c.text = a), b.head.appendChild(c).parentNode.removeChild(c);
	}
	function d(a) {
		var b = !!a && "length" in a && a.length,
			c = pa.type(a);
		return "function" !== c && !pa.isWindow(a) && ("array" === c || 0 === b || ("number" == typeof b && b > 0 && b - 1 in a));
	}
	function e(a, b) {
		return a.nodeName && a.nodeName.toLowerCase() === b.toLowerCase();
	}
	function f(a, b, c) {
		return pa.isFunction(b)
			? pa.grep(a, function (a, d) {
					return !!b.call(a, d, a) !== c;
			  })
			: b.nodeType
			? pa.grep(a, function (a) {
					return (a === b) !== c;
			  })
			: "string" != typeof b
			? pa.grep(a, function (a) {
					return ha.call(b, a) > -1 !== c;
			  })
			: za.test(b)
			? pa.filter(b, a, c)
			: ((b = pa.filter(b, a)),
			  pa.grep(a, function (a) {
					return ha.call(b, a) > -1 !== c && 1 === a.nodeType;
			  }));
	}
	function g(a, b) {
		for (; (a = a[b]) && 1 !== a.nodeType; );
		return a;
	}
	function h(a) {
		var b = {};
		return (
			pa.each(a.match(Ea) || [], function (a, c) {
				b[c] = !0;
			}),
			b
		);
	}
	function i(a) {
		return a;
	}
	function j(a) {
		throw a;
	}
	function k(a, b, c, d) {
		var e;
		try {
			a && pa.isFunction((e = a.promise)) ? e.call(a).done(b).fail(c) : a && pa.isFunction((e = a.then)) ? e.call(a, b, c) : b.apply(void 0, [a].slice(d));
		} catch (a) {
			c.apply(void 0, [a]);
		}
	}
	function l() {
		ca.removeEventListener("DOMContentLoaded", l), a.removeEventListener("load", l), pa.ready();
	}
	function m() {
		this.expando = pa.expando + m.uid++;
	}
	function n(a) {
		return "true" === a || ("false" !== a && ("null" === a ? null : a === +a + "" ? +a : La.test(a) ? JSON.parse(a) : a));
	}
	function o(a, b, c) {
		var d;
		if (void 0 === c && 1 === a.nodeType)
			if (((d = "data-" + b.replace(Ma, "-$&").toLowerCase()), "string" == typeof (c = a.getAttribute(d)))) {
				try {
					c = n(c);
				} catch (a) {}
				Ka.set(a, b, c);
			} else c = void 0;
		return c;
	}
	function p(a, b, c, d) {
		var e,
			f = 1,
			g = 20,
			h = d
				? function () {
						return d.cur();
				  }
				: function () {
						return pa.css(a, b, "");
				  },
			i = h(),
			j = (c && c[3]) || (pa.cssNumber[b] ? "" : "px"),
			k = (pa.cssNumber[b] || ("px" !== j && +i)) && Oa.exec(pa.css(a, b));
		if (k && k[3] !== j) {
			(j = j || k[3]), (c = c || []), (k = +i || 1);
			do {
				(f = f || ".5"), (k /= f), pa.style(a, b, k + j);
			} while (f !== (f = h() / i) && 1 !== f && --g);
		}
		return c && ((k = +k || +i || 0), (e = c[1] ? k + (c[1] + 1) * c[2] : +c[2]), d && ((d.unit = j), (d.start = k), (d.end = e))), e;
	}
	function q(a) {
		var b,
			c = a.ownerDocument,
			d = a.nodeName,
			e = Sa[d];
		return e || ((b = c.body.appendChild(c.createElement(d))), (e = pa.css(b, "display")), b.parentNode.removeChild(b), "none" === e && (e = "block"), (Sa[d] = e), e);
	}
	function r(a, b) {
		for (var c, d, e = [], f = 0, g = a.length; f < g; f++) (d = a[f]), d.style && ((c = d.style.display), b ? ("none" === c && ((e[f] = Ja.get(d, "display") || null), e[f] || (d.style.display = "")), "" === d.style.display && Qa(d) && (e[f] = q(d))) : "none" !== c && ((e[f] = "none"), Ja.set(d, "display", c)));
		for (f = 0; f < g; f++) null != e[f] && (a[f].style.display = e[f]);
		return a;
	}
	function s(a, b) {
		var c;
		return (c = void 0 !== a.getElementsByTagName ? a.getElementsByTagName(b || "*") : void 0 !== a.querySelectorAll ? a.querySelectorAll(b || "*") : []), void 0 === b || (b && e(a, b)) ? pa.merge([a], c) : c;
	}
	function t(a, b) {
		for (var c = 0, d = a.length; c < d; c++) Ja.set(a[c], "globalEval", !b || Ja.get(b[c], "globalEval"));
	}
	function u(a, b, c, d, e) {
		for (var f, g, h, i, j, k, l = b.createDocumentFragment(), m = [], n = 0, o = a.length; n < o; n++)
			if ((f = a[n]) || 0 === f)
				if ("object" === pa.type(f)) pa.merge(m, f.nodeType ? [f] : f);
				else if (Xa.test(f)) {
					for (g = g || l.appendChild(b.createElement("div")), h = (Ua.exec(f) || ["", ""])[1].toLowerCase(), i = Wa[h] || Wa._default, g.innerHTML = i[1] + pa.htmlPrefilter(f) + i[2], k = i[0]; k--; ) g = g.lastChild;
					pa.merge(m, g.childNodes), (g = l.firstChild), (g.textContent = "");
				} else m.push(b.createTextNode(f));
		for (l.textContent = "", n = 0; (f = m[n++]); )
			if (d && pa.inArray(f, d) > -1) e && e.push(f);
			else if (((j = pa.contains(f.ownerDocument, f)), (g = s(l.appendChild(f), "script")), j && t(g), c)) for (k = 0; (f = g[k++]); ) Va.test(f.type || "") && c.push(f);
		return l;
	}
	function v() {
		return !0;
	}
	function w() {
		return !1;
	}
	function x() {
		try {
			return ca.activeElement;
		} catch (a) {}
	}
	function y(a, b, c, d, e, f) {
		var g, h;
		if ("object" == typeof b) {
			"string" != typeof c && ((d = d || c), (c = void 0));
			for (h in b) y(a, h, c, d, b[h], f);
			return a;
		}
		if ((null == d && null == e ? ((e = c), (d = c = void 0)) : null == e && ("string" == typeof c ? ((e = d), (d = void 0)) : ((e = d), (d = c), (c = void 0))), !1 === e)) e = w;
		else if (!e) return a;
		return (
			1 === f &&
				((g = e),
				(e = function (a) {
					return pa().off(a), g.apply(this, arguments);
				}),
				(e.guid = g.guid || (g.guid = pa.guid++))),
			a.each(function () {
				pa.event.add(this, b, e, d, c);
			})
		);
	}
	function z(a, b) {
		return e(a, "table") && e(11 !== b.nodeType ? b : b.firstChild, "tr") ? pa(">tbody", a)[0] || a : a;
	}
	function A(a) {
		return (a.type = (null !== a.getAttribute("type")) + "/" + a.type), a;
	}
	function B(a) {
		var b = db.exec(a.type);
		return b ? (a.type = b[1]) : a.removeAttribute("type"), a;
	}
	function C(a, b) {
		var c, d, e, f, g, h, i, j;
		if (1 === b.nodeType) {
			if (Ja.hasData(a) && ((f = Ja.access(a)), (g = Ja.set(b, f)), (j = f.events))) {
				delete g.handle, (g.events = {});
				for (e in j) for (c = 0, d = j[e].length; c < d; c++) pa.event.add(b, e, j[e][c]);
			}
			Ka.hasData(a) && ((h = Ka.access(a)), (i = pa.extend({}, h)), Ka.set(b, i));
		}
	}
	function D(a, b) {
		var c = b.nodeName.toLowerCase();
		"input" === c && Ta.test(a.type) ? (b.checked = a.checked) : ("input" !== c && "textarea" !== c) || (b.defaultValue = a.defaultValue);
	}
	function E(a, b, d, e) {
		b = fa.apply([], b);
		var f,
			g,
			h,
			i,
			j,
			k,
			l = 0,
			m = a.length,
			n = m - 1,
			o = b[0],
			p = pa.isFunction(o);
		if (p || (m > 1 && "string" == typeof o && !na.checkClone && cb.test(o)))
			return a.each(function (c) {
				var f = a.eq(c);
				p && (b[0] = o.call(this, c, f.html())), E(f, b, d, e);
			});
		if (m && ((f = u(b, a[0].ownerDocument, !1, a, e)), (g = f.firstChild), 1 === f.childNodes.length && (f = g), g || e)) {
			for (h = pa.map(s(f, "script"), A), i = h.length; l < m; l++) (j = f), l !== n && ((j = pa.clone(j, !0, !0)), i && pa.merge(h, s(j, "script"))), d.call(a[l], j, l);
			if (i) for (k = h[h.length - 1].ownerDocument, pa.map(h, B), l = 0; l < i; l++) (j = h[l]), Va.test(j.type || "") && !Ja.access(j, "globalEval") && pa.contains(k, j) && (j.src ? pa._evalUrl && pa._evalUrl(j.src) : c(j.textContent.replace(eb, ""), k));
		}
		return a;
	}
	function F(a, b, c) {
		for (var d, e = b ? pa.filter(b, a) : a, f = 0; null != (d = e[f]); f++) c || 1 !== d.nodeType || pa.cleanData(s(d)), d.parentNode && (c && pa.contains(d.ownerDocument, d) && t(s(d, "script")), d.parentNode.removeChild(d));
		return a;
	}
	function G(a, b, c) {
		var d,
			e,
			f,
			g,
			h = a.style;
		return (c = c || hb(a)), c && ((g = c.getPropertyValue(b) || c[b]), "" !== g || pa.contains(a.ownerDocument, a) || (g = pa.style(a, b)), !na.pixelMarginRight() && gb.test(g) && fb.test(b) && ((d = h.width), (e = h.minWidth), (f = h.maxWidth), (h.minWidth = h.maxWidth = h.width = g), (g = c.width), (h.width = d), (h.minWidth = e), (h.maxWidth = f))), void 0 !== g ? g + "" : g;
	}
	function H(a, b) {
		return {
			get: function () {
				return a() ? void delete this.get : (this.get = b).apply(this, arguments);
			},
		};
	}
	function I(a) {
		if (a in nb) return a;
		for (var b = a[0].toUpperCase() + a.slice(1), c = mb.length; c--; ) if ((a = mb[c] + b) in nb) return a;
	}
	function J(a) {
		var b = pa.cssProps[a];
		return b || (b = pa.cssProps[a] = I(a) || a), b;
	}
	function K(a, b, c) {
		var d = Oa.exec(b);
		return d ? Math.max(0, d[2] - (c || 0)) + (d[3] || "px") : b;
	}
	function L(a, b, c, d, e) {
		var f,
			g = 0;
		for (f = c === (d ? "border" : "content") ? 4 : "width" === b ? 1 : 0; f < 4; f += 2) "margin" === c && (g += pa.css(a, c + Pa[f], !0, e)), d ? ("content" === c && (g -= pa.css(a, "padding" + Pa[f], !0, e)), "margin" !== c && (g -= pa.css(a, "border" + Pa[f] + "Width", !0, e))) : ((g += pa.css(a, "padding" + Pa[f], !0, e)), "padding" !== c && (g += pa.css(a, "border" + Pa[f] + "Width", !0, e)));
		return g;
	}
	function M(a, b, c) {
		var d,
			e = hb(a),
			f = G(a, b, e),
			g = "border-box" === pa.css(a, "boxSizing", !1, e);
		return gb.test(f) ? f : ((d = g && (na.boxSizingReliable() || f === a.style[b])), "auto" === f && (f = a["offset" + b[0].toUpperCase() + b.slice(1)]), (f = parseFloat(f) || 0) + L(a, b, c || (g ? "border" : "content"), d, e) + "px");
	}
	function N(a, b, c, d, e) {
		return new N.prototype.init(a, b, c, d, e);
	}
	function O() {
		pb && (!1 === ca.hidden && a.requestAnimationFrame ? a.requestAnimationFrame(O) : a.setTimeout(O, pa.fx.interval), pa.fx.tick());
	}
	function P() {
		return (
			a.setTimeout(function () {
				ob = void 0;
			}),
			(ob = pa.now())
		);
	}
	function Q(a, b) {
		var c,
			d = 0,
			e = { height: a };
		for (b = b ? 1 : 0; d < 4; d += 2 - b) (c = Pa[d]), (e["margin" + c] = e["padding" + c] = a);
		return b && (e.opacity = e.width = a), e;
	}
	function R(a, b, c) {
		for (var d, e = (U.tweeners[b] || []).concat(U.tweeners["*"]), f = 0, g = e.length; f < g; f++) if ((d = e[f].call(c, b, a))) return d;
	}
	function S(a, b, c) {
		var d,
			e,
			f,
			g,
			h,
			i,
			j,
			k,
			l = "width" in b || "height" in b,
			m = this,
			n = {},
			o = a.style,
			p = a.nodeType && Qa(a),
			q = Ja.get(a, "fxshow");
		c.queue ||
			((g = pa._queueHooks(a, "fx")),
			null == g.unqueued &&
				((g.unqueued = 0),
				(h = g.empty.fire),
				(g.empty.fire = function () {
					g.unqueued || h();
				})),
			g.unqueued++,
			m.always(function () {
				m.always(function () {
					g.unqueued--, pa.queue(a, "fx").length || g.empty.fire();
				});
			}));
		for (d in b)
			if (((e = b[d]), qb.test(e))) {
				if ((delete b[d], (f = f || "toggle" === e), e === (p ? "hide" : "show"))) {
					if ("show" !== e || !q || void 0 === q[d]) continue;
					p = !0;
				}
				n[d] = (q && q[d]) || pa.style(a, d);
			}
		if ((i = !pa.isEmptyObject(b)) || !pa.isEmptyObject(n)) {
			l &&
				1 === a.nodeType &&
				((c.overflow = [o.overflow, o.overflowX, o.overflowY]),
				(j = q && q.display),
				null == j && (j = Ja.get(a, "display")),
				(k = pa.css(a, "display")),
				"none" === k && (j ? (k = j) : (r([a], !0), (j = a.style.display || j), (k = pa.css(a, "display")), r([a]))),
				("inline" === k || ("inline-block" === k && null != j)) &&
					"none" === pa.css(a, "float") &&
					(i ||
						(m.done(function () {
							o.display = j;
						}),
						null == j && ((k = o.display), (j = "none" === k ? "" : k))),
					(o.display = "inline-block"))),
				c.overflow &&
					((o.overflow = "hidden"),
					m.always(function () {
						(o.overflow = c.overflow[0]), (o.overflowX = c.overflow[1]), (o.overflowY = c.overflow[2]);
					})),
				(i = !1);
			for (d in n)
				i ||
					(q ? "hidden" in q && (p = q.hidden) : (q = Ja.access(a, "fxshow", { display: j })),
					f && (q.hidden = !p),
					p && r([a], !0),
					m.done(function () {
						p || r([a]), Ja.remove(a, "fxshow");
						for (d in n) pa.style(a, d, n[d]);
					})),
					(i = R(p ? q[d] : 0, d, m)),
					d in q || ((q[d] = i.start), p && ((i.end = i.start), (i.start = 0)));
		}
	}
	function T(a, b) {
		var c, d, e, f, g;
		for (c in a)
			if (((d = pa.camelCase(c)), (e = b[d]), (f = a[c]), Array.isArray(f) && ((e = f[1]), (f = a[c] = f[0])), c !== d && ((a[d] = f), delete a[c]), (g = pa.cssHooks[d]) && "expand" in g)) {
				(f = g.expand(f)), delete a[d];
				for (c in f) c in a || ((a[c] = f[c]), (b[c] = e));
			} else b[d] = e;
	}
	function U(a, b, c) {
		var d,
			e,
			f = 0,
			g = U.prefilters.length,
			h = pa.Deferred().always(function () {
				delete i.elem;
			}),
			i = function () {
				if (e) return !1;
				for (var b = ob || P(), c = Math.max(0, j.startTime + j.duration - b), d = c / j.duration || 0, f = 1 - d, g = 0, i = j.tweens.length; g < i; g++) j.tweens[g].run(f);
				return h.notifyWith(a, [j, f, c]), f < 1 && i ? c : (i || h.notifyWith(a, [j, 1, 0]), h.resolveWith(a, [j]), !1);
			},
			j = h.promise({
				elem: a,
				props: pa.extend({}, b),
				opts: pa.extend(!0, { specialEasing: {}, easing: pa.easing._default }, c),
				originalProperties: b,
				originalOptions: c,
				startTime: ob || P(),
				duration: c.duration,
				tweens: [],
				createTween: function (b, c) {
					var d = pa.Tween(a, j.opts, b, c, j.opts.specialEasing[b] || j.opts.easing);
					return j.tweens.push(d), d;
				},
				stop: function (b) {
					var c = 0,
						d = b ? j.tweens.length : 0;
					if (e) return this;
					for (e = !0; c < d; c++) j.tweens[c].run(1);
					return b ? (h.notifyWith(a, [j, 1, 0]), h.resolveWith(a, [j, b])) : h.rejectWith(a, [j, b]), this;
				},
			}),
			k = j.props;
		for (T(k, j.opts.specialEasing); f < g; f++) if ((d = U.prefilters[f].call(j, a, k, j.opts))) return pa.isFunction(d.stop) && (pa._queueHooks(j.elem, j.opts.queue).stop = pa.proxy(d.stop, d)), d;
		return pa.map(k, R, j), pa.isFunction(j.opts.start) && j.opts.start.call(a, j), j.progress(j.opts.progress).done(j.opts.done, j.opts.complete).fail(j.opts.fail).always(j.opts.always), pa.fx.timer(pa.extend(i, { elem: a, anim: j, queue: j.opts.queue })), j;
	}
	function V(a) {
		return (a.match(Ea) || []).join(" ");
	}
	function W(a) {
		return (a.getAttribute && a.getAttribute("class")) || "";
	}
	function X(a, b, c, d) {
		var e;
		if (Array.isArray(b))
			pa.each(b, function (b, e) {
				c || Bb.test(a) ? d(a, e) : X(a + "[" + ("object" == typeof e && null != e ? b : "") + "]", e, c, d);
			});
		else if (c || "object" !== pa.type(b)) d(a, b);
		else for (e in b) X(a + "[" + e + "]", b[e], c, d);
	}
	function Y(a) {
		return function (b, c) {
			"string" != typeof b && ((c = b), (b = "*"));
			var d,
				e = 0,
				f = b.toLowerCase().match(Ea) || [];
			if (pa.isFunction(c)) for (; (d = f[e++]); ) "+" === d[0] ? ((d = d.slice(1) || "*"), (a[d] = a[d] || []).unshift(c)) : (a[d] = a[d] || []).push(c);
		};
	}
	function Z(a, b, c, d) {
		function e(h) {
			var i;
			return (
				(f[h] = !0),
				pa.each(a[h] || [], function (a, h) {
					var j = h(b, c, d);
					return "string" != typeof j || g || f[j] ? (g ? !(i = j) : void 0) : (b.dataTypes.unshift(j), e(j), !1);
				}),
				i
			);
		}
		var f = {},
			g = a === Nb;
		return e(b.dataTypes[0]) || (!f["*"] && e("*"));
	}
	function $(a, b) {
		var c,
			d,
			e = pa.ajaxSettings.flatOptions || {};
		for (c in b) void 0 !== b[c] && ((e[c] ? a : d || (d = {}))[c] = b[c]);
		return d && pa.extend(!0, a, d), a;
	}
	function _(a, b, c) {
		for (var d, e, f, g, h = a.contents, i = a.dataTypes; "*" === i[0]; ) i.shift(), void 0 === d && (d = a.mimeType || b.getResponseHeader("Content-Type"));
		if (d)
			for (e in h)
				if (h[e] && h[e].test(d)) {
					i.unshift(e);
					break;
				}
		if (i[0] in c) f = i[0];
		else {
			for (e in c) {
				if (!i[0] || a.converters[e + " " + i[0]]) {
					f = e;
					break;
				}
				g || (g = e);
			}
			f = f || g;
		}
		if (f) return f !== i[0] && i.unshift(f), c[f];
	}
	function aa(a, b, c, d) {
		var e,
			f,
			g,
			h,
			i,
			j = {},
			k = a.dataTypes.slice();
		if (k[1]) for (g in a.converters) j[g.toLowerCase()] = a.converters[g];
		for (f = k.shift(); f; )
			if ((a.responseFields[f] && (c[a.responseFields[f]] = b), !i && d && a.dataFilter && (b = a.dataFilter(b, a.dataType)), (i = f), (f = k.shift())))
				if ("*" === f) f = i;
				else if ("*" !== i && i !== f) {
					if (!(g = j[i + " " + f] || j["* " + f]))
						for (e in j)
							if (((h = e.split(" ")), h[1] === f && (g = j[i + " " + h[0]] || j["* " + h[0]]))) {
								!0 === g ? (g = j[e]) : !0 !== j[e] && ((f = h[0]), k.unshift(h[1]));
								break;
							}
					if (!0 !== g)
						if (g && a.throws) b = g(b);
						else
							try {
								b = g(b);
							} catch (a) {
								return { state: "parsererror", error: g ? a : "No conversion from " + i + " to " + f };
							}
				}
		return { state: "success", data: b };
	}
	var ba = [],
		ca = a.document,
		da = Object.getPrototypeOf,
		ea = ba.slice,
		fa = ba.concat,
		ga = ba.push,
		ha = ba.indexOf,
		ia = {},
		ja = ia.toString,
		ka = ia.hasOwnProperty,
		la = ka.toString,
		ma = la.call(Object),
		na = {},
		oa = "3.2.1",
		pa = function (a, b) {
			return new pa.fn.init(a, b);
		},
		qa = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
		ra = /^-ms-/,
		sa = /-([a-z])/g,
		ta = function (a, b) {
			return b.toUpperCase();
		};
	(pa.fn = pa.prototype = {
		jquery: oa,
		constructor: pa,
		length: 0,
		toArray: function () {
			return ea.call(this);
		},
		get: function (a) {
			return null == a ? ea.call(this) : a < 0 ? this[a + this.length] : this[a];
		},
		pushStack: function (a) {
			var b = pa.merge(this.constructor(), a);
			return (b.prevObject = this), b;
		},
		each: function (a) {
			return pa.each(this, a);
		},
		map: function (a) {
			return this.pushStack(
				pa.map(this, function (b, c) {
					return a.call(b, c, b);
				})
			);
		},
		slice: function () {
			return this.pushStack(ea.apply(this, arguments));
		},
		first: function () {
			return this.eq(0);
		},
		last: function () {
			return this.eq(-1);
		},
		eq: function (a) {
			var b = this.length,
				c = +a + (a < 0 ? b : 0);
			return this.pushStack(c >= 0 && c < b ? [this[c]] : []);
		},
		end: function () {
			return this.prevObject || this.constructor();
		},
		push: ga,
		sort: ba.sort,
		splice: ba.splice,
	}),
		(pa.extend = pa.fn.extend = function () {
			var a,
				b,
				c,
				d,
				e,
				f,
				g = arguments[0] || {},
				h = 1,
				i = arguments.length,
				j = !1;
			for ("boolean" == typeof g && ((j = g), (g = arguments[h] || {}), h++), "object" == typeof g || pa.isFunction(g) || (g = {}), h === i && ((g = this), h--); h < i; h++) if (null != (a = arguments[h])) for (b in a) (c = g[b]), (d = a[b]), g !== d && (j && d && (pa.isPlainObject(d) || (e = Array.isArray(d))) ? (e ? ((e = !1), (f = c && Array.isArray(c) ? c : [])) : (f = c && pa.isPlainObject(c) ? c : {}), (g[b] = pa.extend(j, f, d))) : void 0 !== d && (g[b] = d));
			return g;
		}),
		pa.extend({
			expando: "jQuery" + (oa + Math.random()).replace(/\D/g, ""),
			isReady: !0,
			error: function (a) {
				throw new Error(a);
			},
			noop: function () {},
			isFunction: function (a) {
				return "function" === pa.type(a);
			},
			isWindow: function (a) {
				return null != a && a === a.window;
			},
			isNumeric: function (a) {
				var b = pa.type(a);
				return ("number" === b || "string" === b) && !isNaN(a - parseFloat(a));
			},
			isPlainObject: function (a) {
				var b, c;
				return !(!a || "[object Object]" !== ja.call(a)) && (!(b = da(a)) || ("function" == typeof (c = ka.call(b, "constructor") && b.constructor) && la.call(c) === ma));
			},
			isEmptyObject: function (a) {
				var b;
				for (b in a) return !1;
				return !0;
			},
			type: function (a) {
				return null == a ? a + "" : "object" == typeof a || "function" == typeof a ? ia[ja.call(a)] || "object" : typeof a;
			},
			globalEval: function (a) {
				c(a);
			},
			camelCase: function (a) {
				return a.replace(ra, "ms-").replace(sa, ta);
			},
			each: function (a, b) {
				var c,
					e = 0;
				if (d(a)) for (c = a.length; e < c && !1 !== b.call(a[e], e, a[e]); e++);
				else for (e in a) if (!1 === b.call(a[e], e, a[e])) break;
				return a;
			},
			trim: function (a) {
				return null == a ? "" : (a + "").replace(qa, "");
			},
			makeArray: function (a, b) {
				var c = b || [];
				return null != a && (d(Object(a)) ? pa.merge(c, "string" == typeof a ? [a] : a) : ga.call(c, a)), c;
			},
			inArray: function (a, b, c) {
				return null == b ? -1 : ha.call(b, a, c);
			},
			merge: function (a, b) {
				for (var c = +b.length, d = 0, e = a.length; d < c; d++) a[e++] = b[d];
				return (a.length = e), a;
			},
			grep: function (a, b, c) {
				for (var d = [], e = 0, f = a.length, g = !c; e < f; e++) !b(a[e], e) !== g && d.push(a[e]);
				return d;
			},
			map: function (a, b, c) {
				var e,
					f,
					g = 0,
					h = [];
				if (d(a)) for (e = a.length; g < e; g++) null != (f = b(a[g], g, c)) && h.push(f);
				else for (g in a) null != (f = b(a[g], g, c)) && h.push(f);
				return fa.apply([], h);
			},
			guid: 1,
			proxy: function (a, b) {
				var c, d, e;
				if (("string" == typeof b && ((c = a[b]), (b = a), (a = c)), pa.isFunction(a)))
					return (
						(d = ea.call(arguments, 2)),
						(e = function () {
							return a.apply(b || this, d.concat(ea.call(arguments)));
						}),
						(e.guid = a.guid = a.guid || pa.guid++),
						e
					);
			},
			now: Date.now,
			support: na,
		}),
		"function" == typeof Symbol && (pa.fn[Symbol.iterator] = ba[Symbol.iterator]),
		pa.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (a, b) {
			ia["[object " + b + "]"] = b.toLowerCase();
		});
	var ua =
		/*!
		 * Sizzle CSS Selector Engine v2.3.3
		 * https://sizzlejs.com/
		 *
		 * Copyright jQuery Foundation and other contributors
		 * Released under the MIT license
		 * http://jquery.org/license
		 *
		 * Date: 2016-08-08
		 */
		(function (a) {
			function b(a, b, c, d) {
				var e,
					f,
					g,
					h,
					i,
					j,
					k,
					m = b && b.ownerDocument,
					o = b ? b.nodeType : 9;
				if (((c = c || []), "string" != typeof a || !a || (1 !== o && 9 !== o && 11 !== o))) return c;
				if (!d && ((b ? b.ownerDocument || b : P) !== H && G(b), (b = b || H), J)) {
					if (11 !== o && (i = ra.exec(a)))
						if ((e = i[1])) {
							if (9 === o) {
								if (!(g = b.getElementById(e))) return c;
								if (g.id === e) return c.push(g), c;
							} else if (m && (g = m.getElementById(e)) && N(b, g) && g.id === e) return c.push(g), c;
						} else {
							if (i[2]) return $.apply(c, b.getElementsByTagName(a)), c;
							if ((e = i[3]) && w.getElementsByClassName && b.getElementsByClassName) return $.apply(c, b.getElementsByClassName(e)), c;
						}
					if (w.qsa && !U[a + " "] && (!K || !K.test(a))) {
						if (1 !== o) (m = b), (k = a);
						else if ("object" !== b.nodeName.toLowerCase()) {
							for ((h = b.getAttribute("id")) ? (h = h.replace(va, wa)) : b.setAttribute("id", (h = O)), j = A(a), f = j.length; f--; ) j[f] = "#" + h + " " + n(j[f]);
							(k = j.join(",")), (m = (sa.test(a) && l(b.parentNode)) || b);
						}
						if (k)
							try {
								return $.apply(c, m.querySelectorAll(k)), c;
							} catch (a) {
							} finally {
								h === O && b.removeAttribute("id");
							}
					}
				}
				return C(a.replace(ha, "$1"), b, c, d);
			}
			function c() {
				function a(c, d) {
					return b.push(c + " ") > x.cacheLength && delete a[b.shift()], (a[c + " "] = d);
				}
				var b = [];
				return a;
			}
			function d(a) {
				return (a[O] = !0), a;
			}
			function e(a) {
				var b = H.createElement("fieldset");
				try {
					return !!a(b);
				} catch (a) {
					return !1;
				} finally {
					b.parentNode && b.parentNode.removeChild(b), (b = null);
				}
			}
			function f(a, b) {
				for (var c = a.split("|"), d = c.length; d--; ) x.attrHandle[c[d]] = b;
			}
			function g(a, b) {
				var c = b && a,
					d = c && 1 === a.nodeType && 1 === b.nodeType && a.sourceIndex - b.sourceIndex;
				if (d) return d;
				if (c) for (; (c = c.nextSibling); ) if (c === b) return -1;
				return a ? 1 : -1;
			}
			function h(a) {
				return function (b) {
					return "input" === b.nodeName.toLowerCase() && b.type === a;
				};
			}
			function i(a) {
				return function (b) {
					var c = b.nodeName.toLowerCase();
					return ("input" === c || "button" === c) && b.type === a;
				};
			}
			function j(a) {
				return function (b) {
					return "form" in b ? (b.parentNode && !1 === b.disabled ? ("label" in b ? ("label" in b.parentNode ? b.parentNode.disabled === a : b.disabled === a) : b.isDisabled === a || (b.isDisabled !== !a && ya(b) === a)) : b.disabled === a) : "label" in b && b.disabled === a;
				};
			}
			function k(a) {
				return d(function (b) {
					return (
						(b = +b),
						d(function (c, d) {
							for (var e, f = a([], c.length, b), g = f.length; g--; ) c[(e = f[g])] && (c[e] = !(d[e] = c[e]));
						})
					);
				});
			}
			function l(a) {
				return a && void 0 !== a.getElementsByTagName && a;
			}
			function m() {}
			function n(a) {
				for (var b = 0, c = a.length, d = ""; b < c; b++) d += a[b].value;
				return d;
			}
			function o(a, b, c) {
				var d = b.dir,
					e = b.next,
					f = e || d,
					g = c && "parentNode" === f,
					h = R++;
				return b.first
					? function (b, c, e) {
							for (; (b = b[d]); ) if (1 === b.nodeType || g) return a(b, c, e);
							return !1;
					  }
					: function (b, c, i) {
							var j,
								k,
								l,
								m = [Q, h];
							if (i) {
								for (; (b = b[d]); ) if ((1 === b.nodeType || g) && a(b, c, i)) return !0;
							} else
								for (; (b = b[d]); )
									if (1 === b.nodeType || g)
										if (((l = b[O] || (b[O] = {})), (k = l[b.uniqueID] || (l[b.uniqueID] = {})), e && e === b.nodeName.toLowerCase())) b = b[d] || b;
										else {
											if ((j = k[f]) && j[0] === Q && j[1] === h) return (m[2] = j[2]);
											if (((k[f] = m), (m[2] = a(b, c, i)))) return !0;
										}
							return !1;
					  };
			}
			function p(a) {
				return a.length > 1
					? function (b, c, d) {
							for (var e = a.length; e--; ) if (!a[e](b, c, d)) return !1;
							return !0;
					  }
					: a[0];
			}
			function q(a, c, d) {
				for (var e = 0, f = c.length; e < f; e++) b(a, c[e], d);
				return d;
			}
			function r(a, b, c, d, e) {
				for (var f, g = [], h = 0, i = a.length, j = null != b; h < i; h++) (f = a[h]) && ((c && !c(f, d, e)) || (g.push(f), j && b.push(h)));
				return g;
			}
			function s(a, b, c, e, f, g) {
				return (
					e && !e[O] && (e = s(e)),
					f && !f[O] && (f = s(f, g)),
					d(function (d, g, h, i) {
						var j,
							k,
							l,
							m = [],
							n = [],
							o = g.length,
							p = d || q(b || "*", h.nodeType ? [h] : h, []),
							s = !a || (!d && b) ? p : r(p, m, a, h, i),
							t = c ? (f || (d ? a : o || e) ? [] : g) : s;
						if ((c && c(s, t, h, i), e)) for (j = r(t, n), e(j, [], h, i), k = j.length; k--; ) (l = j[k]) && (t[n[k]] = !(s[n[k]] = l));
						if (d) {
							if (f || a) {
								if (f) {
									for (j = [], k = t.length; k--; ) (l = t[k]) && j.push((s[k] = l));
									f(null, (t = []), j, i);
								}
								for (k = t.length; k--; ) (l = t[k]) && (j = f ? aa(d, l) : m[k]) > -1 && (d[j] = !(g[j] = l));
							}
						} else (t = r(t === g ? t.splice(o, t.length) : t)), f ? f(null, g, t, i) : $.apply(g, t);
					})
				);
			}
			function t(a) {
				for (
					var b,
						c,
						d,
						e = a.length,
						f = x.relative[a[0].type],
						g = f || x.relative[" "],
						h = f ? 1 : 0,
						i = o(
							function (a) {
								return a === b;
							},
							g,
							!0
						),
						j = o(
							function (a) {
								return aa(b, a) > -1;
							},
							g,
							!0
						),
						k = [
							function (a, c, d) {
								var e = (!f && (d || c !== D)) || ((b = c).nodeType ? i(a, c, d) : j(a, c, d));
								return (b = null), e;
							},
						];
					h < e;
					h++
				)
					if ((c = x.relative[a[h].type])) k = [o(p(k), c)];
					else {
						if (((c = x.filter[a[h].type].apply(null, a[h].matches)), c[O])) {
							for (d = ++h; d < e && !x.relative[a[d].type]; d++);
							return s(h > 1 && p(k), h > 1 && n(a.slice(0, h - 1).concat({ value: " " === a[h - 2].type ? "*" : "" })).replace(ha, "$1"), c, h < d && t(a.slice(h, d)), d < e && t((a = a.slice(d))), d < e && n(a));
						}
						k.push(c);
					}
				return p(k);
			}
			function u(a, c) {
				var e = c.length > 0,
					f = a.length > 0,
					g = function (d, g, h, i, j) {
						var k,
							l,
							m,
							n = 0,
							o = "0",
							p = d && [],
							q = [],
							s = D,
							t = d || (f && x.find.TAG("*", j)),
							u = (Q += null == s ? 1 : Math.random() || 0.1),
							v = t.length;
						for (j && (D = g === H || g || j); o !== v && null != (k = t[o]); o++) {
							if (f && k) {
								for (l = 0, g || k.ownerDocument === H || (G(k), (h = !J)); (m = a[l++]); )
									if (m(k, g || H, h)) {
										i.push(k);
										break;
									}
								j && (Q = u);
							}
							e && ((k = !m && k) && n--, d && p.push(k));
						}
						if (((n += o), e && o !== n)) {
							for (l = 0; (m = c[l++]); ) m(p, q, g, h);
							if (d) {
								if (n > 0) for (; o--; ) p[o] || q[o] || (q[o] = Y.call(i));
								q = r(q);
							}
							$.apply(i, q), j && !d && q.length > 0 && n + c.length > 1 && b.uniqueSort(i);
						}
						return j && ((Q = u), (D = s)), p;
					};
				return e ? d(g) : g;
			}
			var v,
				w,
				x,
				y,
				z,
				A,
				B,
				C,
				D,
				E,
				F,
				G,
				H,
				I,
				J,
				K,
				L,
				M,
				N,
				O = "sizzle" + 1 * new Date(),
				P = a.document,
				Q = 0,
				R = 0,
				S = c(),
				T = c(),
				U = c(),
				V = function (a, b) {
					return a === b && (F = !0), 0;
				},
				W = {}.hasOwnProperty,
				X = [],
				Y = X.pop,
				Z = X.push,
				$ = X.push,
				_ = X.slice,
				aa = function (a, b) {
					for (var c = 0, d = a.length; c < d; c++) if (a[c] === b) return c;
					return -1;
				},
				ba = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
				ca = "[\\x20\\t\\r\\n\\f]",
				da = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
				ea = "\\[" + ca + "*(" + da + ")(?:" + ca + "*([*^$|!~]?=)" + ca + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + da + "))|)" + ca + "*\\]",
				fa = ":(" + da + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + ea + ")*)|.*)\\)|)",
				ga = new RegExp(ca + "+", "g"),
				ha = new RegExp("^" + ca + "+|((?:^|[^\\\\])(?:\\\\.)*)" + ca + "+$", "g"),
				ia = new RegExp("^" + ca + "*," + ca + "*"),
				ja = new RegExp("^" + ca + "*([>+~]|" + ca + ")" + ca + "*"),
				ka = new RegExp("=" + ca + "*([^\\]'\"]*?)" + ca + "*\\]", "g"),
				la = new RegExp(fa),
				ma = new RegExp("^" + da + "$"),
				na = { ID: new RegExp("^#(" + da + ")"), CLASS: new RegExp("^\\.(" + da + ")"), TAG: new RegExp("^(" + da + "|[*])"), ATTR: new RegExp("^" + ea), PSEUDO: new RegExp("^" + fa), CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + ca + "*(even|odd|(([+-]|)(\\d*)n|)" + ca + "*(?:([+-]|)" + ca + "*(\\d+)|))" + ca + "*\\)|)", "i"), bool: new RegExp("^(?:" + ba + ")$", "i"), needsContext: new RegExp("^" + ca + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + ca + "*((?:-\\d)?\\d*)" + ca + "*\\)|)(?=[^-]|$)", "i") },
				oa = /^(?:input|select|textarea|button)$/i,
				pa = /^h\d$/i,
				qa = /^[^{]+\{\s*\[native \w/,
				ra = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
				sa = /[+~]/,
				ta = new RegExp("\\\\([\\da-f]{1,6}" + ca + "?|(" + ca + ")|.)", "ig"),
				ua = function (a, b, c) {
					var d = "0x" + b - 65536;
					return d !== d || c ? b : d < 0 ? String.fromCharCode(d + 65536) : String.fromCharCode((d >> 10) | 55296, (1023 & d) | 56320);
				},
				va = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
				wa = function (a, b) {
					return b ? ("\0" === a ? "ï¿½" : a.slice(0, -1) + "\\" + a.charCodeAt(a.length - 1).toString(16) + " ") : "\\" + a;
				},
				xa = function () {
					G();
				},
				ya = o(
					function (a) {
						return !0 === a.disabled && ("form" in a || "label" in a);
					},
					{ dir: "parentNode", next: "legend" }
				);
			try {
				$.apply((X = _.call(P.childNodes)), P.childNodes), X[P.childNodes.length].nodeType;
			} catch (a) {
				$ = {
					apply: X.length
						? function (a, b) {
								Z.apply(a, _.call(b));
						  }
						: function (a, b) {
								for (var c = a.length, d = 0; (a[c++] = b[d++]); );
								a.length = c - 1;
						  },
				};
			}
			(w = b.support = {}),
				(z = b.isXML = function (a) {
					var b = a && (a.ownerDocument || a).documentElement;
					return !!b && "HTML" !== b.nodeName;
				}),
				(G = b.setDocument = function (a) {
					var b,
						c,
						d = a ? a.ownerDocument || a : P;
					return d !== H && 9 === d.nodeType && d.documentElement
						? ((H = d),
						  (I = H.documentElement),
						  (J = !z(H)),
						  P !== H && (c = H.defaultView) && c.top !== c && (c.addEventListener ? c.addEventListener("unload", xa, !1) : c.attachEvent && c.attachEvent("onunload", xa)),
						  (w.attributes = e(function (a) {
								return (a.className = "i"), !a.getAttribute("className");
						  })),
						  (w.getElementsByTagName = e(function (a) {
								return a.appendChild(H.createComment("")), !a.getElementsByTagName("*").length;
						  })),
						  (w.getElementsByClassName = qa.test(H.getElementsByClassName)),
						  (w.getById = e(function (a) {
								return (I.appendChild(a).id = O), !H.getElementsByName || !H.getElementsByName(O).length;
						  })),
						  w.getById
								? ((x.filter.ID = function (a) {
										var b = a.replace(ta, ua);
										return function (a) {
											return a.getAttribute("id") === b;
										};
								  }),
								  (x.find.ID = function (a, b) {
										if (void 0 !== b.getElementById && J) {
											var c = b.getElementById(a);
											return c ? [c] : [];
										}
								  }))
								: ((x.filter.ID = function (a) {
										var b = a.replace(ta, ua);
										return function (a) {
											var c = void 0 !== a.getAttributeNode && a.getAttributeNode("id");
											return c && c.value === b;
										};
								  }),
								  (x.find.ID = function (a, b) {
										if (void 0 !== b.getElementById && J) {
											var c,
												d,
												e,
												f = b.getElementById(a);
											if (f) {
												if ((c = f.getAttributeNode("id")) && c.value === a) return [f];
												for (e = b.getElementsByName(a), d = 0; (f = e[d++]); ) if ((c = f.getAttributeNode("id")) && c.value === a) return [f];
											}
											return [];
										}
								  })),
						  (x.find.TAG = w.getElementsByTagName
								? function (a, b) {
										return void 0 !== b.getElementsByTagName ? b.getElementsByTagName(a) : w.qsa ? b.querySelectorAll(a) : void 0;
								  }
								: function (a, b) {
										var c,
											d = [],
											e = 0,
											f = b.getElementsByTagName(a);
										if ("*" === a) {
											for (; (c = f[e++]); ) 1 === c.nodeType && d.push(c);
											return d;
										}
										return f;
								  }),
						  (x.find.CLASS =
								w.getElementsByClassName &&
								function (a, b) {
									if (void 0 !== b.getElementsByClassName && J) return b.getElementsByClassName(a);
								}),
						  (L = []),
						  (K = []),
						  (w.qsa = qa.test(H.querySelectorAll)) &&
								(e(function (a) {
									(I.appendChild(a).innerHTML = "<a id='" + O + "'></a><select id='" + O + "-\r\\' msallowcapture=''><option selected=''></option></select>"), a.querySelectorAll("[msallowcapture^='']").length && K.push("[*^$]=" + ca + "*(?:''|\"\")"), a.querySelectorAll("[selected]").length || K.push("\\[" + ca + "*(?:value|" + ba + ")"), a.querySelectorAll("[id~=" + O + "-]").length || K.push("~="), a.querySelectorAll(":checked").length || K.push(":checked"), a.querySelectorAll("a#" + O + "+*").length || K.push(".#.+[+~]");
								}),
								e(function (a) {
									a.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
									var b = H.createElement("input");
									b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("name", "D"), a.querySelectorAll("[name=d]").length && K.push("name" + ca + "*[*^$|!~]?="), 2 !== a.querySelectorAll(":enabled").length && K.push(":enabled", ":disabled"), (I.appendChild(a).disabled = !0), 2 !== a.querySelectorAll(":disabled").length && K.push(":enabled", ":disabled"), a.querySelectorAll("*,:x"), K.push(",.*:");
								})),
						  (w.matchesSelector = qa.test((M = I.matches || I.webkitMatchesSelector || I.mozMatchesSelector || I.oMatchesSelector || I.msMatchesSelector))) &&
								e(function (a) {
									(w.disconnectedMatch = M.call(a, "*")), M.call(a, "[s!='']:x"), L.push("!=", fa);
								}),
						  (K = K.length && new RegExp(K.join("|"))),
						  (L = L.length && new RegExp(L.join("|"))),
						  (b = qa.test(I.compareDocumentPosition)),
						  (N =
								b || qa.test(I.contains)
									? function (a, b) {
											var c = 9 === a.nodeType ? a.documentElement : a,
												d = b && b.parentNode;
											return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)));
									  }
									: function (a, b) {
											if (b) for (; (b = b.parentNode); ) if (b === a) return !0;
											return !1;
									  }),
						  (V = b
								? function (a, b) {
										if (a === b) return (F = !0), 0;
										var c = !a.compareDocumentPosition - !b.compareDocumentPosition;
										return c || ((c = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1), 1 & c || (!w.sortDetached && b.compareDocumentPosition(a) === c) ? (a === H || (a.ownerDocument === P && N(P, a)) ? -1 : b === H || (b.ownerDocument === P && N(P, b)) ? 1 : E ? aa(E, a) - aa(E, b) : 0) : 4 & c ? -1 : 1);
								  }
								: function (a, b) {
										if (a === b) return (F = !0), 0;
										var c,
											d = 0,
											e = a.parentNode,
											f = b.parentNode,
											h = [a],
											i = [b];
										if (!e || !f) return a === H ? -1 : b === H ? 1 : e ? -1 : f ? 1 : E ? aa(E, a) - aa(E, b) : 0;
										if (e === f) return g(a, b);
										for (c = a; (c = c.parentNode); ) h.unshift(c);
										for (c = b; (c = c.parentNode); ) i.unshift(c);
										for (; h[d] === i[d]; ) d++;
										return d ? g(h[d], i[d]) : h[d] === P ? -1 : i[d] === P ? 1 : 0;
								  }),
						  H)
						: H;
				}),
				(b.matches = function (a, c) {
					return b(a, null, null, c);
				}),
				(b.matchesSelector = function (a, c) {
					if (((a.ownerDocument || a) !== H && G(a), (c = c.replace(ka, "='$1']")), w.matchesSelector && J && !U[c + " "] && (!L || !L.test(c)) && (!K || !K.test(c))))
						try {
							var d = M.call(a, c);
							if (d || w.disconnectedMatch || (a.document && 11 !== a.document.nodeType)) return d;
						} catch (a) {}
					return b(c, H, null, [a]).length > 0;
				}),
				(b.contains = function (a, b) {
					return (a.ownerDocument || a) !== H && G(a), N(a, b);
				}),
				(b.attr = function (a, b) {
					(a.ownerDocument || a) !== H && G(a);
					var c = x.attrHandle[b.toLowerCase()],
						d = c && W.call(x.attrHandle, b.toLowerCase()) ? c(a, b, !J) : void 0;
					return void 0 !== d ? d : w.attributes || !J ? a.getAttribute(b) : (d = a.getAttributeNode(b)) && d.specified ? d.value : null;
				}),
				(b.escape = function (a) {
					return (a + "").replace(va, wa);
				}),
				(b.error = function (a) {
					throw new Error("Syntax error, unrecognized expression: " + a);
				}),
				(b.uniqueSort = function (a) {
					var b,
						c = [],
						d = 0,
						e = 0;
					if (((F = !w.detectDuplicates), (E = !w.sortStable && a.slice(0)), a.sort(V), F)) {
						for (; (b = a[e++]); ) b === a[e] && (d = c.push(e));
						for (; d--; ) a.splice(c[d], 1);
					}
					return (E = null), a;
				}),
				(y = b.getText = function (a) {
					var b,
						c = "",
						d = 0,
						e = a.nodeType;
					if (e) {
						if (1 === e || 9 === e || 11 === e) {
							if ("string" == typeof a.textContent) return a.textContent;
							for (a = a.firstChild; a; a = a.nextSibling) c += y(a);
						} else if (3 === e || 4 === e) return a.nodeValue;
					} else for (; (b = a[d++]); ) c += y(b);
					return c;
				}),
				(x = b.selectors = {
					cacheLength: 50,
					createPseudo: d,
					match: na,
					attrHandle: {},
					find: {},
					relative: { ">": { dir: "parentNode", first: !0 }, " ": { dir: "parentNode" }, "+": { dir: "previousSibling", first: !0 }, "~": { dir: "previousSibling" } },
					preFilter: {
						ATTR: function (a) {
							return (a[1] = a[1].replace(ta, ua)), (a[3] = (a[3] || a[4] || a[5] || "").replace(ta, ua)), "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4);
						},
						CHILD: function (a) {
							return (a[1] = a[1].toLowerCase()), "nth" === a[1].slice(0, 3) ? (a[3] || b.error(a[0]), (a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3]))), (a[5] = +(a[7] + a[8] || "odd" === a[3]))) : a[3] && b.error(a[0]), a;
						},
						PSEUDO: function (a) {
							var b,
								c = !a[6] && a[2];
							return na.CHILD.test(a[0]) ? null : (a[3] ? (a[2] = a[4] || a[5] || "") : c && la.test(c) && (b = A(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && ((a[0] = a[0].slice(0, b)), (a[2] = c.slice(0, b))), a.slice(0, 3));
						},
					},
					filter: {
						TAG: function (a) {
							var b = a.replace(ta, ua).toLowerCase();
							return "*" === a
								? function () {
										return !0;
								  }
								: function (a) {
										return a.nodeName && a.nodeName.toLowerCase() === b;
								  };
						},
						CLASS: function (a) {
							var b = S[a + " "];
							return (
								b ||
								((b = new RegExp("(^|" + ca + ")" + a + "(" + ca + "|$)")) &&
									S(a, function (a) {
										return b.test(("string" == typeof a.className && a.className) || (void 0 !== a.getAttribute && a.getAttribute("class")) || "");
									}))
							);
						},
						ATTR: function (a, c, d) {
							return function (e) {
								var f = b.attr(e, a);
								return null == f ? "!=" === c : !c || ((f += ""), "=" === c ? f === d : "!=" === c ? f !== d : "^=" === c ? d && 0 === f.indexOf(d) : "*=" === c ? d && f.indexOf(d) > -1 : "$=" === c ? d && f.slice(-d.length) === d : "~=" === c ? (" " + f.replace(ga, " ") + " ").indexOf(d) > -1 : "|=" === c && (f === d || f.slice(0, d.length + 1) === d + "-"));
							};
						},
						CHILD: function (a, b, c, d, e) {
							var f = "nth" !== a.slice(0, 3),
								g = "last" !== a.slice(-4),
								h = "of-type" === b;
							return 1 === d && 0 === e
								? function (a) {
										return !!a.parentNode;
								  }
								: function (b, c, i) {
										var j,
											k,
											l,
											m,
											n,
											o,
											p = f !== g ? "nextSibling" : "previousSibling",
											q = b.parentNode,
											r = h && b.nodeName.toLowerCase(),
											s = !i && !h,
											t = !1;
										if (q) {
											if (f) {
												for (; p; ) {
													for (m = b; (m = m[p]); ) if (h ? m.nodeName.toLowerCase() === r : 1 === m.nodeType) return !1;
													o = p = "only" === a && !o && "nextSibling";
												}
												return !0;
											}
											if (((o = [g ? q.firstChild : q.lastChild]), g && s)) {
												for (m = q, l = m[O] || (m[O] = {}), k = l[m.uniqueID] || (l[m.uniqueID] = {}), j = k[a] || [], n = j[0] === Q && j[1], t = n && j[2], m = n && q.childNodes[n]; (m = (++n && m && m[p]) || (t = n = 0) || o.pop()); )
													if (1 === m.nodeType && ++t && m === b) {
														k[a] = [Q, n, t];
														break;
													}
											} else if ((s && ((m = b), (l = m[O] || (m[O] = {})), (k = l[m.uniqueID] || (l[m.uniqueID] = {})), (j = k[a] || []), (n = j[0] === Q && j[1]), (t = n)), !1 === t)) for (; (m = (++n && m && m[p]) || (t = n = 0) || o.pop()) && ((h ? m.nodeName.toLowerCase() !== r : 1 !== m.nodeType) || !++t || (s && ((l = m[O] || (m[O] = {})), (k = l[m.uniqueID] || (l[m.uniqueID] = {})), (k[a] = [Q, t])), m !== b)); );
											return (t -= e) === d || (t % d == 0 && t / d >= 0);
										}
								  };
						},
						PSEUDO: function (a, c) {
							var e,
								f = x.pseudos[a] || x.setFilters[a.toLowerCase()] || b.error("unsupported pseudo: " + a);
							return f[O]
								? f(c)
								: f.length > 1
								? ((e = [a, a, "", c]),
								  x.setFilters.hasOwnProperty(a.toLowerCase())
										? d(function (a, b) {
												for (var d, e = f(a, c), g = e.length; g--; ) (d = aa(a, e[g])), (a[d] = !(b[d] = e[g]));
										  })
										: function (a) {
												return f(a, 0, e);
										  })
								: f;
						},
					},
					pseudos: {
						not: d(function (a) {
							var b = [],
								c = [],
								e = B(a.replace(ha, "$1"));
							return e[O]
								? d(function (a, b, c, d) {
										for (var f, g = e(a, null, d, []), h = a.length; h--; ) (f = g[h]) && (a[h] = !(b[h] = f));
								  })
								: function (a, d, f) {
										return (b[0] = a), e(b, null, f, c), (b[0] = null), !c.pop();
								  };
						}),
						has: d(function (a) {
							return function (c) {
								return b(a, c).length > 0;
							};
						}),
						contains: d(function (a) {
							return (
								(a = a.replace(ta, ua)),
								function (b) {
									return (b.textContent || b.innerText || y(b)).indexOf(a) > -1;
								}
							);
						}),
						lang: d(function (a) {
							return (
								ma.test(a || "") || b.error("unsupported lang: " + a),
								(a = a.replace(ta, ua).toLowerCase()),
								function (b) {
									var c;
									do {
										if ((c = J ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang"))) return (c = c.toLowerCase()) === a || 0 === c.indexOf(a + "-");
									} while ((b = b.parentNode) && 1 === b.nodeType);
									return !1;
								}
							);
						}),
						target: function (b) {
							var c = a.location && a.location.hash;
							return c && c.slice(1) === b.id;
						},
						root: function (a) {
							return a === I;
						},
						focus: function (a) {
							return a === H.activeElement && (!H.hasFocus || H.hasFocus()) && !!(a.type || a.href || ~a.tabIndex);
						},
						enabled: j(!1),
						disabled: j(!0),
						checked: function (a) {
							var b = a.nodeName.toLowerCase();
							return ("input" === b && !!a.checked) || ("option" === b && !!a.selected);
						},
						selected: function (a) {
							return a.parentNode && a.parentNode.selectedIndex, !0 === a.selected;
						},
						empty: function (a) {
							for (a = a.firstChild; a; a = a.nextSibling) if (a.nodeType < 6) return !1;
							return !0;
						},
						parent: function (a) {
							return !x.pseudos.empty(a);
						},
						header: function (a) {
							return pa.test(a.nodeName);
						},
						input: function (a) {
							return oa.test(a.nodeName);
						},
						button: function (a) {
							var b = a.nodeName.toLowerCase();
							return ("input" === b && "button" === a.type) || "button" === b;
						},
						text: function (a) {
							var b;
							return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || "text" === b.toLowerCase());
						},
						first: k(function () {
							return [0];
						}),
						last: k(function (a, b) {
							return [b - 1];
						}),
						eq: k(function (a, b, c) {
							return [c < 0 ? c + b : c];
						}),
						even: k(function (a, b) {
							for (var c = 0; c < b; c += 2) a.push(c);
							return a;
						}),
						odd: k(function (a, b) {
							for (var c = 1; c < b; c += 2) a.push(c);
							return a;
						}),
						lt: k(function (a, b, c) {
							for (var d = c < 0 ? c + b : c; --d >= 0; ) a.push(d);
							return a;
						}),
						gt: k(function (a, b, c) {
							for (var d = c < 0 ? c + b : c; ++d < b; ) a.push(d);
							return a;
						}),
					},
				}),
				(x.pseudos.nth = x.pseudos.eq);
			for (v in { radio: !0, checkbox: !0, file: !0, password: !0, image: !0 }) x.pseudos[v] = h(v);
			for (v in { submit: !0, reset: !0 }) x.pseudos[v] = i(v);
			return (
				(m.prototype = x.filters = x.pseudos),
				(x.setFilters = new m()),
				(A = b.tokenize = function (a, c) {
					var d,
						e,
						f,
						g,
						h,
						i,
						j,
						k = T[a + " "];
					if (k) return c ? 0 : k.slice(0);
					for (h = a, i = [], j = x.preFilter; h; ) {
						(d && !(e = ia.exec(h))) || (e && (h = h.slice(e[0].length) || h), i.push((f = []))), (d = !1), (e = ja.exec(h)) && ((d = e.shift()), f.push({ value: d, type: e[0].replace(ha, " ") }), (h = h.slice(d.length)));
						for (g in x.filter) !(e = na[g].exec(h)) || (j[g] && !(e = j[g](e))) || ((d = e.shift()), f.push({ value: d, type: g, matches: e }), (h = h.slice(d.length)));
						if (!d) break;
					}
					return c ? h.length : h ? b.error(a) : T(a, i).slice(0);
				}),
				(B = b.compile = function (a, b) {
					var c,
						d = [],
						e = [],
						f = U[a + " "];
					if (!f) {
						for (b || (b = A(a)), c = b.length; c--; ) (f = t(b[c])), f[O] ? d.push(f) : e.push(f);
						(f = U(a, u(e, d))), (f.selector = a);
					}
					return f;
				}),
				(C = b.select = function (a, b, c, d) {
					var e,
						f,
						g,
						h,
						i,
						j = "function" == typeof a && a,
						k = !d && A((a = j.selector || a));
					if (((c = c || []), 1 === k.length)) {
						if (((f = k[0] = k[0].slice(0)), f.length > 2 && "ID" === (g = f[0]).type && 9 === b.nodeType && J && x.relative[f[1].type])) {
							if (!(b = (x.find.ID(g.matches[0].replace(ta, ua), b) || [])[0])) return c;
							j && (b = b.parentNode), (a = a.slice(f.shift().value.length));
						}
						for (e = na.needsContext.test(a) ? 0 : f.length; e-- && ((g = f[e]), !x.relative[(h = g.type)]); )
							if ((i = x.find[h]) && (d = i(g.matches[0].replace(ta, ua), (sa.test(f[0].type) && l(b.parentNode)) || b))) {
								if ((f.splice(e, 1), !(a = d.length && n(f)))) return $.apply(c, d), c;
								break;
							}
					}
					return (j || B(a, k))(d, b, !J, c, !b || (sa.test(a) && l(b.parentNode)) || b), c;
				}),
				(w.sortStable = O.split("").sort(V).join("") === O),
				(w.detectDuplicates = !!F),
				G(),
				(w.sortDetached = e(function (a) {
					return 1 & a.compareDocumentPosition(H.createElement("fieldset"));
				})),
				e(function (a) {
					return (a.innerHTML = "<a href='#'></a>"), "#" === a.firstChild.getAttribute("href");
				}) ||
					f("type|href|height|width", function (a, b, c) {
						if (!c) return a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2);
					}),
				(w.attributes &&
					e(function (a) {
						return (a.innerHTML = "<input/>"), a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value");
					})) ||
					f("value", function (a, b, c) {
						if (!c && "input" === a.nodeName.toLowerCase()) return a.defaultValue;
					}),
				e(function (a) {
					return null == a.getAttribute("disabled");
				}) ||
					f(ba, function (a, b, c) {
						var d;
						if (!c) return !0 === a[b] ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null;
					}),
				b
			);
		})(a);
	(pa.find = ua), (pa.expr = ua.selectors), (pa.expr[":"] = pa.expr.pseudos), (pa.uniqueSort = pa.unique = ua.uniqueSort), (pa.text = ua.getText), (pa.isXMLDoc = ua.isXML), (pa.contains = ua.contains), (pa.escapeSelector = ua.escape);
	var va = function (a, b, c) {
			for (var d = [], e = void 0 !== c; (a = a[b]) && 9 !== a.nodeType; )
				if (1 === a.nodeType) {
					if (e && pa(a).is(c)) break;
					d.push(a);
				}
			return d;
		},
		wa = function (a, b) {
			for (var c = []; a; a = a.nextSibling) 1 === a.nodeType && a !== b && c.push(a);
			return c;
		},
		xa = pa.expr.match.needsContext,
		ya = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i,
		za = /^.[^:#\[\.,]*$/;
	(pa.filter = function (a, b, c) {
		var d = b[0];
		return (
			c && (a = ":not(" + a + ")"),
			1 === b.length && 1 === d.nodeType
				? pa.find.matchesSelector(d, a)
					? [d]
					: []
				: pa.find.matches(
						a,
						pa.grep(b, function (a) {
							return 1 === a.nodeType;
						})
				  )
		);
	}),
		pa.fn.extend({
			find: function (a) {
				var b,
					c,
					d = this.length,
					e = this;
				if ("string" != typeof a)
					return this.pushStack(
						pa(a).filter(function () {
							for (b = 0; b < d; b++) if (pa.contains(e[b], this)) return !0;
						})
					);
				for (c = this.pushStack([]), b = 0; b < d; b++) pa.find(a, e[b], c);
				return d > 1 ? pa.uniqueSort(c) : c;
			},
			filter: function (a) {
				return this.pushStack(f(this, a || [], !1));
			},
			not: function (a) {
				return this.pushStack(f(this, a || [], !0));
			},
			is: function (a) {
				return !!f(this, "string" == typeof a && xa.test(a) ? pa(a) : a || [], !1).length;
			},
		});
	var Aa,
		Ba = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
	((pa.fn.init = function (a, b, c) {
		var d, e;
		if (!a) return this;
		if (((c = c || Aa), "string" == typeof a)) {
			if (!(d = "<" === a[0] && ">" === a[a.length - 1] && a.length >= 3 ? [null, a, null] : Ba.exec(a)) || (!d[1] && b)) return !b || b.jquery ? (b || c).find(a) : this.constructor(b).find(a);
			if (d[1]) {
				if (((b = b instanceof pa ? b[0] : b), pa.merge(this, pa.parseHTML(d[1], b && b.nodeType ? b.ownerDocument || b : ca, !0)), ya.test(d[1]) && pa.isPlainObject(b))) for (d in b) pa.isFunction(this[d]) ? this[d](b[d]) : this.attr(d, b[d]);
				return this;
			}
			return (e = ca.getElementById(d[2])), e && ((this[0] = e), (this.length = 1)), this;
		}
		return a.nodeType ? ((this[0] = a), (this.length = 1), this) : pa.isFunction(a) ? (void 0 !== c.ready ? c.ready(a) : a(pa)) : pa.makeArray(a, this);
	}).prototype = pa.fn),
		(Aa = pa(ca));
	var Ca = /^(?:parents|prev(?:Until|All))/,
		Da = { children: !0, contents: !0, next: !0, prev: !0 };
	pa.fn.extend({
		has: function (a) {
			var b = pa(a, this),
				c = b.length;
			return this.filter(function () {
				for (var a = 0; a < c; a++) if (pa.contains(this, b[a])) return !0;
			});
		},
		closest: function (a, b) {
			var c,
				d = 0,
				e = this.length,
				f = [],
				g = "string" != typeof a && pa(a);
			if (!xa.test(a))
				for (; d < e; d++)
					for (c = this[d]; c && c !== b; c = c.parentNode)
						if (c.nodeType < 11 && (g ? g.index(c) > -1 : 1 === c.nodeType && pa.find.matchesSelector(c, a))) {
							f.push(c);
							break;
						}
			return this.pushStack(f.length > 1 ? pa.uniqueSort(f) : f);
		},
		index: function (a) {
			return a ? ("string" == typeof a ? ha.call(pa(a), this[0]) : ha.call(this, a.jquery ? a[0] : a)) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
		},
		add: function (a, b) {
			return this.pushStack(pa.uniqueSort(pa.merge(this.get(), pa(a, b))));
		},
		addBack: function (a) {
			return this.add(null == a ? this.prevObject : this.prevObject.filter(a));
		},
	}),
		pa.each(
			{
				parent: function (a) {
					var b = a.parentNode;
					return b && 11 !== b.nodeType ? b : null;
				},
				parents: function (a) {
					return va(a, "parentNode");
				},
				parentsUntil: function (a, b, c) {
					return va(a, "parentNode", c);
				},
				next: function (a) {
					return g(a, "nextSibling");
				},
				prev: function (a) {
					return g(a, "previousSibling");
				},
				nextAll: function (a) {
					return va(a, "nextSibling");
				},
				prevAll: function (a) {
					return va(a, "previousSibling");
				},
				nextUntil: function (a, b, c) {
					return va(a, "nextSibling", c);
				},
				prevUntil: function (a, b, c) {
					return va(a, "previousSibling", c);
				},
				siblings: function (a) {
					return wa((a.parentNode || {}).firstChild, a);
				},
				children: function (a) {
					return wa(a.firstChild);
				},
				contents: function (a) {
					return e(a, "iframe") ? a.contentDocument : (e(a, "template") && (a = a.content || a), pa.merge([], a.childNodes));
				},
			},
			function (a, b) {
				pa.fn[a] = function (c, d) {
					var e = pa.map(this, b, c);
					return "Until" !== a.slice(-5) && (d = c), d && "string" == typeof d && (e = pa.filter(d, e)), this.length > 1 && (Da[a] || pa.uniqueSort(e), Ca.test(a) && e.reverse()), this.pushStack(e);
				};
			}
		);
	var Ea = /[^\x20\t\r\n\f]+/g;
	(pa.Callbacks = function (a) {
		a = "string" == typeof a ? h(a) : pa.extend({}, a);
		var b,
			c,
			d,
			e,
			f = [],
			g = [],
			i = -1,
			j = function () {
				for (e = e || a.once, d = b = !0; g.length; i = -1) for (c = g.shift(); ++i < f.length; ) !1 === f[i].apply(c[0], c[1]) && a.stopOnFalse && ((i = f.length), (c = !1));
				a.memory || (c = !1), (b = !1), e && (f = c ? [] : "");
			},
			k = {
				add: function () {
					return (
						f &&
							(c && !b && ((i = f.length - 1), g.push(c)),
							(function b(c) {
								pa.each(c, function (c, d) {
									pa.isFunction(d) ? (a.unique && k.has(d)) || f.push(d) : d && d.length && "string" !== pa.type(d) && b(d);
								});
							})(arguments),
							c && !b && j()),
						this
					);
				},
				remove: function () {
					return (
						pa.each(arguments, function (a, b) {
							for (var c; (c = pa.inArray(b, f, c)) > -1; ) f.splice(c, 1), c <= i && i--;
						}),
						this
					);
				},
				has: function (a) {
					return a ? pa.inArray(a, f) > -1 : f.length > 0;
				},
				empty: function () {
					return f && (f = []), this;
				},
				disable: function () {
					return (e = g = []), (f = c = ""), this;
				},
				disabled: function () {
					return !f;
				},
				lock: function () {
					return (e = g = []), c || b || (f = c = ""), this;
				},
				locked: function () {
					return !!e;
				},
				fireWith: function (a, c) {
					return e || ((c = c || []), (c = [a, c.slice ? c.slice() : c]), g.push(c), b || j()), this;
				},
				fire: function () {
					return k.fireWith(this, arguments), this;
				},
				fired: function () {
					return !!d;
				},
			};
		return k;
	}),
		pa.extend({
			Deferred: function (b) {
				var c = [
						["notify", "progress", pa.Callbacks("memory"), pa.Callbacks("memory"), 2],
						["resolve", "done", pa.Callbacks("once memory"), pa.Callbacks("once memory"), 0, "resolved"],
						["reject", "fail", pa.Callbacks("once memory"), pa.Callbacks("once memory"), 1, "rejected"],
					],
					d = "pending",
					e = {
						state: function () {
							return d;
						},
						always: function () {
							return f.done(arguments).fail(arguments), this;
						},
						catch: function (a) {
							return e.then(null, a);
						},
						pipe: function () {
							var a = arguments;
							return pa
								.Deferred(function (b) {
									pa.each(c, function (c, d) {
										var e = pa.isFunction(a[d[4]]) && a[d[4]];
										f[d[1]](function () {
											var a = e && e.apply(this, arguments);
											a && pa.isFunction(a.promise) ? a.promise().progress(b.notify).done(b.resolve).fail(b.reject) : b[d[0] + "With"](this, e ? [a] : arguments);
										});
									}),
										(a = null);
								})
								.promise();
						},
						then: function (b, d, e) {
							function f(b, c, d, e) {
								return function () {
									var h = this,
										k = arguments,
										l = function () {
											var a, l;
											if (!(b < g)) {
												if ((a = d.apply(h, k)) === c.promise()) throw new TypeError("Thenable self-resolution");
												(l = a && ("object" == typeof a || "function" == typeof a) && a.then), pa.isFunction(l) ? (e ? l.call(a, f(g, c, i, e), f(g, c, j, e)) : (g++, l.call(a, f(g, c, i, e), f(g, c, j, e), f(g, c, i, c.notifyWith)))) : (d !== i && ((h = void 0), (k = [a])), (e || c.resolveWith)(h, k));
											}
										},
										m = e
											? l
											: function () {
													try {
														l();
													} catch (a) {
														pa.Deferred.exceptionHook && pa.Deferred.exceptionHook(a, m.stackTrace), b + 1 >= g && (d !== j && ((h = void 0), (k = [a])), c.rejectWith(h, k));
													}
											  };
									b ? m() : (pa.Deferred.getStackHook && (m.stackTrace = pa.Deferred.getStackHook()), a.setTimeout(m));
								};
							}
							var g = 0;
							return pa
								.Deferred(function (a) {
									c[0][3].add(f(0, a, pa.isFunction(e) ? e : i, a.notifyWith)), c[1][3].add(f(0, a, pa.isFunction(b) ? b : i)), c[2][3].add(f(0, a, pa.isFunction(d) ? d : j));
								})
								.promise();
						},
						promise: function (a) {
							return null != a ? pa.extend(a, e) : e;
						},
					},
					f = {};
				return (
					pa.each(c, function (a, b) {
						var g = b[2],
							h = b[5];
						(e[b[1]] = g.add),
							h &&
								g.add(
									function () {
										d = h;
									},
									c[3 - a][2].disable,
									c[0][2].lock
								),
							g.add(b[3].fire),
							(f[b[0]] = function () {
								return f[b[0] + "With"](this === f ? void 0 : this, arguments), this;
							}),
							(f[b[0] + "With"] = g.fireWith);
					}),
					e.promise(f),
					b && b.call(f, f),
					f
				);
			},
			when: function (a) {
				var b = arguments.length,
					c = b,
					d = Array(c),
					e = ea.call(arguments),
					f = pa.Deferred(),
					g = function (a) {
						return function (c) {
							(d[a] = this), (e[a] = arguments.length > 1 ? ea.call(arguments) : c), --b || f.resolveWith(d, e);
						};
					};
				if (b <= 1 && (k(a, f.done(g(c)).resolve, f.reject, !b), "pending" === f.state() || pa.isFunction(e[c] && e[c].then))) return f.then();
				for (; c--; ) k(e[c], g(c), f.reject);
				return f.promise();
			},
		});
	var Fa = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
	(pa.Deferred.exceptionHook = function (b, c) {
		a.console && a.console.warn && b && Fa.test(b.name) && a.console.warn("jQuery.Deferred exception: " + b.message, b.stack, c);
	}),
		(pa.readyException = function (b) {
			a.setTimeout(function () {
				throw b;
			});
		});
	var Ga = pa.Deferred();
	(pa.fn.ready = function (a) {
		return (
			Ga.then(a).catch(function (a) {
				pa.readyException(a);
			}),
			this
		);
	}),
		pa.extend({
			isReady: !1,
			readyWait: 1,
			ready: function (a) {
				(!0 === a ? --pa.readyWait : pa.isReady) || ((pa.isReady = !0), (!0 !== a && --pa.readyWait > 0) || Ga.resolveWith(ca, [pa]));
			},
		}),
		(pa.ready.then = Ga.then),
		"complete" === ca.readyState || ("loading" !== ca.readyState && !ca.documentElement.doScroll) ? a.setTimeout(pa.ready) : (ca.addEventListener("DOMContentLoaded", l), a.addEventListener("load", l));
	var Ha = function (a, b, c, d, e, f, g) {
			var h = 0,
				i = a.length,
				j = null == c;
			if ("object" === pa.type(c)) {
				e = !0;
				for (h in c) Ha(a, b, h, c[h], !0, f, g);
			} else if (
				void 0 !== d &&
				((e = !0),
				pa.isFunction(d) || (g = !0),
				j &&
					(g
						? (b.call(a, d), (b = null))
						: ((j = b),
						  (b = function (a, b, c) {
								return j.call(pa(a), c);
						  }))),
				b)
			)
				for (; h < i; h++) b(a[h], c, g ? d : d.call(a[h], h, b(a[h], c)));
			return e ? a : j ? b.call(a) : i ? b(a[0], c) : f;
		},
		Ia = function (a) {
			return 1 === a.nodeType || 9 === a.nodeType || !+a.nodeType;
		};
	(m.uid = 1),
		(m.prototype = {
			cache: function (a) {
				var b = a[this.expando];
				return b || ((b = {}), Ia(a) && (a.nodeType ? (a[this.expando] = b) : Object.defineProperty(a, this.expando, { value: b, configurable: !0 }))), b;
			},
			set: function (a, b, c) {
				var d,
					e = this.cache(a);
				if ("string" == typeof b) e[pa.camelCase(b)] = c;
				else for (d in b) e[pa.camelCase(d)] = b[d];
				return e;
			},
			get: function (a, b) {
				return void 0 === b ? this.cache(a) : a[this.expando] && a[this.expando][pa.camelCase(b)];
			},
			access: function (a, b, c) {
				return void 0 === b || (b && "string" == typeof b && void 0 === c) ? this.get(a, b) : (this.set(a, b, c), void 0 !== c ? c : b);
			},
			remove: function (a, b) {
				var c,
					d = a[this.expando];
				if (void 0 !== d) {
					if (void 0 !== b) {
						Array.isArray(b) ? (b = b.map(pa.camelCase)) : ((b = pa.camelCase(b)), (b = b in d ? [b] : b.match(Ea) || [])), (c = b.length);
						for (; c--; ) delete d[b[c]];
					}
					(void 0 === b || pa.isEmptyObject(d)) && (a.nodeType ? (a[this.expando] = void 0) : delete a[this.expando]);
				}
			},
			hasData: function (a) {
				var b = a[this.expando];
				return void 0 !== b && !pa.isEmptyObject(b);
			},
		});
	var Ja = new m(),
		Ka = new m(),
		La = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
		Ma = /[A-Z]/g;
	pa.extend({
		hasData: function (a) {
			return Ka.hasData(a) || Ja.hasData(a);
		},
		data: function (a, b, c) {
			return Ka.access(a, b, c);
		},
		removeData: function (a, b) {
			Ka.remove(a, b);
		},
		_data: function (a, b, c) {
			return Ja.access(a, b, c);
		},
		_removeData: function (a, b) {
			Ja.remove(a, b);
		},
	}),
		pa.fn.extend({
			data: function (a, b) {
				var c,
					d,
					e,
					f = this[0],
					g = f && f.attributes;
				if (void 0 === a) {
					if (this.length && ((e = Ka.get(f)), 1 === f.nodeType && !Ja.get(f, "hasDataAttrs"))) {
						for (c = g.length; c--; ) g[c] && ((d = g[c].name), 0 === d.indexOf("data-") && ((d = pa.camelCase(d.slice(5))), o(f, d, e[d])));
						Ja.set(f, "hasDataAttrs", !0);
					}
					return e;
				}
				return "object" == typeof a
					? this.each(function () {
							Ka.set(this, a);
					  })
					: Ha(
							this,
							function (b) {
								var c;
								if (f && void 0 === b) {
									if (void 0 !== (c = Ka.get(f, a))) return c;
									if (void 0 !== (c = o(f, a))) return c;
								} else
									this.each(function () {
										Ka.set(this, a, b);
									});
							},
							null,
							b,
							arguments.length > 1,
							null,
							!0
					  );
			},
			removeData: function (a) {
				return this.each(function () {
					Ka.remove(this, a);
				});
			},
		}),
		pa.extend({
			queue: function (a, b, c) {
				var d;
				if (a) return (b = (b || "fx") + "queue"), (d = Ja.get(a, b)), c && (!d || Array.isArray(c) ? (d = Ja.access(a, b, pa.makeArray(c))) : d.push(c)), d || [];
			},
			dequeue: function (a, b) {
				b = b || "fx";
				var c = pa.queue(a, b),
					d = c.length,
					e = c.shift(),
					f = pa._queueHooks(a, b),
					g = function () {
						pa.dequeue(a, b);
					};
				"inprogress" === e && ((e = c.shift()), d--), e && ("fx" === b && c.unshift("inprogress"), delete f.stop, e.call(a, g, f)), !d && f && f.empty.fire();
			},
			_queueHooks: function (a, b) {
				var c = b + "queueHooks";
				return (
					Ja.get(a, c) ||
					Ja.access(a, c, {
						empty: pa.Callbacks("once memory").add(function () {
							Ja.remove(a, [b + "queue", c]);
						}),
					})
				);
			},
		}),
		pa.fn.extend({
			queue: function (a, b) {
				var c = 2;
				return (
					"string" != typeof a && ((b = a), (a = "fx"), c--),
					arguments.length < c
						? pa.queue(this[0], a)
						: void 0 === b
						? this
						: this.each(function () {
								var c = pa.queue(this, a, b);
								pa._queueHooks(this, a), "fx" === a && "inprogress" !== c[0] && pa.dequeue(this, a);
						  })
				);
			},
			dequeue: function (a) {
				return this.each(function () {
					pa.dequeue(this, a);
				});
			},
			clearQueue: function (a) {
				return this.queue(a || "fx", []);
			},
			promise: function (a, b) {
				var c,
					d = 1,
					e = pa.Deferred(),
					f = this,
					g = this.length,
					h = function () {
						--d || e.resolveWith(f, [f]);
					};
				for ("string" != typeof a && ((b = a), (a = void 0)), a = a || "fx"; g--; ) (c = Ja.get(f[g], a + "queueHooks")) && c.empty && (d++, c.empty.add(h));
				return h(), e.promise(b);
			},
		});
	var Na = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
		Oa = new RegExp("^(?:([+-])=|)(" + Na + ")([a-z%]*)$", "i"),
		Pa = ["Top", "Right", "Bottom", "Left"],
		Qa = function (a, b) {
			return (a = b || a), "none" === a.style.display || ("" === a.style.display && pa.contains(a.ownerDocument, a) && "none" === pa.css(a, "display"));
		},
		Ra = function (a, b, c, d) {
			var e,
				f,
				g = {};
			for (f in b) (g[f] = a.style[f]), (a.style[f] = b[f]);
			e = c.apply(a, d || []);
			for (f in b) a.style[f] = g[f];
			return e;
		},
		Sa = {};
	pa.fn.extend({
		show: function () {
			return r(this, !0);
		},
		hide: function () {
			return r(this);
		},
		toggle: function (a) {
			return "boolean" == typeof a
				? a
					? this.show()
					: this.hide()
				: this.each(function () {
						Qa(this) ? pa(this).show() : pa(this).hide();
				  });
		},
	});
	var Ta = /^(?:checkbox|radio)$/i,
		Ua = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i,
		Va = /^$|\/(?:java|ecma)script/i,
		Wa = { option: [1, "<select multiple='multiple'>", "</select>"], thead: [1, "<table>", "</table>"], col: [2, "<table><colgroup>", "</colgroup></table>"], tr: [2, "<table><tbody>", "</tbody></table>"], td: [3, "<table><tbody><tr>", "</tr></tbody></table>"], _default: [0, "", ""] };
	(Wa.optgroup = Wa.option), (Wa.tbody = Wa.tfoot = Wa.colgroup = Wa.caption = Wa.thead), (Wa.th = Wa.td);
	var Xa = /<|&#?\w+;/;
	!(function () {
		var a = ca.createDocumentFragment(),
			b = a.appendChild(ca.createElement("div")),
			c = ca.createElement("input");
		c.setAttribute("type", "radio"), c.setAttribute("checked", "checked"), c.setAttribute("name", "t"), b.appendChild(c), (na.checkClone = b.cloneNode(!0).cloneNode(!0).lastChild.checked), (b.innerHTML = "<textarea>x</textarea>"), (na.noCloneChecked = !!b.cloneNode(!0).lastChild.defaultValue);
	})();
	var Ya = ca.documentElement,
		Za = /^key/,
		$a = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
		_a = /^([^.]*)(?:\.(.+)|)/;
	(pa.event = {
		global: {},
		add: function (a, b, c, d, e) {
			var f,
				g,
				h,
				i,
				j,
				k,
				l,
				m,
				n,
				o,
				p,
				q = Ja.get(a);
			if (q)
				for (
					c.handler && ((f = c), (c = f.handler), (e = f.selector)),
						e && pa.find.matchesSelector(Ya, e),
						c.guid || (c.guid = pa.guid++),
						(i = q.events) || (i = q.events = {}),
						(g = q.handle) ||
							(g = q.handle = function (b) {
								return void 0 !== pa && pa.event.triggered !== b.type ? pa.event.dispatch.apply(a, arguments) : void 0;
							}),
						b = (b || "").match(Ea) || [""],
						j = b.length;
					j--;

				)
					(h = _a.exec(b[j]) || []), (n = p = h[1]), (o = (h[2] || "").split(".").sort()), n && ((l = pa.event.special[n] || {}), (n = (e ? l.delegateType : l.bindType) || n), (l = pa.event.special[n] || {}), (k = pa.extend({ type: n, origType: p, data: d, handler: c, guid: c.guid, selector: e, needsContext: e && pa.expr.match.needsContext.test(e), namespace: o.join(".") }, f)), (m = i[n]) || ((m = i[n] = []), (m.delegateCount = 0), (l.setup && !1 !== l.setup.call(a, d, o, g)) || (a.addEventListener && a.addEventListener(n, g))), l.add && (l.add.call(a, k), k.handler.guid || (k.handler.guid = c.guid)), e ? m.splice(m.delegateCount++, 0, k) : m.push(k), (pa.event.global[n] = !0));
		},
		remove: function (a, b, c, d, e) {
			var f,
				g,
				h,
				i,
				j,
				k,
				l,
				m,
				n,
				o,
				p,
				q = Ja.hasData(a) && Ja.get(a);
			if (q && (i = q.events)) {
				for (b = (b || "").match(Ea) || [""], j = b.length; j--; )
					if (((h = _a.exec(b[j]) || []), (n = p = h[1]), (o = (h[2] || "").split(".").sort()), n)) {
						for (l = pa.event.special[n] || {}, n = (d ? l.delegateType : l.bindType) || n, m = i[n] || [], h = h[2] && new RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)"), g = f = m.length; f--; ) (k = m[f]), (!e && p !== k.origType) || (c && c.guid !== k.guid) || (h && !h.test(k.namespace)) || (d && d !== k.selector && ("**" !== d || !k.selector)) || (m.splice(f, 1), k.selector && m.delegateCount--, l.remove && l.remove.call(a, k));
						g && !m.length && ((l.teardown && !1 !== l.teardown.call(a, o, q.handle)) || pa.removeEvent(a, n, q.handle), delete i[n]);
					} else for (n in i) pa.event.remove(a, n + b[j], c, d, !0);
				pa.isEmptyObject(i) && Ja.remove(a, "handle events");
			}
		},
		dispatch: function (a) {
			var b,
				c,
				d,
				e,
				f,
				g,
				h = pa.event.fix(a),
				i = new Array(arguments.length),
				j = (Ja.get(this, "events") || {})[h.type] || [],
				k = pa.event.special[h.type] || {};
			for (i[0] = h, b = 1; b < arguments.length; b++) i[b] = arguments[b];
			if (((h.delegateTarget = this), !k.preDispatch || !1 !== k.preDispatch.call(this, h))) {
				for (g = pa.event.handlers.call(this, h, j), b = 0; (e = g[b++]) && !h.isPropagationStopped(); ) for (h.currentTarget = e.elem, c = 0; (f = e.handlers[c++]) && !h.isImmediatePropagationStopped(); ) (h.rnamespace && !h.rnamespace.test(f.namespace)) || ((h.handleObj = f), (h.data = f.data), void 0 !== (d = ((pa.event.special[f.origType] || {}).handle || f.handler).apply(e.elem, i)) && !1 === (h.result = d) && (h.preventDefault(), h.stopPropagation()));
				return k.postDispatch && k.postDispatch.call(this, h), h.result;
			}
		},
		handlers: function (a, b) {
			var c,
				d,
				e,
				f,
				g,
				h = [],
				i = b.delegateCount,
				j = a.target;
			if (i && j.nodeType && !("click" === a.type && a.button >= 1))
				for (; j !== this; j = j.parentNode || this)
					if (1 === j.nodeType && ("click" !== a.type || !0 !== j.disabled)) {
						for (f = [], g = {}, c = 0; c < i; c++) (d = b[c]), (e = d.selector + " "), void 0 === g[e] && (g[e] = d.needsContext ? pa(e, this).index(j) > -1 : pa.find(e, this, null, [j]).length), g[e] && f.push(d);
						f.length && h.push({ elem: j, handlers: f });
					}
			return (j = this), i < b.length && h.push({ elem: j, handlers: b.slice(i) }), h;
		},
		addProp: function (a, b) {
			Object.defineProperty(pa.Event.prototype, a, {
				enumerable: !0,
				configurable: !0,
				get: pa.isFunction(b)
					? function () {
							if (this.originalEvent) return b(this.originalEvent);
					  }
					: function () {
							if (this.originalEvent) return this.originalEvent[a];
					  },
				set: function (b) {
					Object.defineProperty(this, a, { enumerable: !0, configurable: !0, writable: !0, value: b });
				},
			});
		},
		fix: function (a) {
			return a[pa.expando] ? a : new pa.Event(a);
		},
		special: {
			load: { noBubble: !0 },
			focus: {
				trigger: function () {
					if (this !== x() && this.focus) return this.focus(), !1;
				},
				delegateType: "focusin",
			},
			blur: {
				trigger: function () {
					if (this === x() && this.blur) return this.blur(), !1;
				},
				delegateType: "focusout",
			},
			click: {
				trigger: function () {
					if ("checkbox" === this.type && this.click && e(this, "input")) return this.click(), !1;
				},
				_default: function (a) {
					return e(a.target, "a");
				},
			},
			beforeunload: {
				postDispatch: function (a) {
					void 0 !== a.result && a.originalEvent && (a.originalEvent.returnValue = a.result);
				},
			},
		},
	}),
		(pa.removeEvent = function (a, b, c) {
			a.removeEventListener && a.removeEventListener(b, c);
		}),
		(pa.Event = function (a, b) {
			if (!(this instanceof pa.Event)) return new pa.Event(a, b);
			a && a.type ? ((this.originalEvent = a), (this.type = a.type), (this.isDefaultPrevented = a.defaultPrevented || (void 0 === a.defaultPrevented && !1 === a.returnValue) ? v : w), (this.target = a.target && 3 === a.target.nodeType ? a.target.parentNode : a.target), (this.currentTarget = a.currentTarget), (this.relatedTarget = a.relatedTarget)) : (this.type = a), b && pa.extend(this, b), (this.timeStamp = (a && a.timeStamp) || pa.now()), (this[pa.expando] = !0);
		}),
		(pa.Event.prototype = {
			constructor: pa.Event,
			isDefaultPrevented: w,
			isPropagationStopped: w,
			isImmediatePropagationStopped: w,
			isSimulated: !1,
			preventDefault: function () {
				var a = this.originalEvent;
				(this.isDefaultPrevented = v), a && !this.isSimulated && a.preventDefault();
			},
			stopPropagation: function () {
				var a = this.originalEvent;
				(this.isPropagationStopped = v), a && !this.isSimulated && a.stopPropagation();
			},
			stopImmediatePropagation: function () {
				var a = this.originalEvent;
				(this.isImmediatePropagationStopped = v), a && !this.isSimulated && a.stopImmediatePropagation(), this.stopPropagation();
			},
		}),
		pa.each(
			{
				altKey: !0,
				bubbles: !0,
				cancelable: !0,
				changedTouches: !0,
				ctrlKey: !0,
				detail: !0,
				eventPhase: !0,
				metaKey: !0,
				pageX: !0,
				pageY: !0,
				shiftKey: !0,
				view: !0,
				char: !0,
				charCode: !0,
				key: !0,
				keyCode: !0,
				button: !0,
				buttons: !0,
				clientX: !0,
				clientY: !0,
				offsetX: !0,
				offsetY: !0,
				pointerId: !0,
				pointerType: !0,
				screenX: !0,
				screenY: !0,
				targetTouches: !0,
				toElement: !0,
				touches: !0,
				which: function (a) {
					var b = a.button;
					return null == a.which && Za.test(a.type) ? (null != a.charCode ? a.charCode : a.keyCode) : !a.which && void 0 !== b && $a.test(a.type) ? (1 & b ? 1 : 2 & b ? 3 : 4 & b ? 2 : 0) : a.which;
				},
			},
			pa.event.addProp
		),
		pa.each({ mouseenter: "mouseover", mouseleave: "mouseout", pointerenter: "pointerover", pointerleave: "pointerout" }, function (a, b) {
			pa.event.special[a] = {
				delegateType: b,
				bindType: b,
				handle: function (a) {
					var c,
						d = this,
						e = a.relatedTarget,
						f = a.handleObj;
					return (e && (e === d || pa.contains(d, e))) || ((a.type = f.origType), (c = f.handler.apply(this, arguments)), (a.type = b)), c;
				},
			};
		}),
		pa.fn.extend({
			on: function (a, b, c, d) {
				return y(this, a, b, c, d);
			},
			one: function (a, b, c, d) {
				return y(this, a, b, c, d, 1);
			},
			off: function (a, b, c) {
				var d, e;
				if (a && a.preventDefault && a.handleObj) return (d = a.handleObj), pa(a.delegateTarget).off(d.namespace ? d.origType + "." + d.namespace : d.origType, d.selector, d.handler), this;
				if ("object" == typeof a) {
					for (e in a) this.off(e, b, a[e]);
					return this;
				}
				return (
					(!1 !== b && "function" != typeof b) || ((c = b), (b = void 0)),
					!1 === c && (c = w),
					this.each(function () {
						pa.event.remove(this, a, c, b);
					})
				);
			},
		});
	var ab = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
		bb = /<script|<style|<link/i,
		cb = /checked\s*(?:[^=]|=\s*.checked.)/i,
		db = /^true\/(.*)/,
		eb = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
	pa.extend({
		htmlPrefilter: function (a) {
			return a.replace(ab, "<$1></$2>");
		},
		clone: function (a, b, c) {
			var d,
				e,
				f,
				g,
				h = a.cloneNode(!0),
				i = pa.contains(a.ownerDocument, a);
			if (!(na.noCloneChecked || (1 !== a.nodeType && 11 !== a.nodeType) || pa.isXMLDoc(a))) for (g = s(h), f = s(a), d = 0, e = f.length; d < e; d++) D(f[d], g[d]);
			if (b)
				if (c) for (f = f || s(a), g = g || s(h), d = 0, e = f.length; d < e; d++) C(f[d], g[d]);
				else C(a, h);
			return (g = s(h, "script")), g.length > 0 && t(g, !i && s(a, "script")), h;
		},
		cleanData: function (a) {
			for (var b, c, d, e = pa.event.special, f = 0; void 0 !== (c = a[f]); f++)
				if (Ia(c)) {
					if ((b = c[Ja.expando])) {
						if (b.events) for (d in b.events) e[d] ? pa.event.remove(c, d) : pa.removeEvent(c, d, b.handle);
						c[Ja.expando] = void 0;
					}
					c[Ka.expando] && (c[Ka.expando] = void 0);
				}
		},
	}),
		pa.fn.extend({
			detach: function (a) {
				return F(this, a, !0);
			},
			remove: function (a) {
				return F(this, a);
			},
			text: function (a) {
				return Ha(
					this,
					function (a) {
						return void 0 === a
							? pa.text(this)
							: this.empty().each(function () {
									(1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType) || (this.textContent = a);
							  });
					},
					null,
					a,
					arguments.length
				);
			},
			append: function () {
				return E(this, arguments, function (a) {
					if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
						z(this, a).appendChild(a);
					}
				});
			},
			prepend: function () {
				return E(this, arguments, function (a) {
					if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
						var b = z(this, a);
						b.insertBefore(a, b.firstChild);
					}
				});
			},
			before: function () {
				return E(this, arguments, function (a) {
					this.parentNode && this.parentNode.insertBefore(a, this);
				});
			},
			after: function () {
				return E(this, arguments, function (a) {
					this.parentNode && this.parentNode.insertBefore(a, this.nextSibling);
				});
			},
			empty: function () {
				for (var a, b = 0; null != (a = this[b]); b++) 1 === a.nodeType && (pa.cleanData(s(a, !1)), (a.textContent = ""));
				return this;
			},
			clone: function (a, b) {
				return (
					(a = null != a && a),
					(b = null == b ? a : b),
					this.map(function () {
						return pa.clone(this, a, b);
					})
				);
			},
			html: function (a) {
				return Ha(
					this,
					function (a) {
						var b = this[0] || {},
							c = 0,
							d = this.length;
						if (void 0 === a && 1 === b.nodeType) return b.innerHTML;
						if ("string" == typeof a && !bb.test(a) && !Wa[(Ua.exec(a) || ["", ""])[1].toLowerCase()]) {
							a = pa.htmlPrefilter(a);
							try {
								for (; c < d; c++) (b = this[c] || {}), 1 === b.nodeType && (pa.cleanData(s(b, !1)), (b.innerHTML = a));
								b = 0;
							} catch (a) {}
						}
						b && this.empty().append(a);
					},
					null,
					a,
					arguments.length
				);
			},
			replaceWith: function () {
				var a = [];
				return E(
					this,
					arguments,
					function (b) {
						var c = this.parentNode;
						pa.inArray(this, a) < 0 && (pa.cleanData(s(this)), c && c.replaceChild(b, this));
					},
					a
				);
			},
		}),
		pa.each({ appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith" }, function (a, b) {
			pa.fn[a] = function (a) {
				for (var c, d = [], e = pa(a), f = e.length - 1, g = 0; g <= f; g++) (c = g === f ? this : this.clone(!0)), pa(e[g])[b](c), ga.apply(d, c.get());
				return this.pushStack(d);
			};
		});
	var fb = /^margin/,
		gb = new RegExp("^(" + Na + ")(?!px)[a-z%]+$", "i"),
		hb = function (b) {
			var c = b.ownerDocument.defaultView;
			return (c && c.opener) || (c = a), c.getComputedStyle(b);
		};
	!(function () {
		function b() {
			if (h) {
				(h.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%"), (h.innerHTML = ""), Ya.appendChild(g);
				var b = a.getComputedStyle(h);
				(c = "1%" !== b.top), (f = "2px" === b.marginLeft), (d = "4px" === b.width), (h.style.marginRight = "50%"), (e = "4px" === b.marginRight), Ya.removeChild(g), (h = null);
			}
		}
		var c,
			d,
			e,
			f,
			g = ca.createElement("div"),
			h = ca.createElement("div");
		h.style &&
			((h.style.backgroundClip = "content-box"),
			(h.cloneNode(!0).style.backgroundClip = ""),
			(na.clearCloneStyle = "content-box" === h.style.backgroundClip),
			(g.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute"),
			g.appendChild(h),
			pa.extend(na, {
				pixelPosition: function () {
					return b(), c;
				},
				boxSizingReliable: function () {
					return b(), d;
				},
				pixelMarginRight: function () {
					return b(), e;
				},
				reliableMarginLeft: function () {
					return b(), f;
				},
			}));
	})();
	var ib = /^(none|table(?!-c[ea]).+)/,
		jb = /^--/,
		kb = { position: "absolute", visibility: "hidden", display: "block" },
		lb = { letterSpacing: "0", fontWeight: "400" },
		mb = ["Webkit", "Moz", "ms"],
		nb = ca.createElement("div").style;
	pa.extend({
		cssHooks: {
			opacity: {
				get: function (a, b) {
					if (b) {
						var c = G(a, "opacity");
						return "" === c ? "1" : c;
					}
				},
			},
		},
		cssNumber: { animationIterationCount: !0, columnCount: !0, fillOpacity: !0, flexGrow: !0, flexShrink: !0, fontWeight: !0, lineHeight: !0, opacity: !0, order: !0, orphans: !0, widows: !0, zIndex: !0, zoom: !0 },
		cssProps: { float: "cssFloat" },
		style: function (a, b, c, d) {
			if (a && 3 !== a.nodeType && 8 !== a.nodeType && a.style) {
				var e,
					f,
					g,
					h = pa.camelCase(b),
					i = jb.test(b),
					j = a.style;
				if ((i || (b = J(h)), (g = pa.cssHooks[b] || pa.cssHooks[h]), void 0 === c)) return g && "get" in g && void 0 !== (e = g.get(a, !1, d)) ? e : j[b];
				(f = typeof c), "string" === f && (e = Oa.exec(c)) && e[1] && ((c = p(a, b, e)), (f = "number")), null != c && c === c && ("number" === f && (c += (e && e[3]) || (pa.cssNumber[h] ? "" : "px")), na.clearCloneStyle || "" !== c || 0 !== b.indexOf("background") || (j[b] = "inherit"), (g && "set" in g && void 0 === (c = g.set(a, c, d))) || (i ? j.setProperty(b, c) : (j[b] = c)));
			}
		},
		css: function (a, b, c, d) {
			var e,
				f,
				g,
				h = pa.camelCase(b);
			return jb.test(b) || (b = J(h)), (g = pa.cssHooks[b] || pa.cssHooks[h]), g && "get" in g && (e = g.get(a, !0, c)), void 0 === e && (e = G(a, b, d)), "normal" === e && b in lb && (e = lb[b]), "" === c || c ? ((f = parseFloat(e)), !0 === c || isFinite(f) ? f || 0 : e) : e;
		},
	}),
		pa.each(["height", "width"], function (a, b) {
			pa.cssHooks[b] = {
				get: function (a, c, d) {
					if (c)
						return !ib.test(pa.css(a, "display")) || (a.getClientRects().length && a.getBoundingClientRect().width)
							? M(a, b, d)
							: Ra(a, kb, function () {
									return M(a, b, d);
							  });
				},
				set: function (a, c, d) {
					var e,
						f = d && hb(a),
						g = d && L(a, b, d, "border-box" === pa.css(a, "boxSizing", !1, f), f);
					return g && (e = Oa.exec(c)) && "px" !== (e[3] || "px") && ((a.style[b] = c), (c = pa.css(a, b))), K(a, c, g);
				},
			};
		}),
		(pa.cssHooks.marginLeft = H(na.reliableMarginLeft, function (a, b) {
			if (b)
				return (
					(parseFloat(G(a, "marginLeft")) ||
						a.getBoundingClientRect().left -
							Ra(a, { marginLeft: 0 }, function () {
								return a.getBoundingClientRect().left;
							})) + "px"
				);
		})),
		pa.each({ margin: "", padding: "", border: "Width" }, function (a, b) {
			(pa.cssHooks[a + b] = {
				expand: function (c) {
					for (var d = 0, e = {}, f = "string" == typeof c ? c.split(" ") : [c]; d < 4; d++) e[a + Pa[d] + b] = f[d] || f[d - 2] || f[0];
					return e;
				},
			}),
				fb.test(a) || (pa.cssHooks[a + b].set = K);
		}),
		pa.fn.extend({
			css: function (a, b) {
				return Ha(
					this,
					function (a, b, c) {
						var d,
							e,
							f = {},
							g = 0;
						if (Array.isArray(b)) {
							for (d = hb(a), e = b.length; g < e; g++) f[b[g]] = pa.css(a, b[g], !1, d);
							return f;
						}
						return void 0 !== c ? pa.style(a, b, c) : pa.css(a, b);
					},
					a,
					b,
					arguments.length > 1
				);
			},
		}),
		(pa.Tween = N),
		(N.prototype = {
			constructor: N,
			init: function (a, b, c, d, e, f) {
				(this.elem = a), (this.prop = c), (this.easing = e || pa.easing._default), (this.options = b), (this.start = this.now = this.cur()), (this.end = d), (this.unit = f || (pa.cssNumber[c] ? "" : "px"));
			},
			cur: function () {
				var a = N.propHooks[this.prop];
				return a && a.get ? a.get(this) : N.propHooks._default.get(this);
			},
			run: function (a) {
				var b,
					c = N.propHooks[this.prop];
				return this.options.duration ? (this.pos = b = pa.easing[this.easing](a, this.options.duration * a, 0, 1, this.options.duration)) : (this.pos = b = a), (this.now = (this.end - this.start) * b + this.start), this.options.step && this.options.step.call(this.elem, this.now, this), c && c.set ? c.set(this) : N.propHooks._default.set(this), this;
			},
		}),
		(N.prototype.init.prototype = N.prototype),
		(N.propHooks = {
			_default: {
				get: function (a) {
					var b;
					return 1 !== a.elem.nodeType || (null != a.elem[a.prop] && null == a.elem.style[a.prop]) ? a.elem[a.prop] : ((b = pa.css(a.elem, a.prop, "")), b && "auto" !== b ? b : 0);
				},
				set: function (a) {
					pa.fx.step[a.prop] ? pa.fx.step[a.prop](a) : 1 !== a.elem.nodeType || (null == a.elem.style[pa.cssProps[a.prop]] && !pa.cssHooks[a.prop]) ? (a.elem[a.prop] = a.now) : pa.style(a.elem, a.prop, a.now + a.unit);
				},
			},
		}),
		(N.propHooks.scrollTop = N.propHooks.scrollLeft = {
			set: function (a) {
				a.elem.nodeType && a.elem.parentNode && (a.elem[a.prop] = a.now);
			},
		}),
		(pa.easing = {
			linear: function (a) {
				return a;
			},
			swing: function (a) {
				return 0.5 - Math.cos(a * Math.PI) / 2;
			},
			_default: "swing",
		}),
		(pa.fx = N.prototype.init),
		(pa.fx.step = {});
	var ob,
		pb,
		qb = /^(?:toggle|show|hide)$/,
		rb = /queueHooks$/;
	(pa.Animation = pa.extend(U, {
		tweeners: {
			"*": [
				function (a, b) {
					var c = this.createTween(a, b);
					return p(c.elem, a, Oa.exec(b), c), c;
				},
			],
		},
		tweener: function (a, b) {
			pa.isFunction(a) ? ((b = a), (a = ["*"])) : (a = a.match(Ea));
			for (var c, d = 0, e = a.length; d < e; d++) (c = a[d]), (U.tweeners[c] = U.tweeners[c] || []), U.tweeners[c].unshift(b);
		},
		prefilters: [S],
		prefilter: function (a, b) {
			b ? U.prefilters.unshift(a) : U.prefilters.push(a);
		},
	})),
		(pa.speed = function (a, b, c) {
			var d = a && "object" == typeof a ? pa.extend({}, a) : { complete: c || (!c && b) || (pa.isFunction(a) && a), duration: a, easing: (c && b) || (b && !pa.isFunction(b) && b) };
			return (
				pa.fx.off ? (d.duration = 0) : "number" != typeof d.duration && (d.duration in pa.fx.speeds ? (d.duration = pa.fx.speeds[d.duration]) : (d.duration = pa.fx.speeds._default)),
				(null != d.queue && !0 !== d.queue) || (d.queue = "fx"),
				(d.old = d.complete),
				(d.complete = function () {
					pa.isFunction(d.old) && d.old.call(this), d.queue && pa.dequeue(this, d.queue);
				}),
				d
			);
		}),
		pa.fn.extend({
			fadeTo: function (a, b, c, d) {
				return this.filter(Qa).css("opacity", 0).show().end().animate({ opacity: b }, a, c, d);
			},
			animate: function (a, b, c, d) {
				var e = pa.isEmptyObject(a),
					f = pa.speed(b, c, d),
					g = function () {
						var b = U(this, pa.extend({}, a), f);
						(e || Ja.get(this, "finish")) && b.stop(!0);
					};
				return (g.finish = g), e || !1 === f.queue ? this.each(g) : this.queue(f.queue, g);
			},
			stop: function (a, b, c) {
				var d = function (a) {
					var b = a.stop;
					delete a.stop, b(c);
				};
				return (
					"string" != typeof a && ((c = b), (b = a), (a = void 0)),
					b && !1 !== a && this.queue(a || "fx", []),
					this.each(function () {
						var b = !0,
							e = null != a && a + "queueHooks",
							f = pa.timers,
							g = Ja.get(this);
						if (e) g[e] && g[e].stop && d(g[e]);
						else for (e in g) g[e] && g[e].stop && rb.test(e) && d(g[e]);
						for (e = f.length; e--; ) f[e].elem !== this || (null != a && f[e].queue !== a) || (f[e].anim.stop(c), (b = !1), f.splice(e, 1));
						(!b && c) || pa.dequeue(this, a);
					})
				);
			},
			finish: function (a) {
				return (
					!1 !== a && (a = a || "fx"),
					this.each(function () {
						var b,
							c = Ja.get(this),
							d = c[a + "queue"],
							e = c[a + "queueHooks"],
							f = pa.timers,
							g = d ? d.length : 0;
						for (c.finish = !0, pa.queue(this, a, []), e && e.stop && e.stop.call(this, !0), b = f.length; b--; ) f[b].elem === this && f[b].queue === a && (f[b].anim.stop(!0), f.splice(b, 1));
						for (b = 0; b < g; b++) d[b] && d[b].finish && d[b].finish.call(this);
						delete c.finish;
					})
				);
			},
		}),
		pa.each(["toggle", "show", "hide"], function (a, b) {
			var c = pa.fn[b];
			pa.fn[b] = function (a, d, e) {
				return null == a || "boolean" == typeof a ? c.apply(this, arguments) : this.animate(Q(b, !0), a, d, e);
			};
		}),
		pa.each({ slideDown: Q("show"), slideUp: Q("hide"), slideToggle: Q("toggle"), fadeIn: { opacity: "show" }, fadeOut: { opacity: "hide" }, fadeToggle: { opacity: "toggle" } }, function (a, b) {
			pa.fn[a] = function (a, c, d) {
				return this.animate(b, a, c, d);
			};
		}),
		(pa.timers = []),
		(pa.fx.tick = function () {
			var a,
				b = 0,
				c = pa.timers;
			for (ob = pa.now(); b < c.length; b++) (a = c[b])() || c[b] !== a || c.splice(b--, 1);
			c.length || pa.fx.stop(), (ob = void 0);
		}),
		(pa.fx.timer = function (a) {
			pa.timers.push(a), pa.fx.start();
		}),
		(pa.fx.interval = 13),
		(pa.fx.start = function () {
			pb || ((pb = !0), O());
		}),
		(pa.fx.stop = function () {
			pb = null;
		}),
		(pa.fx.speeds = { slow: 600, fast: 200, _default: 400 }),
		(pa.fn.delay = function (b, c) {
			return (
				(b = pa.fx ? pa.fx.speeds[b] || b : b),
				(c = c || "fx"),
				this.queue(c, function (c, d) {
					var e = a.setTimeout(c, b);
					d.stop = function () {
						a.clearTimeout(e);
					};
				})
			);
		}),
		(function () {
			var a = ca.createElement("input"),
				b = ca.createElement("select"),
				c = b.appendChild(ca.createElement("option"));
			(a.type = "checkbox"), (na.checkOn = "" !== a.value), (na.optSelected = c.selected), (a = ca.createElement("input")), (a.value = "t"), (a.type = "radio"), (na.radioValue = "t" === a.value);
		})();
	var sb,
		tb = pa.expr.attrHandle;
	pa.fn.extend({
		attr: function (a, b) {
			return Ha(this, pa.attr, a, b, arguments.length > 1);
		},
		removeAttr: function (a) {
			return this.each(function () {
				pa.removeAttr(this, a);
			});
		},
	}),
		pa.extend({
			attr: function (a, b, c) {
				var d,
					e,
					f = a.nodeType;
				if (3 !== f && 8 !== f && 2 !== f) return void 0 === a.getAttribute ? pa.prop(a, b, c) : ((1 === f && pa.isXMLDoc(a)) || (e = pa.attrHooks[b.toLowerCase()] || (pa.expr.match.bool.test(b) ? sb : void 0)), void 0 !== c ? (null === c ? void pa.removeAttr(a, b) : e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : (a.setAttribute(b, c + ""), c)) : e && "get" in e && null !== (d = e.get(a, b)) ? d : ((d = pa.find.attr(a, b)), null == d ? void 0 : d));
			},
			attrHooks: {
				type: {
					set: function (a, b) {
						if (!na.radioValue && "radio" === b && e(a, "input")) {
							var c = a.value;
							return a.setAttribute("type", b), c && (a.value = c), b;
						}
					},
				},
			},
			removeAttr: function (a, b) {
				var c,
					d = 0,
					e = b && b.match(Ea);
				if (e && 1 === a.nodeType) for (; (c = e[d++]); ) a.removeAttribute(c);
			},
		}),
		(sb = {
			set: function (a, b, c) {
				return !1 === b ? pa.removeAttr(a, c) : a.setAttribute(c, c), c;
			},
		}),
		pa.each(pa.expr.match.bool.source.match(/\w+/g), function (a, b) {
			var c = tb[b] || pa.find.attr;
			tb[b] = function (a, b, d) {
				var e,
					f,
					g = b.toLowerCase();
				return d || ((f = tb[g]), (tb[g] = e), (e = null != c(a, b, d) ? g : null), (tb[g] = f)), e;
			};
		});
	var ub = /^(?:input|select|textarea|button)$/i,
		vb = /^(?:a|area)$/i;
	pa.fn.extend({
		prop: function (a, b) {
			return Ha(this, pa.prop, a, b, arguments.length > 1);
		},
		removeProp: function (a) {
			return this.each(function () {
				delete this[pa.propFix[a] || a];
			});
		},
	}),
		pa.extend({
			prop: function (a, b, c) {
				var d,
					e,
					f = a.nodeType;
				if (3 !== f && 8 !== f && 2 !== f) return (1 === f && pa.isXMLDoc(a)) || ((b = pa.propFix[b] || b), (e = pa.propHooks[b])), void 0 !== c ? (e && "set" in e && void 0 !== (d = e.set(a, c, b)) ? d : (a[b] = c)) : e && "get" in e && null !== (d = e.get(a, b)) ? d : a[b];
			},
			propHooks: {
				tabIndex: {
					get: function (a) {
						var b = pa.find.attr(a, "tabindex");
						return b ? parseInt(b, 10) : ub.test(a.nodeName) || (vb.test(a.nodeName) && a.href) ? 0 : -1;
					},
				},
			},
			propFix: { for: "htmlFor", class: "className" },
		}),
		na.optSelected ||
			(pa.propHooks.selected = {
				get: function (a) {
					var b = a.parentNode;
					return b && b.parentNode && b.parentNode.selectedIndex, null;
				},
				set: function (a) {
					var b = a.parentNode;
					b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex);
				},
			}),
		pa.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
			pa.propFix[this.toLowerCase()] = this;
		}),
		pa.fn.extend({
			addClass: function (a) {
				var b,
					c,
					d,
					e,
					f,
					g,
					h,
					i = 0;
				if (pa.isFunction(a))
					return this.each(function (b) {
						pa(this).addClass(a.call(this, b, W(this)));
					});
				if ("string" == typeof a && a)
					for (b = a.match(Ea) || []; (c = this[i++]); )
						if (((e = W(c)), (d = 1 === c.nodeType && " " + V(e) + " "))) {
							for (g = 0; (f = b[g++]); ) d.indexOf(" " + f + " ") < 0 && (d += f + " ");
							(h = V(d)), e !== h && c.setAttribute("class", h);
						}
				return this;
			},
			removeClass: function (a) {
				var b,
					c,
					d,
					e,
					f,
					g,
					h,
					i = 0;
				if (pa.isFunction(a))
					return this.each(function (b) {
						pa(this).removeClass(a.call(this, b, W(this)));
					});
				if (!arguments.length) return this.attr("class", "");
				if ("string" == typeof a && a)
					for (b = a.match(Ea) || []; (c = this[i++]); )
						if (((e = W(c)), (d = 1 === c.nodeType && " " + V(e) + " "))) {
							for (g = 0; (f = b[g++]); ) for (; d.indexOf(" " + f + " ") > -1; ) d = d.replace(" " + f + " ", " ");
							(h = V(d)), e !== h && c.setAttribute("class", h);
						}
				return this;
			},
			toggleClass: function (a, b) {
				var c = typeof a;
				return "boolean" == typeof b && "string" === c
					? b
						? this.addClass(a)
						: this.removeClass(a)
					: pa.isFunction(a)
					? this.each(function (c) {
							pa(this).toggleClass(a.call(this, c, W(this), b), b);
					  })
					: this.each(function () {
							var b, d, e, f;
							if ("string" === c) for (d = 0, e = pa(this), f = a.match(Ea) || []; (b = f[d++]); ) e.hasClass(b) ? e.removeClass(b) : e.addClass(b);
							else (void 0 !== a && "boolean" !== c) || ((b = W(this)), b && Ja.set(this, "__className__", b), this.setAttribute && this.setAttribute("class", b || !1 === a ? "" : Ja.get(this, "__className__") || ""));
					  });
			},
			hasClass: function (a) {
				var b,
					c,
					d = 0;
				for (b = " " + a + " "; (c = this[d++]); ) if (1 === c.nodeType && (" " + V(W(c)) + " ").indexOf(b) > -1) return !0;
				return !1;
			},
		});
	var wb = /\r/g;
	pa.fn.extend({
		val: function (a) {
			var b,
				c,
				d,
				e = this[0];
			{
				if (arguments.length)
					return (
						(d = pa.isFunction(a)),
						this.each(function (c) {
							var e;
							1 === this.nodeType &&
								((e = d ? a.call(this, c, pa(this).val()) : a),
								null == e
									? (e = "")
									: "number" == typeof e
									? (e += "")
									: Array.isArray(e) &&
									  (e = pa.map(e, function (a) {
											return null == a ? "" : a + "";
									  })),
								((b = pa.valHooks[this.type] || pa.valHooks[this.nodeName.toLowerCase()]) && "set" in b && void 0 !== b.set(this, e, "value")) || (this.value = e));
						})
					);
				if (e) return (b = pa.valHooks[e.type] || pa.valHooks[e.nodeName.toLowerCase()]) && "get" in b && void 0 !== (c = b.get(e, "value")) ? c : ((c = e.value), "string" == typeof c ? c.replace(wb, "") : null == c ? "" : c);
			}
		},
	}),
		pa.extend({
			valHooks: {
				option: {
					get: function (a) {
						var b = pa.find.attr(a, "value");
						return null != b ? b : V(pa.text(a));
					},
				},
				select: {
					get: function (a) {
						var b,
							c,
							d,
							f = a.options,
							g = a.selectedIndex,
							h = "select-one" === a.type,
							i = h ? null : [],
							j = h ? g + 1 : f.length;
						for (d = g < 0 ? j : h ? g : 0; d < j; d++)
							if (((c = f[d]), (c.selected || d === g) && !c.disabled && (!c.parentNode.disabled || !e(c.parentNode, "optgroup")))) {
								if (((b = pa(c).val()), h)) return b;
								i.push(b);
							}
						return i;
					},
					set: function (a, b) {
						for (var c, d, e = a.options, f = pa.makeArray(b), g = e.length; g--; ) (d = e[g]), (d.selected = pa.inArray(pa.valHooks.option.get(d), f) > -1) && (c = !0);
						return c || (a.selectedIndex = -1), f;
					},
				},
			},
		}),
		pa.each(["radio", "checkbox"], function () {
			(pa.valHooks[this] = {
				set: function (a, b) {
					if (Array.isArray(b)) return (a.checked = pa.inArray(pa(a).val(), b) > -1);
				},
			}),
				na.checkOn ||
					(pa.valHooks[this].get = function (a) {
						return null === a.getAttribute("value") ? "on" : a.value;
					});
		});
	var xb = /^(?:focusinfocus|focusoutblur)$/;
	pa.extend(pa.event, {
		trigger: function (b, c, d, e) {
			var f,
				g,
				h,
				i,
				j,
				k,
				l,
				m = [d || ca],
				n = ka.call(b, "type") ? b.type : b,
				o = ka.call(b, "namespace") ? b.namespace.split(".") : [];
			if (((g = h = d = d || ca), 3 !== d.nodeType && 8 !== d.nodeType && !xb.test(n + pa.event.triggered) && (n.indexOf(".") > -1 && ((o = n.split(".")), (n = o.shift()), o.sort()), (j = n.indexOf(":") < 0 && "on" + n), (b = b[pa.expando] ? b : new pa.Event(n, "object" == typeof b && b)), (b.isTrigger = e ? 2 : 3), (b.namespace = o.join(".")), (b.rnamespace = b.namespace ? new RegExp("(^|\\.)" + o.join("\\.(?:.*\\.|)") + "(\\.|$)") : null), (b.result = void 0), b.target || (b.target = d), (c = null == c ? [b] : pa.makeArray(c, [b])), (l = pa.event.special[n] || {}), e || !l.trigger || !1 !== l.trigger.apply(d, c)))) {
				if (!e && !l.noBubble && !pa.isWindow(d)) {
					for (i = l.delegateType || n, xb.test(i + n) || (g = g.parentNode); g; g = g.parentNode) m.push(g), (h = g);
					h === (d.ownerDocument || ca) && m.push(h.defaultView || h.parentWindow || a);
				}
				for (f = 0; (g = m[f++]) && !b.isPropagationStopped(); ) (b.type = f > 1 ? i : l.bindType || n), (k = (Ja.get(g, "events") || {})[b.type] && Ja.get(g, "handle")), k && k.apply(g, c), (k = j && g[j]) && k.apply && Ia(g) && ((b.result = k.apply(g, c)), !1 === b.result && b.preventDefault());
				return (b.type = n), e || b.isDefaultPrevented() || (l._default && !1 !== l._default.apply(m.pop(), c)) || !Ia(d) || (j && pa.isFunction(d[n]) && !pa.isWindow(d) && ((h = d[j]), h && (d[j] = null), (pa.event.triggered = n), d[n](), (pa.event.triggered = void 0), h && (d[j] = h))), b.result;
			}
		},
		simulate: function (a, b, c) {
			var d = pa.extend(new pa.Event(), c, { type: a, isSimulated: !0 });
			pa.event.trigger(d, null, b);
		},
	}),
		pa.fn.extend({
			trigger: function (a, b) {
				return this.each(function () {
					pa.event.trigger(a, b, this);
				});
			},
			triggerHandler: function (a, b) {
				var c = this[0];
				if (c) return pa.event.trigger(a, b, c, !0);
			},
		}),
		pa.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (a, b) {
			pa.fn[b] = function (a, c) {
				return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b);
			};
		}),
		pa.fn.extend({
			hover: function (a, b) {
				return this.mouseenter(a).mouseleave(b || a);
			},
		}),
		(na.focusin = "onfocusin" in a),
		na.focusin ||
			pa.each({ focus: "focusin", blur: "focusout" }, function (a, b) {
				var c = function (a) {
					pa.event.simulate(b, a.target, pa.event.fix(a));
				};
				pa.event.special[b] = {
					setup: function () {
						var d = this.ownerDocument || this,
							e = Ja.access(d, b);
						e || d.addEventListener(a, c, !0), Ja.access(d, b, (e || 0) + 1);
					},
					teardown: function () {
						var d = this.ownerDocument || this,
							e = Ja.access(d, b) - 1;
						e ? Ja.access(d, b, e) : (d.removeEventListener(a, c, !0), Ja.remove(d, b));
					},
				};
			});
	var yb = a.location,
		zb = pa.now(),
		Ab = /\?/;
	pa.parseXML = function (b) {
		var c;
		if (!b || "string" != typeof b) return null;
		try {
			c = new a.DOMParser().parseFromString(b, "text/xml");
		} catch (a) {
			c = void 0;
		}
		return (c && !c.getElementsByTagName("parsererror").length) || pa.error("Invalid XML: " + b), c;
	};
	var Bb = /\[\]$/,
		Cb = /\r?\n/g,
		Db = /^(?:submit|button|image|reset|file)$/i,
		Eb = /^(?:input|select|textarea|keygen)/i;
	(pa.param = function (a, b) {
		var c,
			d = [],
			e = function (a, b) {
				var c = pa.isFunction(b) ? b() : b;
				d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(null == c ? "" : c);
			};
		if (Array.isArray(a) || (a.jquery && !pa.isPlainObject(a)))
			pa.each(a, function () {
				e(this.name, this.value);
			});
		else for (c in a) X(c, a[c], b, e);
		return d.join("&");
	}),
		pa.fn.extend({
			serialize: function () {
				return pa.param(this.serializeArray());
			},
			serializeArray: function () {
				return this.map(function () {
					var a = pa.prop(this, "elements");
					return a ? pa.makeArray(a) : this;
				})
					.filter(function () {
						var a = this.type;
						return this.name && !pa(this).is(":disabled") && Eb.test(this.nodeName) && !Db.test(a) && (this.checked || !Ta.test(a));
					})
					.map(function (a, b) {
						var c = pa(this).val();
						return null == c
							? null
							: Array.isArray(c)
							? pa.map(c, function (a) {
									return { name: b.name, value: a.replace(Cb, "\r\n") };
							  })
							: { name: b.name, value: c.replace(Cb, "\r\n") };
					})
					.get();
			},
		});
	var Fb = /%20/g,
		Gb = /#.*$/,
		Hb = /([?&])_=[^&]*/,
		Ib = /^(.*?):[ \t]*([^\r\n]*)$/gm,
		Jb = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
		Kb = /^(?:GET|HEAD)$/,
		Lb = /^\/\//,
		Mb = {},
		Nb = {},
		Ob = "*/".concat("*"),
		Pb = ca.createElement("a");
	(Pb.href = yb.href),
		pa.extend({
			active: 0,
			lastModified: {},
			etag: {},
			ajaxSettings: { url: yb.href, type: "GET", isLocal: Jb.test(yb.protocol), global: !0, processData: !0, async: !0, contentType: "application/x-www-form-urlencoded; charset=UTF-8", accepts: { "*": Ob, text: "text/plain", html: "text/html", xml: "application/xml, text/xml", json: "application/json, text/javascript" }, contents: { xml: /\bxml\b/, html: /\bhtml/, json: /\bjson\b/ }, responseFields: { xml: "responseXML", text: "responseText", json: "responseJSON" }, converters: { "* text": String, "text html": !0, "text json": JSON.parse, "text xml": pa.parseXML }, flatOptions: { url: !0, context: !0 } },
			ajaxSetup: function (a, b) {
				return b ? $($(a, pa.ajaxSettings), b) : $(pa.ajaxSettings, a);
			},
			ajaxPrefilter: Y(Mb),
			ajaxTransport: Y(Nb),
			ajax: function (b, c) {
				function d(b, c, d, h) {
					var j,
						m,
						n,
						u,
						v,
						w = c;
					k ||
						((k = !0),
						i && a.clearTimeout(i),
						(e = void 0),
						(g = h || ""),
						(x.readyState = b > 0 ? 4 : 0),
						(j = (b >= 200 && b < 300) || 304 === b),
						d && (u = _(o, x, d)),
						(u = aa(o, u, x, j)),
						j ? (o.ifModified && ((v = x.getResponseHeader("Last-Modified")), v && (pa.lastModified[f] = v), (v = x.getResponseHeader("etag")) && (pa.etag[f] = v)), 204 === b || "HEAD" === o.type ? (w = "nocontent") : 304 === b ? (w = "notmodified") : ((w = u.state), (m = u.data), (n = u.error), (j = !n))) : ((n = w), (!b && w) || ((w = "error"), b < 0 && (b = 0))),
						(x.status = b),
						(x.statusText = (c || w) + ""),
						j ? r.resolveWith(p, [m, w, x]) : r.rejectWith(p, [x, w, n]),
						x.statusCode(t),
						(t = void 0),
						l && q.trigger(j ? "ajaxSuccess" : "ajaxError", [x, o, j ? m : n]),
						s.fireWith(p, [x, w]),
						l && (q.trigger("ajaxComplete", [x, o]), --pa.active || pa.event.trigger("ajaxStop")));
				}
				"object" == typeof b && ((c = b), (b = void 0)), (c = c || {});
				var e,
					f,
					g,
					h,
					i,
					j,
					k,
					l,
					m,
					n,
					o = pa.ajaxSetup({}, c),
					p = o.context || o,
					q = o.context && (p.nodeType || p.jquery) ? pa(p) : pa.event,
					r = pa.Deferred(),
					s = pa.Callbacks("once memory"),
					t = o.statusCode || {},
					u = {},
					v = {},
					w = "canceled",
					x = {
						readyState: 0,
						getResponseHeader: function (a) {
							var b;
							if (k) {
								if (!h) for (h = {}; (b = Ib.exec(g)); ) h[b[1].toLowerCase()] = b[2];
								b = h[a.toLowerCase()];
							}
							return null == b ? null : b;
						},
						getAllResponseHeaders: function () {
							return k ? g : null;
						},
						setRequestHeader: function (a, b) {
							return null == k && ((a = v[a.toLowerCase()] = v[a.toLowerCase()] || a), (u[a] = b)), this;
						},
						overrideMimeType: function (a) {
							return null == k && (o.mimeType = a), this;
						},
						statusCode: function (a) {
							var b;
							if (a)
								if (k) x.always(a[x.status]);
								else for (b in a) t[b] = [t[b], a[b]];
							return this;
						},
						abort: function (a) {
							var b = a || w;
							return e && e.abort(b), d(0, b), this;
						},
					};
				if ((r.promise(x), (o.url = ((b || o.url || yb.href) + "").replace(Lb, yb.protocol + "//")), (o.type = c.method || c.type || o.method || o.type), (o.dataTypes = (o.dataType || "*").toLowerCase().match(Ea) || [""]), null == o.crossDomain)) {
					j = ca.createElement("a");
					try {
						(j.href = o.url), (j.href = j.href), (o.crossDomain = Pb.protocol + "//" + Pb.host != j.protocol + "//" + j.host);
					} catch (a) {
						o.crossDomain = !0;
					}
				}
				if ((o.data && o.processData && "string" != typeof o.data && (o.data = pa.param(o.data, o.traditional)), Z(Mb, o, c, x), k)) return x;
				(l = pa.event && o.global),
					l && 0 == pa.active++ && pa.event.trigger("ajaxStart"),
					(o.type = o.type.toUpperCase()),
					(o.hasContent = !Kb.test(o.type)),
					(f = o.url.replace(Gb, "")),
					o.hasContent ? o.data && o.processData && 0 === (o.contentType || "").indexOf("application/x-www-form-urlencoded") && (o.data = o.data.replace(Fb, "+")) : ((n = o.url.slice(f.length)), o.data && ((f += (Ab.test(f) ? "&" : "?") + o.data), delete o.data), !1 === o.cache && ((f = f.replace(Hb, "$1")), (n = (Ab.test(f) ? "&" : "?") + "_=" + zb++ + n)), (o.url = f + n)),
					o.ifModified && (pa.lastModified[f] && x.setRequestHeader("If-Modified-Since", pa.lastModified[f]), pa.etag[f] && x.setRequestHeader("If-None-Match", pa.etag[f])),
					((o.data && o.hasContent && !1 !== o.contentType) || c.contentType) && x.setRequestHeader("Content-Type", o.contentType),
					x.setRequestHeader("Accept", o.dataTypes[0] && o.accepts[o.dataTypes[0]] ? o.accepts[o.dataTypes[0]] + ("*" !== o.dataTypes[0] ? ", " + Ob + "; q=0.01" : "") : o.accepts["*"]);
				for (m in o.headers) x.setRequestHeader(m, o.headers[m]);
				if (o.beforeSend && (!1 === o.beforeSend.call(p, x, o) || k)) return x.abort();
				if (((w = "abort"), s.add(o.complete), x.done(o.success), x.fail(o.error), (e = Z(Nb, o, c, x)))) {
					if (((x.readyState = 1), l && q.trigger("ajaxSend", [x, o]), k)) return x;
					o.async &&
						o.timeout > 0 &&
						(i = a.setTimeout(function () {
							x.abort("timeout");
						}, o.timeout));
					try {
						(k = !1), e.send(u, d);
					} catch (a) {
						if (k) throw a;
						d(-1, a);
					}
				} else d(-1, "No Transport");
				return x;
			},
			getJSON: function (a, b, c) {
				return pa.get(a, b, c, "json");
			},
			getScript: function (a, b) {
				return pa.get(a, void 0, b, "script");
			},
		}),
		pa.each(["get", "post"], function (a, b) {
			pa[b] = function (a, c, d, e) {
				return pa.isFunction(c) && ((e = e || d), (d = c), (c = void 0)), pa.ajax(pa.extend({ url: a, type: b, dataType: e, data: c, success: d }, pa.isPlainObject(a) && a));
			};
		}),
		(pa._evalUrl = function (a) {
			return pa.ajax({ url: a, type: "GET", dataType: "script", cache: !0, async: !1, global: !1, throws: !0 });
		}),
		pa.fn.extend({
			wrapAll: function (a) {
				var b;
				return (
					this[0] &&
						(pa.isFunction(a) && (a = a.call(this[0])),
						(b = pa(a, this[0].ownerDocument).eq(0).clone(!0)),
						this[0].parentNode && b.insertBefore(this[0]),
						b
							.map(function () {
								for (var a = this; a.firstElementChild; ) a = a.firstElementChild;
								return a;
							})
							.append(this)),
					this
				);
			},
			wrapInner: function (a) {
				return pa.isFunction(a)
					? this.each(function (b) {
							pa(this).wrapInner(a.call(this, b));
					  })
					: this.each(function () {
							var b = pa(this),
								c = b.contents();
							c.length ? c.wrapAll(a) : b.append(a);
					  });
			},
			wrap: function (a) {
				var b = pa.isFunction(a);
				return this.each(function (c) {
					pa(this).wrapAll(b ? a.call(this, c) : a);
				});
			},
			unwrap: function (a) {
				return (
					this.parent(a)
						.not("body")
						.each(function () {
							pa(this).replaceWith(this.childNodes);
						}),
					this
				);
			},
		}),
		(pa.expr.pseudos.hidden = function (a) {
			return !pa.expr.pseudos.visible(a);
		}),
		(pa.expr.pseudos.visible = function (a) {
			return !!(a.offsetWidth || a.offsetHeight || a.getClientRects().length);
		}),
		(pa.ajaxSettings.xhr = function () {
			try {
				return new a.XMLHttpRequest();
			} catch (a) {}
		});
	var Qb = { 0: 200, 1223: 204 },
		Rb = pa.ajaxSettings.xhr();
	(na.cors = !!Rb && "withCredentials" in Rb),
		(na.ajax = Rb = !!Rb),
		pa.ajaxTransport(function (b) {
			var c, d;
			if (na.cors || (Rb && !b.crossDomain))
				return {
					send: function (e, f) {
						var g,
							h = b.xhr();
						if ((h.open(b.type, b.url, b.async, b.username, b.password), b.xhrFields)) for (g in b.xhrFields) h[g] = b.xhrFields[g];
						b.mimeType && h.overrideMimeType && h.overrideMimeType(b.mimeType), b.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest");
						for (g in e) h.setRequestHeader(g, e[g]);
						(c = function (a) {
							return function () {
								c && ((c = d = h.onload = h.onerror = h.onabort = h.onreadystatechange = null), "abort" === a ? h.abort() : "error" === a ? ("number" != typeof h.status ? f(0, "error") : f(h.status, h.statusText)) : f(Qb[h.status] || h.status, h.statusText, "text" !== (h.responseType || "text") || "string" != typeof h.responseText ? { binary: h.response } : { text: h.responseText }, h.getAllResponseHeaders()));
							};
						}),
							(h.onload = c()),
							(d = h.onerror = c("error")),
							void 0 !== h.onabort
								? (h.onabort = d)
								: (h.onreadystatechange = function () {
										4 === h.readyState &&
											a.setTimeout(function () {
												c && d();
											});
								  }),
							(c = c("abort"));
						try {
							h.send((b.hasContent && b.data) || null);
						} catch (a) {
							if (c) throw a;
						}
					},
					abort: function () {
						c && c();
					},
				};
		}),
		pa.ajaxPrefilter(function (a) {
			a.crossDomain && (a.contents.script = !1);
		}),
		pa.ajaxSetup({
			accepts: { script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript" },
			contents: { script: /\b(?:java|ecma)script\b/ },
			converters: {
				"text script": function (a) {
					return pa.globalEval(a), a;
				},
			},
		}),
		pa.ajaxPrefilter("script", function (a) {
			void 0 === a.cache && (a.cache = !1), a.crossDomain && (a.type = "GET");
		}),
		pa.ajaxTransport("script", function (a) {
			if (a.crossDomain) {
				var b, c;
				return {
					send: function (d, e) {
						(b = pa("<script>")
							.prop({ charset: a.scriptCharset, src: a.url })
							.on(
								"load error",
								(c = function (a) {
									b.remove(), (c = null), a && e("error" === a.type ? 404 : 200, a.type);
								})
							)),
							ca.head.appendChild(b[0]);
					},
					abort: function () {
						c && c();
					},
				};
			}
		});
	var Sb = [],
		Tb = /(=)\?(?=&|$)|\?\?/;
	pa.ajaxSetup({
		jsonp: "callback",
		jsonpCallback: function () {
			var a = Sb.pop() || pa.expando + "_" + zb++;
			return (this[a] = !0), a;
		},
	}),
		pa.ajaxPrefilter("json jsonp", function (b, c, d) {
			var e,
				f,
				g,
				h = !1 !== b.jsonp && (Tb.test(b.url) ? "url" : "string" == typeof b.data && 0 === (b.contentType || "").indexOf("application/x-www-form-urlencoded") && Tb.test(b.data) && "data");
			if (h || "jsonp" === b.dataTypes[0])
				return (
					(e = b.jsonpCallback = pa.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback),
					h ? (b[h] = b[h].replace(Tb, "$1" + e)) : !1 !== b.jsonp && (b.url += (Ab.test(b.url) ? "&" : "?") + b.jsonp + "=" + e),
					(b.converters["script json"] = function () {
						return g || pa.error(e + " was not called"), g[0];
					}),
					(b.dataTypes[0] = "json"),
					(f = a[e]),
					(a[e] = function () {
						g = arguments;
					}),
					d.always(function () {
						void 0 === f ? pa(a).removeProp(e) : (a[e] = f), b[e] && ((b.jsonpCallback = c.jsonpCallback), Sb.push(e)), g && pa.isFunction(f) && f(g[0]), (g = f = void 0);
					}),
					"script"
				);
		}),
		(na.createHTMLDocument = (function () {
			var a = ca.implementation.createHTMLDocument("").body;
			return (a.innerHTML = "<form></form><form></form>"), 2 === a.childNodes.length;
		})()),
		(pa.parseHTML = function (a, b, c) {
			if ("string" != typeof a) return [];
			"boolean" == typeof b && ((c = b), (b = !1));
			var d, e, f;
			return b || (na.createHTMLDocument ? ((b = ca.implementation.createHTMLDocument("")), (d = b.createElement("base")), (d.href = ca.location.href), b.head.appendChild(d)) : (b = ca)), (e = ya.exec(a)), (f = !c && []), e ? [b.createElement(e[1])] : ((e = u([a], b, f)), f && f.length && pa(f).remove(), pa.merge([], e.childNodes));
		}),
		(pa.fn.load = function (a, b, c) {
			var d,
				e,
				f,
				g = this,
				h = a.indexOf(" ");
			return (
				h > -1 && ((d = V(a.slice(h))), (a = a.slice(0, h))),
				pa.isFunction(b) ? ((c = b), (b = void 0)) : b && "object" == typeof b && (e = "POST"),
				g.length > 0 &&
					pa
						.ajax({ url: a, type: e || "GET", dataType: "html", data: b })
						.done(function (a) {
							(f = arguments), g.html(d ? pa("<div>").append(pa.parseHTML(a)).find(d) : a);
						})
						.always(
							c &&
								function (a, b) {
									g.each(function () {
										c.apply(this, f || [a.responseText, b, a]);
									});
								}
						),
				this
			);
		}),
		pa.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (a, b) {
			pa.fn[b] = function (a) {
				return this.on(b, a);
			};
		}),
		(pa.expr.pseudos.animated = function (a) {
			return pa.grep(pa.timers, function (b) {
				return a === b.elem;
			}).length;
		}),
		(pa.offset = {
			setOffset: function (a, b, c) {
				var d,
					e,
					f,
					g,
					h,
					i,
					j,
					k = pa.css(a, "position"),
					l = pa(a),
					m = {};
				"static" === k && (a.style.position = "relative"), (h = l.offset()), (f = pa.css(a, "top")), (i = pa.css(a, "left")), (j = ("absolute" === k || "fixed" === k) && (f + i).indexOf("auto") > -1), j ? ((d = l.position()), (g = d.top), (e = d.left)) : ((g = parseFloat(f) || 0), (e = parseFloat(i) || 0)), pa.isFunction(b) && (b = b.call(a, c, pa.extend({}, h))), null != b.top && (m.top = b.top - h.top + g), null != b.left && (m.left = b.left - h.left + e), "using" in b ? b.using.call(a, m) : l.css(m);
			},
		}),
		pa.fn.extend({
			offset: function (a) {
				if (arguments.length)
					return void 0 === a
						? this
						: this.each(function (b) {
								pa.offset.setOffset(this, a, b);
						  });
				var b,
					c,
					d,
					e,
					f = this[0];
				if (f) return f.getClientRects().length ? ((d = f.getBoundingClientRect()), (b = f.ownerDocument), (c = b.documentElement), (e = b.defaultView), { top: d.top + e.pageYOffset - c.clientTop, left: d.left + e.pageXOffset - c.clientLeft }) : { top: 0, left: 0 };
			},
			position: function () {
				if (this[0]) {
					var a,
						b,
						c = this[0],
						d = { top: 0, left: 0 };
					return "fixed" === pa.css(c, "position") ? (b = c.getBoundingClientRect()) : ((a = this.offsetParent()), (b = this.offset()), e(a[0], "html") || (d = a.offset()), (d = { top: d.top + pa.css(a[0], "borderTopWidth", !0), left: d.left + pa.css(a[0], "borderLeftWidth", !0) })), { top: b.top - d.top - pa.css(c, "marginTop", !0), left: b.left - d.left - pa.css(c, "marginLeft", !0) };
				}
			},
			offsetParent: function () {
				return this.map(function () {
					for (var a = this.offsetParent; a && "static" === pa.css(a, "position"); ) a = a.offsetParent;
					return a || Ya;
				});
			},
		}),
		pa.each({ scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function (a, b) {
			var c = "pageYOffset" === b;
			pa.fn[a] = function (d) {
				return Ha(
					this,
					function (a, d, e) {
						var f;
						if ((pa.isWindow(a) ? (f = a) : 9 === a.nodeType && (f = a.defaultView), void 0 === e)) return f ? f[b] : a[d];
						f ? f.scrollTo(c ? f.pageXOffset : e, c ? e : f.pageYOffset) : (a[d] = e);
					},
					a,
					d,
					arguments.length
				);
			};
		}),
		pa.each(["top", "left"], function (a, b) {
			pa.cssHooks[b] = H(na.pixelPosition, function (a, c) {
				if (c) return (c = G(a, b)), gb.test(c) ? pa(a).position()[b] + "px" : c;
			});
		}),
		pa.each({ Height: "height", Width: "width" }, function (a, b) {
			pa.each({ padding: "inner" + a, content: b, "": "outer" + a }, function (c, d) {
				pa.fn[d] = function (e, f) {
					var g = arguments.length && (c || "boolean" != typeof e),
						h = c || (!0 === e || !0 === f ? "margin" : "border");
					return Ha(
						this,
						function (b, c, e) {
							var f;
							return pa.isWindow(b) ? (0 === d.indexOf("outer") ? b["inner" + a] : b.document.documentElement["client" + a]) : 9 === b.nodeType ? ((f = b.documentElement), Math.max(b.body["scroll" + a], f["scroll" + a], b.body["offset" + a], f["offset" + a], f["client" + a])) : void 0 === e ? pa.css(b, c, h) : pa.style(b, c, e, h);
						},
						b,
						g ? e : void 0,
						g
					);
				};
			});
		}),
		pa.fn.extend({
			bind: function (a, b, c) {
				return this.on(a, null, b, c);
			},
			unbind: function (a, b) {
				return this.off(a, null, b);
			},
			delegate: function (a, b, c, d) {
				return this.on(b, a, c, d);
			},
			undelegate: function (a, b, c) {
				return 1 === arguments.length ? this.off(a, "**") : this.off(b, a || "**", c);
			},
		}),
		(pa.holdReady = function (a) {
			a ? pa.readyWait++ : pa.ready(!0);
		}),
		(pa.isArray = Array.isArray),
		(pa.parseJSON = JSON.parse),
		(pa.nodeName = e),
		"function" == typeof define &&
			define.amd &&
			define("jquery", [], function () {
				return pa;
			});
	var Ub = a.jQuery,
		Vb = a.$;
	return (
		(pa.noConflict = function (b) {
			return a.$ === pa && (a.$ = Vb), b && a.jQuery === pa && (a.jQuery = Ub), pa;
		}),
		b || (a.jQuery = a.$ = pa),
		pa
	);
}),
	/*! nouislider - 14.6.3 - 11/19/2020 */
	(function (factory) {
		if (typeof define === "function" && define.amd) {
			// AMD. Register as an anonymous module.
			define([], factory);
		} else if (typeof exports === "object") {
			// Node/CommonJS
			module.exports = factory();
		} else {
			// Browser globals
			window.noUiSlider = factory();
		}
	})(function () {
		"use strict";

		var VERSION = "14.6.3";

		//region Helper Methods

		function isValidFormatter(entry) {
			return typeof entry === "object" && typeof entry.to === "function" && typeof entry.from === "function";
		}

		function removeElement(el) {
			el.parentElement.removeChild(el);
		}

		function isSet(value) {
			return value !== null && value !== undefined;
		}

		// Bindable version
		function preventDefault(e) {
			e.preventDefault();
		}

		// Removes duplicates from an array.
		function unique(array) {
			return array.filter(function (a) {
				return !this[a] ? (this[a] = true) : false;
			}, {});
		}

		// Round a value to the closest 'to'.
		function closest(value, to) {
			return Math.round(value / to) * to;
		}

		// Current position of an element relative to the document.
		function offset(elem, orientation) {
			var rect = elem.getBoundingClientRect();
			var doc = elem.ownerDocument;
			var docElem = doc.documentElement;
			var pageOffset = getPageOffset(doc);

			// getBoundingClientRect contains left scroll in Chrome on Android.
			// I haven't found a feature detection that proves this. Worst case
			// scenario on mis-match: the 'tap' feature on horizontal sliders breaks.
			if (/webkit.*Chrome.*Mobile/i.test(navigator.userAgent)) {
				pageOffset.x = 0;
			}

			return orientation ? rect.top + pageOffset.y - docElem.clientTop : rect.left + pageOffset.x - docElem.clientLeft;
		}

		// Checks whether a value is numerical.
		function isNumeric(a) {
			return typeof a === "number" && !isNaN(a) && isFinite(a);
		}

		// Sets a class and removes it after [duration] ms.
		function addClassFor(element, className, duration) {
			if (duration > 0) {
				addClass(element, className);
				setTimeout(function () {
					removeClass(element, className);
				}, duration);
			}
		}

		// Limits a value to 0 - 100
		function limit(a) {
			return Math.max(Math.min(a, 100), 0);
		}

		// Wraps a variable as an array, if it isn't one yet.
		// Note that an input array is returned by reference!
		function asArray(a) {
			return Array.isArray(a) ? a : [a];
		}

		// Counts decimals
		function countDecimals(numStr) {
			numStr = String(numStr);
			var pieces = numStr.split(".");
			return pieces.length > 1 ? pieces[1].length : 0;
		}

		// http://youmightnotneedjquery.com/#add_class
		function addClass(el, className) {
			if (el.classList && !/\s/.test(className)) {
				el.classList.add(className);
			} else {
				el.className += " " + className;
			}
		}

		// http://youmightnotneedjquery.com/#remove_class
		function removeClass(el, className) {
			if (el.classList && !/\s/.test(className)) {
				el.classList.remove(className);
			} else {
				el.className = el.className.replace(new RegExp("(^|\\b)" + className.split(" ").join("|") + "(\\b|$)", "gi"), " ");
			}
		}

		// https://plainjs.com/javascript/attributes/adding-removing-and-testing-for-classes-9/
		function hasClass(el, className) {
			return el.classList ? el.classList.contains(className) : new RegExp("\\b" + className + "\\b").test(el.className);
		}

		// https://developer.mozilla.org/en-US/docs/Web/API/Window/scrollY#Notes
		function getPageOffset(doc) {
			var supportPageOffset = window.pageXOffset !== undefined;
			var isCSS1Compat = (doc.compatMode || "") === "CSS1Compat";
			var x = supportPageOffset ? window.pageXOffset : isCSS1Compat ? doc.documentElement.scrollLeft : doc.body.scrollLeft;
			var y = supportPageOffset ? window.pageYOffset : isCSS1Compat ? doc.documentElement.scrollTop : doc.body.scrollTop;

			return {
				x: x,
				y: y,
			};
		}

		// we provide a function to compute constants instead
		// of accessing window.* as soon as the module needs it
		// so that we do not compute anything if not needed
		function getActions() {
			// Determine the events to bind. IE11 implements pointerEvents without
			// a prefix, which breaks compatibility with the IE10 implementation.
			return window.navigator.pointerEnabled
				? {
						start: "pointerdown",
						move: "pointermove",
						end: "pointerup",
				  }
				: window.navigator.msPointerEnabled
				? {
						start: "MSPointerDown",
						move: "MSPointerMove",
						end: "MSPointerUp",
				  }
				: {
						start: "mousedown touchstart",
						move: "mousemove touchmove",
						end: "mouseup touchend",
				  };
		}

		// https://github.com/WICG/EventListenerOptions/blob/gh-pages/explainer.md
		// Issue #785
		function getSupportsPassive() {
			var supportsPassive = false;

			/* eslint-disable */
			try {
				var opts = Object.defineProperty({}, "passive", {
					get: function () {
						supportsPassive = true;
					},
				});

				window.addEventListener("test", null, opts);
			} catch (e) {}
			/* eslint-enable */

			return supportsPassive;
		}

		function getSupportsTouchActionNone() {
			return window.CSS && CSS.supports && CSS.supports("touch-action", "none");
		}

		//endregion

		//region Range Calculation

		// Determine the size of a sub-range in relation to a full range.
		function subRangeRatio(pa, pb) {
			return 100 / (pb - pa);
		}

		// (percentage) How many percent is this value of this range?
		function fromPercentage(range, value, startRange) {
			return (value * 100) / (range[startRange + 1] - range[startRange]);
		}

		// (percentage) Where is this value on this range?
		function toPercentage(range, value) {
			return fromPercentage(range, range[0] < 0 ? value + Math.abs(range[0]) : value - range[0], 0);
		}

		// (value) How much is this percentage on this range?
		function isPercentage(range, value) {
			return (value * (range[1] - range[0])) / 100 + range[0];
		}

		function getJ(value, arr) {
			var j = 1;

			while (value >= arr[j]) {
				j += 1;
			}

			return j;
		}

		// (percentage) Input a value, find where, on a scale of 0-100, it applies.
		function toStepping(xVal, xPct, value) {
			if (value >= xVal.slice(-1)[0]) {
				return 100;
			}

			var j = getJ(value, xVal);
			var va = xVal[j - 1];
			var vb = xVal[j];
			var pa = xPct[j - 1];
			var pb = xPct[j];

			return pa + toPercentage([va, vb], value) / subRangeRatio(pa, pb);
		}

		// (value) Input a percentage, find where it is on the specified range.
		function fromStepping(xVal, xPct, value) {
			// There is no range group that fits 100
			if (value >= 100) {
				return xVal.slice(-1)[0];
			}

			var j = getJ(value, xPct);
			var va = xVal[j - 1];
			var vb = xVal[j];
			var pa = xPct[j - 1];
			var pb = xPct[j];

			return isPercentage([va, vb], (value - pa) * subRangeRatio(pa, pb));
		}

		// (percentage) Get the step that applies at a certain value.
		function getStep(xPct, xSteps, snap, value) {
			if (value === 100) {
				return value;
			}

			var j = getJ(value, xPct);
			var a = xPct[j - 1];
			var b = xPct[j];

			// If 'snap' is set, steps are used as fixed points on the slider.
			if (snap) {
				// Find the closest position, a or b.
				if (value - a > (b - a) / 2) {
					return b;
				}

				return a;
			}

			if (!xSteps[j - 1]) {
				return value;
			}

			return xPct[j - 1] + closest(value - xPct[j - 1], xSteps[j - 1]);
		}

		function handleEntryPoint(index, value, that) {
			var percentage;

			// Wrap numerical input in an array.
			if (typeof value === "number") {
				value = [value];
			}

			// Reject any invalid input, by testing whether value is an array.
			if (!Array.isArray(value)) {
				throw new Error("noUiSlider (" + VERSION + "): 'range' contains invalid value.");
			}

			// Covert min/max syntax to 0 and 100.
			if (index === "min") {
				percentage = 0;
			} else if (index === "max") {
				percentage = 100;
			} else {
				percentage = parseFloat(index);
			}

			// Check for correct input.
			if (!isNumeric(percentage) || !isNumeric(value[0])) {
				throw new Error("noUiSlider (" + VERSION + "): 'range' value isn't numeric.");
			}

			// Store values.
			that.xPct.push(percentage);
			that.xVal.push(value[0]);

			// NaN will evaluate to false too, but to keep
			// logging clear, set step explicitly. Make sure
			// not to override the 'step' setting with false.
			if (!percentage) {
				if (!isNaN(value[1])) {
					that.xSteps[0] = value[1];
				}
			} else {
				that.xSteps.push(isNaN(value[1]) ? false : value[1]);
			}

			that.xHighestCompleteStep.push(0);
		}

		function handleStepPoint(i, n, that) {
			// Ignore 'false' stepping.
			if (!n) {
				return;
			}

			// Step over zero-length ranges (#948);
			if (that.xVal[i] === that.xVal[i + 1]) {
				that.xSteps[i] = that.xHighestCompleteStep[i] = that.xVal[i];

				return;
			}

			// Factor to range ratio
			that.xSteps[i] = fromPercentage([that.xVal[i], that.xVal[i + 1]], n, 0) / subRangeRatio(that.xPct[i], that.xPct[i + 1]);

			var totalSteps = (that.xVal[i + 1] - that.xVal[i]) / that.xNumSteps[i];
			var highestStep = Math.ceil(Number(totalSteps.toFixed(3)) - 1);
			var step = that.xVal[i] + that.xNumSteps[i] * highestStep;

			that.xHighestCompleteStep[i] = step;
		}

		//endregion

		//region Spectrum

		function Spectrum(entry, snap, singleStep) {
			this.xPct = [];
			this.xVal = [];
			this.xSteps = [singleStep || false];
			this.xNumSteps = [false];
			this.xHighestCompleteStep = [];

			this.snap = snap;

			var index;
			var ordered = []; // [0, 'min'], [1, '50%'], [2, 'max']

			// Map the object keys to an array.
			for (index in entry) {
				if (entry.hasOwnProperty(index)) {
					ordered.push([entry[index], index]);
				}
			}

			// Sort all entries by value (numeric sort).
			if (ordered.length && typeof ordered[0][0] === "object") {
				ordered.sort(function (a, b) {
					return a[0][0] - b[0][0];
				});
			} else {
				ordered.sort(function (a, b) {
					return a[0] - b[0];
				});
			}

			// Convert all entries to subranges.
			for (index = 0; index < ordered.length; index++) {
				handleEntryPoint(ordered[index][1], ordered[index][0], this);
			}

			// Store the actual step values.
			// xSteps is sorted in the same order as xPct and xVal.
			this.xNumSteps = this.xSteps.slice(0);

			// Convert all numeric steps to the percentage of the subrange they represent.
			for (index = 0; index < this.xNumSteps.length; index++) {
				handleStepPoint(index, this.xNumSteps[index], this);
			}
		}

		Spectrum.prototype.getDistance = function (value) {
			var index;
			var distances = [];

			for (index = 0; index < this.xNumSteps.length - 1; index++) {
				// last "range" can't contain step size as it is purely an endpoint.
				var step = this.xNumSteps[index];

				if (step && (value / step) % 1 !== 0) {
					throw new Error("noUiSlider (" + VERSION + "): 'limit', 'margin' and 'padding' of " + this.xPct[index] + "% range must be divisible by step.");
				}

				// Calculate percentual distance in current range of limit, margin or padding
				distances[index] = fromPercentage(this.xVal, value, index);
			}

			return distances;
		};

		// Calculate the percentual distance over the whole scale of ranges.
		// direction: 0 = backwards / 1 = forwards
		Spectrum.prototype.getAbsoluteDistance = function (value, distances, direction) {
			var xPct_index = 0;

			// Calculate range where to start calculation
			if (value < this.xPct[this.xPct.length - 1]) {
				while (value > this.xPct[xPct_index + 1]) {
					xPct_index++;
				}
			} else if (value === this.xPct[this.xPct.length - 1]) {
				xPct_index = this.xPct.length - 2;
			}

			// If looking backwards and the value is exactly at a range separator then look one range further
			if (!direction && value === this.xPct[xPct_index + 1]) {
				xPct_index++;
			}

			var start_factor;
			var rest_factor = 1;

			var rest_rel_distance = distances[xPct_index];

			var range_pct = 0;

			var rel_range_distance = 0;
			var abs_distance_counter = 0;
			var range_counter = 0;

			// Calculate what part of the start range the value is
			if (direction) {
				start_factor = (value - this.xPct[xPct_index]) / (this.xPct[xPct_index + 1] - this.xPct[xPct_index]);
			} else {
				start_factor = (this.xPct[xPct_index + 1] - value) / (this.xPct[xPct_index + 1] - this.xPct[xPct_index]);
			}

			// Do until the complete distance across ranges is calculated
			while (rest_rel_distance > 0) {
				// Calculate the percentage of total range
				range_pct = this.xPct[xPct_index + 1 + range_counter] - this.xPct[xPct_index + range_counter];

				// Detect if the margin, padding or limit is larger then the current range and calculate
				if (distances[xPct_index + range_counter] * rest_factor + 100 - start_factor * 100 > 100) {
					// If larger then take the percentual distance of the whole range
					rel_range_distance = range_pct * start_factor;
					// Rest factor of relative percentual distance still to be calculated
					rest_factor = (rest_rel_distance - 100 * start_factor) / distances[xPct_index + range_counter];
					// Set start factor to 1 as for next range it does not apply.
					start_factor = 1;
				} else {
					// If smaller or equal then take the percentual distance of the calculate percentual part of that range
					rel_range_distance = ((distances[xPct_index + range_counter] * range_pct) / 100) * rest_factor;
					// No rest left as the rest fits in current range
					rest_factor = 0;
				}

				if (direction) {
					abs_distance_counter = abs_distance_counter - rel_range_distance;
					// Limit range to first range when distance becomes outside of minimum range
					if (this.xPct.length + range_counter >= 1) {
						range_counter--;
					}
				} else {
					abs_distance_counter = abs_distance_counter + rel_range_distance;
					// Limit range to last range when distance becomes outside of maximum range
					if (this.xPct.length - range_counter >= 1) {
						range_counter++;
					}
				}

				// Rest of relative percentual distance still to be calculated
				rest_rel_distance = distances[xPct_index + range_counter] * rest_factor;
			}

			return value + abs_distance_counter;
		};

		Spectrum.prototype.toStepping = function (value) {
			value = toStepping(this.xVal, this.xPct, value);

			return value;
		};

		Spectrum.prototype.fromStepping = function (value) {
			return fromStepping(this.xVal, this.xPct, value);
		};

		Spectrum.prototype.getStep = function (value) {
			value = getStep(this.xPct, this.xSteps, this.snap, value);

			return value;
		};

		Spectrum.prototype.getDefaultStep = function (value, isDown, size) {
			var j = getJ(value, this.xPct);

			// When at the top or stepping down, look at the previous sub-range
			if (value === 100 || (isDown && value === this.xPct[j - 1])) {
				j = Math.max(j - 1, 1);
			}

			return (this.xVal[j] - this.xVal[j - 1]) / size;
		};

		Spectrum.prototype.getNearbySteps = function (value) {
			var j = getJ(value, this.xPct);

			return {
				stepBefore: {
					startValue: this.xVal[j - 2],
					step: this.xNumSteps[j - 2],
					highestStep: this.xHighestCompleteStep[j - 2],
				},
				thisStep: {
					startValue: this.xVal[j - 1],
					step: this.xNumSteps[j - 1],
					highestStep: this.xHighestCompleteStep[j - 1],
				},
				stepAfter: {
					startValue: this.xVal[j],
					step: this.xNumSteps[j],
					highestStep: this.xHighestCompleteStep[j],
				},
			};
		};

		Spectrum.prototype.countStepDecimals = function () {
			var stepDecimals = this.xNumSteps.map(countDecimals);
			return Math.max.apply(null, stepDecimals);
		};

		// Outside testing
		Spectrum.prototype.convert = function (value) {
			return this.getStep(this.toStepping(value));
		};

		//endregion

		//region Options

		/*	Every input option is tested and parsed. This'll prevent
        endless validation in internal methods. These tests are
        structured with an item for every option available. An
        option can be marked as required by setting the 'r' flag.
        The testing function is provided with three arguments:
            - The provided value for the option;
            - A reference to the options object;
            - The name for the option;

        The testing function returns false when an error is detected,
        or true when everything is OK. It can also modify the option
        object, to make sure all values can be correctly looped elsewhere. */

		//region Defaults

		var defaultFormatter = {
			to: function (value) {
				return value !== undefined && value.toFixed(2);
			},
			from: Number,
		};

		var cssClasses = {
			target: "target",
			base: "base",
			origin: "origin",
			handle: "handle",
			handleLower: "handle-lower",
			handleUpper: "handle-upper",
			touchArea: "touch-area",
			horizontal: "horizontal",
			vertical: "vertical",
			background: "background",
			connect: "connect",
			connects: "connects",
			ltr: "ltr",
			rtl: "rtl",
			textDirectionLtr: "txt-dir-ltr",
			textDirectionRtl: "txt-dir-rtl",
			draggable: "draggable",
			drag: "state-drag",
			tap: "state-tap",
			active: "active",
			tooltip: "tooltip",
			pips: "pips",
			pipsHorizontal: "pips-horizontal",
			pipsVertical: "pips-vertical",
			marker: "marker",
			markerHorizontal: "marker-horizontal",
			markerVertical: "marker-vertical",
			markerNormal: "marker-normal",
			markerLarge: "marker-large",
			markerSub: "marker-sub",
			value: "value",
			valueHorizontal: "value-horizontal",
			valueVertical: "value-vertical",
			valueNormal: "value-normal",
			valueLarge: "value-large",
			valueSub: "value-sub",
		};

		// Namespaces of internal event listeners
		var INTERNAL_EVENT_NS = {
			tooltips: ".__tooltips",
			aria: ".__aria",
		};

		//endregion

		function validateFormat(entry) {
			// Any object with a to and from method is supported.
			if (isValidFormatter(entry)) {
				return true;
			}

			throw new Error("noUiSlider (" + VERSION + "): 'format' requires 'to' and 'from' methods.");
		}

		function testStep(parsed, entry) {
			if (!isNumeric(entry)) {
				throw new Error("noUiSlider (" + VERSION + "): 'step' is not numeric.");
			}

			// The step option can still be used to set stepping
			// for linear sliders. Overwritten if set in 'range'.
			parsed.singleStep = entry;
		}

		function testKeyboardPageMultiplier(parsed, entry) {
			if (!isNumeric(entry)) {
				throw new Error("noUiSlider (" + VERSION + "): 'keyboardPageMultiplier' is not numeric.");
			}

			parsed.keyboardPageMultiplier = entry;
		}

		function testKeyboardDefaultStep(parsed, entry) {
			if (!isNumeric(entry)) {
				throw new Error("noUiSlider (" + VERSION + "): 'keyboardDefaultStep' is not numeric.");
			}

			parsed.keyboardDefaultStep = entry;
		}

		function testRange(parsed, entry) {
			// Filter incorrect input.
			if (typeof entry !== "object" || Array.isArray(entry)) {
				throw new Error("noUiSlider (" + VERSION + "): 'range' is not an object.");
			}

			// Catch missing start or end.
			if (entry.min === undefined || entry.max === undefined) {
				throw new Error("noUiSlider (" + VERSION + "): Missing 'min' or 'max' in 'range'.");
			}

			// Catch equal start or end.
			if (entry.min === entry.max) {
				throw new Error("noUiSlider (" + VERSION + "): 'range' 'min' and 'max' cannot be equal.");
			}

			parsed.spectrum = new Spectrum(entry, parsed.snap, parsed.singleStep);
		}

		function testStart(parsed, entry) {
			entry = asArray(entry);

			// Validate input. Values aren't tested, as the public .val method
			// will always provide a valid location.
			if (!Array.isArray(entry) || !entry.length) {
				throw new Error("noUiSlider (" + VERSION + "): 'start' option is incorrect.");
			}

			// Store the number of handles.
			parsed.handles = entry.length;

			// When the slider is initialized, the .val method will
			// be called with the start options.
			parsed.start = entry;
		}

		function testSnap(parsed, entry) {
			// Enforce 100% stepping within subranges.
			parsed.snap = entry;

			if (typeof entry !== "boolean") {
				throw new Error("noUiSlider (" + VERSION + "): 'snap' option must be a boolean.");
			}
		}

		function testAnimate(parsed, entry) {
			// Enforce 100% stepping within subranges.
			parsed.animate = entry;

			if (typeof entry !== "boolean") {
				throw new Error("noUiSlider (" + VERSION + "): 'animate' option must be a boolean.");
			}
		}

		function testAnimationDuration(parsed, entry) {
			parsed.animationDuration = entry;

			if (typeof entry !== "number") {
				throw new Error("noUiSlider (" + VERSION + "): 'animationDuration' option must be a number.");
			}
		}

		function testConnect(parsed, entry) {
			var connect = [false];
			var i;

			// Map legacy options
			if (entry === "lower") {
				entry = [true, false];
			} else if (entry === "upper") {
				entry = [false, true];
			}

			// Handle boolean options
			if (entry === true || entry === false) {
				for (i = 1; i < parsed.handles; i++) {
					connect.push(entry);
				}

				connect.push(false);
			}

			// Reject invalid input
			else if (!Array.isArray(entry) || !entry.length || entry.length !== parsed.handles + 1) {
				throw new Error("noUiSlider (" + VERSION + "): 'connect' option doesn't match handle count.");
			} else {
				connect = entry;
			}

			parsed.connect = connect;
		}

		function testOrientation(parsed, entry) {
			// Set orientation to an a numerical value for easy
			// array selection.
			switch (entry) {
				case "horizontal":
					parsed.ort = 0;
					break;
				case "vertical":
					parsed.ort = 1;
					break;
				default:
					throw new Error("noUiSlider (" + VERSION + "): 'orientation' option is invalid.");
			}
		}

		function testMargin(parsed, entry) {
			if (!isNumeric(entry)) {
				throw new Error("noUiSlider (" + VERSION + "): 'margin' option must be numeric.");
			}

			// Issue #582
			if (entry === 0) {
				return;
			}

			parsed.margin = parsed.spectrum.getDistance(entry);
		}

		function testLimit(parsed, entry) {
			if (!isNumeric(entry)) {
				throw new Error("noUiSlider (" + VERSION + "): 'limit' option must be numeric.");
			}

			parsed.limit = parsed.spectrum.getDistance(entry);

			if (!parsed.limit || parsed.handles < 2) {
				throw new Error("noUiSlider (" + VERSION + "): 'limit' option is only supported on linear sliders with 2 or more handles.");
			}
		}

		function testPadding(parsed, entry) {
			var index;

			if (!isNumeric(entry) && !Array.isArray(entry)) {
				throw new Error("noUiSlider (" + VERSION + "): 'padding' option must be numeric or array of exactly 2 numbers.");
			}

			if (Array.isArray(entry) && !(entry.length === 2 || isNumeric(entry[0]) || isNumeric(entry[1]))) {
				throw new Error("noUiSlider (" + VERSION + "): 'padding' option must be numeric or array of exactly 2 numbers.");
			}

			if (entry === 0) {
				return;
			}

			if (!Array.isArray(entry)) {
				entry = [entry, entry];
			}

			// 'getDistance' returns false for invalid values.
			parsed.padding = [parsed.spectrum.getDistance(entry[0]), parsed.spectrum.getDistance(entry[1])];

			for (index = 0; index < parsed.spectrum.xNumSteps.length - 1; index++) {
				// last "range" can't contain step size as it is purely an endpoint.
				if (parsed.padding[0][index] < 0 || parsed.padding[1][index] < 0) {
					throw new Error("noUiSlider (" + VERSION + "): 'padding' option must be a positive number(s).");
				}
			}

			var totalPadding = entry[0] + entry[1];
			var firstValue = parsed.spectrum.xVal[0];
			var lastValue = parsed.spectrum.xVal[parsed.spectrum.xVal.length - 1];

			if (totalPadding / (lastValue - firstValue) > 1) {
				throw new Error("noUiSlider (" + VERSION + "): 'padding' option must not exceed 100% of the range.");
			}
		}

		function testDirection(parsed, entry) {
			// Set direction as a numerical value for easy parsing.
			// Invert connection for RTL sliders, so that the proper
			// handles get the connect/background classes.
			switch (entry) {
				case "ltr":
					parsed.dir = 0;
					break;
				case "rtl":
					parsed.dir = 1;
					break;
				default:
					throw new Error("noUiSlider (" + VERSION + "): 'direction' option was not recognized.");
			}
		}

		function testBehaviour(parsed, entry) {
			// Make sure the input is a string.
			if (typeof entry !== "string") {
				throw new Error("noUiSlider (" + VERSION + "): 'behaviour' must be a string containing options.");
			}

			// Check if the string contains any keywords.
			// None are required.
			var tap = entry.indexOf("tap") >= 0;
			var drag = entry.indexOf("drag") >= 0;
			var fixed = entry.indexOf("fixed") >= 0;
			var snap = entry.indexOf("snap") >= 0;
			var hover = entry.indexOf("hover") >= 0;
			var unconstrained = entry.indexOf("unconstrained") >= 0;

			if (fixed) {
				if (parsed.handles !== 2) {
					throw new Error("noUiSlider (" + VERSION + "): 'fixed' behaviour must be used with 2 handles");
				}

				// Use margin to enforce fixed state
				testMargin(parsed, parsed.start[1] - parsed.start[0]);
			}

			if (unconstrained && (parsed.margin || parsed.limit)) {
				throw new Error("noUiSlider (" + VERSION + "): 'unconstrained' behaviour cannot be used with margin or limit");
			}

			parsed.events = {
				tap: tap || snap,
				drag: drag,
				fixed: fixed,
				snap: snap,
				hover: hover,
				unconstrained: unconstrained,
			};
		}

		function testTooltips(parsed, entry) {
			if (entry === false) {
				return;
			}

			if (entry === true) {
				parsed.tooltips = [];

				for (var i = 0; i < parsed.handles; i++) {
					parsed.tooltips.push(true);
				}
			} else {
				parsed.tooltips = asArray(entry);

				if (parsed.tooltips.length !== parsed.handles) {
					throw new Error("noUiSlider (" + VERSION + "): must pass a formatter for all handles.");
				}

				parsed.tooltips.forEach(function (formatter) {
					if (typeof formatter !== "boolean" && (typeof formatter !== "object" || typeof formatter.to !== "function")) {
						throw new Error("noUiSlider (" + VERSION + "): 'tooltips' must be passed a formatter or 'false'.");
					}
				});
			}
		}

		function testAriaFormat(parsed, entry) {
			parsed.ariaFormat = entry;
			validateFormat(entry);
		}

		function testFormat(parsed, entry) {
			parsed.format = entry;
			validateFormat(entry);
		}

		function testKeyboardSupport(parsed, entry) {
			parsed.keyboardSupport = entry;

			if (typeof entry !== "boolean") {
				throw new Error("noUiSlider (" + VERSION + "): 'keyboardSupport' option must be a boolean.");
			}
		}

		function testDocumentElement(parsed, entry) {
			// This is an advanced option. Passed values are used without validation.
			parsed.documentElement = entry;
		}

		function testCssPrefix(parsed, entry) {
			if (typeof entry !== "string" && entry !== false) {
				throw new Error("noUiSlider (" + VERSION + "): 'cssPrefix' must be a string or `false`.");
			}

			parsed.cssPrefix = entry;
		}

		function testCssClasses(parsed, entry) {
			if (typeof entry !== "object") {
				throw new Error("noUiSlider (" + VERSION + "): 'cssClasses' must be an object.");
			}

			if (typeof parsed.cssPrefix === "string") {
				parsed.cssClasses = {};

				for (var key in entry) {
					if (!entry.hasOwnProperty(key)) {
						continue;
					}

					parsed.cssClasses[key] = parsed.cssPrefix + entry[key];
				}
			} else {
				parsed.cssClasses = entry;
			}
		}

		// Test all developer settings and parse to assumption-safe values.
		function testOptions(options) {
			// To prove a fix for #537, freeze options here.
			// If the object is modified, an error will be thrown.
			// Object.freeze(options);

			var parsed = {
				margin: 0,
				limit: 0,
				padding: 0,
				animate: true,
				animationDuration: 300,
				ariaFormat: defaultFormatter,
				format: defaultFormatter,
			};

			// Tests are executed in the order they are presented here.
			var tests = {
				step: { r: false, t: testStep },
				keyboardPageMultiplier: { r: false, t: testKeyboardPageMultiplier },
				keyboardDefaultStep: { r: false, t: testKeyboardDefaultStep },
				start: { r: true, t: testStart },
				connect: { r: true, t: testConnect },
				direction: { r: true, t: testDirection },
				snap: { r: false, t: testSnap },
				animate: { r: false, t: testAnimate },
				animationDuration: { r: false, t: testAnimationDuration },
				range: { r: true, t: testRange },
				orientation: { r: false, t: testOrientation },
				margin: { r: false, t: testMargin },
				limit: { r: false, t: testLimit },
				padding: { r: false, t: testPadding },
				behaviour: { r: true, t: testBehaviour },
				ariaFormat: { r: false, t: testAriaFormat },
				format: { r: false, t: testFormat },
				tooltips: { r: false, t: testTooltips },
				keyboardSupport: { r: true, t: testKeyboardSupport },
				documentElement: { r: false, t: testDocumentElement },
				cssPrefix: { r: true, t: testCssPrefix },
				cssClasses: { r: true, t: testCssClasses },
			};

			var defaults = {
				connect: false,
				direction: "ltr",
				behaviour: "tap",
				orientation: "horizontal",
				keyboardSupport: true,
				cssPrefix: "noUi-",
				cssClasses: cssClasses,
				keyboardPageMultiplier: 5,
				keyboardDefaultStep: 10,
			};

			// AriaFormat defaults to regular format, if any.
			if (options.format && !options.ariaFormat) {
				options.ariaFormat = options.format;
			}

			// Run all options through a testing mechanism to ensure correct
			// input. It should be noted that options might get modified to
			// be handled properly. E.g. wrapping integers in arrays.
			Object.keys(tests).forEach(function (name) {
				// If the option isn't set, but it is required, throw an error.
				if (!isSet(options[name]) && defaults[name] === undefined) {
					if (tests[name].r) {
						throw new Error("noUiSlider (" + VERSION + "): '" + name + "' is required.");
					}

					return true;
				}

				tests[name].t(parsed, !isSet(options[name]) ? defaults[name] : options[name]);
			});

			// Forward pips options
			parsed.pips = options.pips;

			// All recent browsers accept unprefixed transform.
			// We need -ms- for IE9 and -webkit- for older Android;
			// Assume use of -webkit- if unprefixed and -ms- are not supported.
			// https://caniuse.com/#feat=transforms2d
			var d = document.createElement("div");
			var msPrefix = d.style.msTransform !== undefined;
			var noPrefix = d.style.transform !== undefined;

			parsed.transformRule = noPrefix ? "transform" : msPrefix ? "msTransform" : "webkitTransform";

			// Pips don't move, so we can place them using left/top.
			var styles = [
				["left", "top"],
				["right", "bottom"],
			];

			parsed.style = styles[parsed.dir][parsed.ort];

			return parsed;
		}

		//endregion

		function scope(target, options, originalOptions) {
			var actions = getActions();
			var supportsTouchActionNone = getSupportsTouchActionNone();
			var supportsPassive = supportsTouchActionNone && getSupportsPassive();

			// All variables local to 'scope' are prefixed with 'scope_'

			// Slider DOM Nodes
			var scope_Target = target;
			var scope_Base;
			var scope_Handles;
			var scope_Connects;
			var scope_Pips;
			var scope_Tooltips;

			// Slider state values
			var scope_Spectrum = options.spectrum;
			var scope_Values = [];
			var scope_Locations = [];
			var scope_HandleNumbers = [];
			var scope_ActiveHandlesCount = 0;
			var scope_Events = {};

			// Exposed API
			var scope_Self;

			// Document Nodes
			var scope_Document = target.ownerDocument;
			var scope_DocumentElement = options.documentElement || scope_Document.documentElement;
			var scope_Body = scope_Document.body;

			// Pips constants
			var PIPS_NONE = -1;
			var PIPS_NO_VALUE = 0;
			var PIPS_LARGE_VALUE = 1;
			var PIPS_SMALL_VALUE = 2;

			// For horizontal sliders in standard ltr documents,
			// make .noUi-origin overflow to the left so the document doesn't scroll.
			var scope_DirOffset = scope_Document.dir === "rtl" || options.ort === 1 ? 0 : 100;

			// Creates a node, adds it to target, returns the new node.
			function addNodeTo(addTarget, className) {
				var div = scope_Document.createElement("div");

				if (className) {
					addClass(div, className);
				}

				addTarget.appendChild(div);

				return div;
			}

			// Append a origin to the base
			function addOrigin(base, handleNumber) {
				var origin = addNodeTo(base, options.cssClasses.origin);
				var handle = addNodeTo(origin, options.cssClasses.handle);

				addNodeTo(handle, options.cssClasses.touchArea);

				handle.setAttribute("data-handle", handleNumber);

				if (options.keyboardSupport) {
					// https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex
					// 0 = focusable and reachable
					handle.setAttribute("tabindex", "0");
					handle.addEventListener("keydown", function (event) {
						return eventKeydown(event, handleNumber);
					});
				}

				handle.setAttribute("role", "slider");
				handle.setAttribute("aria-orientation", options.ort ? "vertical" : "horizontal");

				if (handleNumber === 0) {
					addClass(handle, options.cssClasses.handleLower);
				} else if (handleNumber === options.handles - 1) {
					addClass(handle, options.cssClasses.handleUpper);
				}

				return origin;
			}

			// Insert nodes for connect elements
			function addConnect(base, add) {
				if (!add) {
					return false;
				}

				return addNodeTo(base, options.cssClasses.connect);
			}

			// Add handles to the slider base.
			function addElements(connectOptions, base) {
				var connectBase = addNodeTo(base, options.cssClasses.connects);

				scope_Handles = [];
				scope_Connects = [];

				scope_Connects.push(addConnect(connectBase, connectOptions[0]));

				// [::::O====O====O====]
				// connectOptions = [0, 1, 1, 1]

				for (var i = 0; i < options.handles; i++) {
					// Keep a list of all added handles.
					scope_Handles.push(addOrigin(base, i));
					scope_HandleNumbers[i] = i;
					scope_Connects.push(addConnect(connectBase, connectOptions[i + 1]));
				}
			}

			// Initialize a single slider.
			function addSlider(addTarget) {
				// Apply classes and data to the target.
				addClass(addTarget, options.cssClasses.target);

				if (options.dir === 0) {
					addClass(addTarget, options.cssClasses.ltr);
				} else {
					addClass(addTarget, options.cssClasses.rtl);
				}

				if (options.ort === 0) {
					addClass(addTarget, options.cssClasses.horizontal);
				} else {
					addClass(addTarget, options.cssClasses.vertical);
				}

				var textDirection = getComputedStyle(addTarget).direction;

				if (textDirection === "rtl") {
					addClass(addTarget, options.cssClasses.textDirectionRtl);
				} else {
					addClass(addTarget, options.cssClasses.textDirectionLtr);
				}

				return addNodeTo(addTarget, options.cssClasses.base);
			}

			function addTooltip(handle, handleNumber) {
				if (!options.tooltips[handleNumber]) {
					return false;
				}

				return addNodeTo(handle.firstChild, options.cssClasses.tooltip);
			}

			function isSliderDisabled() {
				return scope_Target.hasAttribute("disabled");
			}

			// Disable the slider dragging if any handle is disabled
			function isHandleDisabled(handleNumber) {
				var handleOrigin = scope_Handles[handleNumber];
				return handleOrigin.hasAttribute("disabled");
			}

			function removeTooltips() {
				if (scope_Tooltips) {
					removeEvent("update" + INTERNAL_EVENT_NS.tooltips);
					scope_Tooltips.forEach(function (tooltip) {
						if (tooltip) {
							removeElement(tooltip);
						}
					});
					scope_Tooltips = null;
				}
			}

			// The tooltips option is a shorthand for using the 'update' event.
			function tooltips() {
				removeTooltips();

				// Tooltips are added with options.tooltips in original order.
				scope_Tooltips = scope_Handles.map(addTooltip);

				bindEvent("update" + INTERNAL_EVENT_NS.tooltips, function (values, handleNumber, unencoded) {
					if (!scope_Tooltips[handleNumber]) {
						return;
					}

					var formattedValue = values[handleNumber];

					if (options.tooltips[handleNumber] !== true) {
						formattedValue = options.tooltips[handleNumber].to(unencoded[handleNumber]);
					}

					scope_Tooltips[handleNumber].innerHTML = formattedValue;
				});
			}

			function aria() {
				removeEvent("update" + INTERNAL_EVENT_NS.aria);
				bindEvent("update" + INTERNAL_EVENT_NS.aria, function (values, handleNumber, unencoded, tap, positions) {
					// Update Aria Values for all handles, as a change in one changes min and max values for the next.
					scope_HandleNumbers.forEach(function (index) {
						var handle = scope_Handles[index];

						var min = checkHandlePosition(scope_Locations, index, 0, true, true, true);
						var max = checkHandlePosition(scope_Locations, index, 100, true, true, true);

						var now = positions[index];

						// Formatted value for display
						var text = options.ariaFormat.to(unencoded[index]);

						// Map to slider range values
						min = scope_Spectrum.fromStepping(min).toFixed(1);
						max = scope_Spectrum.fromStepping(max).toFixed(1);
						now = scope_Spectrum.fromStepping(now).toFixed(1);

						handle.children[0].setAttribute("aria-valuemin", min);
						handle.children[0].setAttribute("aria-valuemax", max);
						handle.children[0].setAttribute("aria-valuenow", now);
						handle.children[0].setAttribute("aria-valuetext", text);
					});
				});
			}

			function getGroup(mode, values, stepped) {
				// Use the range.
				if (mode === "range" || mode === "steps") {
					return scope_Spectrum.xVal;
				}

				if (mode === "count") {
					if (values < 2) {
						throw new Error("noUiSlider (" + VERSION + "): 'values' (>= 2) required for mode 'count'.");
					}

					// Divide 0 - 100 in 'count' parts.
					var interval = values - 1;
					var spread = 100 / interval;

					values = [];

					// List these parts and have them handled as 'positions'.
					while (interval--) {
						values[interval] = interval * spread;
					}

					values.push(100);

					mode = "positions";
				}

				if (mode === "positions") {
					// Map all percentages to on-range values.
					return values.map(function (value) {
						return scope_Spectrum.fromStepping(stepped ? scope_Spectrum.getStep(value) : value);
					});
				}

				if (mode === "values") {
					// If the value must be stepped, it needs to be converted to a percentage first.
					if (stepped) {
						return values.map(function (value) {
							// Convert to percentage, apply step, return to value.
							return scope_Spectrum.fromStepping(scope_Spectrum.getStep(scope_Spectrum.toStepping(value)));
						});
					}

					// Otherwise, we can simply use the values.
					return values;
				}
			}

			function generateSpread(density, mode, group) {
				function safeIncrement(value, increment) {
					// Avoid floating point variance by dropping the smallest decimal places.
					return (value + increment).toFixed(7) / 1;
				}

				var indexes = {};
				var firstInRange = scope_Spectrum.xVal[0];
				var lastInRange = scope_Spectrum.xVal[scope_Spectrum.xVal.length - 1];
				var ignoreFirst = false;
				var ignoreLast = false;
				var prevPct = 0;

				// Create a copy of the group, sort it and filter away all duplicates.
				group = unique(
					group.slice().sort(function (a, b) {
						return a - b;
					})
				);

				// Make sure the range starts with the first element.
				if (group[0] !== firstInRange) {
					group.unshift(firstInRange);
					ignoreFirst = true;
				}

				// Likewise for the last one.
				if (group[group.length - 1] !== lastInRange) {
					group.push(lastInRange);
					ignoreLast = true;
				}

				group.forEach(function (current, index) {
					// Get the current step and the lower + upper positions.
					var step;
					var i;
					var q;
					var low = current;
					var high = group[index + 1];
					var newPct;
					var pctDifference;
					var pctPos;
					var type;
					var steps;
					var realSteps;
					var stepSize;
					var isSteps = mode === "steps";

					// When using 'steps' mode, use the provided steps.
					// Otherwise, we'll step on to the next subrange.
					if (isSteps) {
						step = scope_Spectrum.xNumSteps[index];
					}

					// Default to a 'full' step.
					if (!step) {
						step = high - low;
					}

					// Low can be 0, so test for false. Index 0 is already handled.
					if (low === false) {
						return;
					}

					// If high is undefined we are at the last subrange. Make sure it iterates once (#1088)
					if (high === undefined) {
						high = low;
					}

					// Make sure step isn't 0, which would cause an infinite loop (#654)
					step = Math.max(step, 0.0000001);

					// Find all steps in the subrange.
					for (i = low; i <= high; i = safeIncrement(i, step)) {
						// Get the percentage value for the current step,
						// calculate the size for the subrange.
						newPct = scope_Spectrum.toStepping(i);
						pctDifference = newPct - prevPct;

						steps = pctDifference / density;
						realSteps = Math.round(steps);

						// This ratio represents the amount of percentage-space a point indicates.
						// For a density 1 the points/percentage = 1. For density 2, that percentage needs to be re-divided.
						// Round the percentage offset to an even number, then divide by two
						// to spread the offset on both sides of the range.
						stepSize = pctDifference / realSteps;

						// Divide all points evenly, adding the correct number to this subrange.
						// Run up to <= so that 100% gets a point, event if ignoreLast is set.
						for (q = 1; q <= realSteps; q += 1) {
							// The ratio between the rounded value and the actual size might be ~1% off.
							// Correct the percentage offset by the number of points
							// per subrange. density = 1 will result in 100 points on the
							// full range, 2 for 50, 4 for 25, etc.
							pctPos = prevPct + q * stepSize;
							indexes[pctPos.toFixed(5)] = [scope_Spectrum.fromStepping(pctPos), 0];
						}

						// Determine the point type.
						type = group.indexOf(i) > -1 ? PIPS_LARGE_VALUE : isSteps ? PIPS_SMALL_VALUE : PIPS_NO_VALUE;

						// Enforce the 'ignoreFirst' option by overwriting the type for 0.
						if (!index && ignoreFirst && i !== high) {
							type = 0;
						}

						if (!(i === high && ignoreLast)) {
							// Mark the 'type' of this point. 0 = plain, 1 = real value, 2 = step value.
							indexes[newPct.toFixed(5)] = [i, type];
						}

						// Update the percentage count.
						prevPct = newPct;
					}
				});

				return indexes;
			}

			function addMarking(spread, filterFunc, formatter) {
				var element = scope_Document.createElement("div");

				var valueSizeClasses = [];
				valueSizeClasses[PIPS_NO_VALUE] = options.cssClasses.valueNormal;
				valueSizeClasses[PIPS_LARGE_VALUE] = options.cssClasses.valueLarge;
				valueSizeClasses[PIPS_SMALL_VALUE] = options.cssClasses.valueSub;

				var markerSizeClasses = [];
				markerSizeClasses[PIPS_NO_VALUE] = options.cssClasses.markerNormal;
				markerSizeClasses[PIPS_LARGE_VALUE] = options.cssClasses.markerLarge;
				markerSizeClasses[PIPS_SMALL_VALUE] = options.cssClasses.markerSub;

				var valueOrientationClasses = [options.cssClasses.valueHorizontal, options.cssClasses.valueVertical];
				var markerOrientationClasses = [options.cssClasses.markerHorizontal, options.cssClasses.markerVertical];

				addClass(element, options.cssClasses.pips);
				addClass(element, options.ort === 0 ? options.cssClasses.pipsHorizontal : options.cssClasses.pipsVertical);

				function getClasses(type, source) {
					var a = source === options.cssClasses.value;
					var orientationClasses = a ? valueOrientationClasses : markerOrientationClasses;
					var sizeClasses = a ? valueSizeClasses : markerSizeClasses;

					return source + " " + orientationClasses[options.ort] + " " + sizeClasses[type];
				}

				function addSpread(offset, value, type) {
					// Apply the filter function, if it is set.
					type = filterFunc ? filterFunc(value, type) : type;

					if (type === PIPS_NONE) {
						return;
					}

					// Add a marker for every point
					var node = addNodeTo(element, false);
					node.className = getClasses(type, options.cssClasses.marker);
					node.style[options.style] = offset + "%";

					// Values are only appended for points marked '1' or '2'.
					if (type > PIPS_NO_VALUE) {
						node = addNodeTo(element, false);
						node.className = getClasses(type, options.cssClasses.value);
						node.setAttribute("data-value", value);
						node.style[options.style] = offset + "%";
						node.innerHTML = formatter.to(value);
					}
				}

				// Append all points.
				Object.keys(spread).forEach(function (offset) {
					addSpread(offset, spread[offset][0], spread[offset][1]);
				});

				return element;
			}

			function removePips() {
				if (scope_Pips) {
					removeElement(scope_Pips);
					scope_Pips = null;
				}
			}

			function pips(grid) {
				// Fix #669
				removePips();

				var mode = grid.mode;
				var density = grid.density || 1;
				var filter = grid.filter || false;
				var values = grid.values || false;
				var stepped = grid.stepped || false;
				var group = getGroup(mode, values, stepped);
				var spread = generateSpread(density, mode, group);
				var format = grid.format || {
					to: Math.round,
				};

				scope_Pips = scope_Target.appendChild(addMarking(spread, filter, format));

				return scope_Pips;
			}

			// Shorthand for base dimensions.
			function baseSize() {
				var rect = scope_Base.getBoundingClientRect();
				var alt = "offset" + ["Width", "Height"][options.ort];
				return options.ort === 0 ? rect.width || scope_Base[alt] : rect.height || scope_Base[alt];
			}

			// Handler for attaching events trough a proxy.
			function attachEvent(events, element, callback, data) {
				// This function can be used to 'filter' events to the slider.
				// element is a node, not a nodeList

				var method = function (e) {
					e = fixEvent(e, data.pageOffset, data.target || element);

					// fixEvent returns false if this event has a different target
					// when handling (multi-) touch events;
					if (!e) {
						return false;
					}

					// doNotReject is passed by all end events to make sure released touches
					// are not rejected, leaving the slider "stuck" to the cursor;
					if (isSliderDisabled() && !data.doNotReject) {
						return false;
					}

					// Stop if an active 'tap' transition is taking place.
					if (hasClass(scope_Target, options.cssClasses.tap) && !data.doNotReject) {
						return false;
					}

					// Ignore right or middle clicks on start #454
					if (events === actions.start && e.buttons !== undefined && e.buttons > 1) {
						return false;
					}

					// Ignore right or middle clicks on start #454
					if (data.hover && e.buttons) {
						return false;
					}

					// 'supportsPassive' is only true if a browser also supports touch-action: none in CSS.
					// iOS safari does not, so it doesn't get to benefit from passive scrolling. iOS does support
					// touch-action: manipulation, but that allows panning, which breaks
					// sliders after zooming/on non-responsive pages.
					// See: https://bugs.webkit.org/show_bug.cgi?id=133112
					if (!supportsPassive) {
						e.preventDefault();
					}

					e.calcPoint = e.points[options.ort];

					// Call the event handler with the event [ and additional data ].
					callback(e, data);
				};

				var methods = [];

				// Bind a closure on the target for every event type.
				events.split(" ").forEach(function (eventName) {
					element.addEventListener(eventName, method, supportsPassive ? { passive: true } : false);
					methods.push([eventName, method]);
				});

				return methods;
			}

			// Provide a clean event with standardized offset values.
			function fixEvent(e, pageOffset, eventTarget) {
				// Filter the event to register the type, which can be
				// touch, mouse or pointer. Offset changes need to be
				// made on an event specific basis.
				var touch = e.type.indexOf("touch") === 0;
				var mouse = e.type.indexOf("mouse") === 0;
				var pointer = e.type.indexOf("pointer") === 0;

				var x;
				var y;

				// IE10 implemented pointer events with a prefix;
				if (e.type.indexOf("MSPointer") === 0) {
					pointer = true;
				}

				// Erroneous events seem to be passed in occasionally on iOS/iPadOS after user finishes interacting with
				// the slider. They appear to be of type MouseEvent, yet they don't have usual properties set. Ignore
				// events that have no touches or buttons associated with them. (#1057, #1079, #1095)
				if (e.type === "mousedown" && !e.buttons && !e.touches) {
					return false;
				}

				// The only thing one handle should be concerned about is the touches that originated on top of it.
				if (touch) {
					// Returns true if a touch originated on the target.
					var isTouchOnTarget = function (checkTouch) {
						return checkTouch.target === eventTarget || eventTarget.contains(checkTouch.target) || (checkTouch.target.shadowRoot && checkTouch.target.shadowRoot.contains(eventTarget));
					};

					// In the case of touchstart events, we need to make sure there is still no more than one
					// touch on the target so we look amongst all touches.
					if (e.type === "touchstart") {
						var targetTouches = Array.prototype.filter.call(e.touches, isTouchOnTarget);

						// Do not support more than one touch per handle.
						if (targetTouches.length > 1) {
							return false;
						}

						x = targetTouches[0].pageX;
						y = targetTouches[0].pageY;
					} else {
						// In the other cases, find on changedTouches is enough.
						var targetTouch = Array.prototype.find.call(e.changedTouches, isTouchOnTarget);

						// Cancel if the target touch has not moved.
						if (!targetTouch) {
							return false;
						}

						x = targetTouch.pageX;
						y = targetTouch.pageY;
					}
				}

				pageOffset = pageOffset || getPageOffset(scope_Document);

				if (mouse || pointer) {
					x = e.clientX + pageOffset.x;
					y = e.clientY + pageOffset.y;
				}

				e.pageOffset = pageOffset;
				e.points = [x, y];
				e.cursor = mouse || pointer; // Fix #435

				return e;
			}

			// Translate a coordinate in the document to a percentage on the slider
			function calcPointToPercentage(calcPoint) {
				var location = calcPoint - offset(scope_Base, options.ort);
				var proposal = (location * 100) / baseSize();

				// Clamp proposal between 0% and 100%
				// Out-of-bound coordinates may occur when .noUi-base pseudo-elements
				// are used (e.g. contained handles feature)
				proposal = limit(proposal);

				return options.dir ? 100 - proposal : proposal;
			}

			// Find handle closest to a certain percentage on the slider
			function getClosestHandle(clickedPosition) {
				var smallestDifference = 100;
				var handleNumber = false;

				scope_Handles.forEach(function (handle, index) {
					// Disabled handles are ignored
					if (isHandleDisabled(index)) {
						return;
					}

					var handlePosition = scope_Locations[index];
					var differenceWithThisHandle = Math.abs(handlePosition - clickedPosition);

					// Initial state
					var clickAtEdge = differenceWithThisHandle === 100 && smallestDifference === 100;

					// Difference with this handle is smaller than the previously checked handle
					var isCloser = differenceWithThisHandle < smallestDifference;
					var isCloserAfter = differenceWithThisHandle <= smallestDifference && clickedPosition > handlePosition;

					if (isCloser || isCloserAfter || clickAtEdge) {
						handleNumber = index;
						smallestDifference = differenceWithThisHandle;
					}
				});

				return handleNumber;
			}

			// Fire 'end' when a mouse or pen leaves the document.
			function documentLeave(event, data) {
				if (event.type === "mouseout" && event.target.nodeName === "HTML" && event.relatedTarget === null) {
					eventEnd(event, data);
				}
			}

			// Handle movement on document for handle and range drag.
			function eventMove(event, data) {
				// Fix #498
				// Check value of .buttons in 'start' to work around a bug in IE10 mobile (data.buttonsProperty).
				// https://connect.microsoft.com/IE/feedback/details/927005/mobile-ie10-windows-phone-buttons-property-of-pointermove-event-always-zero
				// IE9 has .buttons and .which zero on mousemove.
				// Firefox breaks the spec MDN defines.
				if (navigator.appVersion.indexOf("MSIE 9") === -1 && event.buttons === 0 && data.buttonsProperty !== 0) {
					return eventEnd(event, data);
				}

				// Check if we are moving up or down
				var movement = (options.dir ? -1 : 1) * (event.calcPoint - data.startCalcPoint);

				// Convert the movement into a percentage of the slider width/height
				var proposal = (movement * 100) / data.baseSize;

				moveHandles(movement > 0, proposal, data.locations, data.handleNumbers);
			}

			// Unbind move events on document, call callbacks.
			function eventEnd(event, data) {
				// The handle is no longer active, so remove the class.
				if (data.handle) {
					removeClass(data.handle, options.cssClasses.active);
					scope_ActiveHandlesCount -= 1;
				}

				// Unbind the move and end events, which are added on 'start'.
				data.listeners.forEach(function (c) {
					scope_DocumentElement.removeEventListener(c[0], c[1]);
				});

				if (scope_ActiveHandlesCount === 0) {
					// Remove dragging class.
					removeClass(scope_Target, options.cssClasses.drag);
					setZindex();

					// Remove cursor styles and text-selection events bound to the body.
					if (event.cursor) {
						scope_Body.style.cursor = "";
						scope_Body.removeEventListener("selectstart", preventDefault);
					}
				}

				data.handleNumbers.forEach(function (handleNumber) {
					fireEvent("change", handleNumber);
					fireEvent("set", handleNumber);
					fireEvent("end", handleNumber);
				});
			}

			// Bind move events on document.
			function eventStart(event, data) {
				// Ignore event if any handle is disabled
				if (data.handleNumbers.some(isHandleDisabled)) {
					return false;
				}

				var handle;

				if (data.handleNumbers.length === 1) {
					var handleOrigin = scope_Handles[data.handleNumbers[0]];

					handle = handleOrigin.children[0];
					scope_ActiveHandlesCount += 1;

					// Mark the handle as 'active' so it can be styled.
					addClass(handle, options.cssClasses.active);
				}

				// A drag should never propagate up to the 'tap' event.
				event.stopPropagation();

				// Record the event listeners.
				var listeners = [];

				// Attach the move and end events.
				var moveEvent = attachEvent(actions.move, scope_DocumentElement, eventMove, {
					// The event target has changed so we need to propagate the original one so that we keep
					// relying on it to extract target touches.
					target: event.target,
					handle: handle,
					listeners: listeners,
					startCalcPoint: event.calcPoint,
					baseSize: baseSize(),
					pageOffset: event.pageOffset,
					handleNumbers: data.handleNumbers,
					buttonsProperty: event.buttons,
					locations: scope_Locations.slice(),
				});

				var endEvent = attachEvent(actions.end, scope_DocumentElement, eventEnd, {
					target: event.target,
					handle: handle,
					listeners: listeners,
					doNotReject: true,
					handleNumbers: data.handleNumbers,
				});

				var outEvent = attachEvent("mouseout", scope_DocumentElement, documentLeave, {
					target: event.target,
					handle: handle,
					listeners: listeners,
					doNotReject: true,
					handleNumbers: data.handleNumbers,
				});

				// We want to make sure we pushed the listeners in the listener list rather than creating
				// a new one as it has already been passed to the event handlers.
				listeners.push.apply(listeners, moveEvent.concat(endEvent, outEvent));

				// Text selection isn't an issue on touch devices,
				// so adding cursor styles can be skipped.
				if (event.cursor) {
					// Prevent the 'I' cursor and extend the range-drag cursor.
					scope_Body.style.cursor = getComputedStyle(event.target).cursor;

					// Mark the target with a dragging state.
					if (scope_Handles.length > 1) {
						addClass(scope_Target, options.cssClasses.drag);
					}

					// Prevent text selection when dragging the handles.
					// In noUiSlider <= 9.2.0, this was handled by calling preventDefault on mouse/touch start/move,
					// which is scroll blocking. The selectstart event is supported by FireFox starting from version 52,
					// meaning the only holdout is iOS Safari. This doesn't matter: text selection isn't triggered there.
					// The 'cursor' flag is false.
					// See: http://caniuse.com/#search=selectstart
					scope_Body.addEventListener("selectstart", preventDefault, false);
				}

				data.handleNumbers.forEach(function (handleNumber) {
					fireEvent("start", handleNumber);
				});
			}

			// Move closest handle to tapped location.
			function eventTap(event) {
				// The tap event shouldn't propagate up
				event.stopPropagation();

				var proposal = calcPointToPercentage(event.calcPoint);
				var handleNumber = getClosestHandle(proposal);

				// Tackle the case that all handles are 'disabled'.
				if (handleNumber === false) {
					return false;
				}

				// Flag the slider as it is now in a transitional state.
				// Transition takes a configurable amount of ms (default 300). Re-enable the slider after that.
				if (!options.events.snap) {
					addClassFor(scope_Target, options.cssClasses.tap, options.animationDuration);
				}

				setHandle(handleNumber, proposal, true, true);

				setZindex();

				fireEvent("slide", handleNumber, true);
				fireEvent("update", handleNumber, true);
				fireEvent("change", handleNumber, true);
				fireEvent("set", handleNumber, true);

				if (options.events.snap) {
					eventStart(event, { handleNumbers: [handleNumber] });
				}
			}

			// Fires a 'hover' event for a hovered mouse/pen position.
			function eventHover(event) {
				var proposal = calcPointToPercentage(event.calcPoint);

				var to = scope_Spectrum.getStep(proposal);
				var value = scope_Spectrum.fromStepping(to);

				Object.keys(scope_Events).forEach(function (targetEvent) {
					if ("hover" === targetEvent.split(".")[0]) {
						scope_Events[targetEvent].forEach(function (callback) {
							callback.call(scope_Self, value);
						});
					}
				});
			}

			// Handles keydown on focused handles
			// Don't move the document when pressing arrow keys on focused handles
			function eventKeydown(event, handleNumber) {
				if (isSliderDisabled() || isHandleDisabled(handleNumber)) {
					return false;
				}

				var horizontalKeys = ["Left", "Right"];
				var verticalKeys = ["Down", "Up"];
				var largeStepKeys = ["PageDown", "PageUp"];
				var edgeKeys = ["Home", "End"];

				if (options.dir && !options.ort) {
					// On an right-to-left slider, the left and right keys act inverted
					horizontalKeys.reverse();
				} else if (options.ort && !options.dir) {
					// On a top-to-bottom slider, the up and down keys act inverted
					verticalKeys.reverse();
					largeStepKeys.reverse();
				}

				// Strip "Arrow" for IE compatibility. https://developer.mozilla.org/en-US/docs/Web/API/KeyboardEvent/key
				var key = event.key.replace("Arrow", "");

				var isLargeDown = key === largeStepKeys[0];
				var isLargeUp = key === largeStepKeys[1];
				var isDown = key === verticalKeys[0] || key === horizontalKeys[0] || isLargeDown;
				var isUp = key === verticalKeys[1] || key === horizontalKeys[1] || isLargeUp;
				var isMin = key === edgeKeys[0];
				var isMax = key === edgeKeys[1];

				if (!isDown && !isUp && !isMin && !isMax) {
					return true;
				}

				event.preventDefault();

				var to;

				if (isUp || isDown) {
					var multiplier = options.keyboardPageMultiplier;
					var direction = isDown ? 0 : 1;
					var steps = getNextStepsForHandle(handleNumber);
					var step = steps[direction];

					// At the edge of a slider, do nothing
					if (step === null) {
						return false;
					}

					// No step set, use the default of 10% of the sub-range
					if (step === false) {
						step = scope_Spectrum.getDefaultStep(scope_Locations[handleNumber], isDown, options.keyboardDefaultStep);
					}

					if (isLargeUp || isLargeDown) {
						step *= multiplier;
					}

					// Step over zero-length ranges (#948);
					step = Math.max(step, 0.0000001);

					// Decrement for down steps
					step = (isDown ? -1 : 1) * step;

					to = scope_Values[handleNumber] + step;
				} else if (isMax) {
					// End key
					to = options.spectrum.xVal[options.spectrum.xVal.length - 1];
				} else {
					// Home key
					to = options.spectrum.xVal[0];
				}

				setHandle(handleNumber, scope_Spectrum.toStepping(to), true, true);

				fireEvent("slide", handleNumber);
				fireEvent("update", handleNumber);
				fireEvent("change", handleNumber);
				fireEvent("set", handleNumber);

				return false;
			}

			// Attach events to several slider parts.
			function bindSliderEvents(behaviour) {
				// Attach the standard drag event to the handles.
				if (!behaviour.fixed) {
					scope_Handles.forEach(function (handle, index) {
						// These events are only bound to the visual handle
						// element, not the 'real' origin element.
						attachEvent(actions.start, handle.children[0], eventStart, {
							handleNumbers: [index],
						});
					});
				}

				// Attach the tap event to the slider base.
				if (behaviour.tap) {
					attachEvent(actions.start, scope_Base, eventTap, {});
				}

				// Fire hover events
				if (behaviour.hover) {
					attachEvent(actions.move, scope_Base, eventHover, {
						hover: true,
					});
				}

				// Make the range draggable.
				if (behaviour.drag) {
					scope_Connects.forEach(function (connect, index) {
						if (connect === false || index === 0 || index === scope_Connects.length - 1) {
							return;
						}

						var handleBefore = scope_Handles[index - 1];
						var handleAfter = scope_Handles[index];
						var eventHolders = [connect];

						addClass(connect, options.cssClasses.draggable);

						// When the range is fixed, the entire range can
						// be dragged by the handles. The handle in the first
						// origin will propagate the start event upward,
						// but it needs to be bound manually on the other.
						if (behaviour.fixed) {
							eventHolders.push(handleBefore.children[0]);
							eventHolders.push(handleAfter.children[0]);
						}

						eventHolders.forEach(function (eventHolder) {
							attachEvent(actions.start, eventHolder, eventStart, {
								handles: [handleBefore, handleAfter],
								handleNumbers: [index - 1, index],
							});
						});
					});
				}
			}

			// Attach an event to this slider, possibly including a namespace
			function bindEvent(namespacedEvent, callback) {
				scope_Events[namespacedEvent] = scope_Events[namespacedEvent] || [];
				scope_Events[namespacedEvent].push(callback);

				// If the event bound is 'update,' fire it immediately for all handles.
				if (namespacedEvent.split(".")[0] === "update") {
					scope_Handles.forEach(function (a, index) {
						fireEvent("update", index);
					});
				}
			}

			function isInternalNamespace(namespace) {
				return namespace === INTERNAL_EVENT_NS.aria || namespace === INTERNAL_EVENT_NS.tooltips;
			}

			// Undo attachment of event
			function removeEvent(namespacedEvent) {
				var event = namespacedEvent && namespacedEvent.split(".")[0];
				var namespace = event ? namespacedEvent.substring(event.length) : namespacedEvent;

				Object.keys(scope_Events).forEach(function (bind) {
					var tEvent = bind.split(".")[0];
					var tNamespace = bind.substring(tEvent.length);
					if ((!event || event === tEvent) && (!namespace || namespace === tNamespace)) {
						// only delete protected internal event if intentional
						if (!isInternalNamespace(tNamespace) || namespace === tNamespace) {
							delete scope_Events[bind];
						}
					}
				});
			}

			// External event handling
			function fireEvent(eventName, handleNumber, tap) {
				Object.keys(scope_Events).forEach(function (targetEvent) {
					var eventType = targetEvent.split(".")[0];

					if (eventName === eventType) {
						scope_Events[targetEvent].forEach(function (callback) {
							callback.call(
								// Use the slider public API as the scope ('this')
								scope_Self,
								// Return values as array, so arg_1[arg_2] is always valid.
								scope_Values.map(options.format.to),
								// Handle index, 0 or 1
								handleNumber,
								// Un-formatted slider values
								scope_Values.slice(),
								// Event is fired by tap, true or false
								tap || false,
								// Left offset of the handle, in relation to the slider
								scope_Locations.slice(),
								// add the slider public API to an accessible parameter when this is unavailable
								scope_Self
							);
						});
					}
				});
			}

			// Split out the handle positioning logic so the Move event can use it, too
			function checkHandlePosition(reference, handleNumber, to, lookBackward, lookForward, getValue) {
				var distance;

				// For sliders with multiple handles, limit movement to the other handle.
				// Apply the margin option by adding it to the handle positions.
				if (scope_Handles.length > 1 && !options.events.unconstrained) {
					if (lookBackward && handleNumber > 0) {
						distance = scope_Spectrum.getAbsoluteDistance(reference[handleNumber - 1], options.margin, 0);
						to = Math.max(to, distance);
					}

					if (lookForward && handleNumber < scope_Handles.length - 1) {
						distance = scope_Spectrum.getAbsoluteDistance(reference[handleNumber + 1], options.margin, 1);
						to = Math.min(to, distance);
					}
				}

				// The limit option has the opposite effect, limiting handles to a
				// maximum distance from another. Limit must be > 0, as otherwise
				// handles would be unmovable.
				if (scope_Handles.length > 1 && options.limit) {
					if (lookBackward && handleNumber > 0) {
						distance = scope_Spectrum.getAbsoluteDistance(reference[handleNumber - 1], options.limit, 0);
						to = Math.min(to, distance);
					}

					if (lookForward && handleNumber < scope_Handles.length - 1) {
						distance = scope_Spectrum.getAbsoluteDistance(reference[handleNumber + 1], options.limit, 1);
						to = Math.max(to, distance);
					}
				}

				// The padding option keeps the handles a certain distance from the
				// edges of the slider. Padding must be > 0.
				if (options.padding) {
					if (handleNumber === 0) {
						distance = scope_Spectrum.getAbsoluteDistance(0, options.padding[0], 0);
						to = Math.max(to, distance);
					}

					if (handleNumber === scope_Handles.length - 1) {
						distance = scope_Spectrum.getAbsoluteDistance(100, options.padding[1], 1);
						to = Math.min(to, distance);
					}
				}

				to = scope_Spectrum.getStep(to);

				// Limit percentage to the 0 - 100 range
				to = limit(to);

				// Return false if handle can't move
				if (to === reference[handleNumber] && !getValue) {
					return false;
				}

				return to;
			}

			// Uses slider orientation to create CSS rules. a = base value;
			function inRuleOrder(v, a) {
				var o = options.ort;
				return (o ? a : v) + ", " + (o ? v : a);
			}

			// Moves handle(s) by a percentage
			// (bool, % to move, [% where handle started, ...], [index in scope_Handles, ...])
			function moveHandles(upward, proposal, locations, handleNumbers) {
				var proposals = locations.slice();

				var b = [!upward, upward];
				var f = [upward, !upward];

				// Copy handleNumbers so we don't change the dataset
				handleNumbers = handleNumbers.slice();

				// Check to see which handle is 'leading'.
				// If that one can't move the second can't either.
				if (upward) {
					handleNumbers.reverse();
				}

				// Step 1: get the maximum percentage that any of the handles can move
				if (handleNumbers.length > 1) {
					handleNumbers.forEach(function (handleNumber, o) {
						var to = checkHandlePosition(proposals, handleNumber, proposals[handleNumber] + proposal, b[o], f[o], false);

						// Stop if one of the handles can't move.
						if (to === false) {
							proposal = 0;
						} else {
							proposal = to - proposals[handleNumber];
							proposals[handleNumber] = to;
						}
					});
				}

				// If using one handle, check backward AND forward
				else {
					b = f = [true];
				}

				var state = false;

				// Step 2: Try to set the handles with the found percentage
				handleNumbers.forEach(function (handleNumber, o) {
					state = setHandle(handleNumber, locations[handleNumber] + proposal, b[o], f[o]) || state;
				});

				// Step 3: If a handle moved, fire events
				if (state) {
					handleNumbers.forEach(function (handleNumber) {
						fireEvent("update", handleNumber);
						fireEvent("slide", handleNumber);
					});
				}
			}

			// Takes a base value and an offset. This offset is used for the connect bar size.
			// In the initial design for this feature, the origin element was 1% wide.
			// Unfortunately, a rounding bug in Chrome makes it impossible to implement this feature
			// in this manner: https://bugs.chromium.org/p/chromium/issues/detail?id=798223
			function transformDirection(a, b) {
				return options.dir ? 100 - a - b : a;
			}

			// Updates scope_Locations and scope_Values, updates visual state
			function updateHandlePosition(handleNumber, to) {
				// Update locations.
				scope_Locations[handleNumber] = to;

				// Convert the value to the slider stepping/range.
				scope_Values[handleNumber] = scope_Spectrum.fromStepping(to);

				var translation = 10 * (transformDirection(to, 0) - scope_DirOffset);
				var translateRule = "translate(" + inRuleOrder(translation + "%", "0") + ")";

				scope_Handles[handleNumber].style[options.transformRule] = translateRule;

				updateConnect(handleNumber);
				updateConnect(handleNumber + 1);
			}

			// Handles before the slider middle are stacked later = higher,
			// Handles after the middle later is lower
			// [[7] [8] .......... | .......... [5] [4]
			function setZindex() {
				scope_HandleNumbers.forEach(function (handleNumber) {
					var dir = scope_Locations[handleNumber] > 50 ? -1 : 1;
					var zIndex = 3 + (scope_Handles.length + dir * handleNumber);
					scope_Handles[handleNumber].style.zIndex = zIndex;
				});
			}

			// Test suggested values and apply margin, step.
			// if exactInput is true, don't run checkHandlePosition, then the handle can be placed in between steps (#436)
			function setHandle(handleNumber, to, lookBackward, lookForward, exactInput) {
				if (!exactInput) {
					to = checkHandlePosition(scope_Locations, handleNumber, to, lookBackward, lookForward, false);
				}

				if (to === false) {
					return false;
				}

				updateHandlePosition(handleNumber, to);

				return true;
			}

			// Updates style attribute for connect nodes
			function updateConnect(index) {
				// Skip connects set to false
				if (!scope_Connects[index]) {
					return;
				}

				var l = 0;
				var h = 100;

				if (index !== 0) {
					l = scope_Locations[index - 1];
				}

				if (index !== scope_Connects.length - 1) {
					h = scope_Locations[index];
				}

				// We use two rules:
				// 'translate' to change the left/top offset;
				// 'scale' to change the width of the element;
				// As the element has a width of 100%, a translation of 100% is equal to 100% of the parent (.noUi-base)
				var connectWidth = h - l;
				var translateRule = "translate(" + inRuleOrder(transformDirection(l, connectWidth) + "%", "0") + ")";
				var scaleRule = "scale(" + inRuleOrder(connectWidth / 100, "1") + ")";

				scope_Connects[index].style[options.transformRule] = translateRule + " " + scaleRule;
			}

			// Parses value passed to .set method. Returns current value if not parse-able.
			function resolveToValue(to, handleNumber) {
				// Setting with null indicates an 'ignore'.
				// Inputting 'false' is invalid.
				if (to === null || to === false || to === undefined) {
					return scope_Locations[handleNumber];
				}

				// If a formatted number was passed, attempt to decode it.
				if (typeof to === "number") {
					to = String(to);
				}

				to = options.format.from(to);
				to = scope_Spectrum.toStepping(to);

				// If parsing the number failed, use the current value.
				if (to === false || isNaN(to)) {
					return scope_Locations[handleNumber];
				}

				return to;
			}

			// Set the slider value.
			function valueSet(input, fireSetEvent, exactInput) {
				var values = asArray(input);
				var isInit = scope_Locations[0] === undefined;

				// Event fires by default
				fireSetEvent = fireSetEvent === undefined ? true : !!fireSetEvent;

				// Animation is optional.
				// Make sure the initial values were set before using animated placement.
				if (options.animate && !isInit) {
					addClassFor(scope_Target, options.cssClasses.tap, options.animationDuration);
				}

				// First pass, without lookAhead but with lookBackward. Values are set from left to right.
				scope_HandleNumbers.forEach(function (handleNumber) {
					setHandle(handleNumber, resolveToValue(values[handleNumber], handleNumber), true, false, exactInput);
				});

				var i = scope_HandleNumbers.length === 1 ? 0 : 1;

				// Secondary passes. Now that all base values are set, apply constraints.
				// Iterate all handles to ensure constraints are applied for the entire slider (Issue #1009)
				for (; i < scope_HandleNumbers.length; ++i) {
					scope_HandleNumbers.forEach(function (handleNumber) {
						setHandle(handleNumber, scope_Locations[handleNumber], true, true, exactInput);
					});
				}

				setZindex();

				scope_HandleNumbers.forEach(function (handleNumber) {
					fireEvent("update", handleNumber);

					// Fire the event only for handles that received a new value, as per #579
					if (values[handleNumber] !== null && fireSetEvent) {
						fireEvent("set", handleNumber);
					}
				});
			}

			// Reset slider to initial values
			function valueReset(fireSetEvent) {
				valueSet(options.start, fireSetEvent);
			}

			// Set value for a single handle
			function valueSetHandle(handleNumber, value, fireSetEvent, exactInput) {
				// Ensure numeric input
				handleNumber = Number(handleNumber);

				if (!(handleNumber >= 0 && handleNumber < scope_HandleNumbers.length)) {
					throw new Error("noUiSlider (" + VERSION + "): invalid handle number, got: " + handleNumber);
				}

				// Look both backward and forward, since we don't want this handle to "push" other handles (#960);
				// The exactInput argument can be used to ignore slider stepping (#436)
				setHandle(handleNumber, resolveToValue(value, handleNumber), true, true, exactInput);

				fireEvent("update", handleNumber);

				if (fireSetEvent) {
					fireEvent("set", handleNumber);
				}
			}

			// Get the slider value.
			function valueGet() {
				var values = scope_Values.map(options.format.to);

				// If only one handle is used, return a single value.
				if (values.length === 1) {
					return values[0];
				}

				return values;
			}

			// Removes classes from the root and empties it.
			function destroy() {
				// remove protected internal listeners
				removeEvent(INTERNAL_EVENT_NS.aria);
				removeEvent(INTERNAL_EVENT_NS.tooltips);

				for (var key in options.cssClasses) {
					if (!options.cssClasses.hasOwnProperty(key)) {
						continue;
					}
					removeClass(scope_Target, options.cssClasses[key]);
				}

				while (scope_Target.firstChild) {
					scope_Target.removeChild(scope_Target.firstChild);
				}

				delete scope_Target.noUiSlider;
			}

			function getNextStepsForHandle(handleNumber) {
				var location = scope_Locations[handleNumber];
				var nearbySteps = scope_Spectrum.getNearbySteps(location);
				var value = scope_Values[handleNumber];
				var increment = nearbySteps.thisStep.step;
				var decrement = null;

				// If snapped, directly use defined step value
				if (options.snap) {
					return [value - nearbySteps.stepBefore.startValue || null, nearbySteps.stepAfter.startValue - value || null];
				}

				// If the next value in this step moves into the next step,
				// the increment is the start of the next step - the current value
				if (increment !== false) {
					if (value + increment > nearbySteps.stepAfter.startValue) {
						increment = nearbySteps.stepAfter.startValue - value;
					}
				}

				// If the value is beyond the starting point
				if (value > nearbySteps.thisStep.startValue) {
					decrement = nearbySteps.thisStep.step;
				} else if (nearbySteps.stepBefore.step === false) {
					decrement = false;
				}

				// If a handle is at the start of a step, it always steps back into the previous step first
				else {
					decrement = value - nearbySteps.stepBefore.highestStep;
				}

				// Now, if at the slider edges, there is no in/decrement
				if (location === 100) {
					increment = null;
				} else if (location === 0) {
					decrement = null;
				}

				// As per #391, the comparison for the decrement step can have some rounding issues.
				var stepDecimals = scope_Spectrum.countStepDecimals();

				// Round per #391
				if (increment !== null && increment !== false) {
					increment = Number(increment.toFixed(stepDecimals));
				}

				if (decrement !== null && decrement !== false) {
					decrement = Number(decrement.toFixed(stepDecimals));
				}

				return [decrement, increment];
			}

			// Get the current step size for the slider.
			function getNextSteps() {
				return scope_HandleNumbers.map(getNextStepsForHandle);
			}

			// Updateable: margin, limit, padding, step, range, animate, snap
			function updateOptions(optionsToUpdate, fireSetEvent) {
				// Spectrum is created using the range, snap, direction and step options.
				// 'snap' and 'step' can be updated.
				// If 'snap' and 'step' are not passed, they should remain unchanged.
				var v = valueGet();

				var updateAble = ["margin", "limit", "padding", "range", "animate", "snap", "step", "format", "pips", "tooltips"];

				// Only change options that we're actually passed to update.
				updateAble.forEach(function (name) {
					// Check for undefined. null removes the value.
					if (optionsToUpdate[name] !== undefined) {
						originalOptions[name] = optionsToUpdate[name];
					}
				});

				var newOptions = testOptions(originalOptions);

				// Load new options into the slider state
				updateAble.forEach(function (name) {
					if (optionsToUpdate[name] !== undefined) {
						options[name] = newOptions[name];
					}
				});

				scope_Spectrum = newOptions.spectrum;

				// Limit, margin and padding depend on the spectrum but are stored outside of it. (#677)
				options.margin = newOptions.margin;
				options.limit = newOptions.limit;
				options.padding = newOptions.padding;

				// Update pips, removes existing.
				if (options.pips) {
					pips(options.pips);
				} else {
					removePips();
				}

				// Update tooltips, removes existing.
				if (options.tooltips) {
					tooltips();
				} else {
					removeTooltips();
				}

				// Invalidate the current positioning so valueSet forces an update.
				scope_Locations = [];
				valueSet(optionsToUpdate.start || v, fireSetEvent);
			}

			// Initialization steps
			function setupSlider() {
				// Create the base element, initialize HTML and set classes.
				// Add handles and connect elements.
				scope_Base = addSlider(scope_Target);

				addElements(options.connect, scope_Base);

				// Attach user events.
				bindSliderEvents(options.events);

				// Use the public value method to set the start values.
				valueSet(options.start);

				if (options.pips) {
					pips(options.pips);
				}

				if (options.tooltips) {
					tooltips();
				}

				aria();
			}

			setupSlider();

			// noinspection JSUnusedGlobalSymbols
			scope_Self = {
				destroy: destroy,
				steps: getNextSteps,
				on: bindEvent,
				off: removeEvent,
				get: valueGet,
				set: valueSet,
				setHandle: valueSetHandle,
				reset: valueReset,
				// Exposed for unit testing, don't use this in your application.
				__moveHandles: function (a, b, c) {
					moveHandles(a, b, scope_Locations, c);
				},
				options: originalOptions, // Issue #600, #678
				updateOptions: updateOptions,
				target: scope_Target, // Issue #597
				removePips: removePips,
				removeTooltips: removeTooltips,
				getTooltips: function () {
					return scope_Tooltips;
				},
				getOrigins: function () {
					return scope_Handles;
				},
				pips: pips, // Issue #594
			};

			return scope_Self;
		}

		// Run the standard initializer
		function initialize(target, originalOptions) {
			if (!target || !target.nodeName) {
				throw new Error("noUiSlider (" + VERSION + "): create requires a single element, got: " + target);
			}

			// Throw an error if the slider was already initialized.
			if (target.noUiSlider) {
				throw new Error("noUiSlider (" + VERSION + "): Slider was already initialized.");
			}

			// Test the options and create the slider environment;
			var options = testOptions(originalOptions, target);
			var api = scope(target, options, originalOptions);

			target.noUiSlider = api;

			return api;
		}

		// Use an object instead of a function for future expandability;
		return {
			// Exposed for unit testing, don't use this in your application.
			__spectrum: Spectrum,
			version: VERSION,
			// A reference to the default classes, allows global changes.
			// Use the cssClasses option for changes to one slider.
			cssClasses: cssClasses,
			create: initialize,
		};
	});

/*!
 * Lightbox v2.10.0
 * by Lokesh Dhakar
 *
 * More info:
 * http://lokeshdhakar.com/projects/lightbox2/
 *
 * Copyright 2007, 2018 Lokesh Dhakar
 * Released under the MIT license
 * https://github.com/lokesh/lightbox2/blob/master/LICENSE
 *
 * @preserve
 */ (function (a, b) {
	"function" == typeof define && define.amd ? define(["jquery"], b) : "object" == typeof exports ? (module.exports = b(require("jquery"))) : (a.lightbox = b(a.jQuery));
})(this, function (a) {
	function b(b) {
		(this.album = []), (this.currentImageIndex = void 0), this.init(), (this.options = a.extend({}, this.constructor.defaults)), this.option(b);
	}
	return (
		(b.defaults = { albumLabel: "Image %1 of %2", alwaysShowNavOnTouchDevices: !1, fadeDuration: 600, fitImagesInViewport: !0, imageFadeDuration: 600, positionFromTop: 50, resizeDuration: 700, showImageNumberLabel: !0, wrapAround: !1, disableScrolling: !1, sanitizeTitle: !1 }),
		(b.prototype.option = function (b) {
			a.extend(this.options, b);
		}),
		(b.prototype.imageCountLabel = function (a, b) {
			return this.options.albumLabel.replace(/%1/g, a).replace(/%2/g, b);
		}),
		(b.prototype.init = function () {
			var b = this;
			a(document).ready(function () {
				b.enable(), b.build();
			});
		}),
		(b.prototype.enable = function () {
			var b = this;
			a("body").on("click", "a[rel^=lightbox], area[rel^=lightbox], a[data-lightbox], area[data-lightbox]", function (c) {
				return b.start(a(c.currentTarget)), !1;
			});
		}),
		(b.prototype.build = function () {
			if (!(a("#lightbox").length > 0)) {
				var b = this;
				a('<div id="lightboxOverlay" class="lightboxOverlay"></div><div id="lightbox" class="lightbox"><div class="lb-outerContainer"><div class="lb-container"><img class="lb-image" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" /><div class="lb-nav"><a class="lb-prev" href="" ></a><a class="lb-next" href="" ></a></div><div class="lb-loader"><a class="lb-cancel"></a></div></div></div><div class="lb-dataContainer"><div class="lb-data"><div class="lb-details"><span class="lb-caption"></span><span class="lb-number"></span></div><div class="lb-closeContainer"><a class="lb-close"></a></div></div></div></div>').appendTo(a("body")),
					(this.$lightbox = a("#lightbox")),
					(this.$overlay = a("#lightboxOverlay")),
					(this.$outerContainer = this.$lightbox.find(".lb-outerContainer")),
					(this.$container = this.$lightbox.find(".lb-container")),
					(this.$image = this.$lightbox.find(".lb-image")),
					(this.$nav = this.$lightbox.find(".lb-nav")),
					(this.containerPadding = { top: parseInt(this.$container.css("padding-top"), 10), right: parseInt(this.$container.css("padding-right"), 10), bottom: parseInt(this.$container.css("padding-bottom"), 10), left: parseInt(this.$container.css("padding-left"), 10) }),
					(this.imageBorderWidth = { top: parseInt(this.$image.css("border-top-width"), 10), right: parseInt(this.$image.css("border-right-width"), 10), bottom: parseInt(this.$image.css("border-bottom-width"), 10), left: parseInt(this.$image.css("border-left-width"), 10) }),
					this.$overlay.hide().on("click", function () {
						return b.end(), !1;
					}),
					this.$lightbox.hide().on("click", function (c) {
						return "lightbox" === a(c.target).attr("id") && b.end(), !1;
					}),
					this.$outerContainer.on("click", function (c) {
						return "lightbox" === a(c.target).attr("id") && b.end(), !1;
					}),
					this.$lightbox.find(".lb-prev").on("click", function () {
						return 0 === b.currentImageIndex ? b.changeImage(b.album.length - 1) : b.changeImage(b.currentImageIndex - 1), !1;
					}),
					this.$lightbox.find(".lb-next").on("click", function () {
						return b.currentImageIndex === b.album.length - 1 ? b.changeImage(0) : b.changeImage(b.currentImageIndex + 1), !1;
					}),
					this.$nav.on("mousedown", function (a) {
						3 === a.which &&
							(b.$nav.css("pointer-events", "none"),
							b.$lightbox.one("contextmenu", function () {
								setTimeout(
									function () {
										this.$nav.css("pointer-events", "auto");
									}.bind(b),
									0
								);
							}));
					}),
					this.$lightbox.find(".lb-loader, .lb-close").on("click", function () {
						return b.end(), !1;
					});
			}
		}),
		(b.prototype.start = function (b) {
			function c(a) {
				d.album.push({ alt: a.attr("data-alt"), link: a.attr("href"), title: a.attr("data-title") || a.attr("title") });
			}
			var d = this,
				e = a(window);
			e.on("resize", a.proxy(this.sizeOverlay, this)), a("select, object, embed").css({ visibility: "hidden" }), this.sizeOverlay(), (this.album = []);
			var f,
				g = 0,
				h = b.attr("data-lightbox");
			if (h) {
				f = a(b.prop("tagName") + '[data-lightbox="' + h + '"]');
				for (var i = 0; i < f.length; i = ++i) c(a(f[i])), f[i] === b[0] && (g = i);
			} else if ("lightbox" === b.attr("rel")) c(b);
			else {
				f = a(b.prop("tagName") + '[rel="' + b.attr("rel") + '"]');
				for (var j = 0; j < f.length; j = ++j) c(a(f[j])), f[j] === b[0] && (g = j);
			}
			var k = e.scrollTop() + this.options.positionFromTop,
				l = e.scrollLeft();
			this.$lightbox.css({ top: k + "px", left: l + "px" }).fadeIn(this.options.fadeDuration), this.options.disableScrolling && a("html").addClass("lb-disable-scrolling"), this.changeImage(g);
		}),
		(b.prototype.changeImage = function (b) {
			var c = this;
			this.disableKeyboardNav();
			var d = this.$lightbox.find(".lb-image");
			this.$overlay.fadeIn(this.options.fadeDuration), a(".lb-loader").fadeIn("slow"), this.$lightbox.find(".lb-image, .lb-nav, .lb-prev, .lb-next, .lb-dataContainer, .lb-numbers, .lb-caption").hide(), this.$outerContainer.addClass("animating");
			var e = new Image();
			(e.onload = function () {
				var f, g, h, i, j, k;
				d.attr({ alt: c.album[b].alt, src: c.album[b].link }),
					a(e),
					d.width(e.width),
					d.height(e.height),
					c.options.fitImagesInViewport && ((k = a(window).width()), (j = a(window).height()), (i = k - c.containerPadding.left - c.containerPadding.right - c.imageBorderWidth.left - c.imageBorderWidth.right - 20), (h = j - c.containerPadding.top - c.containerPadding.bottom - c.imageBorderWidth.top - c.imageBorderWidth.bottom - 120), c.options.maxWidth && c.options.maxWidth < i && (i = c.options.maxWidth), c.options.maxHeight && c.options.maxHeight < i && (h = c.options.maxHeight), (e.width > i || e.height > h) && (e.width / i > e.height / h ? ((g = i), (f = parseInt(e.height / (e.width / g), 10)), d.width(g), d.height(f)) : ((f = h), (g = parseInt(e.width / (e.height / f), 10)), d.width(g), d.height(f)))),
					c.sizeContainer(d.width(), d.height());
			}),
				(e.src = this.album[b].link),
				(this.currentImageIndex = b);
		}),
		(b.prototype.sizeOverlay = function () {
			this.$overlay.width(a(document).width()).height(a(document).height());
		}),
		(b.prototype.sizeContainer = function (a, b) {
			function c() {
				d.$lightbox.find(".lb-dataContainer").width(g), d.$lightbox.find(".lb-prevLink").height(h), d.$lightbox.find(".lb-nextLink").height(h), d.showImage();
			}
			var d = this,
				e = this.$outerContainer.outerWidth(),
				f = this.$outerContainer.outerHeight(),
				g = a + this.containerPadding.left + this.containerPadding.right + this.imageBorderWidth.left + this.imageBorderWidth.right,
				h = b + this.containerPadding.top + this.containerPadding.bottom + this.imageBorderWidth.top + this.imageBorderWidth.bottom;
			e !== g || f !== h
				? this.$outerContainer.animate({ width: g, height: h }, this.options.resizeDuration, "swing", function () {
						c();
				  })
				: c();
		}),
		(b.prototype.showImage = function () {
			this.$lightbox.find(".lb-loader").stop(!0).hide(), this.$lightbox.find(".lb-image").fadeIn(this.options.imageFadeDuration), this.updateNav(), this.updateDetails(), this.preloadNeighboringImages(), this.enableKeyboardNav();
		}),
		(b.prototype.updateNav = function () {
			var a = !1;
			try {
				document.createEvent("TouchEvent"), (a = !!this.options.alwaysShowNavOnTouchDevices);
			} catch (a) {}
			this.$lightbox.find(".lb-nav").show(), this.album.length > 1 && (this.options.wrapAround ? (a && this.$lightbox.find(".lb-prev, .lb-next").css("opacity", "1"), this.$lightbox.find(".lb-prev, .lb-next").show()) : (this.currentImageIndex > 0 && (this.$lightbox.find(".lb-prev").show(), a && this.$lightbox.find(".lb-prev").css("opacity", "1")), this.currentImageIndex < this.album.length - 1 && (this.$lightbox.find(".lb-next").show(), a && this.$lightbox.find(".lb-next").css("opacity", "1"))));
		}),
		(b.prototype.updateDetails = function () {
			var b = this;
			if (void 0 !== this.album[this.currentImageIndex].title && "" !== this.album[this.currentImageIndex].title) {
				var c = this.$lightbox.find(".lb-caption");
				this.options.sanitizeTitle ? c.text(this.album[this.currentImageIndex].title) : c.html(this.album[this.currentImageIndex].title),
					c
						.fadeIn("fast")
						.find("a")
						.on("click", function (b) {
							void 0 !== a(this).attr("target") ? window.open(a(this).attr("href"), a(this).attr("target")) : (location.href = a(this).attr("href"));
						});
			}
			if (this.album.length > 1 && this.options.showImageNumberLabel) {
				var d = this.imageCountLabel(this.currentImageIndex + 1, this.album.length);
				this.$lightbox.find(".lb-number").text(d).fadeIn("fast");
			} else this.$lightbox.find(".lb-number").hide();
			this.$outerContainer.removeClass("animating"),
				this.$lightbox.find(".lb-dataContainer").fadeIn(this.options.resizeDuration, function () {
					return b.sizeOverlay();
				});
		}),
		(b.prototype.preloadNeighboringImages = function () {
			if (this.album.length > this.currentImageIndex + 1) {
				new Image().src = this.album[this.currentImageIndex + 1].link;
			}
			if (this.currentImageIndex > 0) {
				new Image().src = this.album[this.currentImageIndex - 1].link;
			}
		}),
		(b.prototype.enableKeyboardNav = function () {
			a(document).on("keyup.keyboard", a.proxy(this.keyboardAction, this));
		}),
		(b.prototype.disableKeyboardNav = function () {
			a(document).off(".keyboard");
		}),
		(b.prototype.keyboardAction = function (a) {
			var b = a.keyCode,
				c = String.fromCharCode(b).toLowerCase();
			27 === b || c.match(/x|o|c/) ? this.end() : "p" === c || 37 === b ? (0 !== this.currentImageIndex ? this.changeImage(this.currentImageIndex - 1) : this.options.wrapAround && this.album.length > 1 && this.changeImage(this.album.length - 1)) : ("n" !== c && 39 !== b) || (this.currentImageIndex !== this.album.length - 1 ? this.changeImage(this.currentImageIndex + 1) : this.options.wrapAround && this.album.length > 1 && this.changeImage(0));
		}),
		(b.prototype.end = function () {
			this.disableKeyboardNav(), a(window).off("resize", this.sizeOverlay), this.$lightbox.fadeOut(this.options.fadeDuration), this.$overlay.fadeOut(this.options.fadeDuration), a("select, object, embed").css({ visibility: "visible" }), this.options.disableScrolling && a("html").removeClass("lb-disable-scrolling");
		}),
		new b()
	);
});

!(function (e) {
	"function" == typeof define && define.amd ? define([], e) : "object" == typeof exports ? (module.exports = e()) : (window.wNumb = e());
})(function () {
	"use strict";
	var o = ["decimals", "thousand", "mark", "prefix", "suffix", "encoder", "decoder", "negativeBefore", "negative", "edit", "undo"];
	function w(e) {
		return e.split("").reverse().join("");
	}
	function h(e, t) {
		return e.substring(0, t.length) === t;
	}
	function f(e, t, n) {
		if ((e[t] || e[n]) && e[t] === e[n]) throw new Error(t);
	}
	function x(e) {
		return "number" == typeof e && isFinite(e);
	}
	function n(e, t, n, r, i, o, f, u, s, c, a, p) {
		var d,
			l,
			h,
			g = p,
			v = "",
			m = "";
		return (
			o && (p = o(p)),
			!!x(p) &&
				(!1 !== e && 0 === parseFloat(p.toFixed(e)) && (p = 0),
				p < 0 && ((d = !0), (p = Math.abs(p))),
				!1 !== e &&
					(p = (function (e, t) {
						return (e = e.toString().split("e")), (+((e = (e = Math.round(+(e[0] + "e" + (e[1] ? +e[1] + t : t)))).toString().split("e"))[0] + "e" + (e[1] ? e[1] - t : -t))).toFixed(t);
					})(p, e)),
				-1 !== (p = p.toString()).indexOf(".") ? ((h = (l = p.split("."))[0]), n && (v = n + l[1])) : (h = p),
				t && (h = w((h = w(h).match(/.{1,3}/g)).join(w(t)))),
				d && u && (m += u),
				r && (m += r),
				d && s && (m += s),
				(m += h),
				(m += v),
				i && (m += i),
				c && (m = c(m, g)),
				m)
		);
	}
	function r(e, t, n, r, i, o, f, u, s, c, a, p) {
		var d,
			l = "";
		return (
			a && (p = a(p)),
			!(!p || "string" != typeof p) &&
				(u && h(p, u) && ((p = p.replace(u, "")), (d = !0)),
				r && h(p, r) && (p = p.replace(r, "")),
				s && h(p, s) && ((p = p.replace(s, "")), (d = !0)),
				i &&
					(function (e, t) {
						return e.slice(-1 * t.length) === t;
					})(p, i) &&
					(p = p.slice(0, -1 * i.length)),
				t && (p = p.split(t).join("")),
				n && (p = p.replace(n, ".")),
				d && (l += "-"),
				"" !== (l = (l += p).replace(/[^0-9\.\-.]/g, "")) && ((l = Number(l)), f && (l = f(l)), !!x(l) && l))
		);
	}
	function i(e, t, n) {
		var r,
			i = [];
		for (r = 0; r < o.length; r += 1) i.push(e[o[r]]);
		return i.push(n), t.apply("", i);
	}
	return function e(t) {
		if (!(this instanceof e)) return new e(t);
		"object" == typeof t &&
			((t = (function (e) {
				var t,
					n,
					r,
					i = {};
				for (void 0 === e.suffix && (e.suffix = e.postfix), t = 0; t < o.length; t += 1)
					if (void 0 === (r = e[(n = o[t])])) "negative" !== n || i.negativeBefore ? ("mark" === n && "." !== i.thousand ? (i[n] = ".") : (i[n] = !1)) : (i[n] = "-");
					else if ("decimals" === n) {
						if (!(0 <= r && r < 8)) throw new Error(n);
						i[n] = r;
					} else if ("encoder" === n || "decoder" === n || "edit" === n || "undo" === n) {
						if ("function" != typeof r) throw new Error(n);
						i[n] = r;
					} else {
						if ("string" != typeof r) throw new Error(n);
						i[n] = r;
					}
				return f(i, "mark", "thousand"), f(i, "prefix", "negative"), f(i, "prefix", "negativeBefore"), i;
			})(t)),
			(this.to = function (e) {
				return i(t, n, e);
			}),
			(this.from = function (e) {
				return i(t, r, e);
			}));
	};
});

/*!
 * Bootstrap v4.3.1 (https://getbootstrap.com/)
 * Copyright 2011-2019 The Bootstrap Authors (https://github.com/twbs/bootstrap/graphs/contributors)
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */
!(function (t, e) {
	"object" == typeof exports && "undefined" != typeof module ? e(exports, require("jquery")) : "function" == typeof define && define.amd ? define(["exports", "jquery"], e) : e(((t = t || self).bootstrap = {}), t.jQuery);
})(this, function (t, p) {
	"use strict";
	function i(t, e) {
		for (var n = 0; n < e.length; n++) {
			var i = e[n];
			(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
		}
	}
	function s(t, e, n) {
		return e && i(t.prototype, e), n && i(t, n), t;
	}
	function l(o) {
		for (var t = 1; t < arguments.length; t++) {
			var r = null != arguments[t] ? arguments[t] : {},
				e = Object.keys(r);
			"function" == typeof Object.getOwnPropertySymbols &&
				(e = e.concat(
					Object.getOwnPropertySymbols(r).filter(function (t) {
						return Object.getOwnPropertyDescriptor(r, t).enumerable;
					})
				)),
				e.forEach(function (t) {
					var e, n, i;
					(e = o), (i = r[(n = t)]), n in e ? Object.defineProperty(e, n, { value: i, enumerable: !0, configurable: !0, writable: !0 }) : (e[n] = i);
				});
		}
		return o;
	}
	p = p && p.hasOwnProperty("default") ? p.default : p;
	var e = "transitionend";
	function n(t) {
		var e = this,
			n = !1;
		return (
			p(this).one(m.TRANSITION_END, function () {
				n = !0;
			}),
			setTimeout(function () {
				n || m.triggerTransitionEnd(e);
			}, t),
			this
		);
	}
	var m = {
		TRANSITION_END: "bsTransitionEnd",
		getUID: function (t) {
			for (; (t += ~~(1e6 * Math.random())), document.getElementById(t); );
			return t;
		},
		getSelectorFromElement: function (t) {
			var e = t.getAttribute("data-target");
			if (!e || "#" === e) {
				var n = t.getAttribute("href");
				e = n && "#" !== n ? n.trim() : "";
			}
			try {
				return document.querySelector(e) ? e : null;
			} catch (t) {
				return null;
			}
		},
		getTransitionDurationFromElement: function (t) {
			if (!t) return 0;
			var e = p(t).css("transition-duration"),
				n = p(t).css("transition-delay"),
				i = parseFloat(e),
				o = parseFloat(n);
			return i || o ? ((e = e.split(",")[0]), (n = n.split(",")[0]), 1e3 * (parseFloat(e) + parseFloat(n))) : 0;
		},
		reflow: function (t) {
			return t.offsetHeight;
		},
		triggerTransitionEnd: function (t) {
			p(t).trigger(e);
		},
		supportsTransitionEnd: function () {
			return Boolean(e);
		},
		isElement: function (t) {
			return (t[0] || t).nodeType;
		},
		typeCheckConfig: function (t, e, n) {
			for (var i in n)
				if (Object.prototype.hasOwnProperty.call(n, i)) {
					var o = n[i],
						r = e[i],
						s =
							r && m.isElement(r)
								? "element"
								: ((a = r),
								  {}.toString
										.call(a)
										.match(/\s([a-z]+)/i)[1]
										.toLowerCase());
					if (!new RegExp(o).test(s)) throw new Error(t.toUpperCase() + ': Option "' + i + '" provided type "' + s + '" but expected type "' + o + '".');
				}
			var a;
		},
		findShadowRoot: function (t) {
			if (!document.documentElement.attachShadow) return null;
			if ("function" != typeof t.getRootNode) return t instanceof ShadowRoot ? t : t.parentNode ? m.findShadowRoot(t.parentNode) : null;
			var e = t.getRootNode();
			return e instanceof ShadowRoot ? e : null;
		},
	};
	(p.fn.emulateTransitionEnd = n),
		(p.event.special[m.TRANSITION_END] = {
			bindType: e,
			delegateType: e,
			handle: function (t) {
				if (p(t.target).is(this)) return t.handleObj.handler.apply(this, arguments);
			},
		});
	var o = "alert",
		r = "bs.alert",
		a = "." + r,
		c = p.fn[o],
		h = { CLOSE: "close" + a, CLOSED: "closed" + a, CLICK_DATA_API: "click" + a + ".data-api" },
		u = "alert",
		f = "fade",
		d = "show",
		g = (function () {
			function i(t) {
				this._element = t;
			}
			var t = i.prototype;
			return (
				(t.close = function (t) {
					var e = this._element;
					t && (e = this._getRootElement(t)), this._triggerCloseEvent(e).isDefaultPrevented() || this._removeElement(e);
				}),
				(t.dispose = function () {
					p.removeData(this._element, r), (this._element = null);
				}),
				(t._getRootElement = function (t) {
					var e = m.getSelectorFromElement(t),
						n = !1;
					return e && (n = document.querySelector(e)), n || (n = p(t).closest("." + u)[0]), n;
				}),
				(t._triggerCloseEvent = function (t) {
					var e = p.Event(h.CLOSE);
					return p(t).trigger(e), e;
				}),
				(t._removeElement = function (e) {
					var n = this;
					if ((p(e).removeClass(d), p(e).hasClass(f))) {
						var t = m.getTransitionDurationFromElement(e);
						p(e)
							.one(m.TRANSITION_END, function (t) {
								return n._destroyElement(e, t);
							})
							.emulateTransitionEnd(t);
					} else this._destroyElement(e);
				}),
				(t._destroyElement = function (t) {
					p(t).detach().trigger(h.CLOSED).remove();
				}),
				(i._jQueryInterface = function (n) {
					return this.each(function () {
						var t = p(this),
							e = t.data(r);
						e || ((e = new i(this)), t.data(r, e)), "close" === n && e[n](this);
					});
				}),
				(i._handleDismiss = function (e) {
					return function (t) {
						t && t.preventDefault(), e.close(this);
					};
				}),
				s(i, null, [
					{
						key: "VERSION",
						get: function () {
							return "4.3.1";
						},
					},
				]),
				i
			);
		})();
	p(document).on(h.CLICK_DATA_API, '[data-dismiss="alert"]', g._handleDismiss(new g())),
		(p.fn[o] = g._jQueryInterface),
		(p.fn[o].Constructor = g),
		(p.fn[o].noConflict = function () {
			return (p.fn[o] = c), g._jQueryInterface;
		});
	var _ = "button",
		v = "bs.button",
		y = "." + v,
		E = ".data-api",
		b = p.fn[_],
		w = "active",
		C = "btn",
		T = "focus",
		S = '[data-toggle^="button"]',
		D = '[data-toggle="buttons"]',
		I = 'input:not([type="hidden"])',
		A = ".active",
		O = ".btn",
		N = { CLICK_DATA_API: "click" + y + E, FOCUS_BLUR_DATA_API: "focus" + y + E + " blur" + y + E },
		k = (function () {
			function n(t) {
				this._element = t;
			}
			var t = n.prototype;
			return (
				(t.toggle = function () {
					var t = !0,
						e = !0,
						n = p(this._element).closest(D)[0];
					if (n) {
						var i = this._element.querySelector(I);
						if (i) {
							if ("radio" === i.type)
								if (i.checked && this._element.classList.contains(w)) t = !1;
								else {
									var o = n.querySelector(A);
									o && p(o).removeClass(w);
								}
							if (t) {
								if (i.hasAttribute("disabled") || n.hasAttribute("disabled") || i.classList.contains("disabled") || n.classList.contains("disabled")) return;
								(i.checked = !this._element.classList.contains(w)), p(i).trigger("change");
							}
							i.focus(), (e = !1);
						}
					}
					e && this._element.setAttribute("aria-pressed", !this._element.classList.contains(w)), t && p(this._element).toggleClass(w);
				}),
				(t.dispose = function () {
					p.removeData(this._element, v), (this._element = null);
				}),
				(n._jQueryInterface = function (e) {
					return this.each(function () {
						var t = p(this).data(v);
						t || ((t = new n(this)), p(this).data(v, t)), "toggle" === e && t[e]();
					});
				}),
				s(n, null, [
					{
						key: "VERSION",
						get: function () {
							return "4.3.1";
						},
					},
				]),
				n
			);
		})();
	p(document)
		.on(N.CLICK_DATA_API, S, function (t) {
			t.preventDefault();
			var e = t.target;
			p(e).hasClass(C) || (e = p(e).closest(O)), k._jQueryInterface.call(p(e), "toggle");
		})
		.on(N.FOCUS_BLUR_DATA_API, S, function (t) {
			var e = p(t.target).closest(O)[0];
			p(e).toggleClass(T, /^focus(in)?$/.test(t.type));
		}),
		(p.fn[_] = k._jQueryInterface),
		(p.fn[_].Constructor = k),
		(p.fn[_].noConflict = function () {
			return (p.fn[_] = b), k._jQueryInterface;
		});
	var L = "carousel",
		x = "bs.carousel",
		P = "." + x,
		H = ".data-api",
		j = p.fn[L],
		R = { interval: 5e3, keyboard: !0, slide: !1, pause: "hover", wrap: !0, touch: !0 },
		F = { interval: "(number|boolean)", keyboard: "boolean", slide: "(boolean|string)", pause: "(string|boolean)", wrap: "boolean", touch: "boolean" },
		M = "next",
		W = "prev",
		U = "left",
		B = "right",
		q = { SLIDE: "slide" + P, SLID: "slid" + P, KEYDOWN: "keydown" + P, MOUSEENTER: "mouseenter" + P, MOUSELEAVE: "mouseleave" + P, TOUCHSTART: "touchstart" + P, TOUCHMOVE: "touchmove" + P, TOUCHEND: "touchend" + P, POINTERDOWN: "pointerdown" + P, POINTERUP: "pointerup" + P, DRAG_START: "dragstart" + P, LOAD_DATA_API: "load" + P + H, CLICK_DATA_API: "click" + P + H },
		K = "carousel",
		Q = "active",
		V = "slide",
		Y = "carousel-item-right",
		z = "carousel-item-left",
		X = "carousel-item-next",
		G = "carousel-item-prev",
		$ = "pointer-event",
		J = ".active",
		Z = ".active.carousel-item",
		tt = ".carousel-item",
		et = ".carousel-item img",
		nt = ".carousel-item-next, .carousel-item-prev",
		it = ".carousel-indicators",
		ot = "[data-slide], [data-slide-to]",
		rt = '[data-ride="carousel"]',
		st = { TOUCH: "touch", PEN: "pen" },
		at = (function () {
			function r(t, e) {
				(this._items = null), (this._interval = null), (this._activeElement = null), (this._isPaused = !1), (this._isSliding = !1), (this.touchTimeout = null), (this.touchStartX = 0), (this.touchDeltaX = 0), (this._config = this._getConfig(e)), (this._element = t), (this._indicatorsElement = this._element.querySelector(it)), (this._touchSupported = "ontouchstart" in document.documentElement || 0 < navigator.maxTouchPoints), (this._pointerEvent = Boolean(window.PointerEvent || window.MSPointerEvent)), this._addEventListeners();
			}
			var t = r.prototype;
			return (
				(t.next = function () {
					this._isSliding || this._slide(M);
				}),
				(t.nextWhenVisible = function () {
					!document.hidden && p(this._element).is(":visible") && "hidden" !== p(this._element).css("visibility") && this.next();
				}),
				(t.prev = function () {
					this._isSliding || this._slide(W);
				}),
				(t.pause = function (t) {
					t || (this._isPaused = !0), this._element.querySelector(nt) && (m.triggerTransitionEnd(this._element), this.cycle(!0)), clearInterval(this._interval), (this._interval = null);
				}),
				(t.cycle = function (t) {
					t || (this._isPaused = !1), this._interval && (clearInterval(this._interval), (this._interval = null)), this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval));
				}),
				(t.to = function (t) {
					var e = this;
					this._activeElement = this._element.querySelector(Z);
					var n = this._getItemIndex(this._activeElement);
					if (!(t > this._items.length - 1 || t < 0))
						if (this._isSliding)
							p(this._element).one(q.SLID, function () {
								return e.to(t);
							});
						else {
							if (n === t) return this.pause(), void this.cycle();
							var i = n < t ? M : W;
							this._slide(i, this._items[t]);
						}
				}),
				(t.dispose = function () {
					p(this._element).off(P), p.removeData(this._element, x), (this._items = null), (this._config = null), (this._element = null), (this._interval = null), (this._isPaused = null), (this._isSliding = null), (this._activeElement = null), (this._indicatorsElement = null);
				}),
				(t._getConfig = function (t) {
					return (t = l({}, R, t)), m.typeCheckConfig(L, t, F), t;
				}),
				(t._handleSwipe = function () {
					var t = Math.abs(this.touchDeltaX);
					if (!(t <= 40)) {
						var e = t / this.touchDeltaX;
						0 < e && this.prev(), e < 0 && this.next();
					}
				}),
				(t._addEventListeners = function () {
					var e = this;
					this._config.keyboard &&
						p(this._element).on(q.KEYDOWN, function (t) {
							return e._keydown(t);
						}),
						"hover" === this._config.pause &&
							p(this._element)
								.on(q.MOUSEENTER, function (t) {
									return e.pause(t);
								})
								.on(q.MOUSELEAVE, function (t) {
									return e.cycle(t);
								}),
						this._config.touch && this._addTouchEventListeners();
				}),
				(t._addTouchEventListeners = function () {
					var n = this;
					if (this._touchSupported) {
						var e = function (t) {
								n._pointerEvent && st[t.originalEvent.pointerType.toUpperCase()] ? (n.touchStartX = t.originalEvent.clientX) : n._pointerEvent || (n.touchStartX = t.originalEvent.touches[0].clientX);
							},
							i = function (t) {
								n._pointerEvent && st[t.originalEvent.pointerType.toUpperCase()] && (n.touchDeltaX = t.originalEvent.clientX - n.touchStartX),
									n._handleSwipe(),
									"hover" === n._config.pause &&
										(n.pause(),
										n.touchTimeout && clearTimeout(n.touchTimeout),
										(n.touchTimeout = setTimeout(function (t) {
											return n.cycle(t);
										}, 500 + n._config.interval)));
							};
						p(this._element.querySelectorAll(et)).on(q.DRAG_START, function (t) {
							return t.preventDefault();
						}),
							this._pointerEvent
								? (p(this._element).on(q.POINTERDOWN, function (t) {
										return e(t);
								  }),
								  p(this._element).on(q.POINTERUP, function (t) {
										return i(t);
								  }),
								  this._element.classList.add($))
								: (p(this._element).on(q.TOUCHSTART, function (t) {
										return e(t);
								  }),
								  p(this._element).on(q.TOUCHMOVE, function (t) {
										var e;
										(e = t).originalEvent.touches && 1 < e.originalEvent.touches.length ? (n.touchDeltaX = 0) : (n.touchDeltaX = e.originalEvent.touches[0].clientX - n.touchStartX);
								  }),
								  p(this._element).on(q.TOUCHEND, function (t) {
										return i(t);
								  }));
					}
				}),
				(t._keydown = function (t) {
					if (!/input|textarea/i.test(t.target.tagName))
						switch (t.which) {
							case 37:
								t.preventDefault(), this.prev();
								break;
							case 39:
								t.preventDefault(), this.next();
						}
				}),
				(t._getItemIndex = function (t) {
					return (this._items = t && t.parentNode ? [].slice.call(t.parentNode.querySelectorAll(tt)) : []), this._items.indexOf(t);
				}),
				(t._getItemByDirection = function (t, e) {
					var n = t === M,
						i = t === W,
						o = this._getItemIndex(e),
						r = this._items.length - 1;
					if (((i && 0 === o) || (n && o === r)) && !this._config.wrap) return e;
					var s = (o + (t === W ? -1 : 1)) % this._items.length;
					return -1 === s ? this._items[this._items.length - 1] : this._items[s];
				}),
				(t._triggerSlideEvent = function (t, e) {
					var n = this._getItemIndex(t),
						i = this._getItemIndex(this._element.querySelector(Z)),
						o = p.Event(q.SLIDE, { relatedTarget: t, direction: e, from: i, to: n });
					return p(this._element).trigger(o), o;
				}),
				(t._setActiveIndicatorElement = function (t) {
					if (this._indicatorsElement) {
						var e = [].slice.call(this._indicatorsElement.querySelectorAll(J));
						p(e).removeClass(Q);
						var n = this._indicatorsElement.children[this._getItemIndex(t)];
						n && p(n).addClass(Q);
					}
				}),
				(t._slide = function (t, e) {
					var n,
						i,
						o,
						r = this,
						s = this._element.querySelector(Z),
						a = this._getItemIndex(s),
						l = e || (s && this._getItemByDirection(t, s)),
						c = this._getItemIndex(l),
						h = Boolean(this._interval);
					if (((o = t === M ? ((n = z), (i = X), U) : ((n = Y), (i = G), B)), l && p(l).hasClass(Q))) this._isSliding = !1;
					else if (!this._triggerSlideEvent(l, o).isDefaultPrevented() && s && l) {
						(this._isSliding = !0), h && this.pause(), this._setActiveIndicatorElement(l);
						var u = p.Event(q.SLID, { relatedTarget: l, direction: o, from: a, to: c });
						if (p(this._element).hasClass(V)) {
							p(l).addClass(i), m.reflow(l), p(s).addClass(n), p(l).addClass(n);
							var f = parseInt(l.getAttribute("data-interval"), 10);
							this._config.interval = f ? ((this._config.defaultInterval = this._config.defaultInterval || this._config.interval), f) : this._config.defaultInterval || this._config.interval;
							var d = m.getTransitionDurationFromElement(s);
							p(s)
								.one(m.TRANSITION_END, function () {
									p(l)
										.removeClass(n + " " + i)
										.addClass(Q),
										p(s).removeClass(Q + " " + i + " " + n),
										(r._isSliding = !1),
										setTimeout(function () {
											return p(r._element).trigger(u);
										}, 0);
								})
								.emulateTransitionEnd(d);
						} else p(s).removeClass(Q), p(l).addClass(Q), (this._isSliding = !1), p(this._element).trigger(u);
						h && this.cycle();
					}
				}),
				(r._jQueryInterface = function (i) {
					return this.each(function () {
						var t = p(this).data(x),
							e = l({}, R, p(this).data());
						"object" == typeof i && (e = l({}, e, i));
						var n = "string" == typeof i ? i : e.slide;
						if ((t || ((t = new r(this, e)), p(this).data(x, t)), "number" == typeof i)) t.to(i);
						else if ("string" == typeof n) {
							if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');
							t[n]();
						} else e.interval && e.ride && (t.pause(), t.cycle());
					});
				}),
				(r._dataApiClickHandler = function (t) {
					var e = m.getSelectorFromElement(this);
					if (e) {
						var n = p(e)[0];
						if (n && p(n).hasClass(K)) {
							var i = l({}, p(n).data(), p(this).data()),
								o = this.getAttribute("data-slide-to");
							o && (i.interval = !1), r._jQueryInterface.call(p(n), i), o && p(n).data(x).to(o), t.preventDefault();
						}
					}
				}),
				s(r, null, [
					{
						key: "VERSION",
						get: function () {
							return "4.3.1";
						},
					},
					{
						key: "Default",
						get: function () {
							return R;
						},
					},
				]),
				r
			);
		})();
	p(document).on(q.CLICK_DATA_API, ot, at._dataApiClickHandler),
		p(window).on(q.LOAD_DATA_API, function () {
			for (var t = [].slice.call(document.querySelectorAll(rt)), e = 0, n = t.length; e < n; e++) {
				var i = p(t[e]);
				at._jQueryInterface.call(i, i.data());
			}
		}),
		(p.fn[L] = at._jQueryInterface),
		(p.fn[L].Constructor = at),
		(p.fn[L].noConflict = function () {
			return (p.fn[L] = j), at._jQueryInterface;
		});
	var lt = "collapse",
		ct = "bs.collapse",
		ht = "." + ct,
		ut = p.fn[lt],
		ft = { toggle: !0, parent: "" },
		dt = { toggle: "boolean", parent: "(string|element)" },
		pt = { SHOW: "show" + ht, SHOWN: "shown" + ht, HIDE: "hide" + ht, HIDDEN: "hidden" + ht, CLICK_DATA_API: "click" + ht + ".data-api" },
		mt = "show",
		gt = "collapse",
		_t = "collapsing",
		vt = "collapsed",
		yt = "width",
		Et = "height",
		bt = ".show, .collapsing",
		wt = '[data-toggle="collapse"]',
		Ct = (function () {
			function a(e, t) {
				(this._isTransitioning = !1), (this._element = e), (this._config = this._getConfig(t)), (this._triggerArray = [].slice.call(document.querySelectorAll('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]')));
				for (var n = [].slice.call(document.querySelectorAll(wt)), i = 0, o = n.length; i < o; i++) {
					var r = n[i],
						s = m.getSelectorFromElement(r),
						a = [].slice.call(document.querySelectorAll(s)).filter(function (t) {
							return t === e;
						});
					null !== s && 0 < a.length && ((this._selector = s), this._triggerArray.push(r));
				}
				(this._parent = this._config.parent ? this._getParent() : null), this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray), this._config.toggle && this.toggle();
			}
			var t = a.prototype;
			return (
				(t.toggle = function () {
					p(this._element).hasClass(mt) ? this.hide() : this.show();
				}),
				(t.show = function () {
					var t,
						e,
						n = this;
					if (
						!this._isTransitioning &&
						!p(this._element).hasClass(mt) &&
						(this._parent &&
							0 ===
								(t = [].slice.call(this._parent.querySelectorAll(bt)).filter(function (t) {
									return "string" == typeof n._config.parent ? t.getAttribute("data-parent") === n._config.parent : t.classList.contains(gt);
								})).length &&
							(t = null),
						!(t && (e = p(t).not(this._selector).data(ct)) && e._isTransitioning))
					) {
						var i = p.Event(pt.SHOW);
						if ((p(this._element).trigger(i), !i.isDefaultPrevented())) {
							t && (a._jQueryInterface.call(p(t).not(this._selector), "hide"), e || p(t).data(ct, null));
							var o = this._getDimension();
							p(this._element).removeClass(gt).addClass(_t), (this._element.style[o] = 0), this._triggerArray.length && p(this._triggerArray).removeClass(vt).attr("aria-expanded", !0), this.setTransitioning(!0);
							var r = "scroll" + (o[0].toUpperCase() + o.slice(1)),
								s = m.getTransitionDurationFromElement(this._element);
							p(this._element)
								.one(m.TRANSITION_END, function () {
									p(n._element).removeClass(_t).addClass(gt).addClass(mt), (n._element.style[o] = ""), n.setTransitioning(!1), p(n._element).trigger(pt.SHOWN);
								})
								.emulateTransitionEnd(s),
								(this._element.style[o] = this._element[r] + "px");
						}
					}
				}),
				(t.hide = function () {
					var t = this;
					if (!this._isTransitioning && p(this._element).hasClass(mt)) {
						var e = p.Event(pt.HIDE);
						if ((p(this._element).trigger(e), !e.isDefaultPrevented())) {
							var n = this._getDimension();
							(this._element.style[n] = this._element.getBoundingClientRect()[n] + "px"), m.reflow(this._element), p(this._element).addClass(_t).removeClass(gt).removeClass(mt);
							var i = this._triggerArray.length;
							if (0 < i)
								for (var o = 0; o < i; o++) {
									var r = this._triggerArray[o],
										s = m.getSelectorFromElement(r);
									if (null !== s) p([].slice.call(document.querySelectorAll(s))).hasClass(mt) || p(r).addClass(vt).attr("aria-expanded", !1);
								}
							this.setTransitioning(!0);
							this._element.style[n] = "";
							var a = m.getTransitionDurationFromElement(this._element);
							p(this._element)
								.one(m.TRANSITION_END, function () {
									t.setTransitioning(!1), p(t._element).removeClass(_t).addClass(gt).trigger(pt.HIDDEN);
								})
								.emulateTransitionEnd(a);
						}
					}
				}),
				(t.setTransitioning = function (t) {
					this._isTransitioning = t;
				}),
				(t.dispose = function () {
					p.removeData(this._element, ct), (this._config = null), (this._parent = null), (this._element = null), (this._triggerArray = null), (this._isTransitioning = null);
				}),
				(t._getConfig = function (t) {
					return ((t = l({}, ft, t)).toggle = Boolean(t.toggle)), m.typeCheckConfig(lt, t, dt), t;
				}),
				(t._getDimension = function () {
					return p(this._element).hasClass(yt) ? yt : Et;
				}),
				(t._getParent = function () {
					var t,
						n = this;
					m.isElement(this._config.parent) ? ((t = this._config.parent), "undefined" != typeof this._config.parent.jquery && (t = this._config.parent[0])) : (t = document.querySelector(this._config.parent));
					var e = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]',
						i = [].slice.call(t.querySelectorAll(e));
					return (
						p(i).each(function (t, e) {
							n._addAriaAndCollapsedClass(a._getTargetFromElement(e), [e]);
						}),
						t
					);
				}),
				(t._addAriaAndCollapsedClass = function (t, e) {
					var n = p(t).hasClass(mt);
					e.length && p(e).toggleClass(vt, !n).attr("aria-expanded", n);
				}),
				(a._getTargetFromElement = function (t) {
					var e = m.getSelectorFromElement(t);
					return e ? document.querySelector(e) : null;
				}),
				(a._jQueryInterface = function (i) {
					return this.each(function () {
						var t = p(this),
							e = t.data(ct),
							n = l({}, ft, t.data(), "object" == typeof i && i ? i : {});
						if ((!e && n.toggle && /show|hide/.test(i) && (n.toggle = !1), e || ((e = new a(this, n)), t.data(ct, e)), "string" == typeof i)) {
							if ("undefined" == typeof e[i]) throw new TypeError('No method named "' + i + '"');
							e[i]();
						}
					});
				}),
				s(a, null, [
					{
						key: "VERSION",
						get: function () {
							return "4.3.1";
						},
					},
					{
						key: "Default",
						get: function () {
							return ft;
						},
					},
				]),
				a
			);
		})();
	p(document).on(pt.CLICK_DATA_API, wt, function (t) {
		"A" === t.currentTarget.tagName && t.preventDefault();
		var n = p(this),
			e = m.getSelectorFromElement(this),
			i = [].slice.call(document.querySelectorAll(e));
		p(i).each(function () {
			var t = p(this),
				e = t.data(ct) ? "toggle" : n.data();
			Ct._jQueryInterface.call(t, e);
		});
	}),
		(p.fn[lt] = Ct._jQueryInterface),
		(p.fn[lt].Constructor = Ct),
		(p.fn[lt].noConflict = function () {
			return (p.fn[lt] = ut), Ct._jQueryInterface;
		});
	for (var Tt = "undefined" != typeof window && "undefined" != typeof document, St = ["Edge", "Trident", "Firefox"], Dt = 0, It = 0; It < St.length; It += 1)
		if (Tt && 0 <= navigator.userAgent.indexOf(St[It])) {
			Dt = 1;
			break;
		}
	var At =
		Tt && window.Promise
			? function (t) {
					var e = !1;
					return function () {
						e ||
							((e = !0),
							window.Promise.resolve().then(function () {
								(e = !1), t();
							}));
					};
			  }
			: function (t) {
					var e = !1;
					return function () {
						e ||
							((e = !0),
							setTimeout(function () {
								(e = !1), t();
							}, Dt));
					};
			  };
	function Ot(t) {
		return t && "[object Function]" === {}.toString.call(t);
	}
	function Nt(t, e) {
		if (1 !== t.nodeType) return [];
		var n = t.ownerDocument.defaultView.getComputedStyle(t, null);
		return e ? n[e] : n;
	}
	function kt(t) {
		return "HTML" === t.nodeName ? t : t.parentNode || t.host;
	}
	function Lt(t) {
		if (!t) return document.body;
		switch (t.nodeName) {
			case "HTML":
			case "BODY":
				return t.ownerDocument.body;
			case "#document":
				return t.body;
		}
		var e = Nt(t),
			n = e.overflow,
			i = e.overflowX,
			o = e.overflowY;
		return /(auto|scroll|overlay)/.test(n + o + i) ? t : Lt(kt(t));
	}
	var xt = Tt && !(!window.MSInputMethodContext || !document.documentMode),
		Pt = Tt && /MSIE 10/.test(navigator.userAgent);
	function Ht(t) {
		return 11 === t ? xt : 10 === t ? Pt : xt || Pt;
	}
	function jt(t) {
		if (!t) return document.documentElement;
		for (var e = Ht(10) ? document.body : null, n = t.offsetParent || null; n === e && t.nextElementSibling; ) n = (t = t.nextElementSibling).offsetParent;
		var i = n && n.nodeName;
		return i && "BODY" !== i && "HTML" !== i ? (-1 !== ["TH", "TD", "TABLE"].indexOf(n.nodeName) && "static" === Nt(n, "position") ? jt(n) : n) : t ? t.ownerDocument.documentElement : document.documentElement;
	}
	function Rt(t) {
		return null !== t.parentNode ? Rt(t.parentNode) : t;
	}
	function Ft(t, e) {
		if (!(t && t.nodeType && e && e.nodeType)) return document.documentElement;
		var n = t.compareDocumentPosition(e) & Node.DOCUMENT_POSITION_FOLLOWING,
			i = n ? t : e,
			o = n ? e : t,
			r = document.createRange();
		r.setStart(i, 0), r.setEnd(o, 0);
		var s,
			a,
			l = r.commonAncestorContainer;
		if ((t !== l && e !== l) || i.contains(o)) return "BODY" === (a = (s = l).nodeName) || ("HTML" !== a && jt(s.firstElementChild) !== s) ? jt(l) : l;
		var c = Rt(t);
		return c.host ? Ft(c.host, e) : Ft(t, Rt(e).host);
	}
	function Mt(t) {
		var e = "top" === (1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : "top") ? "scrollTop" : "scrollLeft",
			n = t.nodeName;
		if ("BODY" !== n && "HTML" !== n) return t[e];
		var i = t.ownerDocument.documentElement;
		return (t.ownerDocument.scrollingElement || i)[e];
	}
	function Wt(t, e) {
		var n = "x" === e ? "Left" : "Top",
			i = "Left" === n ? "Right" : "Bottom";
		return parseFloat(t["border" + n + "Width"], 10) + parseFloat(t["border" + i + "Width"], 10);
	}
	function Ut(t, e, n, i) {
		return Math.max(e["offset" + t], e["scroll" + t], n["client" + t], n["offset" + t], n["scroll" + t], Ht(10) ? parseInt(n["offset" + t]) + parseInt(i["margin" + ("Height" === t ? "Top" : "Left")]) + parseInt(i["margin" + ("Height" === t ? "Bottom" : "Right")]) : 0);
	}
	function Bt(t) {
		var e = t.body,
			n = t.documentElement,
			i = Ht(10) && getComputedStyle(n);
		return { height: Ut("Height", e, n, i), width: Ut("Width", e, n, i) };
	}
	var qt = (function () {
			function i(t, e) {
				for (var n = 0; n < e.length; n++) {
					var i = e[n];
					(i.enumerable = i.enumerable || !1), (i.configurable = !0), "value" in i && (i.writable = !0), Object.defineProperty(t, i.key, i);
				}
			}
			return function (t, e, n) {
				return e && i(t.prototype, e), n && i(t, n), t;
			};
		})(),
		Kt = function (t, e, n) {
			return e in t ? Object.defineProperty(t, e, { value: n, enumerable: !0, configurable: !0, writable: !0 }) : (t[e] = n), t;
		},
		Qt =
			Object.assign ||
			function (t) {
				for (var e = 1; e < arguments.length; e++) {
					var n = arguments[e];
					for (var i in n) Object.prototype.hasOwnProperty.call(n, i) && (t[i] = n[i]);
				}
				return t;
			};
	function Vt(t) {
		return Qt({}, t, { right: t.left + t.width, bottom: t.top + t.height });
	}
	function Yt(t) {
		var e = {};
		try {
			if (Ht(10)) {
				e = t.getBoundingClientRect();
				var n = Mt(t, "top"),
					i = Mt(t, "left");
				(e.top += n), (e.left += i), (e.bottom += n), (e.right += i);
			} else e = t.getBoundingClientRect();
		} catch (t) {}
		var o = { left: e.left, top: e.top, width: e.right - e.left, height: e.bottom - e.top },
			r = "HTML" === t.nodeName ? Bt(t.ownerDocument) : {},
			s = r.width || t.clientWidth || o.right - o.left,
			a = r.height || t.clientHeight || o.bottom - o.top,
			l = t.offsetWidth - s,
			c = t.offsetHeight - a;
		if (l || c) {
			var h = Nt(t);
			(l -= Wt(h, "x")), (c -= Wt(h, "y")), (o.width -= l), (o.height -= c);
		}
		return Vt(o);
	}
	function zt(t, e) {
		var n = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
			i = Ht(10),
			o = "HTML" === e.nodeName,
			r = Yt(t),
			s = Yt(e),
			a = Lt(t),
			l = Nt(e),
			c = parseFloat(l.borderTopWidth, 10),
			h = parseFloat(l.borderLeftWidth, 10);
		n && o && ((s.top = Math.max(s.top, 0)), (s.left = Math.max(s.left, 0)));
		var u = Vt({ top: r.top - s.top - c, left: r.left - s.left - h, width: r.width, height: r.height });
		if (((u.marginTop = 0), (u.marginLeft = 0), !i && o)) {
			var f = parseFloat(l.marginTop, 10),
				d = parseFloat(l.marginLeft, 10);
			(u.top -= c - f), (u.bottom -= c - f), (u.left -= h - d), (u.right -= h - d), (u.marginTop = f), (u.marginLeft = d);
		}
		return (
			(i && !n ? e.contains(a) : e === a && "BODY" !== a.nodeName) &&
				(u = (function (t, e) {
					var n = 2 < arguments.length && void 0 !== arguments[2] && arguments[2],
						i = Mt(e, "top"),
						o = Mt(e, "left"),
						r = n ? -1 : 1;
					return (t.top += i * r), (t.bottom += i * r), (t.left += o * r), (t.right += o * r), t;
				})(u, e)),
			u
		);
	}
	function Xt(t) {
		if (!t || !t.parentElement || Ht()) return document.documentElement;
		for (var e = t.parentElement; e && "none" === Nt(e, "transform"); ) e = e.parentElement;
		return e || document.documentElement;
	}
	function Gt(t, e, n, i) {
		var o = 4 < arguments.length && void 0 !== arguments[4] && arguments[4],
			r = { top: 0, left: 0 },
			s = o ? Xt(t) : Ft(t, e);
		if ("viewport" === i)
			r = (function (t) {
				var e = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
					n = t.ownerDocument.documentElement,
					i = zt(t, n),
					o = Math.max(n.clientWidth, window.innerWidth || 0),
					r = Math.max(n.clientHeight, window.innerHeight || 0),
					s = e ? 0 : Mt(n),
					a = e ? 0 : Mt(n, "left");
				return Vt({ top: s - i.top + i.marginTop, left: a - i.left + i.marginLeft, width: o, height: r });
			})(s, o);
		else {
			var a = void 0;
			"scrollParent" === i ? "BODY" === (a = Lt(kt(e))).nodeName && (a = t.ownerDocument.documentElement) : (a = "window" === i ? t.ownerDocument.documentElement : i);
			var l = zt(a, s, o);
			if (
				"HTML" !== a.nodeName ||
				(function t(e) {
					var n = e.nodeName;
					if ("BODY" === n || "HTML" === n) return !1;
					if ("fixed" === Nt(e, "position")) return !0;
					var i = kt(e);
					return !!i && t(i);
				})(s)
			)
				r = l;
			else {
				var c = Bt(t.ownerDocument),
					h = c.height,
					u = c.width;
				(r.top += l.top - l.marginTop), (r.bottom = h + l.top), (r.left += l.left - l.marginLeft), (r.right = u + l.left);
			}
		}
		var f = "number" == typeof (n = n || 0);
		return (r.left += f ? n : n.left || 0), (r.top += f ? n : n.top || 0), (r.right -= f ? n : n.right || 0), (r.bottom -= f ? n : n.bottom || 0), r;
	}
	function $t(t, e, i, n, o) {
		var r = 5 < arguments.length && void 0 !== arguments[5] ? arguments[5] : 0;
		if (-1 === t.indexOf("auto")) return t;
		var s = Gt(i, n, r, o),
			a = { top: { width: s.width, height: e.top - s.top }, right: { width: s.right - e.right, height: s.height }, bottom: { width: s.width, height: s.bottom - e.bottom }, left: { width: e.left - s.left, height: s.height } },
			l = Object.keys(a)
				.map(function (t) {
					return Qt({ key: t }, a[t], { area: ((e = a[t]), e.width * e.height) });
					var e;
				})
				.sort(function (t, e) {
					return e.area - t.area;
				}),
			c = l.filter(function (t) {
				var e = t.width,
					n = t.height;
				return e >= i.clientWidth && n >= i.clientHeight;
			}),
			h = 0 < c.length ? c[0].key : l[0].key,
			u = t.split("-")[1];
		return h + (u ? "-" + u : "");
	}
	function Jt(t, e, n) {
		var i = 3 < arguments.length && void 0 !== arguments[3] ? arguments[3] : null;
		return zt(n, i ? Xt(e) : Ft(e, n), i);
	}
	function Zt(t) {
		var e = t.ownerDocument.defaultView.getComputedStyle(t),
			n = parseFloat(e.marginTop || 0) + parseFloat(e.marginBottom || 0),
			i = parseFloat(e.marginLeft || 0) + parseFloat(e.marginRight || 0);
		return { width: t.offsetWidth + i, height: t.offsetHeight + n };
	}
	function te(t) {
		var e = { left: "right", right: "left", bottom: "top", top: "bottom" };
		return t.replace(/left|right|bottom|top/g, function (t) {
			return e[t];
		});
	}
	function ee(t, e, n) {
		n = n.split("-")[0];
		var i = Zt(t),
			o = { width: i.width, height: i.height },
			r = -1 !== ["right", "left"].indexOf(n),
			s = r ? "top" : "left",
			a = r ? "left" : "top",
			l = r ? "height" : "width",
			c = r ? "width" : "height";
		return (o[s] = e[s] + e[l] / 2 - i[l] / 2), (o[a] = n === a ? e[a] - i[c] : e[te(a)]), o;
	}
	function ne(t, e) {
		return Array.prototype.find ? t.find(e) : t.filter(e)[0];
	}
	function ie(t, n, e) {
		return (
			(void 0 === e
				? t
				: t.slice(
						0,
						(function (t, e, n) {
							if (Array.prototype.findIndex)
								return t.findIndex(function (t) {
									return t[e] === n;
								});
							var i = ne(t, function (t) {
								return t[e] === n;
							});
							return t.indexOf(i);
						})(t, "name", e)
				  )
			).forEach(function (t) {
				t.function && console.warn("`modifier.function` is deprecated, use `modifier.fn`!");
				var e = t.function || t.fn;
				t.enabled && Ot(e) && ((n.offsets.popper = Vt(n.offsets.popper)), (n.offsets.reference = Vt(n.offsets.reference)), (n = e(n, t)));
			}),
			n
		);
	}
	function oe(t, n) {
		return t.some(function (t) {
			var e = t.name;
			return t.enabled && e === n;
		});
	}
	function re(t) {
		for (var e = [!1, "ms", "Webkit", "Moz", "O"], n = t.charAt(0).toUpperCase() + t.slice(1), i = 0; i < e.length; i++) {
			var o = e[i],
				r = o ? "" + o + n : t;
			if ("undefined" != typeof document.body.style[r]) return r;
		}
		return null;
	}
	function se(t) {
		var e = t.ownerDocument;
		return e ? e.defaultView : window;
	}
	function ae(t, e, n, i) {
		(n.updateBound = i), se(t).addEventListener("resize", n.updateBound, { passive: !0 });
		var o = Lt(t);
		return (
			(function t(e, n, i, o) {
				var r = "BODY" === e.nodeName,
					s = r ? e.ownerDocument.defaultView : e;
				s.addEventListener(n, i, { passive: !0 }), r || t(Lt(s.parentNode), n, i, o), o.push(s);
			})(o, "scroll", n.updateBound, n.scrollParents),
			(n.scrollElement = o),
			(n.eventsEnabled = !0),
			n
		);
	}
	function le() {
		var t, e;
		this.state.eventsEnabled &&
			(cancelAnimationFrame(this.scheduleUpdate),
			(this.state =
				((t = this.reference),
				(e = this.state),
				se(t).removeEventListener("resize", e.updateBound),
				e.scrollParents.forEach(function (t) {
					t.removeEventListener("scroll", e.updateBound);
				}),
				(e.updateBound = null),
				(e.scrollParents = []),
				(e.scrollElement = null),
				(e.eventsEnabled = !1),
				e)));
	}
	function ce(t) {
		return "" !== t && !isNaN(parseFloat(t)) && isFinite(t);
	}
	function he(n, i) {
		Object.keys(i).forEach(function (t) {
			var e = "";
			-1 !== ["width", "height", "top", "right", "bottom", "left"].indexOf(t) && ce(i[t]) && (e = "px"), (n.style[t] = i[t] + e);
		});
	}
	var ue = Tt && /Firefox/i.test(navigator.userAgent);
	function fe(t, e, n) {
		var i = ne(t, function (t) {
				return t.name === e;
			}),
			o =
				!!i &&
				t.some(function (t) {
					return t.name === n && t.enabled && t.order < i.order;
				});
		if (!o) {
			var r = "`" + e + "`",
				s = "`" + n + "`";
			console.warn(s + " modifier is required by " + r + " modifier in order to work, be sure to include it before " + r + "!");
		}
		return o;
	}
	var de = ["auto-start", "auto", "auto-end", "top-start", "top", "top-end", "right-start", "right", "right-end", "bottom-end", "bottom", "bottom-start", "left-end", "left", "left-start"],
		pe = de.slice(3);
	function me(t) {
		var e = 1 < arguments.length && void 0 !== arguments[1] && arguments[1],
			n = pe.indexOf(t),
			i = pe.slice(n + 1).concat(pe.slice(0, n));
		return e ? i.reverse() : i;
	}
	var ge = "flip",
		_e = "clockwise",
		ve = "counterclockwise";
	function ye(t, o, r, e) {
		var s = [0, 0],
			a = -1 !== ["right", "left"].indexOf(e),
			n = t.split(/(\+|\-)/).map(function (t) {
				return t.trim();
			}),
			i = n.indexOf(
				ne(n, function (t) {
					return -1 !== t.search(/,|\s/);
				})
			);
		n[i] && -1 === n[i].indexOf(",") && console.warn("Offsets separated by white space(s) are deprecated, use a comma (,) instead.");
		var l = /\s*,\s*|\s+/,
			c = -1 !== i ? [n.slice(0, i).concat([n[i].split(l)[0]]), [n[i].split(l)[1]].concat(n.slice(i + 1))] : [n];
		return (
			(c = c.map(function (t, e) {
				var n = (1 === e ? !a : a) ? "height" : "width",
					i = !1;
				return t
					.reduce(function (t, e) {
						return "" === t[t.length - 1] && -1 !== ["+", "-"].indexOf(e) ? ((t[t.length - 1] = e), (i = !0), t) : i ? ((t[t.length - 1] += e), (i = !1), t) : t.concat(e);
					}, [])
					.map(function (t) {
						return (function (t, e, n, i) {
							var o = t.match(/((?:\-|\+)?\d*\.?\d*)(.*)/),
								r = +o[1],
								s = o[2];
							if (!r) return t;
							if (0 !== s.indexOf("%")) return "vh" !== s && "vw" !== s ? r : (("vh" === s ? Math.max(document.documentElement.clientHeight, window.innerHeight || 0) : Math.max(document.documentElement.clientWidth, window.innerWidth || 0)) / 100) * r;
							var a = void 0;
							switch (s) {
								case "%p":
									a = n;
									break;
								case "%":
								case "%r":
								default:
									a = i;
							}
							return (Vt(a)[e] / 100) * r;
						})(t, n, o, r);
					});
			})).forEach(function (n, i) {
				n.forEach(function (t, e) {
					ce(t) && (s[i] += t * ("-" === n[e - 1] ? -1 : 1));
				});
			}),
			s
		);
	}
	var Ee = {
			placement: "bottom",
			positionFixed: !1,
			eventsEnabled: !0,
			removeOnDestroy: !1,
			onCreate: function () {},
			onUpdate: function () {},
			modifiers: {
				shift: {
					order: 100,
					enabled: !0,
					fn: function (t) {
						var e = t.placement,
							n = e.split("-")[0],
							i = e.split("-")[1];
						if (i) {
							var o = t.offsets,
								r = o.reference,
								s = o.popper,
								a = -1 !== ["bottom", "top"].indexOf(n),
								l = a ? "left" : "top",
								c = a ? "width" : "height",
								h = { start: Kt({}, l, r[l]), end: Kt({}, l, r[l] + r[c] - s[c]) };
							t.offsets.popper = Qt({}, s, h[i]);
						}
						return t;
					},
				},
				offset: {
					order: 200,
					enabled: !0,
					fn: function (t, e) {
						var n = e.offset,
							i = t.placement,
							o = t.offsets,
							r = o.popper,
							s = o.reference,
							a = i.split("-")[0],
							l = void 0;
						return (l = ce(+n) ? [+n, 0] : ye(n, r, s, a)), "left" === a ? ((r.top += l[0]), (r.left -= l[1])) : "right" === a ? ((r.top += l[0]), (r.left += l[1])) : "top" === a ? ((r.left += l[0]), (r.top -= l[1])) : "bottom" === a && ((r.left += l[0]), (r.top += l[1])), (t.popper = r), t;
					},
					offset: 0,
				},
				preventOverflow: {
					order: 300,
					enabled: !0,
					fn: function (t, i) {
						var e = i.boundariesElement || jt(t.instance.popper);
						t.instance.reference === e && (e = jt(e));
						var n = re("transform"),
							o = t.instance.popper.style,
							r = o.top,
							s = o.left,
							a = o[n];
						(o.top = ""), (o.left = ""), (o[n] = "");
						var l = Gt(t.instance.popper, t.instance.reference, i.padding, e, t.positionFixed);
						(o.top = r), (o.left = s), (o[n] = a), (i.boundaries = l);
						var c = i.priority,
							h = t.offsets.popper,
							u = {
								primary: function (t) {
									var e = h[t];
									return h[t] < l[t] && !i.escapeWithReference && (e = Math.max(h[t], l[t])), Kt({}, t, e);
								},
								secondary: function (t) {
									var e = "right" === t ? "left" : "top",
										n = h[e];
									return h[t] > l[t] && !i.escapeWithReference && (n = Math.min(h[e], l[t] - ("right" === t ? h.width : h.height))), Kt({}, e, n);
								},
							};
						return (
							c.forEach(function (t) {
								var e = -1 !== ["left", "top"].indexOf(t) ? "primary" : "secondary";
								h = Qt({}, h, u[e](t));
							}),
							(t.offsets.popper = h),
							t
						);
					},
					priority: ["left", "right", "top", "bottom"],
					padding: 5,
					boundariesElement: "scrollParent",
				},
				keepTogether: {
					order: 400,
					enabled: !0,
					fn: function (t) {
						var e = t.offsets,
							n = e.popper,
							i = e.reference,
							o = t.placement.split("-")[0],
							r = Math.floor,
							s = -1 !== ["top", "bottom"].indexOf(o),
							a = s ? "right" : "bottom",
							l = s ? "left" : "top",
							c = s ? "width" : "height";
						return n[a] < r(i[l]) && (t.offsets.popper[l] = r(i[l]) - n[c]), n[l] > r(i[a]) && (t.offsets.popper[l] = r(i[a])), t;
					},
				},
				arrow: {
					order: 500,
					enabled: !0,
					fn: function (t, e) {
						var n;
						if (!fe(t.instance.modifiers, "arrow", "keepTogether")) return t;
						var i = e.element;
						if ("string" == typeof i) {
							if (!(i = t.instance.popper.querySelector(i))) return t;
						} else if (!t.instance.popper.contains(i)) return console.warn("WARNING: `arrow.element` must be child of its popper element!"), t;
						var o = t.placement.split("-")[0],
							r = t.offsets,
							s = r.popper,
							a = r.reference,
							l = -1 !== ["left", "right"].indexOf(o),
							c = l ? "height" : "width",
							h = l ? "Top" : "Left",
							u = h.toLowerCase(),
							f = l ? "left" : "top",
							d = l ? "bottom" : "right",
							p = Zt(i)[c];
						a[d] - p < s[u] && (t.offsets.popper[u] -= s[u] - (a[d] - p)), a[u] + p > s[d] && (t.offsets.popper[u] += a[u] + p - s[d]), (t.offsets.popper = Vt(t.offsets.popper));
						var m = a[u] + a[c] / 2 - p / 2,
							g = Nt(t.instance.popper),
							_ = parseFloat(g["margin" + h], 10),
							v = parseFloat(g["border" + h + "Width"], 10),
							y = m - t.offsets.popper[u] - _ - v;
						return (y = Math.max(Math.min(s[c] - p, y), 0)), (t.arrowElement = i), (t.offsets.arrow = (Kt((n = {}), u, Math.round(y)), Kt(n, f, ""), n)), t;
					},
					element: "[x-arrow]",
				},
				flip: {
					order: 600,
					enabled: !0,
					fn: function (p, m) {
						if (oe(p.instance.modifiers, "inner")) return p;
						if (p.flipped && p.placement === p.originalPlacement) return p;
						var g = Gt(p.instance.popper, p.instance.reference, m.padding, m.boundariesElement, p.positionFixed),
							_ = p.placement.split("-")[0],
							v = te(_),
							y = p.placement.split("-")[1] || "",
							E = [];
						switch (m.behavior) {
							case ge:
								E = [_, v];
								break;
							case _e:
								E = me(_);
								break;
							case ve:
								E = me(_, !0);
								break;
							default:
								E = m.behavior;
						}
						return (
							E.forEach(function (t, e) {
								if (_ !== t || E.length === e + 1) return p;
								(_ = p.placement.split("-")[0]), (v = te(_));
								var n,
									i = p.offsets.popper,
									o = p.offsets.reference,
									r = Math.floor,
									s = ("left" === _ && r(i.right) > r(o.left)) || ("right" === _ && r(i.left) < r(o.right)) || ("top" === _ && r(i.bottom) > r(o.top)) || ("bottom" === _ && r(i.top) < r(o.bottom)),
									a = r(i.left) < r(g.left),
									l = r(i.right) > r(g.right),
									c = r(i.top) < r(g.top),
									h = r(i.bottom) > r(g.bottom),
									u = ("left" === _ && a) || ("right" === _ && l) || ("top" === _ && c) || ("bottom" === _ && h),
									f = -1 !== ["top", "bottom"].indexOf(_),
									d = !!m.flipVariations && ((f && "start" === y && a) || (f && "end" === y && l) || (!f && "start" === y && c) || (!f && "end" === y && h));
								(s || u || d) && ((p.flipped = !0), (s || u) && (_ = E[e + 1]), d && (y = "end" === (n = y) ? "start" : "start" === n ? "end" : n), (p.placement = _ + (y ? "-" + y : "")), (p.offsets.popper = Qt({}, p.offsets.popper, ee(p.instance.popper, p.offsets.reference, p.placement))), (p = ie(p.instance.modifiers, p, "flip")));
							}),
							p
						);
					},
					behavior: "flip",
					padding: 5,
					boundariesElement: "viewport",
				},
				inner: {
					order: 700,
					enabled: !1,
					fn: function (t) {
						var e = t.placement,
							n = e.split("-")[0],
							i = t.offsets,
							o = i.popper,
							r = i.reference,
							s = -1 !== ["left", "right"].indexOf(n),
							a = -1 === ["top", "left"].indexOf(n);
						return (o[s ? "left" : "top"] = r[n] - (a ? o[s ? "width" : "height"] : 0)), (t.placement = te(e)), (t.offsets.popper = Vt(o)), t;
					},
				},
				hide: {
					order: 800,
					enabled: !0,
					fn: function (t) {
						if (!fe(t.instance.modifiers, "hide", "preventOverflow")) return t;
						var e = t.offsets.reference,
							n = ne(t.instance.modifiers, function (t) {
								return "preventOverflow" === t.name;
							}).boundaries;
						if (e.bottom < n.top || e.left > n.right || e.top > n.bottom || e.right < n.left) {
							if (!0 === t.hide) return t;
							(t.hide = !0), (t.attributes["x-out-of-boundaries"] = "");
						} else {
							if (!1 === t.hide) return t;
							(t.hide = !1), (t.attributes["x-out-of-boundaries"] = !1);
						}
						return t;
					},
				},
				computeStyle: {
					order: 850,
					enabled: !0,
					fn: function (t, e) {
						var n = e.x,
							i = e.y,
							o = t.offsets.popper,
							r = ne(t.instance.modifiers, function (t) {
								return "applyStyle" === t.name;
							}).gpuAcceleration;
						void 0 !== r && console.warn("WARNING: `gpuAcceleration` option moved to `computeStyle` modifier and will not be supported in future versions of Popper.js!");
						var s,
							a,
							l,
							c,
							h,
							u,
							f,
							d,
							p,
							m,
							g,
							_,
							v,
							y,
							E = void 0 !== r ? r : e.gpuAcceleration,
							b = jt(t.instance.popper),
							w = Yt(b),
							C = { position: o.position },
							T =
								((s = t),
								(a = window.devicePixelRatio < 2 || !ue),
								(l = s.offsets),
								(c = l.popper),
								(h = l.reference),
								(u = Math.round),
								(f = Math.floor),
								(d = function (t) {
									return t;
								}),
								(p = u(h.width)),
								(m = u(c.width)),
								(g = -1 !== ["left", "right"].indexOf(s.placement)),
								(_ = -1 !== s.placement.indexOf("-")),
								(y = a ? u : d),
								{ left: (v = a ? (g || _ || p % 2 == m % 2 ? u : f) : d)(p % 2 == 1 && m % 2 == 1 && !_ && a ? c.left - 1 : c.left), top: y(c.top), bottom: y(c.bottom), right: v(c.right) }),
							S = "bottom" === n ? "top" : "bottom",
							D = "right" === i ? "left" : "right",
							I = re("transform"),
							A = void 0,
							O = void 0;
						if (((O = "bottom" === S ? ("HTML" === b.nodeName ? -b.clientHeight + T.bottom : -w.height + T.bottom) : T.top), (A = "right" === D ? ("HTML" === b.nodeName ? -b.clientWidth + T.right : -w.width + T.right) : T.left), E && I)) (C[I] = "translate3d(" + A + "px, " + O + "px, 0)"), (C[S] = 0), (C[D] = 0), (C.willChange = "transform");
						else {
							var N = "bottom" === S ? -1 : 1,
								k = "right" === D ? -1 : 1;
							(C[S] = O * N), (C[D] = A * k), (C.willChange = S + ", " + D);
						}
						var L = { "x-placement": t.placement };
						return (t.attributes = Qt({}, L, t.attributes)), (t.styles = Qt({}, C, t.styles)), (t.arrowStyles = Qt({}, t.offsets.arrow, t.arrowStyles)), t;
					},
					gpuAcceleration: !0,
					x: "bottom",
					y: "right",
				},
				applyStyle: {
					order: 900,
					enabled: !0,
					fn: function (t) {
						var e, n;
						return (
							he(t.instance.popper, t.styles),
							(e = t.instance.popper),
							(n = t.attributes),
							Object.keys(n).forEach(function (t) {
								!1 !== n[t] ? e.setAttribute(t, n[t]) : e.removeAttribute(t);
							}),
							t.arrowElement && Object.keys(t.arrowStyles).length && he(t.arrowElement, t.arrowStyles),
							t
						);
					},
					onLoad: function (t, e, n, i, o) {
						var r = Jt(o, e, t, n.positionFixed),
							s = $t(n.placement, r, e, t, n.modifiers.flip.boundariesElement, n.modifiers.flip.padding);
						return e.setAttribute("x-placement", s), he(e, { position: n.positionFixed ? "fixed" : "absolute" }), n;
					},
					gpuAcceleration: void 0,
				},
			},
		},
		be = (function () {
			function r(t, e) {
				var n = this,
					i = 2 < arguments.length && void 0 !== arguments[2] ? arguments[2] : {};
				!(function (t, e) {
					if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
				})(this, r),
					(this.scheduleUpdate = function () {
						return requestAnimationFrame(n.update);
					}),
					(this.update = At(this.update.bind(this))),
					(this.options = Qt({}, r.Defaults, i)),
					(this.state = { isDestroyed: !1, isCreated: !1, scrollParents: [] }),
					(this.reference = t && t.jquery ? t[0] : t),
					(this.popper = e && e.jquery ? e[0] : e),
					(this.options.modifiers = {}),
					Object.keys(Qt({}, r.Defaults.modifiers, i.modifiers)).forEach(function (t) {
						n.options.modifiers[t] = Qt({}, r.Defaults.modifiers[t] || {}, i.modifiers ? i.modifiers[t] : {});
					}),
					(this.modifiers = Object.keys(this.options.modifiers)
						.map(function (t) {
							return Qt({ name: t }, n.options.modifiers[t]);
						})
						.sort(function (t, e) {
							return t.order - e.order;
						})),
					this.modifiers.forEach(function (t) {
						t.enabled && Ot(t.onLoad) && t.onLoad(n.reference, n.popper, n.options, t, n.state);
					}),
					this.update();
				var o = this.options.eventsEnabled;
				o && this.enableEventListeners(), (this.state.eventsEnabled = o);
			}
			return (
				qt(r, [
					{
						key: "update",
						value: function () {
							return function () {
								if (!this.state.isDestroyed) {
									var t = { instance: this, styles: {}, arrowStyles: {}, attributes: {}, flipped: !1, offsets: {} };
									(t.offsets.reference = Jt(this.state, this.popper, this.reference, this.options.positionFixed)), (t.placement = $t(this.options.placement, t.offsets.reference, this.popper, this.reference, this.options.modifiers.flip.boundariesElement, this.options.modifiers.flip.padding)), (t.originalPlacement = t.placement), (t.positionFixed = this.options.positionFixed), (t.offsets.popper = ee(this.popper, t.offsets.reference, t.placement)), (t.offsets.popper.position = this.options.positionFixed ? "fixed" : "absolute"), (t = ie(this.modifiers, t)), this.state.isCreated ? this.options.onUpdate(t) : ((this.state.isCreated = !0), this.options.onCreate(t));
								}
							}.call(this);
						},
					},
					{
						key: "destroy",
						value: function () {
							return function () {
								return (this.state.isDestroyed = !0), oe(this.modifiers, "applyStyle") && (this.popper.removeAttribute("x-placement"), (this.popper.style.position = ""), (this.popper.style.top = ""), (this.popper.style.left = ""), (this.popper.style.right = ""), (this.popper.style.bottom = ""), (this.popper.style.willChange = ""), (this.popper.style[re("transform")] = "")), this.disableEventListeners(), this.options.removeOnDestroy && this.popper.parentNode.removeChild(this.popper), this;
							}.call(this);
						},
					},
					{
						key: "enableEventListeners",
						value: function () {
							return function () {
								this.state.eventsEnabled || (this.state = ae(this.reference, this.options, this.state, this.scheduleUpdate));
							}.call(this);
						},
					},
					{
						key: "disableEventListeners",
						value: function () {
							return le.call(this);
						},
					},
				]),
				r
			);
		})();
	(be.Utils = ("undefined" != typeof window ? window : global).PopperUtils), (be.placements = de), (be.Defaults = Ee);
	var we = "dropdown",
		Ce = "bs.dropdown",
		Te = "." + Ce,
		Se = ".data-api",
		De = p.fn[we],
		Ie = new RegExp("38|40|27"),
		Ae = { HIDE: "hide" + Te, HIDDEN: "hidden" + Te, SHOW: "show" + Te, SHOWN: "shown" + Te, CLICK: "click" + Te, CLICK_DATA_API: "click" + Te + Se, KEYDOWN_DATA_API: "keydown" + Te + Se, KEYUP_DATA_API: "keyup" + Te + Se },
		Oe = "disabled",
		Ne = "show",
		ke = "dropup",
		Le = "dropright",
		xe = "dropleft",
		Pe = "dropdown-menu-right",
		He = "position-static",
		je = '[data-toggle="dropdown"]',
		Re = ".dropdown form",
		Fe = ".dropdown-menu",
		Me = ".navbar-nav",
		We = ".dropdown-menu .dropdown-item:not(.disabled):not(:disabled)",
		Ue = "top-start",
		Be = "top-end",
		qe = "bottom-start",
		Ke = "bottom-end",
		Qe = "right-start",
		Ve = "left-start",
		Ye = { offset: 0, flip: !0, boundary: "scrollParent", reference: "toggle", display: "dynamic" },
		ze = { offset: "(number|string|function)", flip: "boolean", boundary: "(string|element)", reference: "(string|element)", display: "string" },
		Xe = (function () {
			function c(t, e) {
				(this._element = t), (this._popper = null), (this._config = this._getConfig(e)), (this._menu = this._getMenuElement()), (this._inNavbar = this._detectNavbar()), this._addEventListeners();
			}
			var t = c.prototype;
			return (
				(t.toggle = function () {
					if (!this._element.disabled && !p(this._element).hasClass(Oe)) {
						var t = c._getParentFromElement(this._element),
							e = p(this._menu).hasClass(Ne);
						if ((c._clearMenus(), !e)) {
							var n = { relatedTarget: this._element },
								i = p.Event(Ae.SHOW, n);
							if ((p(t).trigger(i), !i.isDefaultPrevented())) {
								if (!this._inNavbar) {
									if ("undefined" == typeof be) throw new TypeError("Bootstrap's dropdowns require Popper.js (https://popper.js.org/)");
									var o = this._element;
									"parent" === this._config.reference ? (o = t) : m.isElement(this._config.reference) && ((o = this._config.reference), "undefined" != typeof this._config.reference.jquery && (o = this._config.reference[0])), "scrollParent" !== this._config.boundary && p(t).addClass(He), (this._popper = new be(o, this._menu, this._getPopperConfig()));
								}
								"ontouchstart" in document.documentElement && 0 === p(t).closest(Me).length && p(document.body).children().on("mouseover", null, p.noop), this._element.focus(), this._element.setAttribute("aria-expanded", !0), p(this._menu).toggleClass(Ne), p(t).toggleClass(Ne).trigger(p.Event(Ae.SHOWN, n));
							}
						}
					}
				}),
				(t.show = function () {
					if (!(this._element.disabled || p(this._element).hasClass(Oe) || p(this._menu).hasClass(Ne))) {
						var t = { relatedTarget: this._element },
							e = p.Event(Ae.SHOW, t),
							n = c._getParentFromElement(this._element);
						p(n).trigger(e), e.isDefaultPrevented() || (p(this._menu).toggleClass(Ne), p(n).toggleClass(Ne).trigger(p.Event(Ae.SHOWN, t)));
					}
				}),
				(t.hide = function () {
					if (!this._element.disabled && !p(this._element).hasClass(Oe) && p(this._menu).hasClass(Ne)) {
						var t = { relatedTarget: this._element },
							e = p.Event(Ae.HIDE, t),
							n = c._getParentFromElement(this._element);
						p(n).trigger(e), e.isDefaultPrevented() || (p(this._menu).toggleClass(Ne), p(n).toggleClass(Ne).trigger(p.Event(Ae.HIDDEN, t)));
					}
				}),
				(t.dispose = function () {
					p.removeData(this._element, Ce), p(this._element).off(Te), (this._element = null), (this._menu = null) !== this._popper && (this._popper.destroy(), (this._popper = null));
				}),
				(t.update = function () {
					(this._inNavbar = this._detectNavbar()), null !== this._popper && this._popper.scheduleUpdate();
				}),
				(t._addEventListeners = function () {
					var e = this;
					p(this._element).on(Ae.CLICK, function (t) {
						t.preventDefault(), t.stopPropagation(), e.toggle();
					});
				}),
				(t._getConfig = function (t) {
					return (t = l({}, this.constructor.Default, p(this._element).data(), t)), m.typeCheckConfig(we, t, this.constructor.DefaultType), t;
				}),
				(t._getMenuElement = function () {
					if (!this._menu) {
						var t = c._getParentFromElement(this._element);
						t && (this._menu = t.querySelector(Fe));
					}
					return this._menu;
				}),
				(t._getPlacement = function () {
					var t = p(this._element.parentNode),
						e = qe;
					return t.hasClass(ke) ? ((e = Ue), p(this._menu).hasClass(Pe) && (e = Be)) : t.hasClass(Le) ? (e = Qe) : t.hasClass(xe) ? (e = Ve) : p(this._menu).hasClass(Pe) && (e = Ke), e;
				}),
				(t._detectNavbar = function () {
					return 0 < p(this._element).closest(".navbar").length;
				}),
				(t._getOffset = function () {
					var e = this,
						t = {};
					return (
						"function" == typeof this._config.offset
							? (t.fn = function (t) {
									return (t.offsets = l({}, t.offsets, e._config.offset(t.offsets, e._element) || {})), t;
							  })
							: (t.offset = this._config.offset),
						t
					);
				}),
				(t._getPopperConfig = function () {
					var t = { placement: this._getPlacement(), modifiers: { offset: this._getOffset(), flip: { enabled: this._config.flip }, preventOverflow: { boundariesElement: this._config.boundary } } };
					return "static" === this._config.display && (t.modifiers.applyStyle = { enabled: !1 }), t;
				}),
				(c._jQueryInterface = function (e) {
					return this.each(function () {
						var t = p(this).data(Ce);
						if ((t || ((t = new c(this, "object" == typeof e ? e : null)), p(this).data(Ce, t)), "string" == typeof e)) {
							if ("undefined" == typeof t[e]) throw new TypeError('No method named "' + e + '"');
							t[e]();
						}
					});
				}),
				(c._clearMenus = function (t) {
					if (!t || (3 !== t.which && ("keyup" !== t.type || 9 === t.which)))
						for (var e = [].slice.call(document.querySelectorAll(je)), n = 0, i = e.length; n < i; n++) {
							var o = c._getParentFromElement(e[n]),
								r = p(e[n]).data(Ce),
								s = { relatedTarget: e[n] };
							if ((t && "click" === t.type && (s.clickEvent = t), r)) {
								var a = r._menu;
								if (p(o).hasClass(Ne) && !(t && (("click" === t.type && /input|textarea/i.test(t.target.tagName)) || ("keyup" === t.type && 9 === t.which)) && p.contains(o, t.target))) {
									var l = p.Event(Ae.HIDE, s);
									p(o).trigger(l), l.isDefaultPrevented() || ("ontouchstart" in document.documentElement && p(document.body).children().off("mouseover", null, p.noop), e[n].setAttribute("aria-expanded", "false"), p(a).removeClass(Ne), p(o).removeClass(Ne).trigger(p.Event(Ae.HIDDEN, s)));
								}
							}
						}
				}),
				(c._getParentFromElement = function (t) {
					var e,
						n = m.getSelectorFromElement(t);
					return n && (e = document.querySelector(n)), e || t.parentNode;
				}),
				(c._dataApiKeydownHandler = function (t) {
					if ((/input|textarea/i.test(t.target.tagName) ? !(32 === t.which || (27 !== t.which && ((40 !== t.which && 38 !== t.which) || p(t.target).closest(Fe).length))) : Ie.test(t.which)) && (t.preventDefault(), t.stopPropagation(), !this.disabled && !p(this).hasClass(Oe))) {
						var e = c._getParentFromElement(this),
							n = p(e).hasClass(Ne);
						if (n && (!n || (27 !== t.which && 32 !== t.which))) {
							var i = [].slice.call(e.querySelectorAll(We));
							if (0 !== i.length) {
								var o = i.indexOf(t.target);
								38 === t.which && 0 < o && o--, 40 === t.which && o < i.length - 1 && o++, o < 0 && (o = 0), i[o].focus();
							}
						} else {
							if (27 === t.which) {
								var r = e.querySelector(je);
								p(r).trigger("focus");
							}
							p(this).trigger("click");
						}
					}
				}),
				s(c, null, [
					{
						key: "VERSION",
						get: function () {
							return "4.3.1";
						},
					},
					{
						key: "Default",
						get: function () {
							return Ye;
						},
					},
					{
						key: "DefaultType",
						get: function () {
							return ze;
						},
					},
				]),
				c
			);
		})();
	p(document)
		.on(Ae.KEYDOWN_DATA_API, je, Xe._dataApiKeydownHandler)
		.on(Ae.KEYDOWN_DATA_API, Fe, Xe._dataApiKeydownHandler)
		.on(Ae.CLICK_DATA_API + " " + Ae.KEYUP_DATA_API, Xe._clearMenus)
		.on(Ae.CLICK_DATA_API, je, function (t) {
			t.preventDefault(), t.stopPropagation(), Xe._jQueryInterface.call(p(this), "toggle");
		})
		.on(Ae.CLICK_DATA_API, Re, function (t) {
			t.stopPropagation();
		}),
		(p.fn[we] = Xe._jQueryInterface),
		(p.fn[we].Constructor = Xe),
		(p.fn[we].noConflict = function () {
			return (p.fn[we] = De), Xe._jQueryInterface;
		});
	var Ge = "modal",
		$e = "bs.modal",
		Je = "." + $e,
		Ze = p.fn[Ge],
		tn = { backdrop: !0, keyboard: !0, focus: !0, show: !0 },
		en = { backdrop: "(boolean|string)", keyboard: "boolean", focus: "boolean", show: "boolean" },
		nn = { HIDE: "hide" + Je, HIDDEN: "hidden" + Je, SHOW: "show" + Je, SHOWN: "shown" + Je, FOCUSIN: "focusin" + Je, RESIZE: "resize" + Je, CLICK_DISMISS: "click.dismiss" + Je, KEYDOWN_DISMISS: "keydown.dismiss" + Je, MOUSEUP_DISMISS: "mouseup.dismiss" + Je, MOUSEDOWN_DISMISS: "mousedown.dismiss" + Je, CLICK_DATA_API: "click" + Je + ".data-api" },
		on = "modal-dialog-scrollable",
		rn = "modal-scrollbar-measure",
		sn = "modal-backdrop",
		an = "modal-open",
		ln = "fade",
		cn = "show",
		hn = ".modal-dialog",
		un = ".modal-body",
		fn = '[data-toggle="modal"]',
		dn = '[data-dismiss="modal"]',
		pn = ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top",
		mn = ".sticky-top",
		gn = (function () {
			function o(t, e) {
				(this._config = this._getConfig(e)), (this._element = t), (this._dialog = t.querySelector(hn)), (this._backdrop = null), (this._isShown = !1), (this._isBodyOverflowing = !1), (this._ignoreBackdropClick = !1), (this._isTransitioning = !1), (this._scrollbarWidth = 0);
			}
			var t = o.prototype;
			return (
				(t.toggle = function (t) {
					return this._isShown ? this.hide() : this.show(t);
				}),
				(t.show = function (t) {
					var e = this;
					if (!this._isShown && !this._isTransitioning) {
						p(this._element).hasClass(ln) && (this._isTransitioning = !0);
						var n = p.Event(nn.SHOW, { relatedTarget: t });
						p(this._element).trigger(n),
							this._isShown ||
								n.isDefaultPrevented() ||
								((this._isShown = !0),
								this._checkScrollbar(),
								this._setScrollbar(),
								this._adjustDialog(),
								this._setEscapeEvent(),
								this._setResizeEvent(),
								p(this._element).on(nn.CLICK_DISMISS, dn, function (t) {
									return e.hide(t);
								}),
								p(this._dialog).on(nn.MOUSEDOWN_DISMISS, function () {
									p(e._element).one(nn.MOUSEUP_DISMISS, function (t) {
										p(t.target).is(e._element) && (e._ignoreBackdropClick = !0);
									});
								}),
								this._showBackdrop(function () {
									return e._showElement(t);
								}));
					}
				}),
				(t.hide = function (t) {
					var e = this;
					if ((t && t.preventDefault(), this._isShown && !this._isTransitioning)) {
						var n = p.Event(nn.HIDE);
						if ((p(this._element).trigger(n), this._isShown && !n.isDefaultPrevented())) {
							this._isShown = !1;
							var i = p(this._element).hasClass(ln);
							if ((i && (this._isTransitioning = !0), this._setEscapeEvent(), this._setResizeEvent(), p(document).off(nn.FOCUSIN), p(this._element).removeClass(cn), p(this._element).off(nn.CLICK_DISMISS), p(this._dialog).off(nn.MOUSEDOWN_DISMISS), i)) {
								var o = m.getTransitionDurationFromElement(this._element);
								p(this._element)
									.one(m.TRANSITION_END, function (t) {
										return e._hideModal(t);
									})
									.emulateTransitionEnd(o);
							} else this._hideModal();
						}
					}
				}),
				(t.dispose = function () {
					[window, this._element, this._dialog].forEach(function (t) {
						return p(t).off(Je);
					}),
						p(document).off(nn.FOCUSIN),
						p.removeData(this._element, $e),
						(this._config = null),
						(this._element = null),
						(this._dialog = null),
						(this._backdrop = null),
						(this._isShown = null),
						(this._isBodyOverflowing = null),
						(this._ignoreBackdropClick = null),
						(this._isTransitioning = null),
						(this._scrollbarWidth = null);
				}),
				(t.handleUpdate = function () {
					this._adjustDialog();
				}),
				(t._getConfig = function (t) {
					return (t = l({}, tn, t)), m.typeCheckConfig(Ge, t, en), t;
				}),
				(t._showElement = function (t) {
					var e = this,
						n = p(this._element).hasClass(ln);
					(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE) || document.body.appendChild(this._element), (this._element.style.display = "block"), this._element.removeAttribute("aria-hidden"), this._element.setAttribute("aria-modal", !0), p(this._dialog).hasClass(on) ? (this._dialog.querySelector(un).scrollTop = 0) : (this._element.scrollTop = 0), n && m.reflow(this._element), p(this._element).addClass(cn), this._config.focus && this._enforceFocus();
					var i = p.Event(nn.SHOWN, { relatedTarget: t }),
						o = function () {
							e._config.focus && e._element.focus(), (e._isTransitioning = !1), p(e._element).trigger(i);
						};
					if (n) {
						var r = m.getTransitionDurationFromElement(this._dialog);
						p(this._dialog).one(m.TRANSITION_END, o).emulateTransitionEnd(r);
					} else o();
				}),
				(t._enforceFocus = function () {
					var e = this;
					p(document)
						.off(nn.FOCUSIN)
						.on(nn.FOCUSIN, function (t) {
							document !== t.target && e._element !== t.target && 0 === p(e._element).has(t.target).length && e._element.focus();
						});
				}),
				(t._setEscapeEvent = function () {
					var e = this;
					this._isShown && this._config.keyboard
						? p(this._element).on(nn.KEYDOWN_DISMISS, function (t) {
								27 === t.which && (t.preventDefault(), e.hide());
						  })
						: this._isShown || p(this._element).off(nn.KEYDOWN_DISMISS);
				}),
				(t._setResizeEvent = function () {
					var e = this;
					this._isShown
						? p(window).on(nn.RESIZE, function (t) {
								return e.handleUpdate(t);
						  })
						: p(window).off(nn.RESIZE);
				}),
				(t._hideModal = function () {
					var t = this;
					(this._element.style.display = "none"),
						this._element.setAttribute("aria-hidden", !0),
						this._element.removeAttribute("aria-modal"),
						(this._isTransitioning = !1),
						this._showBackdrop(function () {
							p(document.body).removeClass(an), t._resetAdjustments(), t._resetScrollbar(), p(t._element).trigger(nn.HIDDEN);
						});
				}),
				(t._removeBackdrop = function () {
					this._backdrop && (p(this._backdrop).remove(), (this._backdrop = null));
				}),
				(t._showBackdrop = function (t) {
					var e = this,
						n = p(this._element).hasClass(ln) ? ln : "";
					if (this._isShown && this._config.backdrop) {
						if (
							((this._backdrop = document.createElement("div")),
							(this._backdrop.className = sn),
							n && this._backdrop.classList.add(n),
							p(this._backdrop).appendTo(document.body),
							p(this._element).on(nn.CLICK_DISMISS, function (t) {
								e._ignoreBackdropClick ? (e._ignoreBackdropClick = !1) : t.target === t.currentTarget && ("static" === e._config.backdrop ? e._element.focus() : e.hide());
							}),
							n && m.reflow(this._backdrop),
							p(this._backdrop).addClass(cn),
							!t)
						)
							return;
						if (!n) return void t();
						var i = m.getTransitionDurationFromElement(this._backdrop);
						p(this._backdrop).one(m.TRANSITION_END, t).emulateTransitionEnd(i);
					} else if (!this._isShown && this._backdrop) {
						p(this._backdrop).removeClass(cn);
						var o = function () {
							e._removeBackdrop(), t && t();
						};
						if (p(this._element).hasClass(ln)) {
							var r = m.getTransitionDurationFromElement(this._backdrop);
							p(this._backdrop).one(m.TRANSITION_END, o).emulateTransitionEnd(r);
						} else o();
					} else t && t();
				}),
				(t._adjustDialog = function () {
					var t = this._element.scrollHeight > document.documentElement.clientHeight;
					!this._isBodyOverflowing && t && (this._element.style.paddingLeft = this._scrollbarWidth + "px"), this._isBodyOverflowing && !t && (this._element.style.paddingRight = this._scrollbarWidth + "px");
				}),
				(t._resetAdjustments = function () {
					(this._element.style.paddingLeft = ""), (this._element.style.paddingRight = "");
				}),
				(t._checkScrollbar = function () {
					var t = document.body.getBoundingClientRect();
					(this._isBodyOverflowing = t.left + t.right < window.innerWidth), (this._scrollbarWidth = this._getScrollbarWidth());
				}),
				(t._setScrollbar = function () {
					var o = this;
					if (this._isBodyOverflowing) {
						var t = [].slice.call(document.querySelectorAll(pn)),
							e = [].slice.call(document.querySelectorAll(mn));
						p(t).each(function (t, e) {
							var n = e.style.paddingRight,
								i = p(e).css("padding-right");
							p(e)
								.data("padding-right", n)
								.css("padding-right", parseFloat(i) + o._scrollbarWidth + "px");
						}),
							p(e).each(function (t, e) {
								var n = e.style.marginRight,
									i = p(e).css("margin-right");
								p(e)
									.data("margin-right", n)
									.css("margin-right", parseFloat(i) - o._scrollbarWidth + "px");
							});
						var n = document.body.style.paddingRight,
							i = p(document.body).css("padding-right");
						p(document.body)
							.data("padding-right", n)
							.css("padding-right", parseFloat(i) + this._scrollbarWidth + "px");
					}
					p(document.body).addClass(an);
				}),
				(t._resetScrollbar = function () {
					var t = [].slice.call(document.querySelectorAll(pn));
					p(t).each(function (t, e) {
						var n = p(e).data("padding-right");
						p(e).removeData("padding-right"), (e.style.paddingRight = n || "");
					});
					var e = [].slice.call(document.querySelectorAll("" + mn));
					p(e).each(function (t, e) {
						var n = p(e).data("margin-right");
						"undefined" != typeof n && p(e).css("margin-right", n).removeData("margin-right");
					});
					var n = p(document.body).data("padding-right");
					p(document.body).removeData("padding-right"), (document.body.style.paddingRight = n || "");
				}),
				(t._getScrollbarWidth = function () {
					var t = document.createElement("div");
					(t.className = rn), document.body.appendChild(t);
					var e = t.getBoundingClientRect().width - t.clientWidth;
					return document.body.removeChild(t), e;
				}),
				(o._jQueryInterface = function (n, i) {
					return this.each(function () {
						var t = p(this).data($e),
							e = l({}, tn, p(this).data(), "object" == typeof n && n ? n : {});
						if ((t || ((t = new o(this, e)), p(this).data($e, t)), "string" == typeof n)) {
							if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');
							t[n](i);
						} else e.show && t.show(i);
					});
				}),
				s(o, null, [
					{
						key: "VERSION",
						get: function () {
							return "4.3.1";
						},
					},
					{
						key: "Default",
						get: function () {
							return tn;
						},
					},
				]),
				o
			);
		})();
	p(document).on(nn.CLICK_DATA_API, fn, function (t) {
		var e,
			n = this,
			i = m.getSelectorFromElement(this);
		i && (e = document.querySelector(i));
		var o = p(e).data($e) ? "toggle" : l({}, p(e).data(), p(this).data());
		("A" !== this.tagName && "AREA" !== this.tagName) || t.preventDefault();
		var r = p(e).one(nn.SHOW, function (t) {
			t.isDefaultPrevented() ||
				r.one(nn.HIDDEN, function () {
					p(n).is(":visible") && n.focus();
				});
		});
		gn._jQueryInterface.call(p(e), o, this);
	}),
		(p.fn[Ge] = gn._jQueryInterface),
		(p.fn[Ge].Constructor = gn),
		(p.fn[Ge].noConflict = function () {
			return (p.fn[Ge] = Ze), gn._jQueryInterface;
		});
	var _n = ["background", "cite", "href", "itemtype", "longdesc", "poster", "src", "xlink:href"],
		vn = { "*": ["class", "dir", "id", "lang", "role", /^aria-[\w-]*$/i], a: ["target", "href", "title", "rel"], area: [], b: [], br: [], col: [], code: [], div: [], em: [], hr: [], h1: [], h2: [], h3: [], h4: [], h5: [], h6: [], i: [], img: ["src", "alt", "title", "width", "height"], li: [], ol: [], p: [], pre: [], s: [], small: [], span: [], sub: [], sup: [], strong: [], u: [], ul: [] },
		yn = /^(?:(?:https?|mailto|ftp|tel|file):|[^&:/?#]*(?:[/?#]|$))/gi,
		En = /^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[a-z0-9+/]+=*$/i;
	function bn(t, s, e) {
		if (0 === t.length) return t;
		if (e && "function" == typeof e) return e(t);
		for (
			var n = new window.DOMParser().parseFromString(t, "text/html"),
				a = Object.keys(s),
				l = [].slice.call(n.body.querySelectorAll("*")),
				i = function (t, e) {
					var n = l[t],
						i = n.nodeName.toLowerCase();
					if (-1 === a.indexOf(n.nodeName.toLowerCase())) return n.parentNode.removeChild(n), "continue";
					var o = [].slice.call(n.attributes),
						r = [].concat(s["*"] || [], s[i] || []);
					o.forEach(function (t) {
						(function (t, e) {
							var n = t.nodeName.toLowerCase();
							if (-1 !== e.indexOf(n)) return -1 === _n.indexOf(n) || Boolean(t.nodeValue.match(yn) || t.nodeValue.match(En));
							for (
								var i = e.filter(function (t) {
										return t instanceof RegExp;
									}),
									o = 0,
									r = i.length;
								o < r;
								o++
							)
								if (n.match(i[o])) return !0;
							return !1;
						})(t, r) || n.removeAttribute(t.nodeName);
					});
				},
				o = 0,
				r = l.length;
			o < r;
			o++
		)
			i(o);
		return n.body.innerHTML;
	}
	var wn = "tooltip",
		Cn = "bs.tooltip",
		Tn = "." + Cn,
		Sn = p.fn[wn],
		Dn = "bs-tooltip",
		In = new RegExp("(^|\\s)" + Dn + "\\S+", "g"),
		An = ["sanitize", "whiteList", "sanitizeFn"],
		On = { animation: "boolean", template: "string", title: "(string|element|function)", trigger: "string", delay: "(number|object)", html: "boolean", selector: "(string|boolean)", placement: "(string|function)", offset: "(number|string|function)", container: "(string|element|boolean)", fallbackPlacement: "(string|array)", boundary: "(string|element)", sanitize: "boolean", sanitizeFn: "(null|function)", whiteList: "object" },
		Nn = { AUTO: "auto", TOP: "top", RIGHT: "right", BOTTOM: "bottom", LEFT: "left" },
		kn = { animation: !0, template: '<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>', trigger: "hover focus", title: "", delay: 0, html: !1, selector: !1, placement: "top", offset: 0, container: !1, fallbackPlacement: "flip", boundary: "scrollParent", sanitize: !0, sanitizeFn: null, whiteList: vn },
		Ln = "show",
		xn = "out",
		Pn = { HIDE: "hide" + Tn, HIDDEN: "hidden" + Tn, SHOW: "show" + Tn, SHOWN: "shown" + Tn, INSERTED: "inserted" + Tn, CLICK: "click" + Tn, FOCUSIN: "focusin" + Tn, FOCUSOUT: "focusout" + Tn, MOUSEENTER: "mouseenter" + Tn, MOUSELEAVE: "mouseleave" + Tn },
		Hn = "fade",
		jn = "show",
		Rn = ".tooltip-inner",
		Fn = ".arrow",
		Mn = "hover",
		Wn = "focus",
		Un = "click",
		Bn = "manual",
		qn = (function () {
			function i(t, e) {
				if ("undefined" == typeof be) throw new TypeError("Bootstrap's tooltips require Popper.js (https://popper.js.org/)");
				(this._isEnabled = !0), (this._timeout = 0), (this._hoverState = ""), (this._activeTrigger = {}), (this._popper = null), (this.element = t), (this.config = this._getConfig(e)), (this.tip = null), this._setListeners();
			}
			var t = i.prototype;
			return (
				(t.enable = function () {
					this._isEnabled = !0;
				}),
				(t.disable = function () {
					this._isEnabled = !1;
				}),
				(t.toggleEnabled = function () {
					this._isEnabled = !this._isEnabled;
				}),
				(t.toggle = function (t) {
					if (this._isEnabled)
						if (t) {
							var e = this.constructor.DATA_KEY,
								n = p(t.currentTarget).data(e);
							n || ((n = new this.constructor(t.currentTarget, this._getDelegateConfig())), p(t.currentTarget).data(e, n)), (n._activeTrigger.click = !n._activeTrigger.click), n._isWithActiveTrigger() ? n._enter(null, n) : n._leave(null, n);
						} else {
							if (p(this.getTipElement()).hasClass(jn)) return void this._leave(null, this);
							this._enter(null, this);
						}
				}),
				(t.dispose = function () {
					clearTimeout(this._timeout), p.removeData(this.element, this.constructor.DATA_KEY), p(this.element).off(this.constructor.EVENT_KEY), p(this.element).closest(".modal").off("hide.bs.modal"), this.tip && p(this.tip).remove(), (this._isEnabled = null), (this._timeout = null), (this._hoverState = null), (this._activeTrigger = null) !== this._popper && this._popper.destroy(), (this._popper = null), (this.element = null), (this.config = null), (this.tip = null);
				}),
				(t.show = function () {
					var e = this;
					if ("none" === p(this.element).css("display")) throw new Error("Please use show on visible elements");
					var t = p.Event(this.constructor.Event.SHOW);
					if (this.isWithContent() && this._isEnabled) {
						p(this.element).trigger(t);
						var n = m.findShadowRoot(this.element),
							i = p.contains(null !== n ? n : this.element.ownerDocument.documentElement, this.element);
						if (t.isDefaultPrevented() || !i) return;
						var o = this.getTipElement(),
							r = m.getUID(this.constructor.NAME);
						o.setAttribute("id", r), this.element.setAttribute("aria-describedby", r), this.setContent(), this.config.animation && p(o).addClass(Hn);
						var s = "function" == typeof this.config.placement ? this.config.placement.call(this, o, this.element) : this.config.placement,
							a = this._getAttachment(s);
						this.addAttachmentClass(a);
						var l = this._getContainer();
						p(o).data(this.constructor.DATA_KEY, this),
							p.contains(this.element.ownerDocument.documentElement, this.tip) || p(o).appendTo(l),
							p(this.element).trigger(this.constructor.Event.INSERTED),
							(this._popper = new be(this.element, o, {
								placement: a,
								modifiers: { offset: this._getOffset(), flip: { behavior: this.config.fallbackPlacement }, arrow: { element: Fn }, preventOverflow: { boundariesElement: this.config.boundary } },
								onCreate: function (t) {
									t.originalPlacement !== t.placement && e._handlePopperPlacementChange(t);
								},
								onUpdate: function (t) {
									return e._handlePopperPlacementChange(t);
								},
							})),
							p(o).addClass(jn),
							"ontouchstart" in document.documentElement && p(document.body).children().on("mouseover", null, p.noop);
						var c = function () {
							e.config.animation && e._fixTransition();
							var t = e._hoverState;
							(e._hoverState = null), p(e.element).trigger(e.constructor.Event.SHOWN), t === xn && e._leave(null, e);
						};
						if (p(this.tip).hasClass(Hn)) {
							var h = m.getTransitionDurationFromElement(this.tip);
							p(this.tip).one(m.TRANSITION_END, c).emulateTransitionEnd(h);
						} else c();
					}
				}),
				(t.hide = function (t) {
					var e = this,
						n = this.getTipElement(),
						i = p.Event(this.constructor.Event.HIDE),
						o = function () {
							e._hoverState !== Ln && n.parentNode && n.parentNode.removeChild(n), e._cleanTipClass(), e.element.removeAttribute("aria-describedby"), p(e.element).trigger(e.constructor.Event.HIDDEN), null !== e._popper && e._popper.destroy(), t && t();
						};
					if ((p(this.element).trigger(i), !i.isDefaultPrevented())) {
						if ((p(n).removeClass(jn), "ontouchstart" in document.documentElement && p(document.body).children().off("mouseover", null, p.noop), (this._activeTrigger[Un] = !1), (this._activeTrigger[Wn] = !1), (this._activeTrigger[Mn] = !1), p(this.tip).hasClass(Hn))) {
							var r = m.getTransitionDurationFromElement(n);
							p(n).one(m.TRANSITION_END, o).emulateTransitionEnd(r);
						} else o();
						this._hoverState = "";
					}
				}),
				(t.update = function () {
					null !== this._popper && this._popper.scheduleUpdate();
				}),
				(t.isWithContent = function () {
					return Boolean(this.getTitle());
				}),
				(t.addAttachmentClass = function (t) {
					p(this.getTipElement()).addClass(Dn + "-" + t);
				}),
				(t.getTipElement = function () {
					return (this.tip = this.tip || p(this.config.template)[0]), this.tip;
				}),
				(t.setContent = function () {
					var t = this.getTipElement();
					this.setElementContent(p(t.querySelectorAll(Rn)), this.getTitle()), p(t).removeClass(Hn + " " + jn);
				}),
				(t.setElementContent = function (t, e) {
					"object" != typeof e || (!e.nodeType && !e.jquery) ? (this.config.html ? (this.config.sanitize && (e = bn(e, this.config.whiteList, this.config.sanitizeFn)), t.html(e)) : t.text(e)) : this.config.html ? p(e).parent().is(t) || t.empty().append(e) : t.text(p(e).text());
				}),
				(t.getTitle = function () {
					var t = this.element.getAttribute("data-original-title");
					return t || (t = "function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title), t;
				}),
				(t._getOffset = function () {
					var e = this,
						t = {};
					return (
						"function" == typeof this.config.offset
							? (t.fn = function (t) {
									return (t.offsets = l({}, t.offsets, e.config.offset(t.offsets, e.element) || {})), t;
							  })
							: (t.offset = this.config.offset),
						t
					);
				}),
				(t._getContainer = function () {
					return !1 === this.config.container ? document.body : m.isElement(this.config.container) ? p(this.config.container) : p(document).find(this.config.container);
				}),
				(t._getAttachment = function (t) {
					return Nn[t.toUpperCase()];
				}),
				(t._setListeners = function () {
					var i = this;
					this.config.trigger.split(" ").forEach(function (t) {
						if ("click" === t)
							p(i.element).on(i.constructor.Event.CLICK, i.config.selector, function (t) {
								return i.toggle(t);
							});
						else if (t !== Bn) {
							var e = t === Mn ? i.constructor.Event.MOUSEENTER : i.constructor.Event.FOCUSIN,
								n = t === Mn ? i.constructor.Event.MOUSELEAVE : i.constructor.Event.FOCUSOUT;
							p(i.element)
								.on(e, i.config.selector, function (t) {
									return i._enter(t);
								})
								.on(n, i.config.selector, function (t) {
									return i._leave(t);
								});
						}
					}),
						p(this.element)
							.closest(".modal")
							.on("hide.bs.modal", function () {
								i.element && i.hide();
							}),
						this.config.selector ? (this.config = l({}, this.config, { trigger: "manual", selector: "" })) : this._fixTitle();
				}),
				(t._fixTitle = function () {
					var t = typeof this.element.getAttribute("data-original-title");
					(this.element.getAttribute("title") || "string" !== t) && (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""), this.element.setAttribute("title", ""));
				}),
				(t._enter = function (t, e) {
					var n = this.constructor.DATA_KEY;
					(e = e || p(t.currentTarget).data(n)) || ((e = new this.constructor(t.currentTarget, this._getDelegateConfig())), p(t.currentTarget).data(n, e)),
						t && (e._activeTrigger["focusin" === t.type ? Wn : Mn] = !0),
						p(e.getTipElement()).hasClass(jn) || e._hoverState === Ln
							? (e._hoverState = Ln)
							: (clearTimeout(e._timeout),
							  (e._hoverState = Ln),
							  e.config.delay && e.config.delay.show
									? (e._timeout = setTimeout(function () {
											e._hoverState === Ln && e.show();
									  }, e.config.delay.show))
									: e.show());
				}),
				(t._leave = function (t, e) {
					var n = this.constructor.DATA_KEY;
					(e = e || p(t.currentTarget).data(n)) || ((e = new this.constructor(t.currentTarget, this._getDelegateConfig())), p(t.currentTarget).data(n, e)),
						t && (e._activeTrigger["focusout" === t.type ? Wn : Mn] = !1),
						e._isWithActiveTrigger() ||
							(clearTimeout(e._timeout),
							(e._hoverState = xn),
							e.config.delay && e.config.delay.hide
								? (e._timeout = setTimeout(function () {
										e._hoverState === xn && e.hide();
								  }, e.config.delay.hide))
								: e.hide());
				}),
				(t._isWithActiveTrigger = function () {
					for (var t in this._activeTrigger) if (this._activeTrigger[t]) return !0;
					return !1;
				}),
				(t._getConfig = function (t) {
					var e = p(this.element).data();
					return (
						Object.keys(e).forEach(function (t) {
							-1 !== An.indexOf(t) && delete e[t];
						}),
						"number" == typeof (t = l({}, this.constructor.Default, e, "object" == typeof t && t ? t : {})).delay && (t.delay = { show: t.delay, hide: t.delay }),
						"number" == typeof t.title && (t.title = t.title.toString()),
						"number" == typeof t.content && (t.content = t.content.toString()),
						m.typeCheckConfig(wn, t, this.constructor.DefaultType),
						t.sanitize && (t.template = bn(t.template, t.whiteList, t.sanitizeFn)),
						t
					);
				}),
				(t._getDelegateConfig = function () {
					var t = {};
					if (this.config) for (var e in this.config) this.constructor.Default[e] !== this.config[e] && (t[e] = this.config[e]);
					return t;
				}),
				(t._cleanTipClass = function () {
					var t = p(this.getTipElement()),
						e = t.attr("class").match(In);
					null !== e && e.length && t.removeClass(e.join(""));
				}),
				(t._handlePopperPlacementChange = function (t) {
					var e = t.instance;
					(this.tip = e.popper), this._cleanTipClass(), this.addAttachmentClass(this._getAttachment(t.placement));
				}),
				(t._fixTransition = function () {
					var t = this.getTipElement(),
						e = this.config.animation;
					null === t.getAttribute("x-placement") && (p(t).removeClass(Hn), (this.config.animation = !1), this.hide(), this.show(), (this.config.animation = e));
				}),
				(i._jQueryInterface = function (n) {
					return this.each(function () {
						var t = p(this).data(Cn),
							e = "object" == typeof n && n;
						if ((t || !/dispose|hide/.test(n)) && (t || ((t = new i(this, e)), p(this).data(Cn, t)), "string" == typeof n)) {
							if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');
							t[n]();
						}
					});
				}),
				s(i, null, [
					{
						key: "VERSION",
						get: function () {
							return "4.3.1";
						},
					},
					{
						key: "Default",
						get: function () {
							return kn;
						},
					},
					{
						key: "NAME",
						get: function () {
							return wn;
						},
					},
					{
						key: "DATA_KEY",
						get: function () {
							return Cn;
						},
					},
					{
						key: "Event",
						get: function () {
							return Pn;
						},
					},
					{
						key: "EVENT_KEY",
						get: function () {
							return Tn;
						},
					},
					{
						key: "DefaultType",
						get: function () {
							return On;
						},
					},
				]),
				i
			);
		})();
	(p.fn[wn] = qn._jQueryInterface),
		(p.fn[wn].Constructor = qn),
		(p.fn[wn].noConflict = function () {
			return (p.fn[wn] = Sn), qn._jQueryInterface;
		});
	var Kn = "popover",
		Qn = "bs.popover",
		Vn = "." + Qn,
		Yn = p.fn[Kn],
		zn = "bs-popover",
		Xn = new RegExp("(^|\\s)" + zn + "\\S+", "g"),
		Gn = l({}, qn.Default, { placement: "right", trigger: "click", content: "", template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>' }),
		$n = l({}, qn.DefaultType, { content: "(string|element|function)" }),
		Jn = "fade",
		Zn = "show",
		ti = ".popover-header",
		ei = ".popover-body",
		ni = { HIDE: "hide" + Vn, HIDDEN: "hidden" + Vn, SHOW: "show" + Vn, SHOWN: "shown" + Vn, INSERTED: "inserted" + Vn, CLICK: "click" + Vn, FOCUSIN: "focusin" + Vn, FOCUSOUT: "focusout" + Vn, MOUSEENTER: "mouseenter" + Vn, MOUSELEAVE: "mouseleave" + Vn },
		ii = (function (t) {
			var e, n;
			function i() {
				return t.apply(this, arguments) || this;
			}
			(n = t), ((e = i).prototype = Object.create(n.prototype)), ((e.prototype.constructor = e).__proto__ = n);
			var o = i.prototype;
			return (
				(o.isWithContent = function () {
					return this.getTitle() || this._getContent();
				}),
				(o.addAttachmentClass = function (t) {
					p(this.getTipElement()).addClass(zn + "-" + t);
				}),
				(o.getTipElement = function () {
					return (this.tip = this.tip || p(this.config.template)[0]), this.tip;
				}),
				(o.setContent = function () {
					var t = p(this.getTipElement());
					this.setElementContent(t.find(ti), this.getTitle());
					var e = this._getContent();
					"function" == typeof e && (e = e.call(this.element)), this.setElementContent(t.find(ei), e), t.removeClass(Jn + " " + Zn);
				}),
				(o._getContent = function () {
					return this.element.getAttribute("data-content") || this.config.content;
				}),
				(o._cleanTipClass = function () {
					var t = p(this.getTipElement()),
						e = t.attr("class").match(Xn);
					null !== e && 0 < e.length && t.removeClass(e.join(""));
				}),
				(i._jQueryInterface = function (n) {
					return this.each(function () {
						var t = p(this).data(Qn),
							e = "object" == typeof n ? n : null;
						if ((t || !/dispose|hide/.test(n)) && (t || ((t = new i(this, e)), p(this).data(Qn, t)), "string" == typeof n)) {
							if ("undefined" == typeof t[n]) throw new TypeError('No method named "' + n + '"');
							t[n]();
						}
					});
				}),
				s(i, null, [
					{
						key: "VERSION",
						get: function () {
							return "4.3.1";
						},
					},
					{
						key: "Default",
						get: function () {
							return Gn;
						},
					},
					{
						key: "NAME",
						get: function () {
							return Kn;
						},
					},
					{
						key: "DATA_KEY",
						get: function () {
							return Qn;
						},
					},
					{
						key: "Event",
						get: function () {
							return ni;
						},
					},
					{
						key: "EVENT_KEY",
						get: function () {
							return Vn;
						},
					},
					{
						key: "DefaultType",
						get: function () {
							return $n;
						},
					},
				]),
				i
			);
		})(qn);
	(p.fn[Kn] = ii._jQueryInterface),
		(p.fn[Kn].Constructor = ii),
		(p.fn[Kn].noConflict = function () {
			return (p.fn[Kn] = Yn), ii._jQueryInterface;
		});
	var oi = "scrollspy",
		ri = "bs.scrollspy",
		si = "." + ri,
		ai = p.fn[oi],
		li = { offset: 10, method: "auto", target: "" },
		ci = { offset: "number", method: "string", target: "(string|element)" },
		hi = { ACTIVATE: "activate" + si, SCROLL: "scroll" + si, LOAD_DATA_API: "load" + si + ".data-api" },
		ui = "dropdown-item",
		fi = "active",
		di = '[data-spy="scroll"]',
		pi = ".nav, .list-group",
		mi = ".nav-link",
		gi = ".nav-item",
		_i = ".list-group-item",
		vi = ".dropdown",
		yi = ".dropdown-item",
		Ei = ".dropdown-toggle",
		bi = "offset",
		wi = "position",
		Ci = (function () {
			function n(t, e) {
				var n = this;
				(this._element = t),
					(this._scrollElement = "BODY" === t.tagName ? window : t),
					(this._config = this._getConfig(e)),
					(this._selector = this._config.target + " " + mi + "," + this._config.target + " " + _i + "," + this._config.target + " " + yi),
					(this._offsets = []),
					(this._targets = []),
					(this._activeTarget = null),
					(this._scrollHeight = 0),
					p(this._scrollElement).on(hi.SCROLL, function (t) {
						return n._process(t);
					}),
					this.refresh(),
					this._process();
			}
			var t = n.prototype;
			return (
				(t.refresh = function () {
					var e = this,
						t = this._scrollElement === this._scrollElement.window ? bi : wi,
						o = "auto" === this._config.method ? t : this._config.method,
						r = o === wi ? this._getScrollTop() : 0;
					(this._offsets = []),
						(this._targets = []),
						(this._scrollHeight = this._getScrollHeight()),
						[].slice
							.call(document.querySelectorAll(this._selector))
							.map(function (t) {
								var e,
									n = m.getSelectorFromElement(t);
								if ((n && (e = document.querySelector(n)), e)) {
									var i = e.getBoundingClientRect();
									if (i.width || i.height) return [p(e)[o]().top + r, n];
								}
								return null;
							})
							.filter(function (t) {
								return t;
							})
							.sort(function (t, e) {
								return t[0] - e[0];
							})
							.forEach(function (t) {
								e._offsets.push(t[0]), e._targets.push(t[1]);
							});
				}),
				(t.dispose = function () {
					p.removeData(this._element, ri), p(this._scrollElement).off(si), (this._element = null), (this._scrollElement = null), (this._config = null), (this._selector = null), (this._offsets = null), (this._targets = null), (this._activeTarget = null), (this._scrollHeight = null);
				}),
				(t._getConfig = function (t) {
					if ("string" != typeof (t = l({}, li, "object" == typeof t && t ? t : {})).target) {
						var e = p(t.target).attr("id");
						e || ((e = m.getUID(oi)), p(t.target).attr("id", e)), (t.target = "#" + e);
					}
					return m.typeCheckConfig(oi, t, ci), t;
				}),
				(t._getScrollTop = function () {
					return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop;
				}),
				(t._getScrollHeight = function () {
					return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
				}),
				(t._getOffsetHeight = function () {
					return this._scrollElement === window ? window.innerHeight : this._scrollElement.getBoundingClientRect().height;
				}),
				(t._process = function () {
					var t = this._getScrollTop() + this._config.offset,
						e = this._getScrollHeight(),
						n = this._config.offset + e - this._getOffsetHeight();
					if ((this._scrollHeight !== e && this.refresh(), n <= t)) {
						var i = this._targets[this._targets.length - 1];
						this._activeTarget !== i && this._activate(i);
					} else {
						if (this._activeTarget && t < this._offsets[0] && 0 < this._offsets[0]) return (this._activeTarget = null), void this._clear();
						for (var o = this._offsets.length; o--; ) {
							this._activeTarget !== this._targets[o] && t >= this._offsets[o] && ("undefined" == typeof this._offsets[o + 1] || t < this._offsets[o + 1]) && this._activate(this._targets[o]);
						}
					}
				}),
				(t._activate = function (e) {
					(this._activeTarget = e), this._clear();
					var t = this._selector.split(",").map(function (t) {
							return t + '[data-target="' + e + '"],' + t + '[href="' + e + '"]';
						}),
						n = p([].slice.call(document.querySelectorAll(t.join(","))));
					n.hasClass(ui)
						? (n.closest(vi).find(Ei).addClass(fi), n.addClass(fi))
						: (n.addClass(fi),
						  n
								.parents(pi)
								.prev(mi + ", " + _i)
								.addClass(fi),
						  n.parents(pi).prev(gi).children(mi).addClass(fi)),
						p(this._scrollElement).trigger(hi.ACTIVATE, { relatedTarget: e });
				}),
				(t._clear = function () {
					[].slice
						.call(document.querySelectorAll(this._selector))
						.filter(function (t) {
							return t.classList.contains(fi);
						})
						.forEach(function (t) {
							return t.classList.remove(fi);
						});
				}),
				(n._jQueryInterface = function (e) {
					return this.each(function () {
						var t = p(this).data(ri);
						if ((t || ((t = new n(this, "object" == typeof e && e)), p(this).data(ri, t)), "string" == typeof e)) {
							if ("undefined" == typeof t[e]) throw new TypeError('No method named "' + e + '"');
							t[e]();
						}
					});
				}),
				s(n, null, [
					{
						key: "VERSION",
						get: function () {
							return "4.3.1";
						},
					},
					{
						key: "Default",
						get: function () {
							return li;
						},
					},
				]),
				n
			);
		})();
	p(window).on(hi.LOAD_DATA_API, function () {
		for (var t = [].slice.call(document.querySelectorAll(di)), e = t.length; e--; ) {
			var n = p(t[e]);
			Ci._jQueryInterface.call(n, n.data());
		}
	}),
		(p.fn[oi] = Ci._jQueryInterface),
		(p.fn[oi].Constructor = Ci),
		(p.fn[oi].noConflict = function () {
			return (p.fn[oi] = ai), Ci._jQueryInterface;
		});
	var Ti = "bs.tab",
		Si = "." + Ti,
		Di = p.fn.tab,
		Ii = { HIDE: "hide" + Si, HIDDEN: "hidden" + Si, SHOW: "show" + Si, SHOWN: "shown" + Si, CLICK_DATA_API: "click" + Si + ".data-api" },
		Ai = "dropdown-menu",
		Oi = "active",
		Ni = "disabled",
		ki = "fade",
		Li = "show",
		xi = ".dropdown",
		Pi = ".nav, .list-group",
		Hi = ".active",
		ji = "> li > .active",
		Ri = '[data-toggle="tab"], [data-toggle="pill"], [data-toggle="list"]',
		Fi = ".dropdown-toggle",
		Mi = "> .dropdown-menu .active",
		Wi = (function () {
			function i(t) {
				this._element = t;
			}
			var t = i.prototype;
			return (
				(t.show = function () {
					var n = this;
					if (!((this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && p(this._element).hasClass(Oi)) || p(this._element).hasClass(Ni))) {
						var t,
							i,
							e = p(this._element).closest(Pi)[0],
							o = m.getSelectorFromElement(this._element);
						if (e) {
							var r = "UL" === e.nodeName || "OL" === e.nodeName ? ji : Hi;
							i = (i = p.makeArray(p(e).find(r)))[i.length - 1];
						}
						var s = p.Event(Ii.HIDE, { relatedTarget: this._element }),
							a = p.Event(Ii.SHOW, { relatedTarget: i });
						if ((i && p(i).trigger(s), p(this._element).trigger(a), !a.isDefaultPrevented() && !s.isDefaultPrevented())) {
							o && (t = document.querySelector(o)), this._activate(this._element, e);
							var l = function () {
								var t = p.Event(Ii.HIDDEN, { relatedTarget: n._element }),
									e = p.Event(Ii.SHOWN, { relatedTarget: i });
								p(i).trigger(t), p(n._element).trigger(e);
							};
							t ? this._activate(t, t.parentNode, l) : l();
						}
					}
				}),
				(t.dispose = function () {
					p.removeData(this._element, Ti), (this._element = null);
				}),
				(t._activate = function (t, e, n) {
					var i = this,
						o = (!e || ("UL" !== e.nodeName && "OL" !== e.nodeName) ? p(e).children(Hi) : p(e).find(ji))[0],
						r = n && o && p(o).hasClass(ki),
						s = function () {
							return i._transitionComplete(t, o, n);
						};
					if (o && r) {
						var a = m.getTransitionDurationFromElement(o);
						p(o).removeClass(Li).one(m.TRANSITION_END, s).emulateTransitionEnd(a);
					} else s();
				}),
				(t._transitionComplete = function (t, e, n) {
					if (e) {
						p(e).removeClass(Oi);
						var i = p(e.parentNode).find(Mi)[0];
						i && p(i).removeClass(Oi), "tab" === e.getAttribute("role") && e.setAttribute("aria-selected", !1);
					}
					if ((p(t).addClass(Oi), "tab" === t.getAttribute("role") && t.setAttribute("aria-selected", !0), m.reflow(t), t.classList.contains(ki) && t.classList.add(Li), t.parentNode && p(t.parentNode).hasClass(Ai))) {
						var o = p(t).closest(xi)[0];
						if (o) {
							var r = [].slice.call(o.querySelectorAll(Fi));
							p(r).addClass(Oi);
						}
						t.setAttribute("aria-expanded", !0);
					}
					n && n();
				}),
				(i._jQueryInterface = function (n) {
					return this.each(function () {
						var t = p(this),
							e = t.data(Ti);
						if ((e || ((e = new i(this)), t.data(Ti, e)), "string" == typeof n)) {
							if ("undefined" == typeof e[n]) throw new TypeError('No method named "' + n + '"');
							e[n]();
						}
					});
				}),
				s(i, null, [
					{
						key: "VERSION",
						get: function () {
							return "4.3.1";
						},
					},
				]),
				i
			);
		})();
	p(document).on(Ii.CLICK_DATA_API, Ri, function (t) {
		t.preventDefault(), Wi._jQueryInterface.call(p(this), "show");
	}),
		(p.fn.tab = Wi._jQueryInterface),
		(p.fn.tab.Constructor = Wi),
		(p.fn.tab.noConflict = function () {
			return (p.fn.tab = Di), Wi._jQueryInterface;
		});
	var Ui = "toast",
		Bi = "bs.toast",
		qi = "." + Bi,
		Ki = p.fn[Ui],
		Qi = { CLICK_DISMISS: "click.dismiss" + qi, HIDE: "hide" + qi, HIDDEN: "hidden" + qi, SHOW: "show" + qi, SHOWN: "shown" + qi },
		Vi = "fade",
		Yi = "hide",
		zi = "show",
		Xi = "showing",
		Gi = { animation: "boolean", autohide: "boolean", delay: "number" },
		$i = { animation: !0, autohide: !0, delay: 500 },
		Ji = '[data-dismiss="toast"]',
		Zi = (function () {
			function i(t, e) {
				(this._element = t), (this._config = this._getConfig(e)), (this._timeout = null), this._setListeners();
			}
			var t = i.prototype;
			return (
				(t.show = function () {
					var t = this;
					p(this._element).trigger(Qi.SHOW), this._config.animation && this._element.classList.add(Vi);
					var e = function () {
						t._element.classList.remove(Xi), t._element.classList.add(zi), p(t._element).trigger(Qi.SHOWN), t._config.autohide && t.hide();
					};
					if ((this._element.classList.remove(Yi), this._element.classList.add(Xi), this._config.animation)) {
						var n = m.getTransitionDurationFromElement(this._element);
						p(this._element).one(m.TRANSITION_END, e).emulateTransitionEnd(n);
					} else e();
				}),
				(t.hide = function (t) {
					var e = this;
					this._element.classList.contains(zi) &&
						(p(this._element).trigger(Qi.HIDE),
						t
							? this._close()
							: (this._timeout = setTimeout(function () {
									e._close();
							  }, this._config.delay)));
				}),
				(t.dispose = function () {
					clearTimeout(this._timeout), (this._timeout = null), this._element.classList.contains(zi) && this._element.classList.remove(zi), p(this._element).off(Qi.CLICK_DISMISS), p.removeData(this._element, Bi), (this._element = null), (this._config = null);
				}),
				(t._getConfig = function (t) {
					return (t = l({}, $i, p(this._element).data(), "object" == typeof t && t ? t : {})), m.typeCheckConfig(Ui, t, this.constructor.DefaultType), t;
				}),
				(t._setListeners = function () {
					var t = this;
					p(this._element).on(Qi.CLICK_DISMISS, Ji, function () {
						return t.hide(!0);
					});
				}),
				(t._close = function () {
					var t = this,
						e = function () {
							t._element.classList.add(Yi), p(t._element).trigger(Qi.HIDDEN);
						};
					if ((this._element.classList.remove(zi), this._config.animation)) {
						var n = m.getTransitionDurationFromElement(this._element);
						p(this._element).one(m.TRANSITION_END, e).emulateTransitionEnd(n);
					} else e();
				}),
				(i._jQueryInterface = function (n) {
					return this.each(function () {
						var t = p(this),
							e = t.data(Bi);
						if ((e || ((e = new i(this, "object" == typeof n && n)), t.data(Bi, e)), "string" == typeof n)) {
							if ("undefined" == typeof e[n]) throw new TypeError('No method named "' + n + '"');
							e[n](this);
						}
					});
				}),
				s(i, null, [
					{
						key: "VERSION",
						get: function () {
							return "4.3.1";
						},
					},
					{
						key: "DefaultType",
						get: function () {
							return Gi;
						},
					},
					{
						key: "Default",
						get: function () {
							return $i;
						},
					},
				]),
				i
			);
		})();
	(p.fn[Ui] = Zi._jQueryInterface),
		(p.fn[Ui].Constructor = Zi),
		(p.fn[Ui].noConflict = function () {
			return (p.fn[Ui] = Ki), Zi._jQueryInterface;
		}),
		(function () {
			if ("undefined" == typeof p) throw new TypeError("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");
			var t = p.fn.jquery.split(" ")[0].split(".");
			if ((t[0] < 2 && t[1] < 9) || (1 === t[0] && 9 === t[1] && t[2] < 1) || 4 <= t[0]) throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0");
		})(),
		(t.Util = m),
		(t.Alert = g),
		(t.Button = k),
		(t.Carousel = at),
		(t.Collapse = Ct),
		(t.Dropdown = Xe),
		(t.Modal = gn),
		(t.Popover = ii),
		(t.Scrollspy = Ci),
		(t.Tab = Wi),
		(t.Toast = Zi),
		(t.Tooltip = qn),
		Object.defineProperty(t, "__esModule", { value: !0 });
});

/*!
 * Bootstrap-select v1.13.14 (https://developer.snapappointments.com/bootstrap-select)
 *
 * Copyright 2012-2020 SnapAppointments, LLC
 * Licensed under MIT (https://github.com/snapappointments/bootstrap-select/blob/master/LICENSE)
 */

!(function (e, t) {
	void 0 === e && void 0 !== window && (e = window),
		"function" == typeof define && define.amd
			? define(["jquery"], function (e) {
					return t(e);
			  })
			: "object" == typeof module && module.exports
			? (module.exports = t(require("jquery")))
			: t(e.jQuery);
})(this, function (e) {
	!(function (z) {
		"use strict";
		var d = ["sanitize", "whiteList", "sanitizeFn"],
			r = ["background", "cite", "href", "itemtype", "longdesc", "poster", "src", "xlink:href"],
			e = { "*": ["class", "dir", "id", "lang", "role", "tabindex", "style", /^aria-[\w-]*$/i], a: ["target", "href", "title", "rel"], area: [], b: [], br: [], col: [], code: [], div: [], em: [], hr: [], h1: [], h2: [], h3: [], h4: [], h5: [], h6: [], i: [], img: ["src", "alt", "title", "width", "height"], li: [], ol: [], p: [], pre: [], s: [], small: [], span: [], sub: [], sup: [], strong: [], u: [], ul: [] },
			l = /^(?:(?:https?|mailto|ftp|tel|file):|[^&:/?#]*(?:[/?#]|$))/gi,
			a = /^data:(?:image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|video\/(?:mpeg|mp4|ogg|webm)|audio\/(?:mp3|oga|ogg|opus));base64,[a-z0-9+/]+=*$/i;
		function v(e, t) {
			var i = e.nodeName.toLowerCase();
			if (-1 !== z.inArray(i, t)) return -1 === z.inArray(i, r) || Boolean(e.nodeValue.match(l) || e.nodeValue.match(a));
			for (
				var s = z(t).filter(function (e, t) {
						return t instanceof RegExp;
					}),
					n = 0,
					o = s.length;
				n < o;
				n++
			)
				if (i.match(s[n])) return !0;
			return !1;
		}
		function P(e, t, i) {
			if (i && "function" == typeof i) return i(e);
			for (var s = Object.keys(t), n = 0, o = e.length; n < o; n++)
				for (var r = e[n].querySelectorAll("*"), l = 0, a = r.length; l < a; l++) {
					var c = r[l],
						d = c.nodeName.toLowerCase();
					if (-1 !== s.indexOf(d))
						for (var h = [].slice.call(c.attributes), p = [].concat(t["*"] || [], t[d] || []), u = 0, f = h.length; u < f; u++) {
							var m = h[u];
							v(m, p) || c.removeAttribute(m.nodeName);
						}
					else c.parentNode.removeChild(c);
				}
		}
		"classList" in document.createElement("_") ||
			(function (e) {
				if ("Element" in e) {
					var t = "classList",
						i = "prototype",
						s = e.Element[i],
						n = Object,
						o = function () {
							var i = z(this);
							return {
								add: function (e) {
									return (e = Array.prototype.slice.call(arguments).join(" ")), i.addClass(e);
								},
								remove: function (e) {
									return (e = Array.prototype.slice.call(arguments).join(" ")), i.removeClass(e);
								},
								toggle: function (e, t) {
									return i.toggleClass(e, t);
								},
								contains: function (e) {
									return i.hasClass(e);
								},
							};
						};
					if (n.defineProperty) {
						var r = { get: o, enumerable: !0, configurable: !0 };
						try {
							n.defineProperty(s, t, r);
						} catch (e) {
							(void 0 !== e.number && -2146823252 !== e.number) || ((r.enumerable = !1), n.defineProperty(s, t, r));
						}
					} else n[i].__defineGetter__ && s.__defineGetter__(t, o);
				}
			})(window);
		var t,
			c,
			i = document.createElement("_");
		if ((i.classList.add("c1", "c2"), !i.classList.contains("c2"))) {
			var s = DOMTokenList.prototype.add,
				n = DOMTokenList.prototype.remove;
			(DOMTokenList.prototype.add = function () {
				Array.prototype.forEach.call(arguments, s.bind(this));
			}),
				(DOMTokenList.prototype.remove = function () {
					Array.prototype.forEach.call(arguments, n.bind(this));
				});
		}
		if ((i.classList.toggle("c3", !1), i.classList.contains("c3"))) {
			var o = DOMTokenList.prototype.toggle;
			DOMTokenList.prototype.toggle = function (e, t) {
				return 1 in arguments && !this.contains(e) == !t ? t : o.call(this, e);
			};
		}
		function h(e) {
			if (null == this) throw new TypeError();
			var t = String(this);
			if (e && "[object RegExp]" == c.call(e)) throw new TypeError();
			var i = t.length,
				s = String(e),
				n = s.length,
				o = 1 < arguments.length ? arguments[1] : void 0,
				r = o ? Number(o) : 0;
			r != r && (r = 0);
			var l = Math.min(Math.max(r, 0), i);
			if (i < n + l) return !1;
			for (var a = -1; ++a < n; ) if (t.charCodeAt(l + a) != s.charCodeAt(a)) return !1;
			return !0;
		}
		function O(e, t) {
			var i,
				s = e.selectedOptions,
				n = [];
			if (t) {
				for (var o = 0, r = s.length; o < r; o++) (i = s[o]).disabled || ("OPTGROUP" === i.parentNode.tagName && i.parentNode.disabled) || n.push(i);
				return n;
			}
			return s;
		}
		function T(e, t) {
			for (var i, s = [], n = t || e.selectedOptions, o = 0, r = n.length; o < r; o++) (i = n[o]).disabled || ("OPTGROUP" === i.parentNode.tagName && i.parentNode.disabled) || s.push(i.value);
			return e.multiple ? s : s.length ? s[0] : null;
		}
		(i = null),
			String.prototype.startsWith ||
				((t = (function () {
					try {
						var e = {},
							t = Object.defineProperty,
							i = t(e, e, e) && t;
					} catch (e) {}
					return i;
				})()),
				(c = {}.toString),
				t ? t(String.prototype, "startsWith", { value: h, configurable: !0, writable: !0 }) : (String.prototype.startsWith = h)),
			Object.keys ||
				(Object.keys = function (e, t, i) {
					for (t in ((i = []), e)) i.hasOwnProperty.call(e, t) && i.push(t);
					return i;
				}),
			HTMLSelectElement &&
				!HTMLSelectElement.prototype.hasOwnProperty("selectedOptions") &&
				Object.defineProperty(HTMLSelectElement.prototype, "selectedOptions", {
					get: function () {
						return this.querySelectorAll(":checked");
					},
				});
		var p = { useDefault: !1, _set: z.valHooks.select.set };
		z.valHooks.select.set = function (e, t) {
			return t && !p.useDefault && z(e).data("selected", !0), p._set.apply(this, arguments);
		};
		var A = null,
			u = (function () {
				try {
					return new Event("change"), !0;
				} catch (e) {
					return !1;
				}
			})();
		function k(e, t, i, s) {
			for (var n = ["display", "subtext", "tokens"], o = !1, r = 0; r < n.length; r++) {
				var l = n[r],
					a = e[l];
				if (a && ((a = a.toString()), "display" === l && (a = a.replace(/<[^>]+>/g, "")), s && (a = w(a)), (a = a.toUpperCase()), (o = "contains" === i ? 0 <= a.indexOf(t) : a.startsWith(t)))) break;
			}
			return o;
		}
		function L(e) {
			return parseInt(e, 10) || 0;
		}
		z.fn.triggerNative = function (e) {
			var t,
				i = this[0];
			i.dispatchEvent ? (u ? (t = new Event(e, { bubbles: !0 })) : (t = document.createEvent("Event")).initEvent(e, !0, !1), i.dispatchEvent(t)) : i.fireEvent ? (((t = document.createEventObject()).eventType = e), i.fireEvent("on" + e, t)) : this.trigger(e);
		};
		var f = {
				"\xc0": "A",
				"\xc1": "A",
				"\xc2": "A",
				"\xc3": "A",
				"\xc4": "A",
				"\xc5": "A",
				"\xe0": "a",
				"\xe1": "a",
				"\xe2": "a",
				"\xe3": "a",
				"\xe4": "a",
				"\xe5": "a",
				"\xc7": "C",
				"\xe7": "c",
				"\xd0": "D",
				"\xf0": "d",
				"\xc8": "E",
				"\xc9": "E",
				"\xca": "E",
				"\xcb": "E",
				"\xe8": "e",
				"\xe9": "e",
				"\xea": "e",
				"\xeb": "e",
				"\xcc": "I",
				"\xcd": "I",
				"\xce": "I",
				"\xcf": "I",
				"\xec": "i",
				"\xed": "i",
				"\xee": "i",
				"\xef": "i",
				"\xd1": "N",
				"\xf1": "n",
				"\xd2": "O",
				"\xd3": "O",
				"\xd4": "O",
				"\xd5": "O",
				"\xd6": "O",
				"\xd8": "O",
				"\xf2": "o",
				"\xf3": "o",
				"\xf4": "o",
				"\xf5": "o",
				"\xf6": "o",
				"\xf8": "o",
				"\xd9": "U",
				"\xda": "U",
				"\xdb": "U",
				"\xdc": "U",
				"\xf9": "u",
				"\xfa": "u",
				"\xfb": "u",
				"\xfc": "u",
				"\xdd": "Y",
				"\xfd": "y",
				"\xff": "y",
				"\xc6": "Ae",
				"\xe6": "ae",
				"\xde": "Th",
				"\xfe": "th",
				"\xdf": "ss",
				"\u0100": "A",
				"\u0102": "A",
				"\u0104": "A",
				"\u0101": "a",
				"\u0103": "a",
				"\u0105": "a",
				"\u0106": "C",
				"\u0108": "C",
				"\u010a": "C",
				"\u010c": "C",
				"\u0107": "c",
				"\u0109": "c",
				"\u010b": "c",
				"\u010d": "c",
				"\u010e": "D",
				"\u0110": "D",
				"\u010f": "d",
				"\u0111": "d",
				"\u0112": "E",
				"\u0114": "E",
				"\u0116": "E",
				"\u0118": "E",
				"\u011a": "E",
				"\u0113": "e",
				"\u0115": "e",
				"\u0117": "e",
				"\u0119": "e",
				"\u011b": "e",
				"\u011c": "G",
				"\u011e": "G",
				"\u0120": "G",
				"\u0122": "G",
				"\u011d": "g",
				"\u011f": "g",
				"\u0121": "g",
				"\u0123": "g",
				"\u0124": "H",
				"\u0126": "H",
				"\u0125": "h",
				"\u0127": "h",
				"\u0128": "I",
				"\u012a": "I",
				"\u012c": "I",
				"\u012e": "I",
				"\u0130": "I",
				"\u0129": "i",
				"\u012b": "i",
				"\u012d": "i",
				"\u012f": "i",
				"\u0131": "i",
				"\u0134": "J",
				"\u0135": "j",
				"\u0136": "K",
				"\u0137": "k",
				"\u0138": "k",
				"\u0139": "L",
				"\u013b": "L",
				"\u013d": "L",
				"\u013f": "L",
				"\u0141": "L",
				"\u013a": "l",
				"\u013c": "l",
				"\u013e": "l",
				"\u0140": "l",
				"\u0142": "l",
				"\u0143": "N",
				"\u0145": "N",
				"\u0147": "N",
				"\u014a": "N",
				"\u0144": "n",
				"\u0146": "n",
				"\u0148": "n",
				"\u014b": "n",
				"\u014c": "O",
				"\u014e": "O",
				"\u0150": "O",
				"\u014d": "o",
				"\u014f": "o",
				"\u0151": "o",
				"\u0154": "R",
				"\u0156": "R",
				"\u0158": "R",
				"\u0155": "r",
				"\u0157": "r",
				"\u0159": "r",
				"\u015a": "S",
				"\u015c": "S",
				"\u015e": "S",
				"\u0160": "S",
				"\u015b": "s",
				"\u015d": "s",
				"\u015f": "s",
				"\u0161": "s",
				"\u0162": "T",
				"\u0164": "T",
				"\u0166": "T",
				"\u0163": "t",
				"\u0165": "t",
				"\u0167": "t",
				"\u0168": "U",
				"\u016a": "U",
				"\u016c": "U",
				"\u016e": "U",
				"\u0170": "U",
				"\u0172": "U",
				"\u0169": "u",
				"\u016b": "u",
				"\u016d": "u",
				"\u016f": "u",
				"\u0171": "u",
				"\u0173": "u",
				"\u0174": "W",
				"\u0175": "w",
				"\u0176": "Y",
				"\u0177": "y",
				"\u0178": "Y",
				"\u0179": "Z",
				"\u017b": "Z",
				"\u017d": "Z",
				"\u017a": "z",
				"\u017c": "z",
				"\u017e": "z",
				"\u0132": "IJ",
				"\u0133": "ij",
				"\u0152": "Oe",
				"\u0153": "oe",
				"\u0149": "'n",
				"\u017f": "s",
			},
			m = /[\xc0-\xd6\xd8-\xf6\xf8-\xff\u0100-\u017f]/g,
			g = RegExp("[\\u0300-\\u036f\\ufe20-\\ufe2f\\u20d0-\\u20ff\\u1ab0-\\u1aff\\u1dc0-\\u1dff]", "g");
		function b(e) {
			return f[e];
		}
		function w(e) {
			return (e = e.toString()) && e.replace(m, b).replace(g, "");
		}
		var I,
			x,
			y,
			$,
			S =
				((I = { "&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#x27;", "`": "&#x60;" }),
				(x = "(?:" + Object.keys(I).join("|") + ")"),
				(y = RegExp(x)),
				($ = RegExp(x, "g")),
				function (e) {
					return (e = null == e ? "" : "" + e), y.test(e) ? e.replace($, E) : e;
				});
		function E(e) {
			return I[e];
		}
		var C = { 32: " ", 48: "0", 49: "1", 50: "2", 51: "3", 52: "4", 53: "5", 54: "6", 55: "7", 56: "8", 57: "9", 59: ";", 65: "A", 66: "B", 67: "C", 68: "D", 69: "E", 70: "F", 71: "G", 72: "H", 73: "I", 74: "J", 75: "K", 76: "L", 77: "M", 78: "N", 79: "O", 80: "P", 81: "Q", 82: "R", 83: "S", 84: "T", 85: "U", 86: "V", 87: "W", 88: "X", 89: "Y", 90: "Z", 96: "0", 97: "1", 98: "2", 99: "3", 100: "4", 101: "5", 102: "6", 103: "7", 104: "8", 105: "9" },
			N = 27,
			D = 13,
			H = 32,
			W = 9,
			B = 38,
			M = 40,
			R = { success: !1, major: "3" };
		try {
			(R.full = (z.fn.dropdown.Constructor.VERSION || "").split(" ")[0].split(".")), (R.major = R.full[0]), (R.success = !0);
		} catch (e) {}
		var U = 0,
			j = ".bs.select",
			V = { DISABLED: "disabled", DIVIDER: "divider", SHOW: "open", DROPUP: "dropup", MENU: "dropdown-menu", MENURIGHT: "dropdown-menu-right", MENULEFT: "dropdown-menu-left", BUTTONCLASS: "btn-default", POPOVERHEADER: "popover-title", ICONBASE: "glyphicon", TICKICON: "glyphicon-ok" },
			F = { MENU: "." + V.MENU },
			_ = { span: document.createElement("span"), i: document.createElement("i"), subtext: document.createElement("small"), a: document.createElement("a"), li: document.createElement("li"), whitespace: document.createTextNode("\xa0"), fragment: document.createDocumentFragment() };
		_.a.setAttribute("role", "option"), "4" === R.major && (_.a.className = "dropdown-item"), (_.subtext.className = "text-muted"), (_.text = _.span.cloneNode(!1)), (_.text.className = "text"), (_.checkMark = _.span.cloneNode(!1));
		var G = new RegExp(B + "|" + M),
			q = new RegExp("^" + W + "$|" + N),
			K = {
				li: function (e, t, i) {
					var s = _.li.cloneNode(!1);
					return e && (1 === e.nodeType || 11 === e.nodeType ? s.appendChild(e) : (s.innerHTML = e)), void 0 !== t && "" !== t && (s.className = t), null != i && s.classList.add("optgroup-" + i), s;
				},
				a: function (e, t, i) {
					var s = _.a.cloneNode(!0);
					return e && (11 === e.nodeType ? s.appendChild(e) : s.insertAdjacentHTML("beforeend", e)), void 0 !== t && "" !== t && s.classList.add.apply(s.classList, t.split(" ")), i && s.setAttribute("style", i), s;
				},
				text: function (e, t) {
					var i,
						s,
						n = _.text.cloneNode(!1);
					if (e.content) n.innerHTML = e.content;
					else {
						if (((n.textContent = e.text), e.icon)) {
							var o = _.whitespace.cloneNode(!1);
							((s = (!0 === t ? _.i : _.span).cloneNode(!1)).className = this.options.iconBase + " " + e.icon), _.fragment.appendChild(s), _.fragment.appendChild(o);
						}
						e.subtext && (((i = _.subtext.cloneNode(!1)).textContent = e.subtext), n.appendChild(i));
					}
					if (!0 === t) for (; 0 < n.childNodes.length; ) _.fragment.appendChild(n.childNodes[0]);
					else _.fragment.appendChild(n);
					return _.fragment;
				},
				label: function (e) {
					var t,
						i,
						s = _.text.cloneNode(!1);
					if (((s.innerHTML = e.display), e.icon)) {
						var n = _.whitespace.cloneNode(!1);
						((i = _.span.cloneNode(!1)).className = this.options.iconBase + " " + e.icon), _.fragment.appendChild(i), _.fragment.appendChild(n);
					}
					return e.subtext && (((t = _.subtext.cloneNode(!1)).textContent = e.subtext), s.appendChild(t)), _.fragment.appendChild(s), _.fragment;
				},
			},
			Y = function (e, t) {
				var i = this;
				p.useDefault || ((z.valHooks.select.set = p._set), (p.useDefault = !0)),
					(this.$element = z(e)),
					(this.$newElement = null),
					(this.$button = null),
					(this.$menu = null),
					(this.options = t),
					(this.selectpicker = {
						main: {},
						search: {},
						current: {},
						view: {},
						isSearching: !1,
						keydown: {
							keyHistory: "",
							resetKeyHistory: {
								start: function () {
									return setTimeout(function () {
										i.selectpicker.keydown.keyHistory = "";
									}, 800);
								},
							},
						},
					}),
					(this.sizeInfo = {}),
					null === this.options.title && (this.options.title = this.$element.attr("title"));
				var s = this.options.windowPadding;
				"number" == typeof s && (this.options.windowPadding = [s, s, s, s]), (this.val = Y.prototype.val), (this.render = Y.prototype.render), (this.refresh = Y.prototype.refresh), (this.setStyle = Y.prototype.setStyle), (this.selectAll = Y.prototype.selectAll), (this.deselectAll = Y.prototype.deselectAll), (this.destroy = Y.prototype.destroy), (this.remove = Y.prototype.remove), (this.show = Y.prototype.show), (this.hide = Y.prototype.hide), this.init();
			};
		function Z(e) {
			var l,
				a = arguments,
				c = e;
			if (([].shift.apply(a), !R.success)) {
				try {
					R.full = (z.fn.dropdown.Constructor.VERSION || "").split(" ")[0].split(".");
				} catch (e) {
					Y.BootstrapVersion ? (R.full = Y.BootstrapVersion.split(" ")[0].split(".")) : ((R.full = [R.major, "0", "0"]), console.warn("There was an issue retrieving Bootstrap's version. Ensure Bootstrap is being loaded before bootstrap-select and there is no namespace collision. If loading Bootstrap asynchronously, the version may need to be manually specified via $.fn.selectpicker.Constructor.BootstrapVersion.", e));
				}
				(R.major = R.full[0]), (R.success = !0);
			}
			if ("4" === R.major) {
				var t = [];
				Y.DEFAULTS.style === V.BUTTONCLASS && t.push({ name: "style", className: "BUTTONCLASS" }), Y.DEFAULTS.iconBase === V.ICONBASE && t.push({ name: "iconBase", className: "ICONBASE" }), Y.DEFAULTS.tickIcon === V.TICKICON && t.push({ name: "tickIcon", className: "TICKICON" }), (V.DIVIDER = "dropdown-divider"), (V.SHOW = "show"), (V.BUTTONCLASS = "btn-light"), (V.POPOVERHEADER = "popover-header"), (V.ICONBASE = ""), (V.TICKICON = "bs-ok-default");
				for (var i = 0; i < t.length; i++) {
					e = t[i];
					Y.DEFAULTS[e.name] = V[e.className];
				}
			}
			var s = this.each(function () {
				var e = z(this);
				if (e.is("select")) {
					var t = e.data("selectpicker"),
						i = "object" == typeof c && c;
					if (t) {
						if (i) for (var s in i) i.hasOwnProperty(s) && (t.options[s] = i[s]);
					} else {
						var n = e.data();
						for (var o in n) n.hasOwnProperty(o) && -1 !== z.inArray(o, d) && delete n[o];
						var r = z.extend({}, Y.DEFAULTS, z.fn.selectpicker.defaults || {}, n, i);
						(r.template = z.extend({}, Y.DEFAULTS.template, z.fn.selectpicker.defaults ? z.fn.selectpicker.defaults.template : {}, n.template, i.template)), e.data("selectpicker", (t = new Y(this, r)));
					}
					"string" == typeof c && (l = t[c] instanceof Function ? t[c].apply(t, a) : t.options[c]);
				}
			});
			return void 0 !== l ? l : s;
		}
		(Y.VERSION = "1.13.14"),
			(Y.DEFAULTS = {
				noneSelectedText: "Nothing selected",
				noneResultsText: "No results matched {0}",
				countSelectedText: function (e, t) {
					return 1 == e ? "{0} item selected" : "{0} items selected";
				},
				maxOptionsText: function (e, t) {
					return [1 == e ? "Limit reached ({n} item max)" : "Limit reached ({n} items max)", 1 == t ? "Group limit reached ({n} item max)" : "Group limit reached ({n} items max)"];
				},
				selectAllText: "Select All",
				deselectAllText: "Deselect All",
				doneButton: !1,
				doneButtonText: "Close",
				multipleSeparator: ", ",
				styleBase: "btn",
				style: V.BUTTONCLASS,
				size: "auto",
				title: null,
				selectedTextFormat: "values",
				width: !1,
				container: !1,
				hideDisabled: !1,
				showSubtext: !1,
				showIcon: !0,
				showContent: !0,
				dropupAuto: !0,
				header: !1,
				liveSearch: !1,
				liveSearchPlaceholder: null,
				liveSearchNormalize: !1,
				liveSearchStyle: "contains",
				actionsBox: !1,
				iconBase: V.ICONBASE,
				tickIcon: V.TICKICON,
				showTick: !1,
				template: { caret: '<span class="caret"></span>' },
				maxOptions: !1,
				mobile: !1,
				selectOnTab: !1,
				dropdownAlignRight: !1,
				windowPadding: 0,
				virtualScroll: 600,
				display: !1,
				sanitize: !0,
				sanitizeFn: null,
				whiteList: e,
			}),
			(Y.prototype = {
				constructor: Y,
				init: function () {
					var i = this,
						e = this.$element.attr("id");
					U++,
						(this.selectId = "bs-select-" + U),
						this.$element[0].classList.add("bs-select-hidden"),
						(this.multiple = this.$element.prop("multiple")),
						(this.autofocus = this.$element.prop("autofocus")),
						this.$element[0].classList.contains("show-tick") && (this.options.showTick = !0),
						(this.$newElement = this.createDropdown()),
						this.buildData(),
						this.$element.after(this.$newElement).prependTo(this.$newElement),
						(this.$button = this.$newElement.children("button")),
						(this.$menu = this.$newElement.children(F.MENU)),
						(this.$menuInner = this.$menu.children(".inner")),
						(this.$searchbox = this.$menu.find("input")),
						this.$element[0].classList.remove("bs-select-hidden"),
						!0 === this.options.dropdownAlignRight && this.$menu[0].classList.add(V.MENURIGHT),
						void 0 !== e && this.$button.attr("data-id", e),
						this.checkDisabled(),
						this.clickListener(),
						this.options.liveSearch ? (this.liveSearchListener(), (this.focusedParent = this.$searchbox[0])) : (this.focusedParent = this.$menuInner[0]),
						this.setStyle(),
						this.render(),
						this.setWidth(),
						this.options.container
							? this.selectPosition()
							: this.$element.on("hide" + j, function () {
									if (i.isVirtual()) {
										var e = i.$menuInner[0],
											t = e.firstChild.cloneNode(!1);
										e.replaceChild(t, e.firstChild), (e.scrollTop = 0);
									}
							  }),
						this.$menu.data("this", this),
						this.$newElement.data("this", this),
						this.options.mobile && this.mobile(),
						this.$newElement.on({
							"hide.bs.dropdown": function (e) {
								i.$element.trigger("hide" + j, e);
							},
							"hidden.bs.dropdown": function (e) {
								i.$element.trigger("hidden" + j, e);
							},
							"show.bs.dropdown": function (e) {
								i.$element.trigger("show" + j, e);
							},
							"shown.bs.dropdown": function (e) {
								i.$element.trigger("shown" + j, e);
							},
						}),
						i.$element[0].hasAttribute("required") &&
							this.$element.on("invalid" + j, function () {
								i.$button[0].classList.add("bs-invalid"),
									i.$element
										.on("shown" + j + ".invalid", function () {
											i.$element.val(i.$element.val()).off("shown" + j + ".invalid");
										})
										.on("rendered" + j, function () {
											this.validity.valid && i.$button[0].classList.remove("bs-invalid"), i.$element.off("rendered" + j);
										}),
									i.$button.on("blur" + j, function () {
										i.$element.trigger("focus").trigger("blur"), i.$button.off("blur" + j);
									});
							}),
						setTimeout(function () {
							i.buildList(), i.$element.trigger("loaded" + j);
						});
				},
				createDropdown: function () {
					var e = this.multiple || this.options.showTick ? " show-tick" : "",
						t = this.multiple ? ' aria-multiselectable="true"' : "",
						i = "",
						s = this.autofocus ? " autofocus" : "";
					R.major < 4 && this.$element.parent().hasClass("input-group") && (i = " input-group-btn");
					var n,
						o = "",
						r = "",
						l = "",
						a = "";
					return (
						this.options.header && (o = '<div class="' + V.POPOVERHEADER + '"><button type="button" class="close" aria-hidden="true">&times;</button>' + this.options.header + "</div>"),
						this.options.liveSearch && (r = '<div class="bs-searchbox"><input type="search" class="form-control" autocomplete="off"' + (null === this.options.liveSearchPlaceholder ? "" : ' placeholder="' + S(this.options.liveSearchPlaceholder) + '"') + ' role="combobox" aria-label="Search" aria-controls="' + this.selectId + '" aria-autocomplete="list"></div>'),
						this.multiple && this.options.actionsBox && (l = '<div class="bs-actionsbox"><div class="btn-group btn-group-sm btn-block"><button type="button" class="actions-btn bs-select-all btn ' + V.BUTTONCLASS + '">' + this.options.selectAllText + '</button><button type="button" class="actions-btn bs-deselect-all btn ' + V.BUTTONCLASS + '">' + this.options.deselectAllText + "</button></div></div>"),
						this.multiple && this.options.doneButton && (a = '<div class="bs-donebutton"><div class="btn-group btn-block"><button type="button" class="btn btn-sm ' + V.BUTTONCLASS + '">' + this.options.doneButtonText + "</button></div></div>"),
						(n =
							'<div class="dropdown bootstrap-select' +
							e +
							i +
							'"><button type="button" class="' +
							this.options.styleBase +
							' dropdown-toggle" ' +
							("static" === this.options.display ? 'data-display="static"' : "") +
							'data-toggle="dropdown"' +
							s +
							' role="combobox" aria-owns="' +
							this.selectId +
							'" aria-haspopup="listbox" aria-expanded="false"><div class="filter-option"><div class="filter-option-inner"><div class="filter-option-inner-inner"></div></div> </div>' +
							("4" === R.major ? "" : '<span class="bs-caret">' + this.options.template.caret + "</span>") +
							'</button><div class="' +
							V.MENU +
							" " +
							("4" === R.major ? "" : V.SHOW) +
							'">' +
							o +
							r +
							l +
							'<div class="inner ' +
							V.SHOW +
							'" role="listbox" id="' +
							this.selectId +
							'" tabindex="-1" ' +
							t +
							'><ul class="' +
							V.MENU +
							" inner " +
							("4" === R.major ? V.SHOW : "") +
							'" role="presentation"></ul></div>' +
							a +
							"</div></div>"),
						z(n)
					);
				},
				setPositionData: function () {
					this.selectpicker.view.canHighlight = [];
					for (var e = (this.selectpicker.view.size = 0); e < this.selectpicker.current.data.length; e++) {
						var t = this.selectpicker.current.data[e],
							i = !0;
						"divider" === t.type ? ((i = !1), (t.height = this.sizeInfo.dividerHeight)) : "optgroup-label" === t.type ? ((i = !1), (t.height = this.sizeInfo.dropdownHeaderHeight)) : (t.height = this.sizeInfo.liHeight), t.disabled && (i = !1), this.selectpicker.view.canHighlight.push(i), i && (this.selectpicker.view.size++, (t.posinset = this.selectpicker.view.size)), (t.position = (0 === e ? 0 : this.selectpicker.current.data[e - 1].position) + t.height);
					}
				},
				isVirtual: function () {
					return (!1 !== this.options.virtualScroll && this.selectpicker.main.elements.length >= this.options.virtualScroll) || !0 === this.options.virtualScroll;
				},
				createView: function (A, e, t) {
					var L,
						N,
						D = this,
						i = 0,
						H = [];
					if (((this.selectpicker.isSearching = A), (this.selectpicker.current = A ? this.selectpicker.search : this.selectpicker.main), this.setPositionData(), e))
						if (t) i = this.$menuInner[0].scrollTop;
						else if (!D.multiple) {
							var s = D.$element[0],
								n = (s.options[s.selectedIndex] || {}).liIndex;
							if ("number" == typeof n && !1 !== D.options.size) {
								var o = D.selectpicker.main.data[n],
									r = o && o.position;
								r && (i = r - (D.sizeInfo.menuInnerHeight + D.sizeInfo.liHeight) / 2);
							}
						}
					function l(e, t) {
						var i,
							s,
							n,
							o,
							r,
							l,
							a,
							c,
							d = D.selectpicker.current.elements.length,
							h = [],
							p = !0,
							u = D.isVirtual();
						(D.selectpicker.view.scrollTop = e), (i = Math.ceil((D.sizeInfo.menuInnerHeight / D.sizeInfo.liHeight) * 1.5)), (s = Math.round(d / i) || 1);
						for (var f = 0; f < s; f++) {
							var m = (f + 1) * i;
							if ((f === s - 1 && (m = d), (h[f] = [f * i + (f ? 1 : 0), m]), !d)) break;
							void 0 === r && e - 1 <= D.selectpicker.current.data[m - 1].position - D.sizeInfo.menuInnerHeight && (r = f);
						}
						if (
							(void 0 === r && (r = 0),
							(l = [D.selectpicker.view.position0, D.selectpicker.view.position1]),
							(n = Math.max(0, r - 1)),
							(o = Math.min(s - 1, r + 1)),
							(D.selectpicker.view.position0 = !1 === u ? 0 : Math.max(0, h[n][0]) || 0),
							(D.selectpicker.view.position1 = !1 === u ? d : Math.min(d, h[o][1]) || 0),
							(a = l[0] !== D.selectpicker.view.position0 || l[1] !== D.selectpicker.view.position1),
							void 0 !== D.activeIndex && ((N = D.selectpicker.main.elements[D.prevActiveIndex]), (H = D.selectpicker.main.elements[D.activeIndex]), (L = D.selectpicker.main.elements[D.selectedIndex]), t && (D.activeIndex !== D.selectedIndex && D.defocusItem(H), (D.activeIndex = void 0)), D.activeIndex && D.activeIndex !== D.selectedIndex && D.defocusItem(L)),
							void 0 !== D.prevActiveIndex && D.prevActiveIndex !== D.activeIndex && D.prevActiveIndex !== D.selectedIndex && D.defocusItem(N),
							(t || a) &&
								((c = D.selectpicker.view.visibleElements ? D.selectpicker.view.visibleElements.slice() : []),
								(D.selectpicker.view.visibleElements = !1 === u ? D.selectpicker.current.elements : D.selectpicker.current.elements.slice(D.selectpicker.view.position0, D.selectpicker.view.position1)),
								D.setOptionStatus(),
								(A || (!1 === u && t)) &&
									(p = !(function (e, i) {
										return (
											e.length === i.length &&
											e.every(function (e, t) {
												return e === i[t];
											})
										);
									})(c, D.selectpicker.view.visibleElements)),
								(t || !0 === u) && p))
						) {
							var v,
								g,
								b = D.$menuInner[0],
								w = document.createDocumentFragment(),
								I = b.firstChild.cloneNode(!1),
								x = D.selectpicker.view.visibleElements,
								k = [];
							b.replaceChild(I, b.firstChild);
							f = 0;
							for (var y = x.length; f < y; f++) {
								var $,
									S,
									E = x[f];
								D.options.sanitize && ($ = E.lastChild) && (S = D.selectpicker.current.data[f + D.selectpicker.view.position0]) && S.content && !S.sanitized && (k.push($), (S.sanitized = !0)), w.appendChild(E);
							}
							if ((D.options.sanitize && k.length && P(k, D.options.whiteList, D.options.sanitizeFn), !0 === u ? ((v = 0 === D.selectpicker.view.position0 ? 0 : D.selectpicker.current.data[D.selectpicker.view.position0 - 1].position), (g = D.selectpicker.view.position1 > d - 1 ? 0 : D.selectpicker.current.data[d - 1].position - D.selectpicker.current.data[D.selectpicker.view.position1 - 1].position), (b.firstChild.style.marginTop = v + "px"), (b.firstChild.style.marginBottom = g + "px")) : ((b.firstChild.style.marginTop = 0), (b.firstChild.style.marginBottom = 0)), b.firstChild.appendChild(w), !0 === u && D.sizeInfo.hasScrollBar)) {
								var C = b.firstChild.offsetWidth;
								if (t && C < D.sizeInfo.menuInnerInnerWidth && D.sizeInfo.totalMenuWidth > D.sizeInfo.selectWidth) b.firstChild.style.minWidth = D.sizeInfo.menuInnerInnerWidth + "px";
								else if (C > D.sizeInfo.menuInnerInnerWidth) {
									D.$menu[0].style.minWidth = 0;
									var O = b.firstChild.offsetWidth;
									O > D.sizeInfo.menuInnerInnerWidth && ((D.sizeInfo.menuInnerInnerWidth = O), (b.firstChild.style.minWidth = D.sizeInfo.menuInnerInnerWidth + "px")), (D.$menu[0].style.minWidth = "");
								}
							}
						}
						if (((D.prevActiveIndex = D.activeIndex), D.options.liveSearch)) {
							if (A && t) {
								var z,
									T = 0;
								D.selectpicker.view.canHighlight[T] || (T = 1 + D.selectpicker.view.canHighlight.slice(1).indexOf(!0)), (z = D.selectpicker.view.visibleElements[T]), D.defocusItem(D.selectpicker.view.currentActive), (D.activeIndex = (D.selectpicker.current.data[T] || {}).index), D.focusItem(z);
							}
						} else D.$menuInner.trigger("focus");
					}
					l(i, !0),
						this.$menuInner.off("scroll.createView").on("scroll.createView", function (e, t) {
							D.noScroll || l(this.scrollTop, t), (D.noScroll = !1);
						}),
						z(window)
							.off("resize" + j + "." + this.selectId + ".createView")
							.on("resize" + j + "." + this.selectId + ".createView", function () {
								D.$newElement.hasClass(V.SHOW) && l(D.$menuInner[0].scrollTop);
							});
				},
				focusItem: function (e, t, i) {
					if (e) {
						t = t || this.selectpicker.main.data[this.activeIndex];
						var s = e.firstChild;
						s && (s.setAttribute("aria-setsize", this.selectpicker.view.size), s.setAttribute("aria-posinset", t.posinset), !0 !== i && (this.focusedParent.setAttribute("aria-activedescendant", s.id), e.classList.add("active"), s.classList.add("active")));
					}
				},
				defocusItem: function (e) {
					e && (e.classList.remove("active"), e.firstChild && e.firstChild.classList.remove("active"));
				},
				setPlaceholder: function () {
					var e = !1;
					if (this.options.title && !this.multiple) {
						this.selectpicker.view.titleOption || (this.selectpicker.view.titleOption = document.createElement("option")), (e = !0);
						var t = this.$element[0],
							i = !1,
							s = !this.selectpicker.view.titleOption.parentNode;
						if (s) (this.selectpicker.view.titleOption.className = "bs-title-option"), (this.selectpicker.view.titleOption.value = ""), (i = void 0 === z(t.options[t.selectedIndex]).attr("selected") && void 0 === this.$element.data("selected"));
						(!s && 0 === this.selectpicker.view.titleOption.index) || t.insertBefore(this.selectpicker.view.titleOption, t.firstChild), i && (t.selectedIndex = 0);
					}
					return e;
				},
				buildData: function () {
					var p = ':not([hidden]):not([data-hidden="true"])',
						u = [],
						f = 0,
						e = this.setPlaceholder() ? 1 : 0;
					this.options.hideDisabled && (p += ":not(:disabled)");
					var t = this.$element[0].querySelectorAll("select > *" + p);
					function m(e) {
						var t = u[u.length - 1];
						(t && "divider" === t.type && (t.optID || e.optID)) || (((e = e || {}).type = "divider"), u.push(e));
					}
					function v(e, t) {
						if ((((t = t || {}).divider = "true" === e.getAttribute("data-divider")), t.divider)) m({ optID: t.optID });
						else {
							var i = u.length,
								s = e.style.cssText,
								n = s ? S(s) : "",
								o = (e.className || "") + (t.optgroupClass || "");
							t.optID && (o = "opt " + o), (t.optionClass = o.trim()), (t.inlineStyle = n), (t.text = e.textContent), (t.content = e.getAttribute("data-content")), (t.tokens = e.getAttribute("data-tokens")), (t.subtext = e.getAttribute("data-subtext")), (t.icon = e.getAttribute("data-icon")), (e.liIndex = i), (t.display = t.content || t.text), (t.type = "option"), (t.index = i), (t.option = e), (t.selected = !!e.selected), (t.disabled = t.disabled || !!e.disabled), u.push(t);
						}
					}
					function i(e, t) {
						var i = t[e],
							s = t[e - 1],
							n = t[e + 1],
							o = i.querySelectorAll("option" + p);
						if (o.length) {
							var r,
								l,
								a = { display: S(i.label), subtext: i.getAttribute("data-subtext"), icon: i.getAttribute("data-icon"), type: "optgroup-label", optgroupClass: " " + (i.className || "") };
							f++, s && m({ optID: f }), (a.optID = f), u.push(a);
							for (var c = 0, d = o.length; c < d; c++) {
								var h = o[c];
								0 === c && (l = (r = u.length - 1) + d), v(h, { headerIndex: r, lastIndex: l, optID: a.optID, optgroupClass: a.optgroupClass, disabled: i.disabled });
							}
							n && m({ optID: f });
						}
					}
					for (var s = t.length; e < s; e++) {
						var n = t[e];
						"OPTGROUP" !== n.tagName ? v(n, {}) : i(e, t);
					}
					this.selectpicker.main.data = this.selectpicker.current.data = u;
				},
				buildList: function () {
					var s = this,
						e = this.selectpicker.main.data,
						n = [],
						o = 0;
					function t(e) {
						var t,
							i = 0;
						switch (e.type) {
							case "divider":
								t = K.li(!1, V.DIVIDER, e.optID ? e.optID + "div" : void 0);
								break;
							case "option":
								(t = K.li(K.a(K.text.call(s, e), e.optionClass, e.inlineStyle), "", e.optID)).firstChild && (t.firstChild.id = s.selectId + "-" + e.index);
								break;
							case "optgroup-label":
								t = K.li(K.label.call(s, e), "dropdown-header" + e.optgroupClass, e.optID);
						}
						n.push(t), e.display && (i += e.display.length), e.subtext && (i += e.subtext.length), e.icon && (i += 1), o < i && ((o = i), (s.selectpicker.view.widestOption = n[n.length - 1]));
					}
					(!s.options.showTick && !s.multiple) || _.checkMark.parentNode || ((_.checkMark.className = this.options.iconBase + " " + s.options.tickIcon + " check-mark"), _.a.appendChild(_.checkMark));
					for (var i = e.length, r = 0; r < i; r++) {
						t(e[r]);
					}
					this.selectpicker.main.elements = this.selectpicker.current.elements = n;
				},
				findLis: function () {
					return this.$menuInner.find(".inner > li");
				},
				render: function () {
					var e,
						t = this,
						i = this.$element[0],
						s = this.setPlaceholder() && 0 === i.selectedIndex,
						n = O(i, this.options.hideDisabled),
						o = n.length,
						r = this.$button[0],
						l = r.querySelector(".filter-option-inner-inner"),
						a = document.createTextNode(this.options.multipleSeparator),
						c = _.fragment.cloneNode(!1),
						d = !1;
					if ((r.classList.toggle("bs-placeholder", t.multiple ? !o : !T(i, n)), this.tabIndex(), "static" === this.options.selectedTextFormat)) c = K.text.call(this, { text: this.options.title }, !0);
					else if (!1 === (this.multiple && -1 !== this.options.selectedTextFormat.indexOf("count") && 1 < o && ((1 < (e = this.options.selectedTextFormat.split(">")).length && o > e[1]) || (1 === e.length && 2 <= o)))) {
						if (!s) {
							for (var h = 0; h < o && h < 50; h++) {
								var p = n[h],
									u = this.selectpicker.main.data[p.liIndex],
									f = {};
								this.multiple && 0 < h && c.appendChild(a.cloneNode(!1)), p.title ? (f.text = p.title) : u && (u.content && t.options.showContent ? ((f.content = u.content.toString()), (d = !0)) : (t.options.showIcon && (f.icon = u.icon), t.options.showSubtext && !t.multiple && u.subtext && (f.subtext = " " + u.subtext), (f.text = p.textContent.trim()))), c.appendChild(K.text.call(this, f, !0));
							}
							49 < o && c.appendChild(document.createTextNode("..."));
						}
					} else {
						var m = ':not([hidden]):not([data-hidden="true"]):not([data-divider="true"])';
						this.options.hideDisabled && (m += ":not(:disabled)");
						var v = this.$element[0].querySelectorAll("select > option" + m + ", optgroup" + m + " option" + m).length,
							g = "function" == typeof this.options.countSelectedText ? this.options.countSelectedText(o, v) : this.options.countSelectedText;
						c = K.text.call(this, { text: g.replace("{0}", o.toString()).replace("{1}", v.toString()) }, !0);
					}
					if ((null == this.options.title && (this.options.title = this.$element.attr("title")), c.childNodes.length || (c = K.text.call(this, { text: void 0 !== this.options.title ? this.options.title : this.options.noneSelectedText }, !0)), (r.title = c.textContent.replace(/<[^>]*>?/g, "").trim()), this.options.sanitize && d && P([c], t.options.whiteList, t.options.sanitizeFn), (l.innerHTML = ""), l.appendChild(c), R.major < 4 && this.$newElement[0].classList.contains("bs3-has-addon"))) {
						var b = r.querySelector(".filter-expand"),
							w = l.cloneNode(!0);
						(w.className = "filter-expand"), b ? r.replaceChild(w, b) : r.appendChild(w);
					}
					this.$element.trigger("rendered" + j);
				},
				setStyle: function (e, t) {
					var i,
						s = this.$button[0],
						n = this.$newElement[0],
						o = this.options.style.trim();
					this.$element.attr("class") && this.$newElement.addClass(this.$element.attr("class").replace(/selectpicker|mobile-device|bs-select-hidden|validate\[.*\]/gi, "")),
						R.major < 4 && (n.classList.add("bs3"), n.parentNode.classList.contains("input-group") && (n.previousElementSibling || n.nextElementSibling) && (n.previousElementSibling || n.nextElementSibling).classList.contains("input-group-addon") && n.classList.add("bs3-has-addon")),
						(i = e ? e.trim() : o),
						"add" == t ? i && s.classList.add.apply(s.classList, i.split(" ")) : "remove" == t ? i && s.classList.remove.apply(s.classList, i.split(" ")) : (o && s.classList.remove.apply(s.classList, o.split(" ")), i && s.classList.add.apply(s.classList, i.split(" ")));
				},
				liHeight: function (e) {
					if (e || (!1 !== this.options.size && !Object.keys(this.sizeInfo).length)) {
						var t = document.createElement("div"),
							i = document.createElement("div"),
							s = document.createElement("div"),
							n = document.createElement("ul"),
							o = document.createElement("li"),
							r = document.createElement("li"),
							l = document.createElement("li"),
							a = document.createElement("a"),
							c = document.createElement("span"),
							d = this.options.header && 0 < this.$menu.find("." + V.POPOVERHEADER).length ? this.$menu.find("." + V.POPOVERHEADER)[0].cloneNode(!0) : null,
							h = this.options.liveSearch ? document.createElement("div") : null,
							p = this.options.actionsBox && this.multiple && 0 < this.$menu.find(".bs-actionsbox").length ? this.$menu.find(".bs-actionsbox")[0].cloneNode(!0) : null,
							u = this.options.doneButton && this.multiple && 0 < this.$menu.find(".bs-donebutton").length ? this.$menu.find(".bs-donebutton")[0].cloneNode(!0) : null,
							f = this.$element.find("option")[0];
						if (
							((this.sizeInfo.selectWidth = this.$newElement[0].offsetWidth),
							(c.className = "text"),
							(a.className = "dropdown-item " + (f ? f.className : "")),
							(t.className = this.$menu[0].parentNode.className + " " + V.SHOW),
							(t.style.width = 0),
							"auto" === this.options.width && (i.style.minWidth = 0),
							(i.className = V.MENU + " " + V.SHOW),
							(s.className = "inner " + V.SHOW),
							(n.className = V.MENU + " inner " + ("4" === R.major ? V.SHOW : "")),
							(o.className = V.DIVIDER),
							(r.className = "dropdown-header"),
							c.appendChild(document.createTextNode("\u200b")),
							a.appendChild(c),
							l.appendChild(a),
							r.appendChild(c.cloneNode(!0)),
							this.selectpicker.view.widestOption && n.appendChild(this.selectpicker.view.widestOption.cloneNode(!0)),
							n.appendChild(l),
							n.appendChild(o),
							n.appendChild(r),
							d && i.appendChild(d),
							h)
						) {
							var m = document.createElement("input");
							(h.className = "bs-searchbox"), (m.className = "form-control"), h.appendChild(m), i.appendChild(h);
						}
						p && i.appendChild(p), s.appendChild(n), i.appendChild(s), u && i.appendChild(u), t.appendChild(i), document.body.appendChild(t);
						var v,
							g = l.offsetHeight,
							b = r ? r.offsetHeight : 0,
							w = d ? d.offsetHeight : 0,
							I = h ? h.offsetHeight : 0,
							x = p ? p.offsetHeight : 0,
							k = u ? u.offsetHeight : 0,
							y = z(o).outerHeight(!0),
							$ = !!window.getComputedStyle && window.getComputedStyle(i),
							S = i.offsetWidth,
							E = $ ? null : z(i),
							C = { vert: L($ ? $.paddingTop : E.css("paddingTop")) + L($ ? $.paddingBottom : E.css("paddingBottom")) + L($ ? $.borderTopWidth : E.css("borderTopWidth")) + L($ ? $.borderBottomWidth : E.css("borderBottomWidth")), horiz: L($ ? $.paddingLeft : E.css("paddingLeft")) + L($ ? $.paddingRight : E.css("paddingRight")) + L($ ? $.borderLeftWidth : E.css("borderLeftWidth")) + L($ ? $.borderRightWidth : E.css("borderRightWidth")) },
							O = { vert: C.vert + L($ ? $.marginTop : E.css("marginTop")) + L($ ? $.marginBottom : E.css("marginBottom")) + 2, horiz: C.horiz + L($ ? $.marginLeft : E.css("marginLeft")) + L($ ? $.marginRight : E.css("marginRight")) + 2 };
						(s.style.overflowY = "scroll"), (v = i.offsetWidth - S), document.body.removeChild(t), (this.sizeInfo.liHeight = g), (this.sizeInfo.dropdownHeaderHeight = b), (this.sizeInfo.headerHeight = w), (this.sizeInfo.searchHeight = I), (this.sizeInfo.actionsHeight = x), (this.sizeInfo.doneButtonHeight = k), (this.sizeInfo.dividerHeight = y), (this.sizeInfo.menuPadding = C), (this.sizeInfo.menuExtras = O), (this.sizeInfo.menuWidth = S), (this.sizeInfo.menuInnerInnerWidth = S - C.horiz), (this.sizeInfo.totalMenuWidth = this.sizeInfo.menuWidth), (this.sizeInfo.scrollBarWidth = v), (this.sizeInfo.selectHeight = this.$newElement[0].offsetHeight), this.setPositionData();
					}
				},
				getSelectPosition: function () {
					var e,
						t = z(window),
						i = this.$newElement.offset(),
						s = z(this.options.container);
					this.options.container && s.length && !s.is("body") ? (((e = s.offset()).top += parseInt(s.css("borderTopWidth"))), (e.left += parseInt(s.css("borderLeftWidth")))) : (e = { top: 0, left: 0 });
					var n = this.options.windowPadding;
					(this.sizeInfo.selectOffsetTop = i.top - e.top - t.scrollTop()), (this.sizeInfo.selectOffsetBot = t.height() - this.sizeInfo.selectOffsetTop - this.sizeInfo.selectHeight - e.top - n[2]), (this.sizeInfo.selectOffsetLeft = i.left - e.left - t.scrollLeft()), (this.sizeInfo.selectOffsetRight = t.width() - this.sizeInfo.selectOffsetLeft - this.sizeInfo.selectWidth - e.left - n[1]), (this.sizeInfo.selectOffsetTop -= n[0]), (this.sizeInfo.selectOffsetLeft -= n[3]);
				},
				setMenuSize: function (e) {
					this.getSelectPosition();
					var t,
						i,
						s,
						n,
						o,
						r,
						l,
						a,
						c = this.sizeInfo.selectWidth,
						d = this.sizeInfo.liHeight,
						h = this.sizeInfo.headerHeight,
						p = this.sizeInfo.searchHeight,
						u = this.sizeInfo.actionsHeight,
						f = this.sizeInfo.doneButtonHeight,
						m = this.sizeInfo.dividerHeight,
						v = this.sizeInfo.menuPadding,
						g = 0;
					if ((this.options.dropupAuto && ((l = d * this.selectpicker.current.elements.length + v.vert), (a = this.sizeInfo.selectOffsetTop - this.sizeInfo.selectOffsetBot > this.sizeInfo.menuExtras.vert && l + this.sizeInfo.menuExtras.vert + 50 > this.sizeInfo.selectOffsetBot), !0 === this.selectpicker.isSearching && (a = this.selectpicker.dropup), this.$newElement.toggleClass(V.DROPUP, a), (this.selectpicker.dropup = a)), "auto" === this.options.size))
						(n = 3 < this.selectpicker.current.elements.length ? 3 * this.sizeInfo.liHeight + this.sizeInfo.menuExtras.vert - 2 : 0), (i = this.sizeInfo.selectOffsetBot - this.sizeInfo.menuExtras.vert), (s = n + h + p + u + f), (r = Math.max(n - v.vert, 0)), this.$newElement.hasClass(V.DROPUP) && (i = this.sizeInfo.selectOffsetTop - this.sizeInfo.menuExtras.vert), (t = (o = i) - h - p - u - f - v.vert);
					else if (this.options.size && "auto" != this.options.size && this.selectpicker.current.elements.length > this.options.size) {
						for (var b = 0; b < this.options.size; b++) "divider" === this.selectpicker.current.data[b].type && g++;
						(t = (i = d * this.options.size + g * m + v.vert) - v.vert), (o = i + h + p + u + f), (s = r = "");
					}
					this.$menu.css({ "max-height": o + "px", overflow: "hidden", "min-height": s + "px" }),
						this.$menuInner.css({ "max-height": t + "px", "overflow-y": "auto", "min-height": r + "px" }),
						(this.sizeInfo.menuInnerHeight = Math.max(t, 1)),
						this.selectpicker.current.data.length && this.selectpicker.current.data[this.selectpicker.current.data.length - 1].position > this.sizeInfo.menuInnerHeight && ((this.sizeInfo.hasScrollBar = !0), (this.sizeInfo.totalMenuWidth = this.sizeInfo.menuWidth + this.sizeInfo.scrollBarWidth)),
						"auto" === this.options.dropdownAlignRight && this.$menu.toggleClass(V.MENURIGHT, this.sizeInfo.selectOffsetLeft > this.sizeInfo.selectOffsetRight && this.sizeInfo.selectOffsetRight < this.sizeInfo.totalMenuWidth - c),
						this.dropdown && this.dropdown._popper && this.dropdown._popper.update();
				},
				setSize: function (e) {
					if ((this.liHeight(e), this.options.header && this.$menu.css("padding-top", 0), !1 !== this.options.size)) {
						var t = this,
							i = z(window);
						this.setMenuSize(),
							this.options.liveSearch &&
								this.$searchbox.off("input.setMenuSize propertychange.setMenuSize").on("input.setMenuSize propertychange.setMenuSize", function () {
									return t.setMenuSize();
								}),
							"auto" === this.options.size
								? i.off("resize" + j + "." + this.selectId + ".setMenuSize scroll" + j + "." + this.selectId + ".setMenuSize").on("resize" + j + "." + this.selectId + ".setMenuSize scroll" + j + "." + this.selectId + ".setMenuSize", function () {
										return t.setMenuSize();
								  })
								: this.options.size && "auto" != this.options.size && this.selectpicker.current.elements.length > this.options.size && i.off("resize" + j + "." + this.selectId + ".setMenuSize scroll" + j + "." + this.selectId + ".setMenuSize");
					}
					this.createView(!1, !0, e);
				},
				setWidth: function () {
					var i = this;
					"auto" === this.options.width
						? requestAnimationFrame(function () {
								i.$menu.css("min-width", "0"),
									i.$element.on("loaded" + j, function () {
										i.liHeight(), i.setMenuSize();
										var e = i.$newElement.clone().appendTo("body"),
											t = e.css("width", "auto").children("button").outerWidth();
										e.remove(), (i.sizeInfo.selectWidth = Math.max(i.sizeInfo.totalMenuWidth, t)), i.$newElement.css("width", i.sizeInfo.selectWidth + "px");
									});
						  })
						: "fit" === this.options.width
						? (this.$menu.css("min-width", ""), this.$newElement.css("width", "").addClass("fit-width"))
						: this.options.width
						? (this.$menu.css("min-width", ""), this.$newElement.css("width", this.options.width))
						: (this.$menu.css("min-width", ""), this.$newElement.css("width", "")),
						this.$newElement.hasClass("fit-width") && "fit" !== this.options.width && this.$newElement[0].classList.remove("fit-width");
				},
				selectPosition: function () {
					this.$bsContainer = z('<div class="bs-container" />');
					function e(e) {
						var t = {},
							i = r.options.display || (!!z.fn.dropdown.Constructor.Default && z.fn.dropdown.Constructor.Default.display);
						r.$bsContainer.addClass(e.attr("class").replace(/form-control|fit-width/gi, "")).toggleClass(V.DROPUP, e.hasClass(V.DROPUP)), (s = e.offset()), l.is("body") ? (n = { top: 0, left: 0 }) : (((n = l.offset()).top += parseInt(l.css("borderTopWidth")) - l.scrollTop()), (n.left += parseInt(l.css("borderLeftWidth")) - l.scrollLeft())), (o = e.hasClass(V.DROPUP) ? 0 : e[0].offsetHeight), (R.major < 4 || "static" === i) && ((t.top = s.top - n.top + o), (t.left = s.left - n.left)), (t.width = e[0].offsetWidth), r.$bsContainer.css(t);
					}
					var s,
						n,
						o,
						r = this,
						l = z(this.options.container);
					this.$button.on("click.bs.dropdown.data-api", function () {
						r.isDisabled() || (e(r.$newElement), r.$bsContainer.appendTo(r.options.container).toggleClass(V.SHOW, !r.$button.hasClass(V.SHOW)).append(r.$menu));
					}),
						z(window)
							.off("resize" + j + "." + this.selectId + " scroll" + j + "." + this.selectId)
							.on("resize" + j + "." + this.selectId + " scroll" + j + "." + this.selectId, function () {
								r.$newElement.hasClass(V.SHOW) && e(r.$newElement);
							}),
						this.$element.on("hide" + j, function () {
							r.$menu.data("height", r.$menu.height()), r.$bsContainer.detach();
						});
				},
				setOptionStatus: function (e) {
					var t = this;
					if (((t.noScroll = !1), t.selectpicker.view.visibleElements && t.selectpicker.view.visibleElements.length))
						for (var i = 0; i < t.selectpicker.view.visibleElements.length; i++) {
							var s = t.selectpicker.current.data[i + t.selectpicker.view.position0],
								n = s.option;
							n && (!0 !== e && t.setDisabled(s.index, s.disabled), t.setSelected(s.index, n.selected));
						}
				},
				setSelected: function (e, t) {
					var i,
						s,
						n = this.selectpicker.main.elements[e],
						o = this.selectpicker.main.data[e],
						r = void 0 !== this.activeIndex,
						l = this.activeIndex === e || (t && !this.multiple && !r);
					(o.selected = t), (s = n.firstChild), t && (this.selectedIndex = e), n.classList.toggle("selected", t), l ? (this.focusItem(n, o), (this.selectpicker.view.currentActive = n), (this.activeIndex = e)) : this.defocusItem(n), s && (s.classList.toggle("selected", t), t ? s.setAttribute("aria-selected", !0) : this.multiple ? s.setAttribute("aria-selected", !1) : s.removeAttribute("aria-selected")), l || r || !t || void 0 === this.prevActiveIndex || ((i = this.selectpicker.main.elements[this.prevActiveIndex]), this.defocusItem(i));
				},
				setDisabled: function (e, t) {
					var i,
						s = this.selectpicker.main.elements[e];
					(this.selectpicker.main.data[e].disabled = t), (i = s.firstChild), s.classList.toggle(V.DISABLED, t), i && ("4" === R.major && i.classList.toggle(V.DISABLED, t), t ? (i.setAttribute("aria-disabled", t), i.setAttribute("tabindex", -1)) : (i.removeAttribute("aria-disabled"), i.setAttribute("tabindex", 0)));
				},
				isDisabled: function () {
					return this.$element[0].disabled;
				},
				checkDisabled: function () {
					this.isDisabled() ? (this.$newElement[0].classList.add(V.DISABLED), this.$button.addClass(V.DISABLED).attr("tabindex", -1).attr("aria-disabled", !0)) : (this.$button[0].classList.contains(V.DISABLED) && (this.$newElement[0].classList.remove(V.DISABLED), this.$button.removeClass(V.DISABLED).attr("aria-disabled", !1)), -1 != this.$button.attr("tabindex") || this.$element.data("tabindex") || this.$button.removeAttr("tabindex"));
				},
				tabIndex: function () {
					this.$element.data("tabindex") !== this.$element.attr("tabindex") && -98 !== this.$element.attr("tabindex") && "-98" !== this.$element.attr("tabindex") && (this.$element.data("tabindex", this.$element.attr("tabindex")), this.$button.attr("tabindex", this.$element.data("tabindex"))), this.$element.attr("tabindex", -98);
				},
				clickListener: function () {
					var C = this,
						t = z(document);
					function e() {
						C.options.liveSearch ? C.$searchbox.trigger("focus") : C.$menuInner.trigger("focus");
					}
					function i() {
						C.dropdown && C.dropdown._popper && C.dropdown._popper.state.isCreated ? e() : requestAnimationFrame(i);
					}
					t.data("spaceSelect", !1),
						this.$button.on("keyup", function (e) {
							/(32)/.test(e.keyCode.toString(10)) && t.data("spaceSelect") && (e.preventDefault(), t.data("spaceSelect", !1));
						}),
						this.$newElement.on("show.bs.dropdown", function () {
							3 < R.major && !C.dropdown && ((C.dropdown = C.$button.data("bs.dropdown")), (C.dropdown._menu = C.$menu[0]));
						}),
						this.$button.on("click.bs.dropdown.data-api", function () {
							C.$newElement.hasClass(V.SHOW) || C.setSize();
						}),
						this.$element.on("shown" + j, function () {
							C.$menuInner[0].scrollTop !== C.selectpicker.view.scrollTop && (C.$menuInner[0].scrollTop = C.selectpicker.view.scrollTop), 3 < R.major ? requestAnimationFrame(i) : e();
						}),
						this.$menuInner.on("mouseenter", "li a", function (e) {
							var t = this.parentElement,
								i = C.isVirtual() ? C.selectpicker.view.position0 : 0,
								s = Array.prototype.indexOf.call(t.parentElement.children, t),
								n = C.selectpicker.current.data[s + i];
							C.focusItem(t, n, !0);
						}),
						this.$menuInner.on("click", "li a", function (e, t) {
							var i = z(this),
								s = C.$element[0],
								n = C.isVirtual() ? C.selectpicker.view.position0 : 0,
								o = C.selectpicker.current.data[i.parent().index() + n],
								r = o.index,
								l = T(s),
								a = s.selectedIndex,
								c = s.options[a],
								d = !0;
							if ((C.multiple && 1 !== C.options.maxOptions && e.stopPropagation(), e.preventDefault(), !C.isDisabled() && !i.parent().hasClass(V.DISABLED))) {
								var h = o.option,
									p = z(h),
									u = h.selected,
									f = p.parent("optgroup"),
									m = f.find("option"),
									v = C.options.maxOptions,
									g = f.data("maxOptions") || !1;
								if ((r === C.activeIndex && (t = !0), t || ((C.prevActiveIndex = C.activeIndex), (C.activeIndex = void 0)), C.multiple)) {
									if (((h.selected = !u), C.setSelected(r, !u), i.trigger("blur"), !1 !== v || !1 !== g)) {
										var b = v < O(s).length,
											w = g < f.find("option:selected").length;
										if ((v && b) || (g && w))
											if (v && 1 == v) (s.selectedIndex = -1), (h.selected = !0), C.setOptionStatus(!0);
											else if (g && 1 == g) {
												for (var I = 0; I < m.length; I++) {
													var x = m[I];
													(x.selected = !1), C.setSelected(x.liIndex, !1);
												}
												(h.selected = !0), C.setSelected(r, !0);
											} else {
												var k = "string" == typeof C.options.maxOptionsText ? [C.options.maxOptionsText, C.options.maxOptionsText] : C.options.maxOptionsText,
													y = "function" == typeof k ? k(v, g) : k,
													$ = y[0].replace("{n}", v),
													S = y[1].replace("{n}", g),
													E = z('<div class="notify"></div>');
												y[2] && (($ = $.replace("{var}", y[2][1 < v ? 0 : 1])), (S = S.replace("{var}", y[2][1 < g ? 0 : 1]))),
													(h.selected = !1),
													C.$menu.append(E),
													v && b && (E.append(z("<div>" + $ + "</div>")), (d = !1), C.$element.trigger("maxReached" + j)),
													g && w && (E.append(z("<div>" + S + "</div>")), (d = !1), C.$element.trigger("maxReachedGrp" + j)),
													setTimeout(function () {
														C.setSelected(r, !1);
													}, 10),
													E[0].classList.add("fadeOut"),
													setTimeout(function () {
														E.remove();
													}, 1050);
											}
									}
								} else c && (c.selected = !1), (h.selected = !0), C.setSelected(r, !0);
								!C.multiple || (C.multiple && 1 === C.options.maxOptions) ? C.$button.trigger("focus") : C.options.liveSearch && C.$searchbox.trigger("focus"), d && ((!C.multiple && a === s.selectedIndex) || ((A = [h.index, p.prop("selected"), l]), C.$element.triggerNative("change")));
							}
						}),
						this.$menu.on("click", "li." + V.DISABLED + " a, ." + V.POPOVERHEADER + ", ." + V.POPOVERHEADER + " :not(.close)", function (e) {
							e.currentTarget == this && (e.preventDefault(), e.stopPropagation(), C.options.liveSearch && !z(e.target).hasClass("close") ? C.$searchbox.trigger("focus") : C.$button.trigger("focus"));
						}),
						this.$menuInner.on("click", ".divider, .dropdown-header", function (e) {
							e.preventDefault(), e.stopPropagation(), C.options.liveSearch ? C.$searchbox.trigger("focus") : C.$button.trigger("focus");
						}),
						this.$menu.on("click", "." + V.POPOVERHEADER + " .close", function () {
							C.$button.trigger("click");
						}),
						this.$searchbox.on("click", function (e) {
							e.stopPropagation();
						}),
						this.$menu.on("click", ".actions-btn", function (e) {
							C.options.liveSearch ? C.$searchbox.trigger("focus") : C.$button.trigger("focus"), e.preventDefault(), e.stopPropagation(), z(this).hasClass("bs-select-all") ? C.selectAll() : C.deselectAll();
						}),
						this.$element
							.on("change" + j, function () {
								C.render(), C.$element.trigger("changed" + j, A), (A = null);
							})
							.on("focus" + j, function () {
								C.options.mobile || C.$button.trigger("focus");
							});
				},
				liveSearchListener: function () {
					var u = this,
						f = document.createElement("li");
					this.$button.on("click.bs.dropdown.data-api", function () {
						u.$searchbox.val() && u.$searchbox.val("");
					}),
						this.$searchbox.on("click.bs.dropdown.data-api focus.bs.dropdown.data-api touchend.bs.dropdown.data-api", function (e) {
							e.stopPropagation();
						}),
						this.$searchbox.on("input propertychange", function () {
							var e = u.$searchbox.val();
							if (((u.selectpicker.search.elements = []), (u.selectpicker.search.data = []), e)) {
								var t = [],
									i = e.toUpperCase(),
									s = {},
									n = [],
									o = u._searchStyle(),
									r = u.options.liveSearchNormalize;
								r && (i = w(i));
								for (var l = 0; l < u.selectpicker.main.data.length; l++) {
									var a = u.selectpicker.main.data[l];
									s[l] || (s[l] = k(a, i, o, r)), s[l] && void 0 !== a.headerIndex && -1 === n.indexOf(a.headerIndex) && (0 < a.headerIndex && ((s[a.headerIndex - 1] = !0), n.push(a.headerIndex - 1)), (s[a.headerIndex] = !0), n.push(a.headerIndex), (s[a.lastIndex + 1] = !0)), s[l] && "optgroup-label" !== a.type && n.push(l);
								}
								l = 0;
								for (var c = n.length; l < c; l++) {
									var d = n[l],
										h = n[l - 1],
										p = ((a = u.selectpicker.main.data[d]), u.selectpicker.main.data[h]);
									("divider" !== a.type || ("divider" === a.type && p && "divider" !== p.type && c - 1 !== l)) && (u.selectpicker.search.data.push(a), t.push(u.selectpicker.main.elements[d]));
								}
								(u.activeIndex = void 0), (u.noScroll = !0), u.$menuInner.scrollTop(0), (u.selectpicker.search.elements = t), u.createView(!0), t.length || ((f.className = "no-results"), (f.innerHTML = u.options.noneResultsText.replace("{0}", '"' + S(e) + '"')), u.$menuInner[0].firstChild.appendChild(f));
							} else u.$menuInner.scrollTop(0), u.createView(!1);
						});
				},
				_searchStyle: function () {
					return this.options.liveSearchStyle || "contains";
				},
				val: function (e) {
					var t = this.$element[0];
					if (void 0 === e) return this.$element.val();
					var i = T(t);
					if (((A = [null, null, i]), this.$element.val(e).trigger("changed" + j, A), this.$newElement.hasClass(V.SHOW)))
						if (this.multiple) this.setOptionStatus(!0);
						else {
							var s = (t.options[t.selectedIndex] || {}).liIndex;
							"number" == typeof s && (this.setSelected(this.selectedIndex, !1), this.setSelected(s, !0));
						}
					return this.render(), (A = null), this.$element;
				},
				changeAll: function (e) {
					if (this.multiple) {
						void 0 === e && (e = !0);
						var t = this.$element[0],
							i = 0,
							s = 0,
							n = T(t);
						t.classList.add("bs-select-hidden");
						for (var o = 0, r = this.selectpicker.current.data, l = r.length; o < l; o++) {
							var a = r[o],
								c = a.option;
							c && !a.disabled && "divider" !== a.type && (a.selected && i++, !0 === (c.selected = e) && s++);
						}
						t.classList.remove("bs-select-hidden"), i !== s && (this.setOptionStatus(), (A = [null, null, n]), this.$element.triggerNative("change"));
					}
				},
				selectAll: function () {
					return this.changeAll(!0);
				},
				deselectAll: function () {
					return this.changeAll(!1);
				},
				toggle: function (e) {
					(e = e || window.event) && e.stopPropagation(), this.$button.trigger("click.bs.dropdown.data-api");
				},
				keydown: function (e) {
					var t,
						i,
						s,
						n,
						o,
						r = z(this),
						l = r.hasClass("dropdown-toggle"),
						a = (l ? r.closest(".dropdown") : r.closest(F.MENU)).data("this"),
						c = a.findLis(),
						d = !1,
						h = e.which === W && !l && !a.options.selectOnTab,
						p = G.test(e.which) || h,
						u = a.$menuInner[0].scrollTop,
						f = !0 === a.isVirtual() ? a.selectpicker.view.position0 : 0;
					if (!(112 <= e.which && e.which <= 123))
						if (!(i = a.$newElement.hasClass(V.SHOW)) && (p || (48 <= e.which && e.which <= 57) || (96 <= e.which && e.which <= 105) || (65 <= e.which && e.which <= 90)) && (a.$button.trigger("click.bs.dropdown.data-api"), a.options.liveSearch)) a.$searchbox.trigger("focus");
						else {
							if ((e.which === N && i && (e.preventDefault(), a.$button.trigger("click.bs.dropdown.data-api").trigger("focus")), p)) {
								if (!c.length) return;
								-1 !== (t = (s = a.selectpicker.main.elements[a.activeIndex]) ? Array.prototype.indexOf.call(s.parentElement.children, s) : -1) && a.defocusItem(s), e.which === B ? (-1 !== t && t--, t + f < 0 && (t += c.length), a.selectpicker.view.canHighlight[t + f] || (-1 === (t = a.selectpicker.view.canHighlight.slice(0, t + f).lastIndexOf(!0) - f) && (t = c.length - 1))) : (e.which !== M && !h) || (++t + f >= a.selectpicker.view.canHighlight.length && (t = 0), a.selectpicker.view.canHighlight[t + f] || (t = t + 1 + a.selectpicker.view.canHighlight.slice(t + f + 1).indexOf(!0))), e.preventDefault();
								var m = f + t;
								e.which === B ? (0 === f && t === c.length - 1 ? ((a.$menuInner[0].scrollTop = a.$menuInner[0].scrollHeight), (m = a.selectpicker.current.elements.length - 1)) : (d = (o = (n = a.selectpicker.current.data[m]).position - n.height) < u)) : (e.which !== M && !h) || (0 === t ? (m = a.$menuInner[0].scrollTop = 0) : (d = u < (o = (n = a.selectpicker.current.data[m]).position - a.sizeInfo.menuInnerHeight))), (s = a.selectpicker.current.elements[m]), (a.activeIndex = a.selectpicker.current.data[m].index), a.focusItem(s), (a.selectpicker.view.currentActive = s), d && (a.$menuInner[0].scrollTop = o), a.options.liveSearch ? a.$searchbox.trigger("focus") : r.trigger("focus");
							} else if ((!r.is("input") && !q.test(e.which)) || (e.which === H && a.selectpicker.keydown.keyHistory)) {
								var v,
									g,
									b = [];
								e.preventDefault(), (a.selectpicker.keydown.keyHistory += C[e.which]), a.selectpicker.keydown.resetKeyHistory.cancel && clearTimeout(a.selectpicker.keydown.resetKeyHistory.cancel), (a.selectpicker.keydown.resetKeyHistory.cancel = a.selectpicker.keydown.resetKeyHistory.start()), (g = a.selectpicker.keydown.keyHistory), /^(.)\1+$/.test(g) && (g = g.charAt(0));
								for (var w = 0; w < a.selectpicker.current.data.length; w++) {
									var I = a.selectpicker.current.data[w];
									k(I, g, "startsWith", !0) && a.selectpicker.view.canHighlight[w] && b.push(I.index);
								}
								if (b.length) {
									var x = 0;
									c.removeClass("active").find("a").removeClass("active"), 1 === g.length && (-1 === (x = b.indexOf(a.activeIndex)) || x === b.length - 1 ? (x = 0) : x++), (v = b[x]), (d = 0 < u - (n = a.selectpicker.main.data[v]).position ? ((o = n.position - n.height), !0) : ((o = n.position - a.sizeInfo.menuInnerHeight), n.position > u + a.sizeInfo.menuInnerHeight)), (s = a.selectpicker.main.elements[v]), (a.activeIndex = b[x]), a.focusItem(s), s && s.firstChild.focus(), d && (a.$menuInner[0].scrollTop = o), r.trigger("focus");
								}
							}
							i && ((e.which === H && !a.selectpicker.keydown.keyHistory) || e.which === D || (e.which === W && a.options.selectOnTab)) && (e.which !== H && e.preventDefault(), (a.options.liveSearch && e.which === H) || (a.$menuInner.find(".active a").trigger("click", !0), r.trigger("focus"), a.options.liveSearch || (e.preventDefault(), z(document).data("spaceSelect", !0))));
						}
				},
				mobile: function () {
					this.$element[0].classList.add("mobile-device");
				},
				refresh: function () {
					var e = z.extend({}, this.options, this.$element.data());
					(this.options = e), this.checkDisabled(), this.setStyle(), this.render(), this.buildData(), this.buildList(), this.setWidth(), this.setSize(!0), this.$element.trigger("refreshed" + j);
				},
				hide: function () {
					this.$newElement.hide();
				},
				show: function () {
					this.$newElement.show();
				},
				remove: function () {
					this.$newElement.remove(), this.$element.remove();
				},
				destroy: function () {
					this.$newElement.before(this.$element).remove(), this.$bsContainer ? this.$bsContainer.remove() : this.$menu.remove(), this.$element.off(j).removeData("selectpicker").removeClass("bs-select-hidden selectpicker"), z(window).off(j + "." + this.selectId);
				},
			});
		var J = z.fn.selectpicker;
		(z.fn.selectpicker = Z),
			(z.fn.selectpicker.Constructor = Y),
			(z.fn.selectpicker.noConflict = function () {
				return (z.fn.selectpicker = J), this;
			});
		var Q = z.fn.dropdown.Constructor._dataApiKeydownHandler || z.fn.dropdown.Constructor.prototype.keydown;
		z(document)
			.off("keydown.bs.dropdown.data-api")
			.on("keydown.bs.dropdown.data-api", ':not(.bootstrap-select) > [data-toggle="dropdown"]', Q)
			.on("keydown.bs.dropdown.data-api", ":not(.bootstrap-select) > .dropdown-menu", Q)
			.on("keydown" + j, '.bootstrap-select [data-toggle="dropdown"], .bootstrap-select [role="listbox"], .bootstrap-select .bs-searchbox input', Y.prototype.keydown)
			.on("focusin.modal", '.bootstrap-select [data-toggle="dropdown"], .bootstrap-select [role="listbox"], .bootstrap-select .bs-searchbox input', function (e) {
				e.stopPropagation();
			}),
			z(window).on("load" + j + ".data-api", function () {
				z(".selectpicker").each(function () {
					var e = z(this);
					Z.call(e, e.data());
				});
			});
	})(e);
});
