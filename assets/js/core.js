/**
 * Custom Scripts.
 * Note: use this file to add or override any other scripts.
 * DON'T EDIT TEMPLATE JS FILES DIRECTLY, JUST USE THIS FILE.
 */

var transparent = true;
var scroll_up = true;
var core, $navbar, scroll_distance;

$(function () {
	$(document).ready(function () {
		"use strict";

		// HEADER CLASS ON SCROLL
		$navbar = $("header[color-on-scroll]");
		scroll_distance = $navbar.attr("color-on-scroll") || 500;

		if ($("header").length != 0) {
			core.checkScrollForTransparentNavbar();
			$(window).on("scroll", core.checkScrollForTransparentNavbar);
		}

		// HAMBURGER MENU
		$(".navbar-toggler").on("click", function (e) {
			e.preventDefault();
			$(".navbar-menu").toggleClass("is-active");
			$(".navbar-right").toggleClass("is-active");
			$(this).toggleClass("is-active");
		});

		// BRANDS EXPAND
		var i = 0;
		if ($(window).width() >= 992) {
			$(".brands li").each(function () {
				i++;
				if (i == 17) {
					$(this).addClass("brands-opacity");
				}
				if (i == 18) {
					$(this).addClass("brands-opacity");
				}
				if (i == 19) {
					$(this).addClass("brands-opacity");
				}
				if (i == 20) {
					$(this).addClass("brands-opacity");
				}
				if (i > 20) {
					$(this).hide();
				}
			});
		} else {
			$(".brands li").each(function () {
				i++;
				if (i == 10) {
					$(this).addClass("brands-opacity");
				}
				if (i == 11) {
					$(this).addClass("brands-opacity");
				}
				if (i == 12) {
					$(this).addClass("brands-opacity");
				}
				if (i > 12) {
					$(this).hide();
				}
			});
		}

		//FINANCING PROGRAMS
		$(".fp-xs-item-nav li a").on("click", function (e) {
			e.preventDefault();
			$(".fp-xs-item-nav li a").removeClass("active");
			$(this).parent().parent().parent().parent().find(".fp-xs-item-tab").removeClass("is-active");
			$(this).parent().parent().parent().parent().addClass("is-active");
			$(this).addClass("active");
			$(this).parent().parent().parent().parent().find($(this).attr("data-open-tab")).addClass("is-active");
			$(this)
				.parent()
				.parent()
				.scrollLeft($(".active").position().left - $(".active").width());
		});

		$(".brands-expand").on("click", function (e) {
			e.preventDefault();
			$(".brands li").each(function () {
				$(this).show();
				$(this).removeClass("brands-opacity");
			});
			$(this).hide();
		});

		// CAR DETAILS HEADER
		if ($("#car-details").length != 0) {
			var careDetailsHeader = $("#car-details").offset().top,
				$window = $(window);
			if ($(window).width() < 992) {
				var $carOverview = $("#car-overview").offset().top - 200;
			} else {
				var $carOverview = $("#car-overview").offset().top + 90;
			}

			$window.on("scroll", function () {
				if ($window.scrollTop() >= careDetailsHeader) {
					$(".car-details-header").addClass("is-active");
				} else {
					$(".car-details-header").removeClass("is-active");
				}
			});

			$window.on("scroll", function () {
				if ($window.scrollTop() >= $carOverview) {
					$(".car-details-nav-wrapper-xs").addClass("is-active");
				} else {
					$(".car-details-nav-wrapper-xs").removeClass("is-active");
				}
			});

			var lastId,
				carDetailsNav = $(".car-details-nav"),
				carDetailsNavItems = carDetailsNav.find("a"),
				scrollItems = carDetailsNavItems.map(function () {
					var item = $($(this).attr("href"));
					if (item.length) {
						return item;
					}
				});

			if ($(window).width() > 992) {
				var carDetailsNavHeight = carDetailsNav.outerHeight() + 200;
			} else {
				var carDetailsNavHeight = carDetailsNav.outerHeight() + 80;
			}

			carDetailsNavItems.on("click", function (e) {
				e.preventDefault();
				var href = $(this).attr("href"),
					offsetTop = href === "#" ? 0 : $(href).offset().top - carDetailsNavHeight + 1;
				$("html,body").stop().animate(
					{
						scrollTop: offsetTop,
					},
					300
				);
			});

			$(window).on("scroll", function () {
				var fromTop = $(this).scrollTop() + carDetailsNavHeight;
				var cur = scrollItems.map(function () {
					if ($(this).offset().top < fromTop) return this;
				});
				cur = cur[cur.length - 1];
				var id = cur && cur.length ? cur[0].id : "";

				if (lastId !== id) {
					lastId = id;
					carDetailsNavItems
						.parent()
						.removeClass("active")
						.end()
						.filter("[href='#" + id + "']")
						.parent()
						.addClass("active");

					if ($(window).width() < 992) {
						if ($(".car-details-nav li").hasClass("active")) {
							$(".car-details-nav").scrollLeft($(".car-details-nav li.active").position().left - $(".car-details-nav li.active").width());
						}
					}
				}
			});
		}

		if ($(".browse-by-brand-header").length != 0) {
			var $browseByBrand = $(".browse-by-brand-header").offset().top;

			$(window).on("scroll", function () {
				if ($(window).scrollTop() >= $browseByBrand) {
					$(".browse-by-brand-header").addClass("is-active");
				} else {
					$(".browse-by-brand-header").removeClass("is-active");
				}
			});
		}

		// SWIPER SLIDERS
		var recentViews = new Swiper(".car-items", {
			spaceBetween: 20,
			slidesPerView: 1,
			autoHeight: true,
			navigation: {
				nextEl: ".car-items-nav-next",
				prevEl: ".car-items-nav-prev",
			},
			breakpoints: {
				800: {
					slidesPerView: 2,
					spaceBetween: 20,
				},
				1200: {
					slidesPerView: 3,
					spaceBetween: 20,
				},
			},
		});

		var careREcommendationSlider = new Swiper(".car-recommendation-slider", {
			slidesPerView: 1,
			slidesPerColumn: 1,
			slidesPerGroup: 1,
			spaceBetween: 0,
			pagination: {
				el: ".swiper-pagination",
			},
			breakpoints: {
				800: {
					slidesPerView: 2,
					spaceBetween: 20,
				},
				1200: {
					slidesPerView: 2,
					slidesPerColumn: 2,
					slidesPerGroup: 2,
					spaceBetween: 0,
				},
			},
		});

		var carItemsSecond = new Swiper(".car-items-2", {
			spaceBetween: 20,
			slidesPerView: 1,
			autoHeight: true,
			navigation: {
				nextEl: ".car-items-nav-next",
				prevEl: ".car-items-nav-prev",
			},
			breakpoints: {
				800: {
					slidesPerView: 2,
					spaceBetween: 20,
				},
				1200: {
					slidesPerView: 3,
					spaceBetween: 20,
				},
			},
		});

		var recentViewsImages = new Swiper(".car-item-image-slider", {
			spaceBetween: 0,
			autoHeight: true,
			pagination: {
				clickable: true,
				el: ".swiper-pagination",
				renderBullet: function (index, className) {
					return `<span class="dot swiper-pagination-bullet"><i class="icon-bullet"></i></span>`;
				},
			},
			navigation: {
				nextEl: ".swiper-button-next",
				prevEl: ".swiper-button-prev",
			},
		});

		var carDetailsSlider = new Swiper(".car-details-slider", {
			spaceBetween: 0,
			autoHeight: true,
			pagination: {
				clickable: true,
				el: ".swiper-pagination",
				renderBullet: function (index, className) {
					return `<span class="dot swiper-pagination-bullet"><i class="icon-bullet"></i></span>`;
				},
			},
			navigation: {
				nextEl: ".swiper-button-next",
				prevEl: ".swiper-button-prev",
			},
			on: {
				init: function (sw) {
					$(".gallery-number span").html("(" + (sw.activeIndex + 1) + "/" + sw.slides.length + ")");
				},
				slideChange: function (sw) {
					$(".gallery-number span").html("(" + (sw.activeIndex + 1) + "/" + sw.slides.length + ")");
				},
			},
		});

		$(".tooltip-link").on("click", function (e) {
			e.preventDefault();
		});

		$(".rate-item li a").on("click", function (e) {
			e.preventDefault();
		});

		//ACCORDION
		var allPanelsFaq = $(".accordion-item:not(.is-active) .accordion-item__content").hide();
		var allPanelsFaqItems = $(".accordion-item .accordion-item__content");
		$(".accordion .accordion-item__link a").click(function (e) {
			e.preventDefault();
			var $this = $(this);

			if ($(this).parent().parent().hasClass("is-active")) {
				$(this).parent().parent().removeClass("is-active");
			} else {
				$(this).parent().parent().addClass("is-active");
			}

			$(this).parent().next().slideToggle();

			return !1;
		});

		// INPUT SLIDER
		if ($("#down-payment").length != 0) {
			nouSlider();
		}

		if ($("#price-range").length != 0) {
			priceRangeSliderFunction();
		}

		//MODAL DIALOG
		$(".open-modal").on("click", function (e) {
			e.preventDefault();
			$(".modal").removeClass("is-active");
			$($(this).attr("data-modal")).addClass("is-active");
		});
		$(".close-modal").on("click", function (e) {
			e.preventDefault();
			$(".modal").removeClass("is-active");
			$($(this).attr("data-modal")).removeClass("is-active");
		});

		// CUSTOM SELECTs
		$("select").selectpicker();

		$(".dropdown-c-toggle").on("click", function (e) {
			e.preventDefault();
			$(this).parent().toggleClass("is-active");
		});

		$(".dropdown-c .dropdown-menu li a").on("click", function (e) {
			e.preventDefault();
			$(".dropdown-c").removeClass("is-active");
		});

		$(".compare-item h4").on("click", function (e) {
			e.preventDefault();
			$(this).toggleClass("active");
			$(this).parent().find(".compare-item-content").slideToggle();
		});

		//STEPS
		$(".step-next").on("click", function (e) {
			e.preventDefault();
			$(".steps-item").removeClass("active");
			$(".steps-items").css("height", $($(this).attr("data-step")).outerHeight());
			$("html,body").animate({ scrollTop: 0 }, "slow");
			$(".steps-nav-loader").css("width", $(".steps-nav").width() / $(this).attr("data-loader"));
			$(".steps-nav-item").removeClass("b");
			$($(this).attr("data-nav")).addClass("active b");
			$($(this).attr("data-step")).addClass("active");
		});

		$(".step-back").on("click", function (e) {
			e.preventDefault();
			$(".steps-item").removeClass("active");
			$(".steps-items").css("height", $($(this).attr("data-step")).outerHeight());
			$("html,body").animate({ scrollTop: 0 }, "slow");
			$(".steps-nav-loader").css("width", $(".steps-nav").width() / $(this).attr("data-loader"));
			$(".steps-nav-item").removeClass("b");
			$($(this).attr("data-nav-prev")).addClass("b");
			$($(this).attr("data-nav")).removeClass("active");
			$($(this).attr("data-step")).addClass("active");
		});

		var checksAttribute = $(".attribute-item-checkbox"),
			checksBodyStyle = $(".body-style-checkbox"),
			max = 4,
			array = [];

		checksAttribute.each(function () {
			$(this).on("click", function (e) {
				var checkedAttribute = $(".attribute-item-checkbox:checked");
				if (checkedAttribute.length <= max) {
					$(this).toggleClass("active");
					reorderAttributes();
				} else {
					return false;
				}
			});
		});

		checksBodyStyle.each(function () {
			$(this).on("click", function (e) {
				var checkedBodyStyle = $(".body-style-checkbox:checked");
				if (checkedBodyStyle.length <= max) {
					$(this).toggleClass("active");
					reorderBodyStyle();
				} else {
					return false;
				}
			});
		});

		function reorderAttributes(e, el) {
			$(".attributes-item label p span").hide();
			var selected = $(".attribute-item-checkbox:checked");
			selected.each(function (i, cb) {
				$(cb)
					.closest(".attributes-item")
					.find("label p span")
					.text(i + 1)
					.show();
			});
		}

		function reorderBodyStyle(e, el) {
			$(".attributes-item label p span").hide();
			var selected = $(".body-style-checkbox:checked");
			selected.each(function (i, cb) {
				$(cb)
					.closest(".attributes-item")
					.find("label p span")
					.text(i + 1)
					.show();
			});
		}

		$(".s").on("click", function (e) {
			e.preventDefault();
		});

		//SEARCH FORM
		var searchFormCounter = 0;
		if ($(window).width() < 768) {
			$(".search-form-filter .search-form-filter-item").each(function () {
				searchFormCounter++;
				if (searchFormCounter > 4) {
					$(this).hide();
				}
			});
		}

		$(".search-filters-expand").on("click", function (e) {
			e.preventDefault();
			$(".search-form-filter .search-form-filter-item").show();
			$(this).hide();
		});
	});
});

core = {
	misc: {
		navbar_menu_visible: 0,
	},

	checkScrollForTransparentNavbar: debounce(function () {
		if ($(document).scrollTop() > scroll_distance) {
			if (transparent) {
				transparent = false;
				$("header").addClass("nav-scroll");
				$("header .navbar-right-menu").addClass("pos");
			}
		} else {
			if (!transparent) {
				transparent = true;
				$("header").removeClass("nav-scroll");
				setTimeout(function () {
					$("header .navbar-right-menu").removeClass("pos");
				}, 1000);
			}
		}
	}, 17),
};

function debounce(func, wait, immediate) {
	var timeout;
	return function () {
		var context = this,
			args = arguments;
		clearTimeout(timeout);
		timeout = setTimeout(function () {
			timeout = null;
			if (!immediate) func.apply(context, args);
		}, wait);
		if (immediate && !timeout) func.apply(context, args);
	};
}

function nouSlider() {
	var downPaymentSlider = document.getElementById("down-payment");
	var loanLengthSlider = document.getElementById("loan-length");
	var endPaymentSlider = document.getElementById("end-payment");

	var downPaymentFormat = document.getElementById("down-payment-format");
	var loanLengthFormat = document.getElementById("loan-length-format");
	var endPaymentFormat = document.getElementById("end-payment-format");

	noUiSlider.create(downPaymentSlider, {
		start: 150000,
		connect: "lower",
		range: {
			min: 0,
			max: 345000,
		},
		format: wNumb({
			decimals: 0,
			thousand: ",",
		}),
	});

	noUiSlider.create(loanLengthSlider, {
		start: 11,
		connect: "lower",
		range: {
			min: 0,
			max: 24,
		},
		format: wNumb({
			decimals: 0,
		}),
	});

	noUiSlider.create(endPaymentSlider, {
		start: 33000,
		connect: "lower",
		range: {
			min: 0,
			max: 76000,
		},
		format: wNumb({
			decimals: 0,
			thousand: ",",
		}),
	});

	downPaymentSlider.noUiSlider.on("update", function (values, handle) {
		downPaymentFormat.value = values[handle];
	});

	loanLengthSlider.noUiSlider.on("update", function (values, handle) {
		loanLengthFormat.value = values[handle];
	});

	endPaymentSlider.noUiSlider.on("update", function (values, handle) {
		endPaymentFormat.value = values[handle];
	});

	downPaymentFormat.addEventListener("change", function () {
		downPaymentSlider.noUiSlider.set(this.value);
	});

	loanLengthFormat.addEventListener("change", function () {
		loanLengthSlider.noUiSlider.set(this.value);
	});

	endPaymentFormat.addEventListener("change", function () {
		endPaymentSlider.noUiSlider.set(this.value);
	});
}

function priceRangeSliderFunction() {
	var priceRangeSlider = document.getElementById("price-range-slider");
	var seatsSlider = document.getElementById("seats-slider");

	noUiSlider.create(priceRangeSlider, {
		start: [180000, 20000000],
		connect: true,
		step: 10000,
		tooltips: true,
		range: {
			min: 0,
			max: 20000000,
		},
		format: wNumb({
			decimals: 0,
			thousand: ",",
			postfix: " EGP",
		}),
	});

	noUiSlider.create(seatsSlider, {
		start: [4, 8],
		connect: true,
		step: 1,
		tooltips: true,
		range: {
			min: 2,
			max: 8,
		},
		format: wNumb({
			decimals: 0,
		}),
	});
}
